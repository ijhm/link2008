﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Security.Cryptography;
using CibExchangeLib;
//using System.DirectoryServices;

namespace CibExchangeLib
{
    public class CibExchangeAdaptor
    {
        public ExchangeService service;
        private Config config;
        private MeetingRoomConfig currentRoom;

        public CibExchangeAdaptor(string configFile)
        {
            config = new CibExchangeLib.Config(configFile);
        }

        public CibExchangeAdaptor(string serverVersion,string uid,string pwd)
        {
            var ServerVersion = Utilities.GetServerVersion(serverVersion);
            var ServerUrl = "https://outlook.office365.com/EWS/Exchange.asmx";
            service = new ExchangeService(ServerVersion);
            service.Credentials = new WebCredentials(uid, pwd);
            service.Url = new Uri(ServerUrl);

        }

        public CibExchangeAdaptor(string serverVersion, string uid, string pwd,string impersonationUser)
        {
            var ServerVersion = Utilities.GetServerVersion(serverVersion);
            var ServerUrl = "https://outlook.office365.com/EWS/Exchange.asmx";
            service = new ExchangeService(ServerVersion);
            service.Credentials = new WebCredentials(uid, pwd);
            service.Url = new Uri(ServerUrl);
            service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, impersonationUser);
        }

        public void Connect(string meetingRoom)
        {
            currentRoom = config.GetConfig(meetingRoom);
            service = new ExchangeService(currentRoom.ServerVersion);
            service.Credentials = currentRoom.UserCredentials;
            service.Url = new Uri(currentRoom.ServerUrl);
        }


        public string AuthHash
        {
            get
            {
                return GetHashString(config.Username + "_" + config.Password);
            }
        }

        public string AuthToken(string username,string password)
        {
            var result = "";
            if (config.Username==username && config.Password==password)
            {
                result = AuthHash;
            }
            return result;     
        }   


        public List<RoomModel> GetRooms()
        {
            var result = new List<RoomModel>();
            foreach (var item in config.Rooms)
            {
                var room = new RoomModel();
                room.Name = item.Name;
                room.MeetingRoom = item.MeetingRoom;
                result.Add(room);
            }
            return result;
        }

        public List<MeetingListModel> GetBookings(string meetingRoom)
        {
            Connect(meetingRoom);
            DateTime start = DateTime.Now;
            DateTime end = start.AddDays(currentRoom.DefaultWindow);
            return GetBookings(meetingRoom, start, end);
        }

        public bool GetRoomAlias(string meetingRoomAddr,string alias)
        {
            bool result = false;
            PropertySet AllProps = new PropertySet(BasePropertySet.FirstClassProperties);
            NameResolutionCollection ncCol = service.ResolveName(meetingRoomAddr, ResolveNameSearchLocation.DirectoryOnly, true, AllProps);
            foreach (NameResolution nr in ncCol)
            {
                result = (alias == nr.Contact.DisplayName);
//                Console.WriteLine(nr.Contact.DisplayName);
//                Console.WriteLine(nr.Contact.Notes);

            }
            return result;
        }

        public bool CheckEmail(string emailAddress)
        {
            bool result = false;
            PropertySet AllProps = new PropertySet(BasePropertySet.FirstClassProperties);
            NameResolutionCollection ncCol = service.ResolveName(emailAddress, ResolveNameSearchLocation.DirectoryOnly, true, AllProps);
            foreach (NameResolution nr in ncCol)
            {
                result = true;
            }
            return result;
        }

        public List<MeetingListModel> GetItems(string emailAddress)
        {
            var folder = Folder.Bind(service, WellKnownFolderName.Inbox);

            ItemView view = new ItemView(100);
            view.PropertySet = new PropertySet(AppointmentSchema.Subject,
                                                AppointmentSchema.Start,
                                                AppointmentSchema.End,
                                                AppointmentSchema.DateTimeCreated,
                                                AppointmentSchema.DateTimeReceived,
                                                AppointmentSchema.Location,
                                                AppointmentSchema.AppointmentState,
                                                AppointmentSchema.AppointmentType,
                                                AppointmentSchema.IsMeeting,
                                                AppointmentSchema.IsCancelled,
                                                AppointmentSchema.MyResponseType,
                                                AppointmentSchema.Duration
                                                );
            foreach (var item in folder.FindItems(view).ToList())
            {
                Console.WriteLine(item.Subject);
                if (item.Subject=="MasterData")
                {

                }
              //  result.Add(new MeetingListModel(item));
            }
            return null;
        }


        public List<MeetingListModel> GetBookings(string meetingRoom, DateTime startDate, DateTime endDate)
        {
            Connect(meetingRoom);
            //Check timespan
            TimeSpan daySpan = startDate - endDate;
            if (Math.Abs(daySpan.TotalDays)> currentRoom.MaxListDays)
            {
                throw new Exception("Too many days specified. Maximum allowed is:" + currentRoom.MaxListDays.ToString());
            }
            CalendarFolder folder = CalendarFolder.Bind(service, WellKnownFolderName.Calendar);
            CalendarView view = new CalendarView(startDate, endDate);
            view.PropertySet = new PropertySet(AppointmentSchema.Subject,
                                                AppointmentSchema.Start,
                                                AppointmentSchema.End,
                                                AppointmentSchema.DateTimeCreated,
                                                AppointmentSchema.DateTimeReceived,
                                                AppointmentSchema.Location,
                                                AppointmentSchema.AppointmentState,
                                                AppointmentSchema.AppointmentType,
                                                AppointmentSchema.IsMeeting,
                                                AppointmentSchema.IsCancelled,
                                                AppointmentSchema.MyResponseType,
                                                AppointmentSchema.Duration
                                                );
            var result = new List<MeetingListModel>();
            foreach (var item in folder.FindAppointments(view).ToList())
            {
                result.Add(new MeetingListModel(item));
            }
            return result;
        }

        public List<MeetingListModel> GetBookings(ExchangeService service, string meetingRoom, DateTime startDate, DateTime endDate)
        {
            List<MeetingListModel> result = new List<MeetingListModel>();

            /*
            //Check timespan
            TimeSpan daySpan = startDate - endDate;

            var m = new Mailbox("lutonmeetingroom@connectib.com");
            var fid = new FolderId(WellKnownFolderName.Calendar, m);
            var f = Folder.Bind(service, fid);
            var t = f;

            */


            //I think this needs a proper AD
            /*

string filter = "(&(objectClass=*)(msExchRecipientDisplayType=7))";
//Assembly System.DirectoryServices.dll
DirectorySearcher search = new DirectorySearcher(filter);
List<AttendeeInfo> rooms = new List<AttendeeInfo>();
foreach (SearchResult result2 in search.FindAll())
{
    ResultPropertyCollection r = result2.Properties;
    DirectoryEntry entry = result2.GetDirectoryEntry();
    // entry.Properties["displayName"].Value.ToString() will bring the room name
    rooms.Add(new AttendeeInfo(entry.Properties["mail"].Value.ToString().Trim()));
}


List<AttendeeInfo> attend = new List<AttendeeInfo>();
foreach (AttendeeInfo inf in rooms)
{
    attend.Clear();
    attend.Add(inf.SmtpAddress);

    AvailabilityOptions options = new AvailabilityOptions();
    options.MaximumSuggestionsPerDay = 48;
    // service is ExchangeService object contains your authentication with exchange server
    GetUserAvailabilityResults results = service.GetUserAvailability(attend, new TimeWindow(startDate,endDate), AvailabilityData.FreeBusyAndSuggestions, options);

    foreach (AttendeeAvailability attendeeAvailability in results.AttendeesAvailability)
    {
        Console.WriteLine();
        Console.WriteLine();
        if (attendeeAvailability.ErrorCode == ServiceError.NoError)
        {
            foreach (Microsoft.Exchange.WebServices.Data.CalendarEvent calendarEvent in
            attendeeAvailability.CalendarEvents)
            {
                Console.WriteLine("Calendar event");
                Console.WriteLine(" Starttime: " + calendarEvent.StartTime.ToString());
                Console.WriteLine(" Endtime: " + calendarEvent.EndTime.ToString());
                if (calendarEvent.Details != null)
                {
                    Console.WriteLine(" Subject:" + calendarEvent.Details.Subject);

                }
            }
        }
    }
}


*/



            //Never finds anything...
            /*
            var folderView = new FolderView(1);
            folderView.Traversal = FolderTraversal.Deep;
            var filter = new SearchFilter.IsEqualTo(FolderSchema.WellKnownFolderName, "Bristol Room One");
            var results = service.FindFolders(WellKnownFolderName.Inbox, filter, folderView);
            */

            //Works with a real user but not a resource mailbox

 //           var rooms = service.GetRoomLists();
 //           var test = service.GetRooms(meetingRoom);
            CalendarView view = new CalendarView(startDate, endDate);
            FolderId calFolder = new FolderId(WellKnownFolderName.Calendar, meetingRoom);
            FindItemsResults<Appointment> appointments = service.FindAppointments(calFolder, view);

            foreach (var item in appointments)
            {
                result.Add(new MeetingListModel { Id=item.Id.ToString(), Subject = item.Subject, StartDate = item.Start, EndDate = item.End });
            }

            /*
            view.PropertySet = new PropertySet(AppointmentSchema.Subject,
                                                AppointmentSchema.Start,
                                                AppointmentSchema.End,
                                                AppointmentSchema.DateTimeCreated,
                                                AppointmentSchema.DateTimeReceived,
                                                AppointmentSchema.Location,
                                                AppointmentSchema.AppointmentState,
                                                AppointmentSchema.AppointmentType,
                                                AppointmentSchema.IsMeeting,
                                                AppointmentSchema.IsCancelled,
                                                AppointmentSchema.MyResponseType,
                                                AppointmentSchema.Duration
                                                );
            var result = new List<MeetingListModel>();
            foreach (var item in folder.FindAppointments(view).ToList())
            {
                result.Add(new MeetingListModel(item));
            }
            */

            return result;
        }

        public MeetingDetailModel GetBooking(ExchangeService service, string meetingRoom, string appointmentId)
        {
            MeetingDetailModel result = null;
            try
            {
                var propRoomBookingCapacity = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.PublicStrings, "RoomBookingCapacity", MapiPropertyType.Long);

                /*
                var properties = new PropertySet(AppointmentSchema.Subject,
                                                    AppointmentSchema.Start,
                                                    AppointmentSchema.End,
                                                    AppointmentSchema.Location,
                                                    AppointmentSchema.RequiredAttendees,
                                                    AppointmentSchema.Body,
                                                    AppointmentSchema.Organizer,
                                                    AppointmentSchema.Duration,
                                                    AppointmentSchema.ExtendedProperties
                                                    );
                                                    */

                var userFields = new ExtendedPropertyDefinition[] { propRoomBookingCapacity };
                var properties = new PropertySet(BasePropertySet.FirstClassProperties, userFields);


//                ExtendedAppointmentData


                Appointment app = Appointment.Bind(service, appointmentId, properties);
                if (app.Subject== "Hewlett Packard Enterprise - Technology Services - Introductory Document")
                {
   //                 app.SetExtendedProperty(propRoomBookingCapacity, 2);
   //                 app.Update(ConflictResolutionMode.AlwaysOverwrite);
                    object output;
                    app.TryGetProperty(propRoomBookingCapacity, out output);
                }
                foreach(var item in app.ExtendedProperties)
                {
                    Console.WriteLine(item.PropertyDefinition.Name + " = " + item.Value.ToString());
                }
                result = new MeetingDetailModel(app);
            }
            catch (Exception)
            {
            }
            return result;
        }


        public MeetingDetailModel GetBooking(string meetingRoom, string appointmentId)
        {
            Connect(meetingRoom);

            MeetingDetailModel result = null;
            try
            {

                var properties = new PropertySet(AppointmentSchema.Subject,
                                                    AppointmentSchema.Start,
                                                    AppointmentSchema.End,
                                                    AppointmentSchema.Location,
                                                    AppointmentSchema.RequiredAttendees,
                                                    AppointmentSchema.Body,
                                                    AppointmentSchema.Organizer,
                                                    AppointmentSchema.Duration,
                                                    AppointmentSchema.ExtendedProperties
                                                    );

                Appointment app = Appointment.Bind(service, appointmentId, properties);
                result = new MeetingDetailModel(app);
            }
            catch (Exception)
            {
            }
            return result;
        }

        public bool RescueduleAppointment(string roomDataId, DateTime start, DateTime end,DateTime newStart,DateTime newEnd)
        {
            try
            {
                ExtendedPropertyDefinition propRoomDataId = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.PublicStrings, "RoomDataId", MapiPropertyType.String);
                SearchFilter search = new SearchFilter.IsEqualTo(propRoomDataId,roomDataId);
                ItemView view = new ItemView(10);

                view.PropertySet = new PropertySet(AppointmentSchema.Subject,
                                    AppointmentSchema.Start,
                                    AppointmentSchema.End
                                    );

                var folderId = new FolderId(Microsoft.Exchange.WebServices.Data.WellKnownFolderName.Calendar);

                FindItemsResults<Item> results = service.FindItems(folderId, search, view);

                if (results.Items.Count>0)
                {
                    var itemId = results.Items[0].Id;
                    Appointment app = Appointment.Bind(service, itemId);
                    app.Start = newStart;
                    app.End = newEnd;
                    app.Update(ConflictResolutionMode.AlwaysOverwrite);
                }

                return true;
            }
            catch (Exception)
            {
            }
            return false;
        }


        public bool DeleteAppointment(string roomDataId, DateTime start, DateTime end, DateTime newStart, DateTime newEnd)
        {
            try
            {

                ExtendedPropertyDefinition propRoomDataId = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.PublicStrings, "RoomDataId", MapiPropertyType.String);
                SearchFilter search = new SearchFilter.IsEqualTo(propRoomDataId, roomDataId);
                ItemView view = new ItemView(10);

                view.PropertySet = new PropertySet(AppointmentSchema.Subject,
                                    AppointmentSchema.Start,
                                    AppointmentSchema.End
                                    );

                var folderId = new FolderId(Microsoft.Exchange.WebServices.Data.WellKnownFolderName.Calendar);

                FindItemsResults<Item> results = service.FindItems(folderId, search, view);

                if (results.Items.Count > 0)
                {
                    var itemId = results.Items[0].Id;
                    Appointment app = Appointment.Bind(service, itemId);
                    app.Delete(DeleteMode.MoveToDeletedItems);
                }

                return true;
            }
            catch (Exception)
            {
            }
            return false;
        }

        public bool ConflictingAppointment(string meetingRoom,DateTime startDate,DateTime endDate)
        {
            Connect(meetingRoom);

            bool result = false;
            CalendarFolder folder = CalendarFolder.Bind(service, WellKnownFolderName.Calendar);

            CalendarView view = new CalendarView(startDate, endDate);

            view.PropertySet = new PropertySet(AppointmentSchema.Subject,
                                                AppointmentSchema.Start,
                                                AppointmentSchema.End,
                                                AppointmentSchema.Location
                                                );

            var items = folder.FindAppointments(view);
            foreach (var item in items)
            {
                if (item.Location== meetingRoom)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        public ItemId CreateAppointment(string location, DateTime startDate, DateTime endDate,string subject,string body)
        {
         //   Connect(meetingRoom);

            ItemId result = null;
            Appointment app = new Appointment(service);
            app.Subject = subject;
            app.Start = startDate;
            app.End = endDate;
            app.Location = location;
            app.Body = body;

//            ExtendedPropertyDefinition propRoomDataId = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.PublicStrings, "RoomDataId", MapiPropertyType.String);
//            app.SetExtendedProperty(propRoomDataId, roomDataId);

            app.Save();
            result = app.Id;
            return result;
        }

        public bool UpdateAppointment(string appointmentId, DateTime start, DateTime end, string subject,string body)
        {
            try
            {
                var propertySet = new PropertySet(AppointmentSchema.Subject,
                                    AppointmentSchema.Start,
                                    AppointmentSchema.End
                                    );
                Appointment app = Appointment.Bind(service, new ItemId(appointmentId), propertySet);

                if (app!=null)
                {
                    app.Start = start;
                    app.End = end;
                    app.Subject = subject;
                    app.Body = body;
                    app.Update(ConflictResolutionMode.AlwaysOverwrite);
                   
                }

                return true;
            }
            catch (Exception)
            {
            }
            return false;
        }


        public bool DeleteAppointment(string appointmentId)
        {
            bool result = false;
            try
            {
                Appointment app = Appointment.Bind(service, appointmentId, new PropertySet());
                app.Delete(DeleteMode.HardDelete);
                result = true;
            }
            catch (Exception)
            {
            }
            return result;
        }

        private static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = MD5.Create();  //or use SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        private static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public List<string> GetRoomList()
        {
            var list = new List<string>();
            EmailAddressCollection myRoomLists = service.GetRoomLists();
            foreach (EmailAddress address in myRoomLists)
            {
                EmailAddress myRoomList = address.Address;
                PropertySet AllProps = new PropertySet(BasePropertySet.FirstClassProperties);
                NameResolutionCollection ncCol = service.ResolveName(address.Address, ResolveNameSearchLocation.DirectoryOnly, true, AllProps);
                foreach (NameResolution nr in ncCol)
                {
                    list.Add(nr.Contact.DisplayName);
//                    Console.WriteLine(nr.Contact.DisplayName);
//                    Console.WriteLine(nr.Contact.Notes);
                }
            }

            return list;
        }


    }
}
