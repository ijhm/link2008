﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace CibExchangeLib
{
    public class Config
    {
        public string Username;
        public string Password;

        public List<MeetingRoomConfig> Rooms;

        public Config()
        {

        }

        public Config(string filePath)
        {
            Rooms = new List<CibExchangeLib.MeetingRoomConfig>();
            string fileData = System.IO.File.ReadAllText(filePath);
            JObject obj = JObject.Parse(fileData);
            Username = obj["Credentials"]["Username"].ToString();
            Password = obj["Credentials"]["Password"].ToString();

            JArray items = (JArray)obj["MeetingRooms"];
            foreach (var item in items)
            {

                var room = new MeetingRoomConfig((JObject)item);
                Rooms.Add(room);

            }
        }

        public MeetingRoomConfig GetConfig(string meetingRoom)
        {
            foreach (MeetingRoomConfig room in Rooms)
            {
                if (room.MeetingRoom==meetingRoom)
                {
                    return room;
                }
            }
            return null;
        }

        public static string FormatTime(TimeSpan time)
        {
            string result = "";
            if (time.Hours<1)
            {
                result = string.Format("{0} Mins", time.Minutes);
            }
            if (time.Hours == 1)
            {
                result = string.Format("{0} Hr", time.Hours);
                if (time.Minutes>0)
                {
                    result += string.Format(" {0} Mins", time.Minutes);
                }
            }
            if (time.Hours > 1)
            {
                result = string.Format("{0} Hrs", time.Hours);
                if (time.Minutes > 0)
                {
                    result += string.Format(" {0} Mins", time.Minutes);
                }
            }
            if (result=="0 Mins")
            {
                if (time.Days>0)
                {
                    if (time.Days > 1)
                    {
                        result = string.Format("{0} Days", time.Days);
                    }
                    else
                    {
                        result = string.Format("{0} Day", time.Days);
                    }
                }
            }
            else
            {
                if (time.Days > 0)
                {
                    if (time.Days > 1)
                    {
                        result = string.Format("{0} {1} Days", result, time.Days);
                    }
                    else
                    {
                        result = string.Format("{0} {1} Day", result, time.Days);
                    }
                }
            }
            return result;
        }

        public static string FormatTime(int minutes)
        {
            TimeSpan ts = TimeSpan.FromMinutes(minutes);
            return FormatTime(ts);
        }


    }
}
