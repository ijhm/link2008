﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//using System.Threading.Tasks;
using Exchange = Microsoft.Exchange.WebServices.Data;

namespace CibExchangeLib
{
    internal static class Helper
    {
        public static void TryWithBackoff(Action action)
        {
            var attempt_count = 0;
            while (true)
            {
                try
                {
                    attempt_count++;
                    action();
                    break;
                }
                catch (Exchange.ServerBusyException ex)
                {
                    if (attempt_count >= 3)
                    {
                        var message = string.Format("When trying to access the Exchange server we recived {0} Server Busy Excpetions and have failed to complete the action", 3);
                        throw new ApplicationException(message, ex);
                    }

                    Thread.Sleep(ex.BackOffMilliseconds);
                }
            }
        }
    }
}
