﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace CibExchangeLib
{

    public class UserModel
    {
        public string Name;
        public string Email;
    }

    public class RoomModel
    {
        public string Name;
        public string MeetingRoom;
    }

    public class GetBookingRangeModel
    {
        public string MeetingRoom;
        public DateTime StartDate;
        public DateTime EndDate;
    }

    public class MakeBookingModel
    {
        public string MeetingRoom;
        public string Subject;
        public DateTime StartDate;
        public DateTime EndDate;
    }

        public class MeetingListModel
    {
        public string Id;
        public string Subject;
        public string MeetingRoom;
        public DateTime StartDate;
        public DateTime EndDate;
        public string ChangeKey;
        public string Duration;

        public MeetingListModel()
        {

        }

        public MeetingListModel(Appointment app)
        {
            Id = app.Id.UniqueId;
            Subject = app.Subject;
            StartDate = app.Start;
            EndDate = app.End;
            MeetingRoom = app.Location;
            ChangeKey = app.Id.ChangeKey;
            Duration = Config.FormatTime(app.Duration);
        }
    }

    public class MeetingDetailModel
    {
        public string Subject;
        public string Body;
        public string MeetingRoom;
        public DateTime StartDate;
        public DateTime EndDate;
        public UserModel Organizer;
        public List<UserModel> Attendees;
        public string Duration;

        public MeetingDetailModel()
        {

        }

        public MeetingDetailModel(Appointment app)
        {
            Body = "";
            Subject = app.Subject;
            StartDate = app.Start;
            EndDate = app.End;
            MeetingRoom = app.Location;
            Organizer = new UserModel();
            Organizer.Name = app.Organizer.Name;
            Organizer.Email = app.Organizer.Address;
            Attendees = new List<UserModel>();
            Duration = Config.FormatTime(app.Duration);

            foreach (var item in app.RequiredAttendees)
            {
                var user = new UserModel();
                user.Name = item.Name;
                user.Email = item.Address;
                Attendees.Add(user);
            }
        }


    }


}
