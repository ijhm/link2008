﻿using Microsoft.Exchange.WebServices.Data;
using Newtonsoft;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using System.Threading.Tasks;

namespace CibExchangeLib
{
    
    public class MeetingRoomConfig
    {
        public enum enumServerType
        {
            Office365,
            Custom
        }

        public string Name;
        public ExchangeVersion ServerVersion;
        public enumServerType ServerType;
        public string ServerUrl;
        public string MeetingRoom;
        public string Username;
        public string Password;
        public int DefaultWindow;
        public int MaxListDays;

        public WebCredentials UserCredentials;

        public MeetingRoomConfig(JObject obj)
        {
            Name = obj["Name"].ToString();
            MeetingRoom = obj["MeetingRoom"].ToString();
            Username = obj["Username"].ToString();
            Password = obj["Password"].ToString();
            ServerUrl = obj["ServerUrl"].ToString();
            DefaultWindow = int.Parse(obj["DefaultWindow"].ToString());
            MaxListDays = int.Parse(obj["MaxListDays"].ToString());
            ServerVersion = Utilities.GetServerVersion(obj["ServerVersion"].ToString());

            if (obj["ServerType"].ToString()=="Office365")
            {
                ServerType = enumServerType.Office365;
                ServerUrl = "https://outlook.office365.com/EWS/Exchange.asmx";
            }
            UserCredentials = new WebCredentials(Username, Password);
        }
        
    }
}
