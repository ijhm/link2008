﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace CibExchangeLib
{
    public class Utilities
    {
        public static ExchangeVersion GetServerVersion(string server)
        {
            ExchangeVersion ServerVersion = ExchangeVersion.Exchange2013_SP1;
            switch (server)
            {
                case "Exchange2007_SP1":
                    ServerVersion = ExchangeVersion.Exchange2007_SP1;
                    break;
                case "Exchange2010":
                    ServerVersion = ExchangeVersion.Exchange2010;
                    break;
                case "Exchange2010_SP1":
                    ServerVersion = ExchangeVersion.Exchange2010_SP1;
                    break;
                case "Exchange2010_SP2":
                    ServerVersion = ExchangeVersion.Exchange2010_SP2;
                    break;
                case "Exchange2013":
                    ServerVersion = ExchangeVersion.Exchange2013;
                    break;
                case "Exchange2013_SP1":
                    ServerVersion = ExchangeVersion.Exchange2013_SP1;
                    break;
            }
            return ServerVersion;
        }
    }
}
