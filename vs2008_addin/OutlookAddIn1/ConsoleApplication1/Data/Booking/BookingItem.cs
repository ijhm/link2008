﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class BookingItem
    {
        public enum enum_QtyType
        {
            PerSession = 1,
            PerServing = 2,
            PerHour = 3
        }

        public enum enum_ItemType {
            None = 0,
            Facilities = 1,
            Catering = 2,
            Features = 3,
            Layout = 4,
        }

        public int Id { get; set; }
        public enum_ItemType ItemType { get; set; }
        public string Name { get; set; }
        public Currency Cost { get; set; }
        public Decimal RawCost { get; set; }
        public int MaxQty { get; set; }
        public int MinLeadTime { get; set; }
        public int SetupTime { get; set; }
        public int ClearDownTime { get; set; }
        public string AlertEmail { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public enum_QtyType QtyType { get; set; }



        public string TypeName
        {
            get
            {
                switch (ItemType)
                {
                    case enum_ItemType.Facilities:
                        return "Facilities";
                    case enum_ItemType.Catering:
                        return "Catering";
                    case enum_ItemType.Features:
                        return "Features";
                    case enum_ItemType.Layout:
                        return "Layout";
                    default:
                        return "N/A";
                }
            }
        }


        public string CostUnit
        {
            get
            {
                switch (QtyType)
                {
                    case enum_QtyType.PerSession:
                        return "Per Session";
                    case enum_QtyType.PerServing:
                        return "Per Serving";
                    case enum_QtyType.PerHour:
                        return "Per Hour";
                    default:
                        return "-";
                }
            }
        }

        public int DefaultQty
        {
            get
            {
                switch (ItemType)
                {
                    case enum_ItemType.Facilities:
                        return 1;
                    case enum_ItemType.Catering:
                        return -1;
                    case enum_ItemType.Features:
                        return 1;
                    case enum_ItemType.Layout:
                        return 1;
                    default:
                        return 0;
                }
            }
        }

        public string CostToCurrencyString() {
            return Cost.ToString();
        }

        public string CostToCurrencyHtmlString() {
            return System.Net.WebUtility.HtmlEncode(CostToCurrencyString());
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
