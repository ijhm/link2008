﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data {
    [JsonConverter(typeof(CurrencyConverter))]
    public class Currency : IComparable, IComparable<Currency>, IEquatable<Currency> {
        private const int LEFT_PROCEEDS_RIGHT = -1;
        private const int LEFT_EQUALS_RIGHT = 0;
        private const int LEFT_FOLLOWS_RIGHT = 1;

        /* Note: Defaulting to UK settings becasue thats what the Link plugin defaulted.  This way we have backwards compatibility */
        /* Note: The Region class will update the Symbol and HasSymbolOnLeft properties as BookingItems get added */
        /* Note: All other classes that use this class must set the Symbol and HasSymbolOnLeft properties */

        public bool HasSymbolOnLeft { get; set; }
        public string Symbol { get; set; }
        public decimal Value { get; set; }

        public override bool Equals(object obj) {
            return Equals(this, obj as Currency);
        }

        public bool Equals(Currency other) {
            return Equals(this, other);
        }

        public int CompareTo(object obj) {
            return CompareTo(this, obj as Currency);
        }

        public int CompareTo(Currency other) {
            return CompareTo(this, other);
        }

        public override int GetHashCode() {
            return ToString().GetHashCode();
        }

        public override string ToString() {
            if (HasSymbolOnLeft) {
                if (Value==0)
                {
                    return "{Symbol}0.00";
                }
                else
                {
                    return "{Symbol}{Value:#.##}";
                }
            }
            else {
                if (Value == 0)
                {
                    return "0.00{Symbol}";
                }
                else
                {
                    return "{Value:#.##}{Symbol}";
                }
            }
        }

        public string ToHtmlString() {
            return System.Net.WebUtility.HtmlEncode(ToString());
        }

        private static void ThrowIfCurrencySymbolMissmatch(Currency left, Currency right) {
        }

        private static void ThrowIfNull(object value, string arg_name) {
            if(value == null) {
                throw new ArgumentNullException(arg_name);
            }
        }

        private static bool Equals(Currency left, Currency right) {
            if (object.ReferenceEquals(left, right)) {
                return true;
            }

            if (((object)left) == null || ((object)right) == null) {
                return false;
            }

            return (
                left.Symbol == right.Symbol &&
                left.Value == right.Value
            );
        }

        private static int CompareTo(Currency left, Currency right) {
            if (object.ReferenceEquals(left, right)) {
                return LEFT_EQUALS_RIGHT;
            }

            if (((object)left) == null) {
                return LEFT_PROCEEDS_RIGHT;
            }

            if (((object)right) == null) {
                return LEFT_FOLLOWS_RIGHT;
            }

            ThrowIfCurrencySymbolMissmatch(left, right);

            return left.Value.CompareTo(right.Value);
        }

        public static bool operator ==(Currency left, Currency right) {
            return Equals(left, right);
        }

        public static bool operator !=(Currency left, Currency right) {
            return !Equals(left, right);
        }

        public static bool operator <(Currency left, Currency right) {
            return (CompareTo(left, right) == LEFT_PROCEEDS_RIGHT);
        }

        public static bool operator >(Currency left, Currency right) {
            return (CompareTo(left, right) == LEFT_FOLLOWS_RIGHT);
        }

        public static bool operator <=(Currency left, Currency right) {
            return (CompareTo(left, right) <= LEFT_EQUALS_RIGHT);
        }

        public static bool operator >=(Currency left, Currency right) {
            return (CompareTo(left, right) >= LEFT_EQUALS_RIGHT);
        }

        public static Currency operator +(Currency left, Currency right) {
            ThrowIfCurrencySymbolMissmatch(left, right);

            return new Currency() {
                HasSymbolOnLeft = left.HasSymbolOnLeft,
                Symbol = left.Symbol,
                Value = left.Value + right.Value
            };
        }

        public static Currency operator -(Currency left, Currency right) {
            ThrowIfCurrencySymbolMissmatch(left, right);

            return new Currency() {
                HasSymbolOnLeft = left.HasSymbolOnLeft,
                Symbol = left.Symbol,
                Value = left.Value - right.Value
            };
        }

        public static Currency operator *(Currency left, Currency right) {
            ThrowIfCurrencySymbolMissmatch(left, right);

            return new Currency() {
                HasSymbolOnLeft = left.HasSymbolOnLeft,
                Symbol = left.Symbol,
                Value = left.Value * right.Value
            };
        }

        public static Currency operator *(Currency left, int right) {
            return new Currency() {
                HasSymbolOnLeft = left.HasSymbolOnLeft,
                Symbol = left.Symbol,
                Value = left.Value * right
            };
        }

        public static Currency operator *(int left, Currency right) {
            return new Currency() {
                HasSymbolOnLeft = right.HasSymbolOnLeft,
                Symbol = right.Symbol,
                Value = right.Value * left
            };
        }

        public static Currency operator /(Currency left, Currency right) {
            ThrowIfCurrencySymbolMissmatch(left, right);

            return new Currency() {
                HasSymbolOnLeft = left.HasSymbolOnLeft,
                Symbol = left.Symbol,
                Value = left.Value / right.Value
            };
        }

        public static Currency operator %(Currency left, Currency right) {
            ThrowIfCurrencySymbolMissmatch(left, right);

            return new Currency() {
                HasSymbolOnLeft = left.HasSymbolOnLeft,
                Symbol = left.Symbol,
                Value = left.Value % right.Value
            };
        }
        
        public static Currency operator +(Currency item) {
            ThrowIfNull(item, nameof(item));

            return item;
        }

        public static Currency operator -(Currency item) {
            ThrowIfNull(item, nameof(item));

            return new Currency() {
                HasSymbolOnLeft = item.HasSymbolOnLeft,
                Symbol = item.Symbol,
                Value = decimal.Negate(item.Value)
            };
        }

        public static Currency operator ++(Currency item) {
            ThrowIfNull(item, nameof(item));

            return new Currency() {
                HasSymbolOnLeft = item.HasSymbolOnLeft,
                Symbol = item.Symbol,
                Value = decimal.Add(item.Value, decimal.One)
            };
        }

        public static Currency operator --(Currency item) {
            ThrowIfNull(item, nameof(item));

            return new Currency() {
                HasSymbolOnLeft = item.HasSymbolOnLeft,
                Symbol = item.Symbol,
                Value = decimal.Subtract(item.Value, decimal.One)
            };
        }
    }

    public class CurrencyConverter : JsonConverter {
        public override bool CanConvert(Type object_type) {
            return (object_type == typeof(Currency));
        }

        public override object ReadJson(JsonReader reader, Type object_type, object existing_value, JsonSerializer serializer) {
            if ((object_type != typeof(Currency))) {
                return null;
            }

            var value_str = reader.Value as string;

            decimal value;
            if (decimal.TryParse(value_str, out value) == false) {
                return null;
            }

            return new Currency() { Value = value };
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            var item = value as Currency;

            if (item == null) {
                return;
            }

            /* Note: We are not including the symbol for backwards compatibility reasons */
            writer.WriteValue(item.Value);
        }
    }
}
