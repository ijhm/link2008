﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Drawing;
using OneSpace.OutlookPlugin.Data.Mapping;

namespace OneSpace.OutlookPlugin.Data
{
    public class RoomBookingData {
        private static readonly SchemaVersion MIN_VERSION = new SchemaVersion(1, 1, 1);

        public SchemaVersion SchemaVersion { get; set; }

        public enum enum_ItemType {
            Equipment = 1,
            Catering = 2,
            Features = 3,
            Layout = 4
        }
        public List<Region> Regions { get; set; }
        public List<ServiceMailbox> ServiceMailboxes { get; set; }
        public SecurityRole SecurityRoles { get; set; }

        [Obsolete("OneSpace Link no longer stores the EWS Credentials in the config.", false)]
        public EWSCredential EWSCredentials { get; set; }

        public MapDisplaySettings DisplaySettings { get; set; }
        [JsonIgnore]
        public Image MapAreaImage { get; set; }

        #region Utility methods
        public Room GetRoomById(int id) {
            foreach (Region region in Regions) {
                foreach (Location loc in region.Locations) {
                    foreach (Floor floor in loc.Floors) {
                        foreach (RoomMailbox room in floor.Rooms) {
                            if (room.Id == id) {
                                return room;
                            }
                        }
                        foreach (var room in floor.LinkedRooms) {
                            if (room.Id == id) {
                                return room;
                            }
                        }
                    }
                }
            }
            return null;
        }

        public Region GetRegionByLocationId(int id) {
            foreach (Region region in Regions) {
                foreach (Location loc in region.Locations) {
                    if (loc.Id == id) {
                        return region;
                    }
                }
            }
            return null;
        }

        public Location GetLocationById(int id) {
            foreach (Region region in Regions) {
                foreach (Location loc in region.Locations) {
                    if (loc.Id == id) {
                        return loc;
                    }
                }
            }
            return null;
        }

        public Floor GetFloorById(int id) {
            foreach (Region region in Regions) {
                foreach (Location loc in region.Locations) {
                    foreach (Floor floor in loc.Floors) {
                        if (floor.Id == id) {
                            return floor;
                        }
                    }
                }
            }
            return null;
        }

        public Floor GetFloorFromRoomById(int id) {
            foreach (Region region in Regions) {
                foreach (Location loc in region.Locations) {
                    foreach (Floor floor in loc.Floors) {
                        foreach (RoomMailbox room in floor.Rooms) {
                            if (room.Id == id) {
                                return floor;
                            }
                        }
                        foreach (var room in floor.LinkedRooms) {
                            if (room.Id == id) {
                                return floor;
                            }
                        }
                    }
                }
            }
            return null;
        }

        public Location GetLocationFromRoomById(int id) {
            foreach (Region region in Regions) {
                foreach (Location loc in region.Locations) {
                    foreach (Floor floor in loc.Floors) {
                        foreach (RoomMailbox room in floor.Rooms) {
                            if (room.Id == id) {
                                return loc;
                            }
                        }
                        foreach (var room in floor.LinkedRooms) {
                            if (room.Id == id) {
                                return loc;
                            }
                        }
                    }
                }
            }
            return null;
        }

        public Region GetRegionByRoomId(int id) {
            foreach (Region region in Regions) {
                foreach (Location loc in region.Locations) {
                    foreach (Floor floor in loc.Floors) {
                        foreach (RoomMailbox room in floor.Rooms) {
                            if (room.Id == id) {
                                return region;
                            }
                        }
                        foreach(var room in floor.LinkedRooms) {
                            if(room.Id == id) {
                                return region;
                            }
                        }
                    }
                }
            }

            return null;
        }

        public Region GetRegionFromLocationId(int id) {
            foreach (Region region in Regions) {
                foreach (Location loc in region.Locations) {
                    if (loc.Id == id) {
                        return region;
                    }
                }
            }
            return null;
        }

        public BookingItem GetAssetById(int id) {
            foreach (Region region in Regions) {
                foreach (var item in region.BookingItems) {
                    if (item.Id == id) {
                        return item;
                    }
                }
            }
            return null;
        }
        #endregion

        public RoomBookingData(string jsonData)
        {
            var data = JsonConvert.DeserializeObject<RoomBookingData>(jsonData);


            this.Regions = data.Regions;
            this.ServiceMailboxes = data.ServiceMailboxes;
            this.SecurityRoles = data.SecurityRoles;
            this.EWSCredentials = data.EWSCredentials;
            this.DisplaySettings = data.DisplaySettings;

            for (int r=0;r<this.Regions.Count;r++)
            {
                for (int i=0;i<this.Regions[r].BookingItems.Count;i++)
                {
                    this.Regions[r].BookingItems[i].Cost = new Currency
                    {
                        HasSymbolOnLeft = this.Regions[r].HasSymbolOnLeft,
                        Symbol = this.Regions[r].CurrencySymbol,
                        Value = this.Regions[r].BookingItems[i].RawCost
                    };
                }
            }
        }

        public List<Pin> EstatePins
        {
            get
            {
                var result = new List<Pin>();
                foreach (Region region in this.Regions)
                {
                    Pin pin = new Pin
                    {
                        Poly = region.Poly,
                        MetaId = region.Id,
                        SelectionType = Pin.enum_SelectionType.Estate
                    };

//                    region.RegionPin.SelectionType = Pin.enum_SelectionType.Estate;
//                    region.RegionPin.MetaId = region.Id;
                    result.Add(pin);
                }
                return result;
            }
        }

        public RoomBookingData()
        {
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
