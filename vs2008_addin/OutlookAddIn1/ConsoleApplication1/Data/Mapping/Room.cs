﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public abstract class Room
    {
        private Brush Fill_Free;
        private Brush Fill_Booked;

        [JsonIgnore]
        public GraphicsPath Path = new GraphicsPath();
        public int Id { get; set; }
        public string Name { get; set; }
        public string Attendee { get; set; }
        public int Capacitiy { get; set; }
        public int SetupTime { get; set; }
        public int ClearDownTime { get; set; }
        public string AlertEmail { get; set; }
        public List<int> Features { get; set; }
        public List<int> Facilities { get; set; }
        public List<int> Catering { get; set; }
        public List<int> Layout { get; set; }
        public int MinLeadTime { get; set; }

        public List<Point> Poly = new List<Point>();

        public string Notes { get; set; }
        public string Description { get; set; }

        public Room()
        {
            Fill_Free = new SolidBrush(Color.FromArgb(70, 100, 200, 100));
            Fill_Booked = new SolidBrush(Color.FromArgb(70, 200, 100, 100));

            Features = new List<int>();
            Facilities = new List<int>();
        }
        public override string ToString()
        {
            return Name;
        }

        public void AddPoint(int x, int y)
        {
            Poly.Add(new Point(x, y));
        }

        public void Setup()
        {
        }


        public string StatusName
        {
            get
            {
                return string.Empty;
                
            }
        }
    }
}
