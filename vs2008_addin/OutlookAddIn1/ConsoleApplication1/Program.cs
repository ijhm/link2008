﻿using Microsoft.Exchange.WebServices.Data;
using OneSpace.OutlookPlugin.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace LinkConfigAnalysis
{
    class Program
    {
        static int width = 769;
        static int height = 433;

        static void Main(string[] args)
        {

            if (args.Length!=3) 
            {
                Console.WriteLine("Link Config Analysis parameters:");
                Console.WriteLine("LinkConfigAnalysis.exe <username> <password> <dir path>");
                Environment.Exit(1);
            }
            else
            {
                string uid = args[0];
                string pwd = args[1];
                string dirPath = args[2];
                int pos = uid.IndexOf("@");
                string domain = uid.Substring(pos+1);

                string configPath = System.IO.Path.Combine(dirPath, "config.json");
                var exch = new CibExchangeLib.CibExchangeAdaptor("Exchange2013_SP1",uid,pwd);
                string jsonData = System.IO.File.ReadAllText(configPath);
                var data = new RoomBookingData(jsonData);

                /*
                GetRooms(exch.service, uid);

                var roomList = exch.GetRoomList();
                // Display the room lists.
                foreach (string address in roomList)
                {
                    Console.WriteLine(address);
                }

    */
                var check = "";

                DateTime start = DateTime.Now;
                DateTime end = start.AddDays(7);
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine("Checking system");
                Console.WriteLine("===============");
                Console.Write("Checking service account:");
                string serviceEmail = string.Format("{0}@{1}", "onespace", domain);
                if (exch.CheckEmail(serviceEmail))
                {
                    Console.WriteLine("Ok");
//                    var exch2 = new CibExchangeLib.CibExchangeAdaptor("Exchange2013_SP1", uid, pwd, serviceEmail);
//                    var obj = exch2.GetItems(serviceEmail);
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Unable to access Onespace service account");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                Console.WriteLine("________________________________________________________");
                Console.WriteLine("Checking config");
                Console.WriteLine("===============");
                Console.WriteLine(string.Format("Poly:{0}", CheckPinsOk(data.EstatePins)));
                Console.WriteLine(string.Format("Estate:{0}", CheckImage(dirPath,"estate.png")));

                for (int r=0;r<data.Regions.Count;r++)
                {
                    Console.WriteLine(string.Format(" Region:{0}", data.Regions[r].Name));
                    Console.WriteLine(string.Format("  Poly:{0}", CheckPolyOk(data.Regions[r].Poly)));
                    Console.WriteLine(string.Format("  Image:{0}", CheckImage(dirPath,string.Format("r{0}.png",data.Regions[r].Id))));

                    for (int l=0;l<data.Regions[r].Locations.Count;l++)
                    {
                        Console.WriteLine(string.Format("   Location:{0}", data.Regions[r].Locations[l].Name));
                        Console.WriteLine(string.Format("    Poly:{0}", CheckPolyOk(data.Regions[r].Poly)));
                        Console.WriteLine(string.Format("    Image:{0}", CheckImage(dirPath, string.Format("l{0}.png", data.Regions[r].Locations[l].Id))));
                        for (int f=0;f<data.Regions[r].Locations[l].Floors.Count;f++)
                        {
                            Console.WriteLine(string.Format("    Floor:{0}", data.Regions[r].Locations[l].Floors[f].Name));
                            Console.WriteLine(string.Format("     Poly:{0}", CheckPolyOk(data.Regions[r].Locations[l].Floors[f].Poly)));
                            Console.WriteLine(string.Format("     Image:{0}", CheckImage(dirPath, string.Format("f{0}.png", data.Regions[r].Locations[l].Floors[f].Id))));
                            for (int i=0;i<data.Regions[r].Locations[l].Floors[f].Rooms.Count;i++)
                            {
                                var room = data.Regions[r].Locations[l].Floors[f].Rooms[i];
                                Console.WriteLine(string.Format("    Room:{0} ", room.Name));
                                Console.WriteLine("     Checking");
                                Console.Write("      Exists:");
                                try
                                {
                              //      var items = exch.GetBookings(exch.service, room.MailBox, start, end);
                                    if (exch.GetRoomAlias(room.MailBox,room.Attendee))
                                    {
                                        Console.WriteLine("Ok");
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Mailbox/attendee not resolved");
                                        Console.ForegroundColor = ConsoleColor.White;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Error " + ex.Message);
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                                Console.Write("      Permissions:");
                                try
                                {
                                    var items = exch.GetBookings(exch.service, room.MailBox, start, end);
                                    Console.WriteLine("Ok");
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                                catch (Exception ex)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Error " + ex.Message);
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                                Console.Write("      Features:");
                                check = CheckArray(data.Regions[r], room.Features, enum_ItemType.Features);
                                if (check=="")
                                {
                                    Console.WriteLine("Ok");
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("One or more items are inconsistance:" + check);
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                                Console.Write("      Facilities:");
                                check = CheckArray(data.Regions[r], room.Facilities, enum_ItemType.Facilities);
                                if (check == "")
                                {
                                    Console.WriteLine("Ok");
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("One or more items are inconsistance:" + check);
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                                Console.Write("      Catering:");
                                check = CheckArray(data.Regions[r], room.Catering, enum_ItemType.Catering);
                                if (check == "")
                                {
                                    Console.WriteLine("Ok");
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("One or more items are inconsistance:" + check);
                                    Console.ForegroundColor = ConsoleColor.White;
                                }
                                Console.Write("      Layout:");
                                check = CheckArray(data.Regions[r], room.Layout, enum_ItemType.Layout);
                                if (check == "")
                                {
                                    Console.WriteLine("Ok");
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("One or more items are inconsistance:" + check);
                                    Console.ForegroundColor = ConsoleColor.White;
                                }

                            }
                        }
                    }
                }

                Console.WriteLine("Scanning complete");

            }
        }

        private static string CheckArray(Region region,List<int> roomArray, enum_ItemType type)
        {
            string result = "";
            int found = 0;
            for (int r=0;r<roomArray.Count;r++)
            {
                bool gotIt = false;
                for (int fe = 0; fe < region.BookingItems.Count; fe++)
                {
                    if (region.BookingItems[fe].ItemType == type)
                    {
                        if (roomArray[r]== region.BookingItems[fe].Id)
                        {
                            found++;
                            gotIt = true;
                        }
                    }
                }
                if (!gotIt)
                {
                    result += "[" + roomArray[r] + "]";
                }
            }
            if (found==roomArray.Count)
            {
                return "";
            }
            else
            {
                return result;
            }
        }

        public static string CheckPinsOk(List<Pin> poly)
        {
            bool result = true;
            for (int i = 0; i < poly.Count; i++)
            {
                if (poly[i].X > width ||
                    poly[i].Y > height)
                {
                    result = false;
                }
            }
            if (result)
            {
                return "Ok";
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                return "Point too large";
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public static string CheckPolyOk(List<System.Drawing.Point> poly)
        {
            bool result = true;
            for(int i=0;i<poly.Count;i++)
            {
                if (poly[i].X > width ||
                    poly[i].Y > height)
                {
                    result = false;
                }
            }
            if (result)
            {
                return "Ok";
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                return "Point too large";
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public static string CheckImage(string dirPath,string fileName)
        {
            string filePath = System.IO.Path.Combine(dirPath, fileName);
            if (!System.IO.File.Exists(filePath))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                return "File " + fileName + " not found";
                Console.ForegroundColor = ConsoleColor.White;
            }
            System.Drawing.Image img;
            var fs = new System.IO.FileStream(filePath, System.IO.FileMode.Open);
            img = System.Drawing.Image.FromStream(fs);
            if (img.Width>width || img.Height>height)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                return "Image " + fileName + " is too big";
                Console.ForegroundColor = ConsoleColor.White;
            }
            return "Ok";
        }

        private static void GetRooms(ExchangeService service, string email)
        {
            // Return all the room lists in the organization
            EmailAddressCollection myRoomLists = service.GetRoomLists();

            // Retrieve the room list that matches your criteria
            EmailAddress myAddress = new EmailAddress(email);
            foreach (EmailAddress address in myRoomLists)
            {
                if (address == myAddress)
                {
                    Console.WriteLine("Found {0} in room list", myAddress);
                }
                // Expand the selected collection to get a list of rooms.
                System.Collections.ObjectModel.Collection<EmailAddress> myRoomAddresses = service.GetRooms(address);

                // Display the individual rooms.
                foreach (EmailAddress addr in myRoomAddresses)
                {
                    Console.WriteLine("Email Address: {0}", addr.Address);
                }
            }

        }
    }
}
