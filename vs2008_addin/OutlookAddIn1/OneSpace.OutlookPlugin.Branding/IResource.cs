﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Branding {
    public interface IResource {
        string LinkName { get; }
        Color PrimaryColour { get; }
        Color TextColour { get; }
        Image Banner { get; }
        Icon GeneralIcon { get; }

        string RoomsRegionName { get; }
        string ServicesRegionName { get; }
        string PoweredByText { get; }
        string AboutTitle { get; }
        string AppTitle { get; }
        string RibbonName { get; }
        string LinkUrl { get; }
        string BookingChartTitle { get; }
        string ReportingTitle { get; }
        string reportBookings { get; }
        string showPoweredBy { get; }
        string defaultXlsReportFileName { get; }
        string printBookingReport { get; }
        string reportServicesOrdered { get; }
        string printTemplate { get; }
        string reportCostCentre { get;}
        string xlsReportSummary { get; }
        string xlsReportServices { get; }
        string xlsReportCostCentres { get; }
        string licensePath { get; }

        Icon ReportsIcon { get; }
        Icon BookingsIcon { get; }
        Icon AdminIcon { get; }
        Icon AboutIcon { get; }
        Icon RoomsIcon { get; }

        Image ReportsImage { get; }
        Image BookingsImage { get; }
        Image AdminImage { get; }
        Image AboutImage { get; }
        Image RoomsImage { get; }
        Image ServicesImage { get; }

    }
}
