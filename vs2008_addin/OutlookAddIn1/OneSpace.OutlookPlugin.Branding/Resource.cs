﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
//using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Branding 
{
    public static class Resource 
    {
        /* Todo: Load the IResource Implentation required */
        private static IResource _resource = null;

        public static void Initialise(string resource_assembly) 
        {
            var asm_codebase = new Uri(typeof(IResource).Assembly.CodeBase);
            var resource_asm_folder = Path.GetDirectoryName(asm_codebase.LocalPath);
            var resource_assembly_full_path = Path.Combine(resource_asm_folder, resource_assembly);
            var branding_asm = Assembly.LoadFile(resource_assembly_full_path);

            var i_resource_type = typeof(IResource);

            var resource_type = (
                from a in branding_asm.GetTypes()
                where i_resource_type.IsAssignableFrom(a)
                select a
            ).FirstOrDefault();

            if (resource_type == null) {
                throw new Exception("The specified resource assembly dosn't contain a valid IResource");
            }

            _resource = Activator.CreateInstance(resource_type) as IResource;
        }

        public static string LinkName 
        { 
            get 
            {
                return _resource.LinkName; 
            }
        }

        public static Image Banner 
        { 
            get 
            {
                if  (_resource==null)
                {
                    return new Bitmap(1729, 549);
                }
                else
                {
                    return _resource.Banner; 
                }
            }
        } 

        public static Color PrimaryColour 
        {
            get
            { 
                return _resource.PrimaryColour; 
            }
        }

        public static Color TextColour 
        {
            get 
            {
                if (_resource==null)
                {
                    return Color.Black;
                }
                else
                {
                    return _resource.TextColour;
                }
            }
        } 
        

        public static Icon GeneralIcon 
        {
            get 
            { 
                return  _resource.GeneralIcon; 
            } 
        }

        public static string RoomsRegionName 
        {
            get 
            {
                if (_resource==null)
                {
                    return "{TEMPLATE}";
                }
                else
                {
                    return _resource.RoomsRegionName;
                }
            }
        }
       
        public static string ServicesRegionName
        {
            get 
            {
                if (_resource==null)
                {
                    return "{TEMPLATE}";
                }
                else
                {
                    return _resource.ServicesRegionName;
                }
            }
        }

        public static string PoweredByText 
        {
            get 
            {
                if (_resource == null)
                {
                    return "{TEMPLATE}";
                }
                else
                {
                    return _resource.PoweredByText;
                }
            }
        }

        public static string AboutTitle 
        {
            get 
            { 
                return  _resource.AboutTitle; 
            } 
        }
 
        public static string AppTitle 
        {
            get 
            { 
                return  _resource.AppTitle; 
            } 
        }
 
        public static string RibbonName 
        {
            get 
            { 
                return  _resource.RibbonName; 
            } 
        }
 
        public static string LinkUrl
        {
            get 
            { 
                return  _resource.LinkUrl; 
            } 
        }
            
        public static string BookingChartTitle 
        {
            get 
            { 
                return  _resource.BookingChartTitle; 
            } 
        }
            
        public static string ReportingTitle 
        {
            get 
            { 
                return  _resource.ReportingTitle; 
            } 
        }
        
        public static string reportBookings 
        {
            get 
            { 
                return  _resource.reportBookings; 
            } 
        }
            
        public static string showPoweredBy 
        {
            get 
            { 
                return  _resource.showPoweredBy; 
            } 
        }
            
        public static string defaultXlsReportFileName
        {
            get 
            { 
                return  _resource.defaultXlsReportFileName; 
            } 
        }
            
        public static string printBookingReport
        {
            get 
            { 
                return  _resource.printBookingReport; 
            } 
        }
            
        public static string reportServicesOrdered
        {
            get 
            { 
                return  _resource.reportServicesOrdered; 
            } 
        }
        
        public static string printTemplate
        {
            get 
            { 
                return  _resource.printTemplate; 
            } 
        }

        public static string reportCostCentre
        {
            get 
            { 
                return  _resource.reportCostCentre; 
            } 
        }

        public static string xlsReportSummary 
        {
            get 
            { 
                return  _resource.xlsReportSummary; 
            } 
        }
            
        public static string xlsReportServices 
        {
            get 
            { 
                return  _resource.xlsReportServices; 
            } 
        }
            
        public static string xlsReportCostCentres 
        {
            get 
            { 
                return  _resource.xlsReportCostCentres; 
            } 
        }
            
        public static string licensePath 
        {
            get 
            { 
                return  _resource.licensePath;
            } 
        }

        public static Icon ReportsIcon 
        {
            get 
            { 
                return  _resource.ReportsIcon;
            } 
        }
            
        public static Icon BookingsIcon 
        {
            get 
            { 
                return  _resource.BookingsIcon;
            } 
        }
            
        public static Icon AdminIcon 
        {
            get 
            { 
                return  _resource.AdminIcon;
            } 
        }
    
        public static Icon AboutIcon 
        {
            get 
            { 
                return  _resource.AboutIcon;
            } 
        } 

        public static Icon RoomsIcon 
        {
            get 
            { 
                return  _resource.RoomsIcon;
            } 
        } 

        public static Image ReportsImage 
        {
            get 
            { 
                return  _resource.ReportsImage;
            } 
        } 
            
        public static Image BookingsImage 
        {
            get 
            { 
                return  _resource.BookingsImage;
            } 
        }           
            
        public static Image AdminImage 
        {
            get 
            { 
                return  _resource.AdminImage;
            } 
        }

        public static Image AboutImage 
        {
            get 
            { 
                return  _resource.AboutImage;
            } 
        }
            
        public static Image RoomsImage 
        {
            get 
            { 
                if (_resource==null)
                {
                    return new Bitmap(64, 64);
                }
                else
                {
                    return _resource.RoomsImage;
                }
            } 
        }
 
        public static Image ServicesImage {
            get 
            { 
                if (_resource==null)
                {
                    return new Bitmap(64, 64);
                }
                else
                {
                    return _resource.ServicesImage;
                }
            } 
        } 

    }
}
