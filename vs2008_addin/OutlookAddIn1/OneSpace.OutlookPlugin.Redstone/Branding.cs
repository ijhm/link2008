﻿using System;
using System.Drawing;

namespace OneSpace.OutlookPlugin.Redstone {
    public class Branding : OutlookPlugin.Branding.IResource 
    {
        public string LinkName { get { return  Resource.LinkName; } }

        public Image Banner { get { return  Resource.Banner; } }

        public Color PrimaryColour { get { return  ColorTranslator.FromHtml(Resource.PrimaryColour); } }

        public Color TextColour { get { return  ColorTranslator.FromHtml(Resource.TextColour); } }

        public Icon GeneralIcon { get { return  Resource.ServicesIcon; } }

        public string RoomsRegionName { get { return  Resource.RoomsRegionName; } }

        public string ServicesRegionName { get { return  Resource.ServicesRegionName; } }

        public string PoweredByText { get { return  Resource.PoweredByText; } }

        public string AboutTitle { get { return  Resource.AboutTitle; } }

        public string AppTitle { get { return  Resource.AppTitle; } }

        public string RibbonName { get { return  Resource.RibbonName; } }

        public string LinkUrl { get { return  Resource.LinkUrl; } }

        public string BookingChartTitle { get { return  Resource.BookingChartTitle; } }

        public string ReportingTitle { get { return  Resource.ReportingTitle; } }

        public string reportBookings { get { return  Resource.ReportBookings; } }

        public string showPoweredBy { get { return  Resource.showPoweredBy; } }
        
        public string defaultXlsReportFileName { get { return  Resource.defaultXlsReportFileName; } }

        public string printBookingReport { get { return  Resource.PrintBookingReport; } }

        public string reportServicesOrdered { get { return  Resource.ReportServicesOrdered; } }

        public string printTemplate { get { return  Resource.PrintTemplate; } }

        public string reportCostCentre { get { return  Resource.ReportCostCentre; } }

        public string xlsReportSummary { get { return  Resource.xlsReportSummary; } }

        public string xlsReportServices { get { return  Resource.xlsReportServices; } }

        public string xlsReportCostCentres { get { return  Resource.xlsReportCostCentres; } }

        public string licensePath { get { return  Resource.licensePath; } }

        public Icon ReportsIcon { get { return  Resource.ReportsIcon; } }

        public Icon BookingsIcon { get { return  Resource.BookingIcon; } }

        public Icon AdminIcon { get { return  Resource.AdminIcon; } }

        public Icon AboutIcon { get { return  Resource.AboutIcon; } }

        public Icon RoomsIcon { get { return  Resource.RoomsIcon; } }

        public Image ReportsImage { get { return  Resource.ReportsImage; } }

        public Image BookingsImage { get { return  Resource.BookingImage; } }

        public Image AdminImage { get { return  Resource.AdminImage; } }

        public Image AboutImage { get { return  Resource.AboutImage; } }

        public Image RoomsImage { get { return  Resource.RoomsImage; } }

        public Image ServicesImage { get { return Resource.ServicesImage; } }
    }
}
