﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.UI.Extentions;

namespace OneSpace.OutlookPlugin.UI.Controls {
    public partial class DateSelector : UserControl {
        private List<DateTime> _dates = null;
        private int _selected_index = 0;

        public event EventHandler<EventArgs> SelectedDateChanged;

        public DateSelector() {
            InitializeComponent();
        }

        public void Setup(List<DateTime> dates) {
            if(dates == null) {
                throw new ArgumentNullException(nameof(dates));
            }

            if(dates.Count == 0) {
                throw new ArgumentOutOfRangeException(nameof(dates), "You must supply at least one date");
            }

            var new_date_list = new List<DateTime>();
            foreach (var date in dates) {
                var new_date = new DateTime(date.Date.Ticks, DateTimeKind.Local);

                if (new_date_list.Contains(new_date) == false) {
                    new_date_list.Add(new_date);
                }
            }

            new_date_list.Sort();
            _dates = new_date_list;
            _selected_index = 0;
            label_date.Text = SelectedDate?.ToLongDateString() ?? string.Empty;
            button_previous.Enabled = false;
            button_next.Enabled = true;
        }

        public DateTime? SelectedDate { get { return _dates.GetValueOrDefault(_selected_index); } }

        private void button_previous_Click(object sender, EventArgs e) {
            var has_changed = false;

            if (_selected_index > 0) {
                _selected_index--;
                has_changed = true;
            }
            else if (_selected_index < 0) {
                _selected_index = 0;
                has_changed = true;
            }
            
            if (has_changed) {
                OnSelectedDateChanged();
            }
        }

        private void button_next_Click(object sender, EventArgs e) {
            var max_index = _dates.Count - 1;
            var has_changed = false;

            if (_selected_index < max_index) {
                _selected_index++;
                has_changed = true;
            }
            else if (_selected_index > max_index) {
                _selected_index = max_index;
                has_changed = true;
            }

            if (has_changed) {
                OnSelectedDateChanged();
            }
        }

        private void OnSelectedDateChanged() {
            var max_index = _dates.Count - 1;

            button_previous.Enabled = (_selected_index > 0);
            button_next.Enabled = (_selected_index < max_index);

            label_date.Text = SelectedDate?.ToLongDateString() ?? string.Empty;

            this.OnEventHandler(SelectedDateChanged);
        }
    }
}
