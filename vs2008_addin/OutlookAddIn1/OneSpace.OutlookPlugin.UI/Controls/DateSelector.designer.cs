﻿namespace OneSpace.OutlookPlugin.UI.Controls {
    partial class DateSelector {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.button_previous = new System.Windows.Forms.Button();
            this.button_next = new System.Windows.Forms.Button();
            this.label_date = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_previous
            // 
            this.button_previous.Dock = System.Windows.Forms.DockStyle.Left;
            this.button_previous.Enabled = false;
            this.button_previous.Image = global::OneSpace.OutlookPlugin.UI.Properties.Resources.bullet_arrow_left;
            this.button_previous.Location = new System.Drawing.Point(0, 0);
            this.button_previous.Name = "button_previous";
            this.button_previous.Size = new System.Drawing.Size(20, 20);
            this.button_previous.TabIndex = 0;
            this.button_previous.UseVisualStyleBackColor = true;
            this.button_previous.Click += new System.EventHandler(this.button_previous_Click);
            // 
            // button_next
            // 
            this.button_next.Dock = System.Windows.Forms.DockStyle.Right;
            this.button_next.Enabled = false;
            this.button_next.Image = global::OneSpace.OutlookPlugin.UI.Properties.Resources.bullet_arrow_right;
            this.button_next.Location = new System.Drawing.Point(130, 0);
            this.button_next.Name = "button_next";
            this.button_next.Size = new System.Drawing.Size(20, 20);
            this.button_next.TabIndex = 1;
            this.button_next.UseVisualStyleBackColor = true;
            this.button_next.Click += new System.EventHandler(this.button_next_Click);
            // 
            // label_date
            // 
            this.label_date.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_date.Location = new System.Drawing.Point(20, 0);
            this.label_date.Name = "label_date";
            this.label_date.Size = new System.Drawing.Size(110, 20);
            this.label_date.TabIndex = 2;
            this.label_date.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DateSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label_date);
            this.Controls.Add(this.button_next);
            this.Controls.Add(this.button_previous);
            this.Name = "DateSelector";
            this.Size = new System.Drawing.Size(150, 20);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_previous;
        private System.Windows.Forms.Button button_next;
        private System.Windows.Forms.Label label_date;
    }
}
