﻿namespace OneSpace.OutlookPlugin.UI.Controls {
    partial class HorizontalLine {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label_line = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_line
            // 
            this.label_line.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_line.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label_line.Location = new System.Drawing.Point(0, 0);
            this.label_line.Name = "label_line";
            this.label_line.Size = new System.Drawing.Size(100, 2);
            this.label_line.TabIndex = 0;
            // 
            // HorizontalLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.label_line);
            this.Name = "HorizontalLine";
            this.Size = new System.Drawing.Size(100, 2);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label_line;
    }
}
