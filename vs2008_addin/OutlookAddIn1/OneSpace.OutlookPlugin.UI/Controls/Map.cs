﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.UI.Data;
using System.Diagnostics;

namespace OneSpace.OutlookPlugin.UI.Controls {
    public partial class Map : UserControl {
        private static readonly Color COLOUR_ROOM_AVALIABLE = Color.FromArgb(155, 102, 204, 51);
        private static readonly Color COLOUR_ROOM_SELECTED = Color.FromArgb(79, 204, 0, 51);
        private static readonly Color COLOUR_ROOM_BORDER = Color.Black;
        private static readonly Brush BRUSH_ROOM_AVALIABLE = new SolidBrush(COLOUR_ROOM_AVALIABLE);
        private static readonly Brush BRUSH_ROOM_SELECTED = new SolidBrush(COLOUR_ROOM_SELECTED);
        private static readonly Pen PEN_ROOM_BORDER = new Pen(COLOUR_ROOM_BORDER, 1);

        public event EventHandler<EventArgs> SelectedLocationChanged;
        public event EventHandler<EventArgs> SelectedRoomChanged;

        public Map() {
            InitializeComponent();
            this.SetStyle(
                System.Windows.Forms.ControlStyles.UserPaint |
                System.Windows.Forms.ControlStyles.AllPaintingInWmPaint |
                System.Windows.Forms.ControlStyles.OptimizedDoubleBuffer,
                true);

            picture_box_map.Click += picture_box_map_Click;
        }

        private void AddLocationChildren(List<Location> locations, Location location) {
            foreach(var child in location.Children) {
                locations.Add(child);
                AddLocationChildren(locations, child);
            }
        }

        public void Setup(Location location) {
            var locations = new List<Location>();
            locations.Add(location);
            AddLocationChildren(locations, location);

            Rooms = (new List<Room>()).AsReadOnly();
            Locations = locations.AsReadOnly();
            ChangeSelectedLocation(location);
        }

        public void Setup(List<Room> rooms, Location optional_root_location) {
            Rooms = rooms.AsReadOnly();

            var locations = new List<Location>();
            if(optional_root_location != null) {
                locations.Add(optional_root_location);
                AddLocationChildren(locations, optional_root_location);
            }

            foreach (var room in Rooms) {
                if(room.Parent != null) 
                {
                    var root_location = room.Parent;
                    while (root_location != null && root_location.Parent != null) 
                    {
                        root_location = root_location.Parent;
                    }

                    if (locations.Contains(root_location) == false) 
                    {
                        locations.Add(root_location);
                        AddLocationChildren(locations, root_location);
                    }
                }
            }

            Locations = locations.AsReadOnly();
            DrawPolys();
        }

        public IList<Location> Locations { get; set; }
        public IList<Data.Room> Rooms { get; private set; }

        public Location SelectedLocation { get; private set; }
        public Room SelectedRoom { get; private set; }
        
        public bool TrySelectLocation(Location location) {
            var is_location_in_list = (
                from l in Locations
                where l == location
                select l
            ).Any();

            if (is_location_in_list || location == null) {
                ChangeSelectedLocation(location);
                return true;
            }
            else {
                return false;
            }
        }

        public bool TrySelectRoom(Room room) {
            var is_room_in_list = (
                from r in Rooms
                where r == room
                select r
            ).Any();

            if (is_room_in_list || room == null) {
                ChangeSelectedRoom(room);
                return true;
            }
            else {
                return false;
            }
        }

        private void ChangeSelectedLocation(Location location) {
            if (SelectedLocation != location) {
                var current_room = SelectedRoom;
                var has_room_changed = (current_room != null);

                SelectedRoom = null;
                SelectedLocation = location;
                DrawPolys();

                OnSelectedLocationChanged();

                if (has_room_changed) {
                    OnSelectedRoomChanged();
                }
            }
        }

        private void ChangeSelectedRoom(Room room) {
            if (SelectedRoom != room) {
                var has_selected_location_changed = (room != null && SelectedLocation != room.Parent);

                SelectedRoom = room;

                if (has_selected_location_changed) {
                    SelectedLocation = room.Parent;
                }

                DrawPolys();

                if (has_selected_location_changed) {
                    OnSelectedLocationChanged();
                }
                
                OnSelectedRoomChanged();
            }
        }

        private void DrawPolys() {
            Image image = null;
            var selected_location = SelectedLocation;
            var selected_room = SelectedRoom;

            if(selected_room != null || selected_location != null) 
            {  
                Location  location = (selected_room == null || selected_room.Parent==null)
                    ? selected_location
                    : selected_room.Parent;

                var rooms = (
                    from r in Rooms
                    where r.Parent == location
                    select r
                ).ToList();

                var linked_rooms = (selected_rooms==null || selected_room.LinkedRooms==null)
                    ? new List<Room>()
                    : selected_room.LinkedRooms;

                image = new Bitmap(location.Layout);

                using (var g = Graphics.FromImage(image)) 
                {
                    foreach (var room in rooms) 
                    {
                        var brush = (selected_room == room) ? BRUSH_ROOM_SELECTED : BRUSH_ROOM_AVALIABLE;
                        if (room.Path != null && linked_rooms.Contains(room) == false) 
                        {
                            g.FillPath(brush, room.Path);
                            g.DrawPath(PEN_ROOM_BORDER, room.Path);
                        }

                        if(room.LinkedRooms!=null && room.LinkedRooms.Count > 0) 
                        {
                            foreach(var linked_room in room.LinkedRooms) 
                            {
                                if(rooms.Contains(linked_room) == false || (selected_room == room)) 
                                {
                                    g.FillPath(brush, linked_room.Path);
                                    g.DrawPath(PEN_ROOM_BORDER, linked_room.Path);
                                }
                            }
                        }

                    }
                }
            }

            var old_image = picture_box_map.Image;
            picture_box_map.Image = image /*?? SelectedLocation?.Layout*/ ?? Branding.Resource.Banner;

            if (old_image != null) {
                old_image.Dispose();
            }
        }

        private void picture_box_map_Click(object sender, EventArgs e) {
            /* Note: In MS infinate wisdom they didn't think it would be useful to
             *       have an event handler with correctly typed variables.
             *       So here we cast to the correct variable and pass it to
             *       what should have been the event handler.
             * */
            var me = e as MouseEventArgs;
            if (me != null) {
                picture_box_map_Click(sender, me);
            }
        }

        private void picture_box_map_Click(object sender, MouseEventArgs e) {
            var point = picture_box_map.TransalateToOriginalImageCoord(e.Location);

            var selected_room = 
            (
                from r in Rooms
                where r.Parent == SelectedLocation && IsVisible(r,point)
                select r
            ).FirstOrDefault();

            ChangeSelectedRoom(selected_room);

            if(selected_room == null) 
            {
                var current_selected_location = SelectedLocation;
                var selected_location = (
                    from l in Locations
                    where l.Parent == current_selected_location &&
                          l.Path.IsVisible(point) == true
                    select l
                ).FirstOrDefault() ?? current_selected_location;

                ChangeSelectedLocation(selected_location);
            }
        }

        private bool IsVisible(Room r, Point p)
        {
            if (r == null)
                return false;
            if (r.Path == null)
                return false;
            return r.Path.IsVisible(p);
        }

        private void OnSelectedLocationChanged() {
            this.OnEventHandler(SelectedLocationChanged);
        }

        private void OnSelectedRoomChanged() {
            this.OnEventHandler(SelectedRoomChanged);
        }
    }
}
