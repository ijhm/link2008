﻿namespace OneSpace.OutlookPlugin.UI.Controls {
    partial class Map {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.picture_box_map = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_map)).BeginInit();
            this.SuspendLayout();
            // 
            // picture_box_map
            // 
            this.picture_box_map.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picture_box_map.Location = new System.Drawing.Point(0, 0);
            this.picture_box_map.Name = "picture_box_map";
            this.picture_box_map.Size = new System.Drawing.Size(683, 384);
            this.picture_box_map.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picture_box_map.TabIndex = 0;
            this.picture_box_map.TabStop = false;
            // 
            // Map
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.picture_box_map);
            this.Name = "Map";
            this.Size = new System.Drawing.Size(683, 384);
            ((System.ComponentModel.ISupportInitialize)(this.picture_box_map)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picture_box_map;
    }
}
