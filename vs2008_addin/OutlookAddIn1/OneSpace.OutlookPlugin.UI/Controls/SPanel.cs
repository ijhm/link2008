﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OneSpace.OutlookPlugin.UI.Controls {
    public class SPanel : System.Windows.Forms.Panel {
        protected override void OnMouseWheel(MouseEventArgs e) {
            if (this.VScroll && (Control.ModifierKeys & Keys.Shift) == Keys.Shift) {
                this.VScroll = false;
                base.OnMouseWheel(e);
                this.VScroll = true;
            }
            else {
                base.OnMouseWheel(e);
            }
        }
    }
}
