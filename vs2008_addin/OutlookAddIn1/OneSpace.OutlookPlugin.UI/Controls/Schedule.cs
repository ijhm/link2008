﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Drawing.Drawing2D;
using OneSpace.OutlookPlugin.UI.Data;

namespace OneSpace.OutlookPlugin.UI.Controls {
    public partial class Schedule : UserControl {
        internal class RoomComparer : IComparer<Room> {
            public int Compare(Room x, Room y) {
                return x.Name.CompareTo(y.Name);
            }
        }
        public System.Threading.Timer BackgroundTimer;
        int count = 0;

        internal const int HEADER_ROW_COUNT = 2; /* Date & Time Rows */
        internal const float MM_PER_INCH = 25.4f;
        internal const float OVERSCROLL_MM = 5.0f;
        internal const float DEFAULT_DPI = 96.0f;
        internal const float DEFAULT_DPM = DEFAULT_DPI / MM_PER_INCH;
        internal const float HOUR_WIDTH_MM = 20.0f;
        internal const float HOUR_HIGHT_MM = 12.4f;
        internal const float BOOKING_HIGHT_MM = (HOUR_HIGHT_MM / 100.0f) * 80.0f;
        internal const float BOOKING_OFFSET_MM = (HOUR_HIGHT_MM / 100.0f) * 10.0f;
        internal const float DATE_HEADING_TEXT_OFFSET_MM = 1.0f;
        internal const float FONT_SIZE_DEFAULT_HEADING = 10.0f;
        internal const float FONT_SIZE_DEFAULT_ROOM = 9.0f;
        internal const float LABLE_INDENT_MM = 5.0f;
        internal const float INVLAIDATION_PADDING = 10.0f;

        /* Todo: Determine if this works for languages like Simplified Chinese */
        internal const string FORMAT_DATE = "ddd dd MMM";
        internal const string FONT_NAME_DEFAULT = "Arial";

        internal static readonly TimeSpan ONE_DAY = TimeSpan.FromDays(1);

        internal static readonly Color COLOUR_HEADING = Color.FromArgb(255, 219, 219, 212);
        internal static readonly Color COLOUR_HEADING_TEXT = Branding.Resource.TextColour;
        internal static readonly Color COLOUR_TEXT = Color.DarkSlateGray;
        internal static readonly Color COLOUR_BACK = Color.White;
        internal static readonly Color COLOUR_ROOM_BACK = Color.FromArgb(255, 232, 232, 227);
        internal static readonly Color COLOUR_GRID_LINE = Color.FromArgb(255, 206, 206, 206);
        internal static readonly Color COLOUR_BOOKING = Branding.Resource.PrimaryColour;
        internal static readonly Color COLOUR_BUFFER = Color.DarkGray;
        internal static readonly Color COLOUR_BOOKING_LINKED_LOCATION = Color.DarkGray;
        internal static readonly Color COLOUR_HIGHLIGHT_SECTION = Color.FromArgb(100, Color.LightSkyBlue);

        internal static readonly Brush BRUSH_HEADING = new SolidBrush(COLOUR_HEADING);
        internal static readonly Brush BRUSH_HEADING_TEXT = new SolidBrush(COLOUR_HEADING_TEXT);
        internal static readonly Brush BRUSH_TEXT = new SolidBrush(COLOUR_TEXT);
        internal static readonly Brush BRUSH_BACK = new SolidBrush(COLOUR_BACK);
        internal static readonly Brush BRUSH_ROOM_BACK = new SolidBrush(COLOUR_ROOM_BACK);
        internal static readonly Brush BRUSH_BOOKING = new SolidBrush(COLOUR_BOOKING);
        internal static readonly Brush BRUSH_BUFFER = new SolidBrush(COLOUR_BUFFER);
        internal static readonly Brush BRUSH_BOOKING_SELECTED = new HatchBrush(HatchStyle.BackwardDiagonal, COLOUR_BOOKING);
        internal static readonly Brush BRUSH_BOOKING_LINK_LOCATION = new SolidBrush(COLOUR_BOOKING_LINKED_LOCATION);
        internal static readonly Brush BRUSH_HIGHLIGHT_SECTION = new SolidBrush(COLOUR_HIGHLIGHT_SECTION);
        internal static readonly Pen PEN_GRID_LINE = new Pen(COLOUR_GRID_LINE);

        internal static readonly Font FONT_HEADING = new Font(FONT_NAME_DEFAULT, FONT_SIZE_DEFAULT_HEADING, FontStyle.Bold);
        internal static readonly Font FONT_LABLE = new Font(FONT_NAME_DEFAULT, FONT_SIZE_DEFAULT_ROOM, FontStyle.Regular);
        internal static readonly Font FONT_LABLE_SELECTED = new Font(FONT_NAME_DEFAULT, FONT_SIZE_DEFAULT_ROOM, FontStyle.Bold);

        private const int MAX_ROOMS_PANEL_WIDTH = 400;

        private int _rooms_panel_width = MAX_ROOMS_PANEL_WIDTH;
        private bool _do_suspend_room_and_booking_coordination = false;
        private object _sync_vertical_scroll_bars_locker = new object();
        private CancellationTokenSource _sync_vertical_scroll_bars_cancel_source = null;

        public event EventHandler<EventArgs> SelectedLocationChanged;
        public event EventHandler<EventArgs> SelectedRoomChanged;
        public event EventHandler<EventArgs> SelectedBookingChanged;

        public Schedule() {
            this.SuspendLayout();
            InitializeComponent();
            this.SetStyle(
                System.Windows.Forms.ControlStyles.UserPaint |
                System.Windows.Forms.ControlStyles.AllPaintingInWmPaint |
                System.Windows.Forms.ControlStyles.OptimizedDoubleBuffer,
                true);
            Initialise();
            this.ResumeLayout();
        }

        private async void Initialise()
        {
            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                group_rooms.ForeColor = Branding.Resource.PrimaryColour;
                group_view.ForeColor = Branding.Resource.PrimaryColour;
            }

            this.Resize += Schedule_Resize;
            this.button_schedule.Click += button_schedule_Click;
            this.button_map.Click += button_map_Click;
            this.schedule_rooms.MouseEnter += schedule_rooms_MouseEnter;
            this.panel_rooms.Scroll += panel_rooms_Scroll;
            this.schedule_view.Scroll += schedule_view_Scroll;
            this.schedule_view.MouseWheel += schedule_view_MouseWheel;
            this.panel_rooms.MouseWheel += panel_rooms_MouseWheel;

            this.map.SelectedLocationChanged += map_SelectedLocationChanged;
            this.map.SelectedRoomChanged += map_SelectedRoomChanged;
            this.schedule_rooms.SelectedRoomChanged += schedule_rooms_SelectedRoomChanged;
            this.schedule_view.SelectedRoomChanged += schedule_view_SelectedRoomChanged;
            this.schedule_view.SelectedBookingChanged += schedule_view_SelectedBookingChanged;

            this.schedule_view.Dock = DockStyle.Fill;
            this.map.Dock = DockStyle.Fill;

            button_schedule_Click(this.button_schedule, EventArgs.Empty);

            await Task.Run(() =>
            {
                BackgroundTimer = new System.Threading.Timer(new TimerCallback(SetBackground), null, 500, 500);
            });


        }

        private void SetBackground(object state)
        {
            if (count==1)
            {
                BackgroundTimer.Change(Timeout.Infinite, Timeout.Infinite);
                this.schedule_view.SetBackground();
            }
            count++;
        }

        public void Setup(Location location) {
            map.Setup(location);
            button_map_Click(this, EventArgs.Empty);
        }

        public void Setup(List<Room> rooms, DateTimeOffset begins_on, DateTimeOffset ends_on, Location optional_root_location = null) {
            schedule_rooms.Setup(rooms);
            schedule_view.Setup(rooms, begins_on, ends_on);
            map.Setup(rooms, optional_root_location);
        }

        public void Clear()
        {
            var noRooms = new List<Room>();
            schedule_rooms.Setup(noRooms);
            map.Setup(noRooms, null);
        }

        public void ClearAll()
        {
            var noRooms = new List<Room>();
            schedule_rooms.Rooms = null;
            schedule_rooms.Refresh();
//            map.Setup(noRooms, null);
        }

        public void HighlightSection(DateTimeOffset highlight_begins_on, DateTimeOffset highlight_ends_on) {
            schedule_view.HighlightSection(highlight_begins_on, highlight_ends_on);
        }

        /* Todo: Follow the data and ensure we can change these values to nullable versions */
        public DateTimeOffset BeginsOn { get { return schedule_view.BeginsOn; } }
        public DateTimeOffset EndsOn { get { return schedule_view.EndsOn; } }
        public IList<Data.Room> Rooms { get { return schedule_view.Rooms; } }
        public IList<Data.Location> Locations { get { return map.Locations; } }

        public Booking SelectedBooking { get { return schedule_view.SelectedBooking; } }
        public Room SelectedRoom { get { return schedule_view.SelectedRoom; } }
        public Location SelectedLocation { get { return map.SelectedLocation; } }

        public bool TrySelectRoom(Room room) {
            try {
                _do_suspend_room_and_booking_coordination = true;
                SuspendLayout();

                var current_room = schedule_rooms.SelectedRoom;
                if (schedule_rooms.TrySelectRoom(room) && schedule_view.TrySelectRoom(room)) {
                    if (current_room != schedule_rooms.SelectedRoom) {
                        this.OnEventHandler(SelectedRoomChanged);
                    }

                    return true;
                }
                else {
                    schedule_rooms.TrySelectRoom(current_room);
                    schedule_view.TrySelectRoom(current_room);
                    return false;
                }
            }
            finally {
                ResumeLayout();
                _do_suspend_room_and_booking_coordination = false;
            }
        }

        public bool TrySelectBooking(Booking booking) {
            try {
                _do_suspend_room_and_booking_coordination = true;
                SuspendLayout();

                var current_room = schedule_view.SelectedRoom;
                var current_booking = schedule_view.SelectedBooking;

                if (schedule_view.TrySelectBooking(booking) && schedule_rooms.TrySelectRoom(schedule_view.SelectedRoom)) {
                    if(current_room != schedule_view.SelectedRoom) {
                        this.OnEventHandler(SelectedRoomChanged);
                    }

                    if (current_booking != schedule_view.SelectedBooking) {
                        this.OnEventHandler(SelectedBookingChanged);
                    }

                    return true;
                }
                else {
                    schedule_rooms.TrySelectRoom(current_room);
                    schedule_view.TrySelectRoom(current_room);
                    schedule_view.TrySelectBooking(current_booking);
                    return false;
                }
            }
            finally {
                ResumeLayout();
                _do_suspend_room_and_booking_coordination = false;
            }
        }

        public bool TrySelectLocation(Location location) {
            return map.TrySelectLocation(location);
        }

        public void SwitchToSchedule() {
            button_schedule_Click(this, EventArgs.Empty);
        }

        public void SwitchToMap() {
            button_map_Click(this, EventArgs.Empty);
        }

        private void Schedule_Resize(object sender, EventArgs e) {
            this.split_view.Panel2MinSize = this.split_view.Width - _rooms_panel_width;
        }
        
        private void button_schedule_Click(object sender, EventArgs e) {
            this.SuspendLayout();
            this.schedule_view.Visible = true;
            this.map.Visible = false;
            this.button_map.BackColor = Color.LightGray;
            this.button_schedule.BackColor = Color.White;
            this.schedule_view.Focus();
            this.SyncVerticalScrollBars(panel_rooms, panel_veiw);
            this.ResumeLayout();
        }

        private void button_map_Click(object sender, EventArgs e) {
            this.SuspendLayout();
            this.schedule_view.Visible = false;
            this.map.Visible = true;
            this.button_map.BackColor = Color.White;
            this.button_schedule.BackColor = Color.LightGray;
            this.map.Focus();
            this.ResumeLayout();
        }

        private void schedule_rooms_MouseEnter(object sender, EventArgs e) {
            this.panel_rooms.Focus();
        }
        
        private void panel_rooms_Scroll(object sender, ScrollEventArgs e) {
            if (e.ScrollOrientation == ScrollOrientation.VerticalScroll) {
                schedule_view.VerticalScroll.Value = e.NewValue;
            }
        }

        private void schedule_view_Scroll(object sender, ScrollEventArgs e) {
            if (e.ScrollOrientation == ScrollOrientation.VerticalScroll) {
                panel_rooms.VerticalScroll.Value = e.NewValue;
            }
        }

        private void schedule_view_MouseWheel(object sender, MouseEventArgs e) {
            SyncVerticalScrollBars(schedule_view, panel_rooms);
        }

        private void panel_rooms_MouseWheel(object sender, MouseEventArgs e) {
            SyncVerticalScrollBars(panel_rooms, schedule_view);
        }

        private void SyncVerticalScrollBars(ScrollableControl from, ScrollableControl to) {
            CancellationToken token;
            var sync_now = new Action(() => {
                if (to.VerticalScroll.Maximum >= from.VerticalScroll.Value) {
                    to.VerticalScroll.Value = from.VerticalScroll.Value;
                }
            });

            lock (_sync_vertical_scroll_bars_locker) {
                if (_sync_vertical_scroll_bars_cancel_source != null) {
                    _sync_vertical_scroll_bars_cancel_source.Cancel();
                }

                _sync_vertical_scroll_bars_cancel_source = new CancellationTokenSource();
                token = _sync_vertical_scroll_bars_cancel_source.Token;
            }

            sync_now();

            /* Note: For some reason we need to assign the value a second time after the user stops scrolling otherwise we get a judder. */
            Task.Delay(10, token).ContinueWith((t) => {
                lock (_sync_vertical_scroll_bars_locker) {
                    if (token.IsCancellationRequested) {
                        return;
                    }

                    _sync_vertical_scroll_bars_cancel_source = null;
                }

                if (to.InvokeRequired) {
                    to.Invoke(sync_now);
                }
                else {
                    sync_now();
                }
            });
        }

        private void map_SelectedLocationChanged(object sender, EventArgs e) {
            if (_do_suspend_room_and_booking_coordination == false) {
                _do_suspend_room_and_booking_coordination = true;

                this.OnEventHandler(SelectedLocationChanged);

                _do_suspend_room_and_booking_coordination = false;
            }
        }

        private void map_SelectedRoomChanged(object sender, EventArgs e) {
            if (_do_suspend_room_and_booking_coordination == false) {
                _do_suspend_room_and_booking_coordination = true;

                var selected_location = SelectedLocation;

                schedule_view.TrySelectRoom(map.SelectedRoom);
                schedule_rooms.TrySelectRoom(map.SelectedRoom);

                if(selected_location != SelectedLocation) {
                    this.OnEventHandler(SelectedLocationChanged);
                }

                this.OnEventHandler(SelectedRoomChanged);

                _do_suspend_room_and_booking_coordination = false;
            }
        }

        private void schedule_rooms_SelectedRoomChanged(object sender, EventArgs e) {
            if(_do_suspend_room_and_booking_coordination == false) {
                _do_suspend_room_and_booking_coordination = true;

                var selected_location = SelectedLocation;

                schedule_view.TrySelectRoom(schedule_rooms.SelectedRoom);
                map.TrySelectRoom(schedule_rooms.SelectedRoom);

                if (selected_location != SelectedLocation) {
                    this.OnEventHandler(SelectedLocationChanged);
                }

                this.OnEventHandler(SelectedRoomChanged);

                _do_suspend_room_and_booking_coordination = false;
            }
        }

        private void schedule_view_SelectedRoomChanged(object sender, EventArgs e) {
            if (_do_suspend_room_and_booking_coordination == false) {
                _do_suspend_room_and_booking_coordination = true;

                var selected_location = SelectedLocation;

                schedule_rooms.TrySelectRoom(schedule_view.SelectedRoom);
                map.TrySelectRoom(schedule_view.SelectedRoom);

                if (selected_location != SelectedLocation) {
                    this.OnEventHandler(SelectedLocationChanged);
                }

                this.OnEventHandler(SelectedRoomChanged);

                _do_suspend_room_and_booking_coordination = false;
            }
        }

        private void schedule_view_SelectedBookingChanged(object sender, EventArgs e) {
            if (_do_suspend_room_and_booking_coordination == false) {
                _do_suspend_room_and_booking_coordination = true;

                this.OnEventHandler(SelectedBookingChanged);

                _do_suspend_room_and_booking_coordination = false;
            }
        }
    }
}
