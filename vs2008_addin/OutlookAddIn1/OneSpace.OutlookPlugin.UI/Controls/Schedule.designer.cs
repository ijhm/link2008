﻿namespace OneSpace.OutlookPlugin.UI.Controls {
    partial class Schedule {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Schedule));
            this.split_view = new System.Windows.Forms.SplitContainer();
            this.group_rooms = new System.Windows.Forms.GroupBox();
            this.panel_rooms = new OneSpace.OutlookPlugin.UI.Controls.SPanel();
            this.schedule_rooms = new OneSpace.OutlookPlugin.UI.Controls.ScheduleRoomList();
            this.panel_rooms_header = new System.Windows.Forms.Panel();
            this.hz_rooms_tabs_border = new OneSpace.OutlookPlugin.UI.Controls.HorizontalLine();
            this.group_view = new System.Windows.Forms.GroupBox();
            this.panel_veiw = new System.Windows.Forms.Panel();
            this.schedule_view = new OneSpace.OutlookPlugin.UI.Controls.ScheduleView();
            this.map = new OneSpace.OutlookPlugin.UI.Controls.Map();
            this.panel_view_buttons = new System.Windows.Forms.Panel();
            this.button_map = new System.Windows.Forms.Button();
            this.button_schedule = new System.Windows.Forms.Button();
            this.hz_view_tabs_border = new OneSpace.OutlookPlugin.UI.Controls.HorizontalLine();
            ((System.ComponentModel.ISupportInitialize)(this.split_view)).BeginInit();
            this.split_view.Panel1.SuspendLayout();
            this.split_view.Panel2.SuspendLayout();
            this.split_view.SuspendLayout();
            this.group_rooms.SuspendLayout();
            this.panel_rooms.SuspendLayout();
            this.panel_rooms_header.SuspendLayout();
            this.group_view.SuspendLayout();
            this.panel_veiw.SuspendLayout();
            this.panel_view_buttons.SuspendLayout();
            this.SuspendLayout();
            // 
            // split_view
            // 
            this.split_view.Dock = System.Windows.Forms.DockStyle.Fill;
            this.split_view.Location = new System.Drawing.Point(0, 0);
            this.split_view.Name = "split_view";
            // 
            // split_view.Panel1
            // 
            this.split_view.Panel1.Controls.Add(this.group_rooms);
            this.split_view.Panel1.Padding = new System.Windows.Forms.Padding(3);
            this.split_view.Panel1MinSize = 200;
            // 
            // split_view.Panel2
            // 
            this.split_view.Panel2.Controls.Add(this.group_view);
            this.split_view.Panel2.Padding = new System.Windows.Forms.Padding(3);
            this.split_view.Panel2MinSize = 290;
            this.split_view.Size = new System.Drawing.Size(854, 480);
            this.split_view.SplitterDistance = 200;
            this.split_view.SplitterWidth = 2;
            this.split_view.TabIndex = 0;
            // 
            // group_rooms
            // 
            this.group_rooms.Controls.Add(this.panel_rooms);
            this.group_rooms.Controls.Add(this.panel_rooms_header);
            this.group_rooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group_rooms.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.group_rooms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.group_rooms.Location = new System.Drawing.Point(3, 3);
            this.group_rooms.Name = "group_rooms";
            this.group_rooms.Size = new System.Drawing.Size(194, 474);
            this.group_rooms.TabIndex = 0;
            this.group_rooms.TabStop = false;
            this.group_rooms.Text = "Rooms";
            // 
            // panel_rooms
            // 
            this.panel_rooms.AutoScroll = true;
            this.panel_rooms.BackColor = System.Drawing.Color.White;
            this.panel_rooms.Controls.Add(this.schedule_rooms);
            this.panel_rooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_rooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel_rooms.ForeColor = System.Drawing.Color.Black;
            this.panel_rooms.Location = new System.Drawing.Point(3, 57);
            this.panel_rooms.Name = "panel_rooms";
            this.panel_rooms.Size = new System.Drawing.Size(188, 414);
            this.panel_rooms.TabIndex = 1;
            // 
            // schedule_rooms
            // 
            this.schedule_rooms.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.schedule_rooms.Location = new System.Drawing.Point(0, 0);
            this.schedule_rooms.Name = "schedule_rooms";
            this.schedule_rooms.Size = new System.Drawing.Size(400, 300);
            this.schedule_rooms.TabIndex = 0;
            // 
            // panel_rooms_header
            // 
            this.panel_rooms_header.Controls.Add(this.hz_rooms_tabs_border);
            this.panel_rooms_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_rooms_header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel_rooms_header.ForeColor = System.Drawing.Color.Black;
            this.panel_rooms_header.Location = new System.Drawing.Point(3, 17);
            this.panel_rooms_header.Name = "panel_rooms_header";
            this.panel_rooms_header.Size = new System.Drawing.Size(188, 40);
            this.panel_rooms_header.TabIndex = 0;
            // 
            // hz_rooms_tabs_border
            // 
            this.hz_rooms_tabs_border.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hz_rooms_tabs_border.Location = new System.Drawing.Point(0, 39);
            this.hz_rooms_tabs_border.Name = "hz_rooms_tabs_border";
            this.hz_rooms_tabs_border.Size = new System.Drawing.Size(188, 2);
            this.hz_rooms_tabs_border.TabIndex = 0;
            // 
            // group_view
            // 
            this.group_view.Controls.Add(this.panel_veiw);
            this.group_view.Controls.Add(this.panel_view_buttons);
            this.group_view.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group_view.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.group_view.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.group_view.Location = new System.Drawing.Point(3, 3);
            this.group_view.Name = "group_view";
            this.group_view.Size = new System.Drawing.Size(646, 474);
            this.group_view.TabIndex = 0;
            this.group_view.TabStop = false;
            this.group_view.Text = "View";
            // 
            // panel_veiw
            // 
            this.panel_veiw.AutoScroll = true;
            this.panel_veiw.Controls.Add(this.schedule_view);
            this.panel_veiw.Controls.Add(this.map);
            this.panel_veiw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_veiw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel_veiw.ForeColor = System.Drawing.Color.Black;
            this.panel_veiw.Location = new System.Drawing.Point(3, 57);
            this.panel_veiw.Name = "panel_veiw";
            this.panel_veiw.Size = new System.Drawing.Size(640, 414);
            this.panel_veiw.TabIndex = 2;
            // 
            // schedule_view
            // 
            this.schedule_view.AutoScroll = true;
            this.schedule_view.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.schedule_view.Location = new System.Drawing.Point(6, 8);
            this.schedule_view.Name = "schedule_view";
            this.schedule_view.Size = new System.Drawing.Size(304, 396);
            this.schedule_view.TabIndex = 4;
            // 
            // map
            // 
            this.map.Location = new System.Drawing.Point(316, 7);
            this.map.Locations = null;
            this.map.Name = "map";
            this.map.Size = new System.Drawing.Size(321, 397);
            this.map.TabIndex = 3;
            // 
            // panel_view_buttons
            // 
            this.panel_view_buttons.Controls.Add(this.button_map);
            this.panel_view_buttons.Controls.Add(this.button_schedule);
            this.panel_view_buttons.Controls.Add(this.hz_view_tabs_border);
            this.panel_view_buttons.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_view_buttons.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel_view_buttons.ForeColor = System.Drawing.Color.Black;
            this.panel_view_buttons.Location = new System.Drawing.Point(3, 17);
            this.panel_view_buttons.Name = "panel_view_buttons";
            this.panel_view_buttons.Size = new System.Drawing.Size(640, 40);
            this.panel_view_buttons.TabIndex = 1;
            // 
            // button_map
            // 
            this.button_map.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button_map.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.button_map.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.button_map.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_map.Location = new System.Drawing.Point(87, 12);
            this.button_map.Name = "button_map";
            this.button_map.Size = new System.Drawing.Size(75, 30);
            this.button_map.TabIndex = 1;
            this.button_map.Text = "Map";
            this.button_map.UseVisualStyleBackColor = true;
            // 
            // button_schedule
            // 
            this.button_schedule.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button_schedule.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.button_schedule.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.button_schedule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_schedule.Location = new System.Drawing.Point(6, 12);
            this.button_schedule.Name = "button_schedule";
            this.button_schedule.Size = new System.Drawing.Size(75, 30);
            this.button_schedule.TabIndex = 0;
            this.button_schedule.Text = "Schedule";
            this.button_schedule.UseVisualStyleBackColor = true;
            // 
            // hz_view_tabs_border
            // 
            this.hz_view_tabs_border.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hz_view_tabs_border.Location = new System.Drawing.Point(0, 39);
            this.hz_view_tabs_border.Name = "hz_view_tabs_border";
            this.hz_view_tabs_border.Size = new System.Drawing.Size(640, 2);
            this.hz_view_tabs_border.TabIndex = 2;
            // 
            // Schedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.split_view);
            this.MinimumSize = new System.Drawing.Size(620, 348);
            this.Name = "Schedule";
            this.Size = new System.Drawing.Size(854, 480);
            this.split_view.Panel1.ResumeLayout(false);
            this.split_view.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.split_view)).EndInit();
            this.split_view.ResumeLayout(false);
            this.group_rooms.ResumeLayout(false);
            this.panel_rooms.ResumeLayout(false);
            this.panel_rooms_header.ResumeLayout(false);
            this.group_view.ResumeLayout(false);
            this.panel_veiw.ResumeLayout(false);
            this.panel_view_buttons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer split_view;
        private System.Windows.Forms.GroupBox group_rooms;
        private System.Windows.Forms.GroupBox group_view;
        private System.Windows.Forms.Panel panel_rooms_header;
        private System.Windows.Forms.Panel panel_view_buttons;
        private System.Windows.Forms.Button button_map;
        private System.Windows.Forms.Button button_schedule;
        private HorizontalLine hz_view_tabs_border;
        private System.Windows.Forms.Panel panel_veiw;
        private HorizontalLine hz_rooms_tabs_border;
        private SPanel panel_rooms;
        private Map map;
        private Controls.ScheduleView schedule_view;
        private ScheduleRoomList schedule_rooms;
    }
}
