﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using OneSpace.OutlookPlugin.UI.Data;
using System.Threading;

namespace OneSpace.OutlookPlugin.UI.Controls
{
    public partial class ScheduleOnly : UserControl
    {
        private const int MAX_ROOMS_PANEL_WIDTH = 400;

        private int _rooms_panel_width = MAX_ROOMS_PANEL_WIDTH;
        private bool _do_suspend_room_and_booking_coordination = false;
        private object _sync_vertical_scroll_bars_locker = new object();
        private CancellationTokenSource _sync_vertical_scroll_bars_cancel_source = null;

        public event EventHandler<EventArgs> SelectedLocationChanged;
        public event EventHandler<EventArgs> SelectedRoomChanged;
        public event EventHandler<EventArgs> SelectedBookingChanged;

        public ScheduleOnly()
        {
            this.SuspendLayout();
            InitializeComponent();
            Initialise();
            this.ResumeLayout();
        }

        private void Initialise()
        {
            if(LicenseManager.UsageMode != LicenseUsageMode.Designtime) {
                group_rooms.ForeColor = Branding.Resource.PrimaryColour;
                group_view.ForeColor = Branding.Resource.PrimaryColour;
            }

            this.Resize += Schedule_Resize;
            this.schedule_rooms.MouseEnter += schedule_rooms_MouseEnter;
            this.panel_rooms.Scroll += panel_rooms_Scroll;
            this.schedule_view.Scroll += schedule_view_Scroll;
            this.schedule_view.MouseWheel += schedule_view_MouseWheel;
            this.panel_rooms.MouseWheel += panel_rooms_MouseWheel;

            this.schedule_rooms.SelectedRoomChanged += schedule_rooms_SelectedRoomChanged;
            this.schedule_view.SelectedRoomChanged += schedule_view_SelectedRoomChanged;
            this.schedule_view.SelectedBookingChanged += schedule_view_SelectedBookingChanged;

            this.schedule_view.Dock = DockStyle.Fill;
        }

        public void Setup(List<Room> rooms, DateTimeOffset begins_on, DateTimeOffset ends_on, Location optional_root_location = null)
        {
            schedule_rooms.Setup(rooms);
            schedule_view.Setup(rooms, begins_on, ends_on);
        }

        /* Todo: Follow the data and ensure we can change these values to nullable versions */
        public DateTimeOffset BeginsOn { get { return schedule_view.BeginsOn; } }
        public DateTimeOffset EndsOn { get { return schedule_view.EndsOn; } }
        public IList<Data.Room> Rooms { get { return schedule_view.Rooms; } }

        public Booking SelectedBooking { get { return schedule_view.SelectedBooking; } }
        public Room SelectedRoom { get { return schedule_view.SelectedRoom; } }

        public bool TrySelectRoom(Room room)
        {
            try
            {
                _do_suspend_room_and_booking_coordination = true;
                SuspendLayout();

                var current_room = schedule_rooms.SelectedRoom;
                if (schedule_rooms.TrySelectRoom(room) && schedule_view.TrySelectRoom(room))
                {
                    return true;
                }
                else
                {
                    schedule_rooms.TrySelectRoom(current_room);
                    schedule_view.TrySelectRoom(current_room);
                    return false;
                }
            }
            finally
            {
                ResumeLayout();
                _do_suspend_room_and_booking_coordination = false;
            }
        }

        public bool TrySelectBooking(Booking booking)
        {
            try
            {
                _do_suspend_room_and_booking_coordination = true;
                SuspendLayout();

                var current_room = schedule_view.SelectedRoom;
                var current_booking = schedule_view.SelectedBooking;

                if (schedule_view.TrySelectBooking(booking) && schedule_rooms.TrySelectRoom(schedule_view.SelectedRoom))
                {
                    return true;
                }
                else
                {
                    schedule_rooms.TrySelectRoom(current_room);
                    schedule_view.TrySelectRoom(current_room);
                    schedule_view.TrySelectBooking(current_booking);
                    return false;
                }
            }
            finally
            {
                ResumeLayout();
                _do_suspend_room_and_booking_coordination = false;
            }
        }

        private void Schedule_Resize(object sender, EventArgs e)
        {
            this.split_view.Panel2MinSize = this.split_view.Width - _rooms_panel_width;
        }

        private void button_schedule_Click(object sender, EventArgs e)
        {
            this.SuspendLayout();
            this.schedule_view.Visible = true;
            this.schedule_view.Focus();
            this.SyncVerticalScrollBars(panel_rooms, panel_veiw);
            this.ResumeLayout();
        }

        private void button_map_Click(object sender, EventArgs e)
        {
            this.SuspendLayout();
            this.schedule_view.Visible = false;
            this.ResumeLayout();
        }

        private void schedule_rooms_MouseEnter(object sender, EventArgs e)
        {
            this.panel_rooms.Focus();
        }

        private void panel_rooms_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {
                schedule_view.VerticalScroll.Value = e.NewValue;
            }
        }

        private void schedule_view_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {
                panel_rooms.VerticalScroll.Value = e.NewValue;
            }
        }

        private void schedule_view_MouseWheel(object sender, MouseEventArgs e)
        {
            SyncVerticalScrollBars(schedule_view, panel_rooms);
        }

        private void panel_rooms_MouseWheel(object sender, MouseEventArgs e)
        {
            SyncVerticalScrollBars(panel_rooms, schedule_view);
        }

        private void SyncVerticalScrollBars(ScrollableControl from, ScrollableControl to)
        {
            CancellationToken token;
            var sync_now = new Action(() => {
                if (to.VerticalScroll.Maximum >= from.VerticalScroll.Value)
                {
                    to.VerticalScroll.Value = from.VerticalScroll.Value;
                }
            });

            lock (_sync_vertical_scroll_bars_locker)
            {
                if (_sync_vertical_scroll_bars_cancel_source != null)
                {
                    _sync_vertical_scroll_bars_cancel_source.Cancel();
                }

                _sync_vertical_scroll_bars_cancel_source = new CancellationTokenSource();
                token = _sync_vertical_scroll_bars_cancel_source.Token;
            }

            sync_now();

            /* Note: For some reason we need to assign the value a second time after the user stops scrolling otherwise we get a judder. */
            Task.Delay(10, token).ContinueWith((t) => {
                lock (_sync_vertical_scroll_bars_locker)
                {
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }

                    _sync_vertical_scroll_bars_cancel_source = null;
                }

                if (to.InvokeRequired)
                {
                    to.Invoke(sync_now);
                }
                else
                {
                    sync_now();
                }
            });
        }

        private void map_SelectedLocationChanged(object sender, EventArgs e)
        {
            this.OnEventHandler(SelectedLocationChanged);
        }

        private void map_SelectedRoomChanged(object sender, EventArgs e)
        {
            if (_do_suspend_room_and_booking_coordination == false)
            {
            }

            this.OnEventHandler(SelectedRoomChanged);
        }

        private void schedule_rooms_SelectedRoomChanged(object sender, EventArgs e)
        {
            if (_do_suspend_room_and_booking_coordination == false)
            {
                _do_suspend_room_and_booking_coordination = true;

                schedule_view.TrySelectRoom(schedule_rooms.SelectedRoom);

                _do_suspend_room_and_booking_coordination = false;
            }

            this.OnEventHandler(SelectedRoomChanged);
        }

        private void schedule_view_SelectedRoomChanged(object sender, EventArgs e)
        {
            if (_do_suspend_room_and_booking_coordination == false)
            {
                _do_suspend_room_and_booking_coordination = true;

                schedule_rooms.TrySelectRoom(schedule_view.SelectedRoom);

                _do_suspend_room_and_booking_coordination = false;
            }

            this.OnEventHandler(SelectedRoomChanged);
        }

        private void schedule_view_SelectedBookingChanged(object sender, EventArgs e)
        {
            this.OnEventHandler(SelectedBookingChanged);
        }
    }
}
