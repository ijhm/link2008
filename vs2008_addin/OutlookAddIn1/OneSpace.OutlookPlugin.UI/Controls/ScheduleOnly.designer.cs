﻿namespace OneSpace.OutlookPlugin.UI.Controls
{
    partial class ScheduleOnly
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScheduleOnly));
            this.split_view = new System.Windows.Forms.SplitContainer();
            this.group_rooms = new System.Windows.Forms.GroupBox();
            this.panel_rooms = new OneSpace.OutlookPlugin.UI.Controls.SPanel();
            this.schedule_rooms = new OneSpace.OutlookPlugin.UI.Controls.ScheduleRoomList();
            this.panel_rooms_header = new System.Windows.Forms.Panel();
            this.hz_rooms_tabs_border = new OneSpace.OutlookPlugin.UI.Controls.HorizontalLine();
            this.group_view = new System.Windows.Forms.GroupBox();
            this.panel_veiw = new System.Windows.Forms.Panel();
            this.schedule_view = new OneSpace.OutlookPlugin.UI.Controls.ScheduleView();
            this.panel_view_buttons = new System.Windows.Forms.Panel();
            this.hz_view_tabs_border = new OneSpace.OutlookPlugin.UI.Controls.HorizontalLine();
            ((System.ComponentModel.ISupportInitialize)(this.split_view)).BeginInit();
            this.split_view.Panel1.SuspendLayout();
            this.split_view.Panel2.SuspendLayout();
            this.split_view.SuspendLayout();
            this.group_rooms.SuspendLayout();
            this.panel_rooms.SuspendLayout();
            this.panel_rooms_header.SuspendLayout();
            this.group_view.SuspendLayout();
            this.panel_veiw.SuspendLayout();
            this.panel_view_buttons.SuspendLayout();
            this.SuspendLayout();
            // 
            // split_view
            // 
            this.split_view.Dock = System.Windows.Forms.DockStyle.Fill;
            this.split_view.Location = new System.Drawing.Point(0, 0);
            this.split_view.Margin = new System.Windows.Forms.Padding(4);
            this.split_view.Name = "split_view";
            // 
            // split_view.Panel1
            // 
            this.split_view.Panel1.Controls.Add(this.group_rooms);
            this.split_view.Panel1.Padding = new System.Windows.Forms.Padding(4);
            this.split_view.Panel1MinSize = 200;
            // 
            // split_view.Panel2
            // 
            this.split_view.Panel2.Controls.Add(this.group_view);
            this.split_view.Panel2.Padding = new System.Windows.Forms.Padding(4);
            this.split_view.Panel2MinSize = 290;
            this.split_view.Size = new System.Drawing.Size(1662, 968);
            this.split_view.SplitterDistance = 389;
            this.split_view.SplitterWidth = 3;
            this.split_view.TabIndex = 1;
            // 
            // group_rooms
            // 
            this.group_rooms.Controls.Add(this.panel_rooms);
            this.group_rooms.Controls.Add(this.panel_rooms_header);
            this.group_rooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group_rooms.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.group_rooms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.group_rooms.Location = new System.Drawing.Point(4, 4);
            this.group_rooms.Margin = new System.Windows.Forms.Padding(4);
            this.group_rooms.Name = "group_rooms";
            this.group_rooms.Padding = new System.Windows.Forms.Padding(4);
            this.group_rooms.Size = new System.Drawing.Size(381, 960);
            this.group_rooms.TabIndex = 0;
            this.group_rooms.TabStop = false;
            this.group_rooms.Text = "Rooms";
            // 
            // panel_rooms
            // 
            this.panel_rooms.AutoScroll = true;
            this.panel_rooms.BackColor = System.Drawing.Color.White;
            this.panel_rooms.Controls.Add(this.schedule_rooms);
            this.panel_rooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_rooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel_rooms.ForeColor = System.Drawing.Color.Black;
            this.panel_rooms.Location = new System.Drawing.Point(4, 85);
            this.panel_rooms.Margin = new System.Windows.Forms.Padding(4);
            this.panel_rooms.Name = "panel_rooms";
            this.panel_rooms.Size = new System.Drawing.Size(373, 871);
            this.panel_rooms.TabIndex = 1;
            // 
            // schedule_rooms
            // 
            this.schedule_rooms.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.schedule_rooms.Location = new System.Drawing.Point(0, 0);
            this.schedule_rooms.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.schedule_rooms.Name = "schedule_rooms";
            this.schedule_rooms.Size = new System.Drawing.Size(600, 450);
            this.schedule_rooms.TabIndex = 0;
            // 
            // panel_rooms_header
            // 
            this.panel_rooms_header.Controls.Add(this.hz_rooms_tabs_border);
            this.panel_rooms_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_rooms_header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel_rooms_header.ForeColor = System.Drawing.Color.Black;
            this.panel_rooms_header.Location = new System.Drawing.Point(4, 25);
            this.panel_rooms_header.Margin = new System.Windows.Forms.Padding(4);
            this.panel_rooms_header.Name = "panel_rooms_header";
            this.panel_rooms_header.Size = new System.Drawing.Size(373, 60);
            this.panel_rooms_header.TabIndex = 0;
            // 
            // hz_rooms_tabs_border
            // 
            this.hz_rooms_tabs_border.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hz_rooms_tabs_border.Location = new System.Drawing.Point(0, 58);
            this.hz_rooms_tabs_border.Margin = new System.Windows.Forms.Padding(6);
            this.hz_rooms_tabs_border.Name = "hz_rooms_tabs_border";
            this.hz_rooms_tabs_border.Size = new System.Drawing.Size(373, 3);
            this.hz_rooms_tabs_border.TabIndex = 0;
            // 
            // group_view
            // 
            this.group_view.Controls.Add(this.panel_veiw);
            this.group_view.Controls.Add(this.panel_view_buttons);
            this.group_view.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group_view.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.group_view.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.group_view.Location = new System.Drawing.Point(4, 4);
            this.group_view.Margin = new System.Windows.Forms.Padding(4);
            this.group_view.Name = "group_view";
            this.group_view.Padding = new System.Windows.Forms.Padding(4);
            this.group_view.Size = new System.Drawing.Size(1262, 960);
            this.group_view.TabIndex = 0;
            this.group_view.TabStop = false;
            this.group_view.Text = "View";
            // 
            // panel_veiw
            // 
            this.panel_veiw.AutoScroll = true;
            this.panel_veiw.Controls.Add(this.schedule_view);
            this.panel_veiw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_veiw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel_veiw.ForeColor = System.Drawing.Color.Black;
            this.panel_veiw.Location = new System.Drawing.Point(4, 85);
            this.panel_veiw.Margin = new System.Windows.Forms.Padding(4);
            this.panel_veiw.Name = "panel_veiw";
            this.panel_veiw.Size = new System.Drawing.Size(1254, 871);
            this.panel_veiw.TabIndex = 2;
            // 
            // schedule_view
            // 
            this.schedule_view.AutoScroll = true;
            this.schedule_view.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.schedule_view.Location = new System.Drawing.Point(9, 12);
            this.schedule_view.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.schedule_view.Name = "schedule_view";
            this.schedule_view.Size = new System.Drawing.Size(456, 594);
            this.schedule_view.TabIndex = 4;
            // 
            // panel_view_buttons
            // 
            this.panel_view_buttons.Controls.Add(this.hz_view_tabs_border);
            this.panel_view_buttons.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_view_buttons.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.panel_view_buttons.ForeColor = System.Drawing.Color.Black;
            this.panel_view_buttons.Location = new System.Drawing.Point(4, 25);
            this.panel_view_buttons.Margin = new System.Windows.Forms.Padding(4);
            this.panel_view_buttons.Name = "panel_view_buttons";
            this.panel_view_buttons.Size = new System.Drawing.Size(1254, 60);
            this.panel_view_buttons.TabIndex = 1;
            // 
            // hz_view_tabs_border
            // 
            this.hz_view_tabs_border.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hz_view_tabs_border.Location = new System.Drawing.Point(0, 58);
            this.hz_view_tabs_border.Margin = new System.Windows.Forms.Padding(6);
            this.hz_view_tabs_border.Name = "hz_view_tabs_border";
            this.hz_view_tabs_border.Size = new System.Drawing.Size(1254, 3);
            this.hz_view_tabs_border.TabIndex = 2;
            // 
            // ScheduleOnly
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.split_view);
            this.Name = "ScheduleOnly";
            this.Size = new System.Drawing.Size(1662, 968);
            this.split_view.Panel1.ResumeLayout(false);
            this.split_view.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.split_view)).EndInit();
            this.split_view.ResumeLayout(false);
            this.group_rooms.ResumeLayout(false);
            this.panel_rooms.ResumeLayout(false);
            this.panel_rooms_header.ResumeLayout(false);
            this.group_view.ResumeLayout(false);
            this.panel_veiw.ResumeLayout(false);
            this.panel_view_buttons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer split_view;
        private System.Windows.Forms.GroupBox group_rooms;
        private SPanel panel_rooms;
        private ScheduleRoomList schedule_rooms;
        private System.Windows.Forms.Panel panel_rooms_header;
        private HorizontalLine hz_rooms_tabs_border;
        private System.Windows.Forms.GroupBox group_view;
        private System.Windows.Forms.Panel panel_veiw;
        private ScheduleView schedule_view;
        private System.Windows.Forms.Panel panel_view_buttons;
        private HorizontalLine hz_view_tabs_border;
    }
}
