﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.UI.Data;
using System.Diagnostics;

using static OneSpace.OutlookPlugin.UI.Properties.Resources;

namespace OneSpace.OutlookPlugin.UI.Controls {
    public partial class ScheduleRoomList : UserControl {
        private class Item {
            public string Text { get; set; }
            public PointF TextPosition { get; set; }
            public Room Room { get; set; }
            public RectangleF Rectangle { get; set; }
        }

        private RectangleF _header_rows;
        private List<Item> _items = null;
        private Dictionary<Room, Item> _item_index = null;

        public event EventHandler<EventArgs> SelectedRoomChanged;
        public event EventHandler<EventArgs> MaxHeightExceeded;

        public ScheduleRoomList() {
            InitializeComponent();
        }

        public void Setup(List<Room> rooms) {
            Rooms = rooms.AsReadOnly();
            _items = new List<Item>();
            _item_index = new Dictionary<Room, Item>();

            var location_stack = new SortedDictionary<string, List<Room>>();

            using (var g = CreateGraphics()) {
                var dpm_y = g.DpiY / Schedule.MM_PER_INCH;
                var dpm_x = g.DpiX / Schedule.MM_PER_INCH;

                var hour_height = dpm_y * Schedule.HOUR_HIGHT_MM;
                var hour_width = dpm_x * Schedule.HOUR_WIDTH_MM;
                var overscroll_x = dpm_x * Schedule.OVERSCROLL_MM;
                var overscroll_y = dpm_y * Schedule.OVERSCROLL_MM;
                var widest_room_name_width = 400.0f;

                foreach (var room in Rooms) {
                    var location_path = room.GetLocationPath();
                    
                    if (location_stack.ContainsKey(location_path) == false) {
                        var location_path_size = g.MeasureString(room.Name, Schedule.FONT_LABLE);

                        location_stack.Add(location_path, new List<Room>());

                        if (location_path_size.Width > widest_room_name_width) {
                            widest_room_name_width = location_path_size.Width;
                        }
                    }

                    location_stack[location_path].Add(room);
                }

                var room_count = (Rooms.Count == 0) ? 1 : Rooms.Count;

                var height = (hour_height * (Schedule.HEADER_ROW_COUNT + room_count + location_stack.Count));

                var expected_height = (int)Math.Ceiling(height + overscroll_y);
                var expected_width = (int)Math.Ceiling(widest_room_name_width + overscroll_x);
                
                Height = expected_height;
                Width = expected_width;

                if (Height < expected_height) {
                    OnMaxHeightExceeded();
                }

                var lable_indent = dpm_x * Schedule.LABLE_INDENT_MM;
                var lable_size = g.MeasureString("ABCDEFIJKLMNOPQRSTUVWXYZ", Schedule.FONT_LABLE);
                var center_y_offset = (hour_height / 2.0f) - (lable_size.Height / 2.0f);
                var stack_y = hour_height * Schedule.HEADER_ROW_COUNT;

                _header_rows = new RectangleF(
                    x: 0.0f,
                    y: 0.0f,
                    height: stack_y,
                    width: expected_width
                );

                foreach (var location in location_stack) {
                    var room_list = location.Value;
                    room_list.Sort(new Schedule.RoomComparer());

                    _items.Add(new Item() {
                        Text = location.Key,
                        TextPosition = new PointF(
                            x: lable_indent,
                            y: stack_y + center_y_offset
                        ),
                        Room = null,
                        Rectangle = new RectangleF(
                            x: 0.0f,
                            y: stack_y,
                            height: hour_height,
                            width: expected_width
                        )
                    });

                    stack_y += hour_height;

                    foreach (var room in room_list) {
                        var item = new Item() {
                            Text = room.Name,
                            TextPosition = new PointF(
                                x: lable_indent,
                                y: stack_y + center_y_offset
                            ),
                            Room = room,
                            Rectangle = new RectangleF(
                                x: 0.0f,
                                y: stack_y,
                                height: hour_height,
                                width: expected_width
                            )
                        };

                        _items.Add(item);
                        _item_index.Add(room, item);

                        stack_y += hour_height;
                    }
                }

                if (Rooms.Count == 0) {
                    _items.Add(new Item() {
                        Text = MESSAGE_NO_MATCHING_ROOMS,
                        TextPosition = new PointF(
                            x: lable_indent,
                            y: stack_y + center_y_offset
                        ),
                        Room = null,
                        Rectangle = new RectangleF(
                            x: 0.0f,
                            y: stack_y,
                            height: hour_height,
                            width: expected_width
                        )
                    });
                }
            }

            BackgroundImage = null;
            Invalidate();
        }
        
        public IList<Data.Room> Rooms { get; set; }

        public Room SelectedRoom { get; private set; }

        public bool TrySelectRoom(Room room) {
            var is_room_in_list = (
                from r in Rooms
                where r == room
                select r
            ).Any();

            if (is_room_in_list || room == null) {
                ChangeSelectedRoom(room);
                return true;
            }
            else {
                return false;
            }
        }

        private void ChangeSelectedRoom(Room room) {
            if (SelectedRoom != room) {
                var selected_room = SelectedRoom;

                if(selected_room != null) {
                    if(_item_index.ContainsKey(selected_room)) {
                        var current_item = _item_index[selected_room];
                        this.Invalidate(current_item.Rectangle, Schedule.INVLAIDATION_PADDING);
                    }
                    else {
                        this.Invalidate();
                    }
                }

                SelectedRoom = room;
                if(room != null) {
                    var new_item = _item_index[room];
                    this.Invalidate(new_item.Rectangle, Schedule.INVLAIDATION_PADDING);
                }

                OnSelectedRoomChanged();
            }
        }

        protected override void OnPaint(PaintEventArgs e) {
            if(Rooms != null) {
                var g = e.Graphics;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

                var clip_rec = (RectangleF)e.ClipRectangle;
                g.FillRectangle(Schedule.BRUSH_BACK, clip_rec);

                if (clip_rec.IntersectsWith(_header_rows)) {
                    g.FillRectangle(Schedule.BRUSH_ROOM_BACK, _header_rows);
                }

                var items = (
                    from i in _items
                    where i.Rectangle.IntersectsWith(clip_rec)
                    select i
                ).ToList();

                foreach(var item in items) {
                    var is_room = (item.Room != null);

                    var background_brush = is_room ? Schedule.BRUSH_ROOM_BACK : Schedule.BRUSH_HEADING;
                    var text_brush = is_room ? Schedule.BRUSH_TEXT : Schedule.BRUSH_HEADING_TEXT;
                    var font = is_room ? Schedule.FONT_LABLE : Schedule.FONT_HEADING;

                    g.FillRectangle(background_brush, item.Rectangle);

                    if(item.Room != null && item.Room == SelectedRoom) {
                        var inner_border = new RectangleF(
                            x: item.Rectangle.X + 0.5f,
                            y: item.Rectangle.Y + 0.5f,
                            height: item.Rectangle.Height - 1.0f,
                            width: item.Rectangle.Width - 1.0f
                        );
                        g.DrawRectangle(Pens.Red, inner_border);
                        font = Schedule.FONT_LABLE_SELECTED;
                    }

                    g.DrawString(item.Text, font, text_brush, item.TextPosition);
                }
            }

            base.OnPaint(e);
        }

        protected override void OnClick(EventArgs e) {
            /* Note: In MS infinate wisdom they didn't think it would be useful to
             *       have an event handler with correctly typed variables.
             *       So here we cast to the correct variable and pass it to
             *       what should have been the event handler.
             * */
            var me = e as MouseEventArgs;
            if (me != null) {
                OnClick(me);
            }

            base.OnClick(e);
        }

        private void OnClick(MouseEventArgs e) {
            if(_items != null) {
                var item = (
                    from i in _items
                    where i.Rectangle.Contains(e.Location)
                    select i
                ).FirstOrDefault();

                ChangeSelectedRoom(item?.Room);
            }
        }

        private void OnSelectedRoomChanged() {
            this.OnEventHandler(SelectedRoomChanged);
        }

        private void OnMaxHeightExceeded() {
            Trace.TraceWarning("The Max Height of the control was exceeded and will be truncated.");
            this.OnEventHandler(MaxHeightExceeded);
        }
    }
}
