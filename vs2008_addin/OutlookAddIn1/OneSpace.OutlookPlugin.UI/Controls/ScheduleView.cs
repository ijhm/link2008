﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using OneSpace.OutlookPlugin.UI.Data;
using System.Drawing.Drawing2D;

using OneSpace.OutlookPlugin.UI.Properties.Resources;

namespace OneSpace.OutlookPlugin.UI.Controls {
    /* Todo: Add touch support */
    /* Ref: https://msdn.microsoft.com/en-us/library/windows/desktop/dd940543(v=vs.85).aspx */
    /* Ref: https://msdn.microsoft.com/en-us/library/windows/desktop/dd940542(v=vs.85).aspx */
    /* Ref: https://msdn.microsoft.com/en-us/library/windows/desktop/dd353242(v=vs.85).aspx */

    public partial class ScheduleView : UserControl {
        private ScheduleViewBase _report = null;

        private int _scroll_x = 0;
        private int _scroll_y = 0;

        public event EventHandler<EventArgs> SelectedRoomChanged;
        public event EventHandler<EventArgs> SelectedBookingChanged;

        public ScheduleView() {
            this.SetStyle(
                System.Windows.Forms.ControlStyles.UserPaint |
                System.Windows.Forms.ControlStyles.AllPaintingInWmPaint |
                System.Windows.Forms.ControlStyles.OptimizedDoubleBuffer,
                true);
            InitializeComponent();
            ParentChanged += Schedule_ParentChanged;
        }

        private void Schedule_ParentChanged(object sender, EventArgs e) {
            if(ParentForm != null) {
                ParentForm.Activated += ParentForm_Activated;
            }
        }

        private void ParentForm_Activated(object sender, EventArgs e) {
            AutoScrollPosition = new Point(_scroll_x, _scroll_y);
        }

        protected override void OnMouseWheel(MouseEventArgs e) {
            if (this.VScroll && (Control.ModifierKeys & Keys.Shift) == Keys.Shift) {
                this.VScroll = false;
                base.OnMouseWheel(e);
                _scroll_y = this.VerticalScroll.Value;
                this.VScroll = true;
            }
            else {
                base.OnMouseWheel(e);
                _scroll_x = this.HorizontalScroll.Value;
            }
        }

        protected override void OnScroll(ScrollEventArgs se) {

            switch (se.ScrollOrientation) {
                case ScrollOrientation.HorizontalScroll:
                    _scroll_x = se.NewValue;
                    break;
                case ScrollOrientation.VerticalScroll:
                    _scroll_y = se.NewValue;
                    break;
            }

            base.OnScroll(se);
        }

        public void SetBackground()
        {
            BackgroundImage = Branding.Resource.Banner;
        }

        protected override void OnLayout(LayoutEventArgs e) {
            SuspendLayout();
            base.OnLayout(e);
            ResumeLayout();
        }

        public void Setup(List<Room> rooms, DateTimeOffset begins_on, DateTimeOffset ends_on) {
            if(_report != null && Controls.Contains(_report)) {
                Controls.Remove(_report);
                _report.Dispose();
            }

            _report = new ScheduleViewBase(rooms, begins_on, ends_on);
            _report.Name = "_report";
            _report.Location = new Point(0, 0);
            _report.SelectedRoomChanged += Report_SelectedRoomChanged;
            _report.SelectedBookingChanged += Report_SelectedBookingChanged;

            BackgroundImage = null;
            Controls.Add(_report);

        }

        public void HighlightSection(DateTimeOffset highlight_begins_on, DateTimeOffset highlight_ends_on) {
            if(_report == null) {
                throw new Exception(string.Format(ERROR_CALL_SCHEDULE_VIEW_SETUP_FIRST, nameof(HighlightSection)));
            }

            var highlight_section_x = (int)_report.HighlightSection(highlight_begins_on, highlight_ends_on);
            var scroll_to_x = highlight_section_x - (Width / 4);

            if (scroll_to_x < 0) {
                scroll_to_x = 0;
            }

            AutoScrollPosition = new Point(scroll_to_x, this.VerticalScroll.Value);
        }

        public DateTimeOffset BeginsOn { get { return _report.BeginsOn; } }
        public DateTimeOffset EndsOn { get { return _report.EndsOn; } }
        public IList<Data.Room> Rooms { get { return _report.Rooms; } }

        public Booking SelectedBooking 
        { 
            get 
            { 
                if (_report==null)
                    return null;
                return _report.SelectedBooking; 
            } 
        }

        public Room SelectedRoom 
        { 
            get 
            {
                if (_report == null)
                    return null;
                return _report.SelectedRoom; 
            } 
        }

        public bool TrySelectRoom(Room room) {
            return _report.TrySelectRoom(room);
        }

        public bool TrySelectBooking(Booking booking) {
            return _report.TrySelectBooking(booking);
        }
        
        private void Report_SelectedRoomChanged(object sender, EventArgs e) {
            this.OnEventHandler(SelectedRoomChanged);
        }

        private void Report_SelectedBookingChanged(object sender, EventArgs e) {
            this.OnEventHandler(SelectedBookingChanged);
        }
    }
}
