﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using OneSpace.OutlookPlugin.UI.Data;
using System.Drawing.Drawing2D;

using OneSpace.OutlookPlugin.UI.Properties.Resources;

namespace OneSpace.OutlookPlugin.UI.Controls {
    public partial class ScheduleViewBase : UserControl {
        private class LocationBooking {
            public RectangleF Location { get; set; }
            public Booking Booking { get; set; }
            public bool IsLinkedLocation { get; set; }
            public bool IsLinkedRoomBooking { get; set; }
            public bool IsBufferTime { get; set; }
        }

        private float _dpm_y = Schedule.DEFAULT_DPM;
        private float _dpm_x = Schedule.DEFAULT_DPM;
        private RectangleF _report_rectangle;
        private SortedDictionary<string, List<Room>> _location_stack = null;
        private SortedDictionary<string, RectangleF> _location_stack_rectangels = null;
        private Dictionary<DateTime, float> _date_boundaries = null;
        private List<LocationBooking> _bookings = null;
        private RectangleF? _highlight_section = null;

        public event EventHandler<EventArgs> SelectedRoomChanged;
        public event EventHandler<EventArgs> SelectedBookingChanged;
        public event EventHandler<EventArgs> MaxWidthExceeded;
        public event EventHandler<EventArgs> MaxHeightExceeded;

        public ScheduleViewBase(List<Room> rooms, DateTimeOffset begins_on, DateTimeOffset ends_on) {
            SuspendLayout();
            InitializeComponent();
            Initialise(rooms, begins_on, ends_on);
            ResumeLayout();
        }

        private void Initialise(List<Room> rooms, DateTimeOffset begins_on, DateTimeOffset ends_on) {
            ParentChanged += ScheduleBase_ParentChanged;

            var begins_on_trim = new TimeSpan(begins_on.Ticks % TimeSpan.TicksPerHour);
            var ends_on_trim = new TimeSpan(ends_on.Ticks % TimeSpan.TicksPerHour);

            if (begins_on_trim > TimeSpan.Zero) {
                begins_on = begins_on.Subtract(begins_on_trim);
            }

            if (ends_on_trim > TimeSpan.Zero) {
                ends_on = ends_on.Subtract(ends_on_trim).AddHours(1);
            }

            Rooms = rooms.AsReadOnly();
            BeginsOn = begins_on;
            EndsOn = ends_on;

            BuildLocationStack();
        }

        private void BuildLocationStack() {
            _location_stack = new SortedDictionary<string, List<Data.Room>>();

            foreach (var room in Rooms) {
                var location_path = room.GetLocationPath();

                if (_location_stack.ContainsKey(location_path) == false) {
                    _location_stack.Add(location_path, new List<Data.Room>());
                }

                _location_stack[location_path].Add(room);
            }
        }

        private void ScheduleBase_ParentChanged(object sender, EventArgs e) {
            using (var g = CreateGraphics()) {
                _dpm_y = g.DpiY / Schedule.MM_PER_INCH;
                _dpm_x = g.DpiX / Schedule.MM_PER_INCH;

                var overscroll_x = _dpm_x * Schedule.OVERSCROLL_MM;
                var overscroll_y = _dpm_y * Schedule.OVERSCROLL_MM;
                var total_hours = (float)(EndsOn - BeginsOn).TotalHours;
                var height = (_dpm_y * Schedule.HOUR_HIGHT_MM * (Schedule.HEADER_ROW_COUNT + Rooms.Count + _location_stack.Count));
                var width = (_dpm_x * Schedule.HOUR_WIDTH_MM * total_hours);

                _report_rectangle = new RectangleF(0, 0, width, height);

                var expected_height = (int)Math.Ceiling(_report_rectangle.Height + overscroll_y);
                var expected_width = (int)Math.Ceiling(_report_rectangle.Width + overscroll_x);

                Height = expected_height;
                Width = expected_width;

                if(Height < expected_height) {
                    OnMaxHeightExceeded();
                }

                if (Width < expected_width) {
                    OnMaxWidthExceeded();
                }
            }

            BuildDateBoundaries();
            BuildLocationStackRectangels();
            BuildBookings();
        }

        private void BuildDateBoundaries() {
            _date_boundaries = new Dictionary<DateTime, float>();
            _date_boundaries.Add(BeginsOn.Date, 0.0f);

            var hour_width = _dpm_x * Schedule.HOUR_WIDTH_MM;
            var first_day_width = hour_width * (float)(Schedule.ONE_DAY - BeginsOn.TimeOfDay).TotalHours;
            var full_day_width = hour_width * (float)Schedule.ONE_DAY.TotalHours;

            var begins_on = BeginsOn.Date;
            var ends_on = EndsOn.Date;
            var working_day = BeginsOn.Date.Add(Schedule.ONE_DAY);
            var working_width = first_day_width;

            while (working_day <= ends_on && working_width < _report_rectangle.Width) {
                _date_boundaries.Add(working_day, working_width);
                working_day = working_day.Add(Schedule.ONE_DAY);
                working_width += full_day_width;
            }
        }

        private void BuildLocationStackRectangels() {
            _location_stack_rectangels = new SortedDictionary<string, RectangleF>();

            var hour_height = _dpm_y * Schedule.HOUR_HIGHT_MM;
            var stack_y = hour_height * Schedule.HEADER_ROW_COUNT;

            foreach (var location_stack in _location_stack) {
                /* Note: I'm putting the sort here becasue i didn't want to loop over the _location_stack twice */
                var room_list = location_stack.Value;
                room_list.Sort(new Schedule.RoomComparer());

                _location_stack_rectangels.Add(
                    location_stack.Key,
                    new RectangleF(0.0f,stack_y,hour_height,_report_rectangle.Width
                    )
                );

                stack_y += hour_height * (location_stack.Value.Count + 1);
            }
        }

        private void BuildBookings() 
        {
            _bookings = new List<LocationBooking>();

            var hour_height = _dpm_y * Schedule.HOUR_HIGHT_MM;
            var hour_width = _dpm_x * Schedule.HOUR_WIDTH_MM;

            var top_of_row = hour_height * (Schedule.HEADER_ROW_COUNT - 1);
            var booking_offset_y_px = _dpm_y * Schedule.BOOKING_OFFSET_MM;
            var booking_height_px = _dpm_y * Schedule.BOOKING_HIGHT_MM;

            foreach (var room_list in _location_stack.Values) 
            {
                top_of_row += hour_height;
                foreach (var room in room_list) 
                {
                    top_of_row += hour_height;
                    foreach (var booking in room.Bookings) 
                    {
                        var begins_on = (booking.BeginsOn >= BeginsOn) ? booking.BeginsOn : BeginsOn;
                        var ends_on = (booking.EndsOn <= EndsOn) ? booking.EndsOn : EndsOn;

                        /* Note: The '+ 1.0f' and '- 2.0f' are to ensure the bookings have 2mm between them
                         *       so the user can see they are different.  Otherwise back to back bookings look
                         *       like 1 continus booking.
                         * */

                        var hours_from_start = (float)(begins_on - BeginsOn).TotalHours;
                        var booking_x = (hour_width * hours_from_start) + 1.0f;
                        var booking_y = top_of_row + booking_offset_y_px;
                        
                        var booking_hours = (float)(ends_on - begins_on).TotalHours;
                        var booking_height = booking_height_px;
                        var booking_width = (hour_width * booking_hours) - 2.0f;

                        var booking_rec = new RectangleF(booking_x,booking_y,booking_height,booking_width);

                        _bookings.Add(
                            new LocationBooking() 
                            {
                                Location = booking_rec,
                                Booking = (booking.Room == room) ? booking : null,
                                IsLinkedLocation = IsLinkedLocation(room),
                                IsLinkedRoomBooking = booking.IsPartOfALinkedBooking,
                                IsBufferTime = booking.IsBufferTime
                        });
                    }
                }
            }
        }

        private bool IsLinkedLocation(Room room)
        {
            if (room==null)
                return false;
            if (room.LinkedRooms==null)
                return false;
            return room.LinkedRooms.Count>0;
        }

        public float HighlightSection(DateTimeOffset highlight_begins_on, DateTimeOffset highlight_ends_on) {

            if(highlight_begins_on >= highlight_ends_on) {
                throw new ArgumentOutOfRangeException(
                    string.Format(
                        ERROR_HIGHLIGHT_SECTION_BEGINS_ON_MUST_BE_BEFORE_ENDS_ON,
                        nameof(highlight_begins_on),
                        nameof(highlight_ends_on)
                    )
                );
            }

            if(highlight_begins_on < BeginsOn || highlight_begins_on > EndsOn) {
                throw new ArgumentOutOfRangeException(ERROR_HIGHLIGHT_SECTION_VALUES_MUST_BE_WITHIN_SCEDULE_SETUP_VALUES);
            }

            if (highlight_ends_on < BeginsOn || highlight_ends_on > EndsOn) {
                throw new ArgumentOutOfRangeException(ERROR_HIGHLIGHT_SECTION_VALUES_MUST_BE_WITHIN_SCEDULE_SETUP_VALUES);
            }

            var hour_width = _dpm_x * Schedule.HOUR_WIDTH_MM;
            var hour_height = _dpm_y * Schedule.HOUR_HIGHT_MM;
            var highlight_section_y = hour_height * Schedule.HEADER_ROW_COUNT;

            var highlight_section_x = hour_width * (float)(highlight_begins_on - BeginsOn).TotalHours;
            var highlight_section_width = hour_width * (float)(highlight_ends_on - highlight_begins_on).TotalHours;
            var highlight_section_height = _report_rectangle.Height - highlight_section_y;

            _highlight_section = new RectangleF(
                highlight_section_x,
                highlight_section_y,
                highlight_section_width,
                highlight_section_height
            );

            return highlight_section_x;
        }

        public DateTimeOffset BeginsOn { get; private set; }
        public DateTimeOffset EndsOn { get; private set; }
        public IList<Data.Room> Rooms { get; private set; }

        private LocationBooking SelectedLocationBooking { get; set; }

        public Booking SelectedBooking 
        { 
            get 
            { 
                if (SelectedBooking==null)
                    return null;
                return SelectedLocationBooking.Booking; 
            } 
        }
        public Room SelectedRoom { get; private set; }
        
        public bool TrySelectRoom(Room room) {
            var is_room_in_list = (
                from r in Rooms
                where r == room
                select r
            ).Any();

            if (is_room_in_list || room == null) {
                ChangeSelectedRoom(room);
                return true;
            }
            else {
                return false;
            }
        }

        public bool TrySelectBooking(Booking booking) {
            var location_booking = (
                from b in _bookings
                where b.Booking == booking
                select b
            ).FirstOrDefault();

            if(location_booking != null || booking == null) {
                ChangeSelectedLocationBooking(location_booking);
                return true;
            }
            else {
                return false;
            }
        }

        private void ChangeSelectedRoom(Room room) {
            var current_room = SelectedRoom;

            if (current_room != room) {
                var current_location_booking = SelectedLocationBooking;
                var has_booking_changed = (current_location_booking != null);

                SelectedLocationBooking = null;
                SelectedRoom = room;

                OnSelectedRoomChanged();

                if (has_booking_changed) {
                    this.Invalidate(current_location_booking.Location, Schedule.INVLAIDATION_PADDING);
                    OnSelectedBookingChanged();
                }
            }
        }

        private void ChangeSelectedLocationBooking(LocationBooking location_booking) {
            var current_location_booking = SelectedLocationBooking;

            if (current_location_booking != location_booking) {
                if (current_location_booking != null) {
                    this.Invalidate(current_location_booking.Location, Schedule.INVLAIDATION_PADDING);
                }

                if (location_booking != null) {
                    this.Invalidate(location_booking.Location, Schedule.INVLAIDATION_PADDING);
                }

                var has_selected_room_changed = (SelectedRoom != fRoom(location_booking));
                SelectedLocationBooking = location_booking;

                if (has_selected_room_changed) 
                {
                    SelectedRoom = fRoom(location_booking);
                    OnSelectedRoomChanged();
                }

                OnSelectedBookingChanged();
            }

        }
        
        private Room fRoom(LocationBooking lb)
        {
            if (lb==null)
                return null;
            if (lb.Booking==null)
                return null;
            return lb.Booking.Room;
        }

        protected override void OnPaint(PaintEventArgs e) {
            var g = e.Graphics;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

            var clip_rec = (RectangleF)e.ClipRectangle;
            g.FillRectangle(Schedule.BRUSH_BACK, clip_rec);

            var hour_height = _dpm_y * Schedule.HOUR_HIGHT_MM;
            var hour_width = _dpm_x * Schedule.HOUR_WIDTH_MM;

            /* Todo: Refactor regions into discreet methods */

            #region [ GRID_LINES ]

            /* Note: This is here to ensure we overdraw to prevent missing elements */
            var grid_line_rec = new RectangleF(
                (float)(Math.Floor(clip_rec.X / hour_width) * hour_width),
                (float)(Math.Floor(clip_rec.Y / hour_height) * hour_height),
                (float)(Math.Ceiling(clip_rec.Height / hour_height) * hour_height) + hour_height,
                (float)(Math.Ceiling(clip_rec.Width / hour_width) * hour_width) + hour_width
            );

            if (grid_line_rec.Right > _report_rectangle.Right) {
                var grid_line_overflow = grid_line_rec.Right - _report_rectangle.Right;
                grid_line_rec.Width -= grid_line_overflow;
            }

            if(grid_line_rec.Y == 0.0f) {
                grid_line_rec.Y = hour_height;

                g.DrawLine(
                    Schedule.PEN_GRID_LINE,
                    new PointF(grid_line_rec.Left, hour_height),
                    new PointF(grid_line_rec.Right, hour_height)
                );
            }

            if (grid_line_rec.Bottom > _report_rectangle.Bottom) {
                var bottom_diff = grid_line_rec.Bottom - _report_rectangle.Bottom;
                grid_line_rec.Height -= bottom_diff;
            }

            var working_line_v = grid_line_rec.Left;
            while (working_line_v <= grid_line_rec.Right) {
                g.DrawLine(
                    Schedule.PEN_GRID_LINE,
                    new PointF(working_line_v, grid_line_rec.Top),
                    new PointF(working_line_v, grid_line_rec.Bottom)
                );
                working_line_v += hour_width;
            }

            working_line_v -= hour_width;
            g.DrawLine(
                Schedule.PEN_GRID_LINE,
                new PointF(working_line_v, 0.0f),
                new PointF(working_line_v, hour_height)
            );

            var working_line_h = grid_line_rec.Top + hour_height;
            var grid_line_bottom = (float)Math.Ceiling(grid_line_rec.Bottom);
            while (working_line_h <= grid_line_bottom) {
                g.DrawLine(
                    Schedule.PEN_GRID_LINE,
                    new PointF(grid_line_rec.Left, working_line_h),
                    new PointF(_report_rectangle.Right, working_line_h)
                );
                working_line_h += hour_height;
            }

            #endregion

            #region [ HighlightSection ]

            if (_highlight_section!=null && _highlight_section.IntersectsWith(clip_rec) == true) 
            {
                g.FillRectangle(Schedule.BRUSH_HIGHLIGHT_SECTION, _highlight_section.Value);
            }

            #endregion

            #region [ HEADINGS ]

            var date_offset = _dpm_x * Schedule.DATE_HEADING_TEXT_OFFSET_MM;
            var date_font_size = g.MeasureString("ABCDEFIJKLMNOPQRSTUVWXYZ", Schedule.FONT_HEADING);
            var v_center_text_in_row = (hour_height / 2.0f) - (date_font_size.Height / 2.0f);

            if (clip_rec.Top <= hour_height) {
                var date_row_rec = new RectangleF(
                    clip_rec.X,
                    0.0f,
                    hour_height,
                    clip_rec.Width
                );

                if(date_row_rec.Right > _report_rectangle.Right) {
                    var date_row_overflow = date_row_rec.Right - _report_rectangle.Right;
                    date_row_rec.Width -= date_row_overflow;
                }

                g.FillRectangle(Schedule.BRUSH_HEADING, date_row_rec);

                const int DATE_TEXT_CELLS = 4;

                var left = grid_line_rec.Left;
                var right = grid_line_rec.Right + (hour_width * DATE_TEXT_CELLS);

                var boundaries_and_dates = (
                    from b in _date_boundaries
                    where b.Value >= left &&
                          b.Value <= right
                    select b
                ).ToList();

                foreach (var boundary_and_date in boundaries_and_dates) {
                    g.DrawLine(
                        Schedule.PEN_GRID_LINE,
                        new PointF(boundary_and_date.Value, 0.0f),
                        new PointF(boundary_and_date.Value, hour_height)
                    );

                    g.DrawString(
                        boundary_and_date.Key.ToString(Schedule.FORMAT_DATE),
                        Schedule.FONT_HEADING,
                        Schedule.BRUSH_HEADING_TEXT,
                        new PointF(
                            boundary_and_date.Value + date_offset,
                            v_center_text_in_row
                        )
                    );
                }
            }

            const int HOUR_ROW = 2;

            var hour_top = hour_height * (HOUR_ROW - 1);
            var hour_bottom = hour_height * HOUR_ROW;

            if (grid_line_rec.Top <= hour_top && grid_line_rec.Bottom >= hour_bottom) {
                var hours_from_begins_on = (int)Math.Floor(grid_line_rec.Left / hour_width);
                var hours_to_render = (int)Math.Ceiling(grid_line_rec.Width / hour_width);

                var working_datetime = BeginsOn.AddHours(hours_from_begins_on);
                for(var i = 0; i < hours_to_render; i++) {
                    var hour_string = "{working_datetime.Hour:00}";
                    var hour_string_size = g.MeasureString(hour_string, Schedule.FONT_HEADING);
                    var h_center_text_in_hour = (hour_width / 2.0f) - (hour_string_size.Width / 2.0f);
                    var x = hour_width * (hours_from_begins_on + i) + h_center_text_in_hour;

                    g.DrawString(
                        hour_string,
                        Schedule.FONT_HEADING,
                        Schedule.BRUSH_TEXT,
                        new PointF(
                            x,
                            hour_height + v_center_text_in_row
                        )
                    );

                    working_datetime = working_datetime.AddHours(1);
                }
            }

            var stack_heading_recs = (
                from l in _location_stack_rectangels
                where l.Value.IntersectsWith(clip_rec)
                select l.Value
            );

            foreach (var stack_rec in stack_heading_recs) {
                g.FillRectangle(Schedule.BRUSH_HEADING, stack_rec);
            }

            #endregion

            #region [ BOOKINGS ]

            var bookings = (
                from b in _bookings
                where b.Location.IntersectsWith(clip_rec) == true
                select b
            ).ToList();

            foreach (var booking in bookings) {
                var brush_booking = Schedule.BRUSH_BOOKING;

                if (SelectedLocationBooking == booking) {
                    brush_booking = Schedule.BRUSH_BOOKING_SELECTED;
                }

                if (booking.IsLinkedLocation != booking.IsLinkedRoomBooking) {
                    brush_booking = Schedule.BRUSH_BOOKING_LINK_LOCATION;
                }

                if (booking.IsBufferTime)
                {
                    brush_booking = Schedule.BRUSH_BUFFER;
                }

                g.FillRectangle(brush_booking, booking.Location);
            }

            #endregion

            base.OnPaint(e);
        }

        protected override void OnPaintBackground(PaintEventArgs e) {
            /* Note: We don't want the background to be painted then in
             *       the OnPaint paint over it, that will casue a flicker.
             *       So we do nothing here.
             *  */
        }

        protected override void OnMouseEnter(EventArgs e) {
            Focus();
            base.OnMouseEnter(e);
        }

        protected override void OnClick(EventArgs e) {
            /* Note: In MS infinate wisdom they didn't think it would be useful to
             *       have an event handler with correctly typed variables.
             *       So here we cast to the correct variable and pass it to
             *       what should have been the event handler.
             * */
            var me = e as MouseEventArgs;
            if (me != null) {
                OnClick(me);
            }

            base.OnClick(e);
        }

        private void OnClick(MouseEventArgs e) {
            const bool T = true;
            const bool F = false;
            
            var selected_location_booking = (
                from b in _bookings
                where b.Location.Contains(e.Location) && (
                        (b.IsLinkedLocation == T && b.IsLinkedRoomBooking == T) ||
                        (b.IsLinkedLocation == F && b.IsLinkedRoomBooking == F)
                      )
                select b
            ).FirstOrDefault();

            ChangeSelectedLocationBooking(selected_location_booking);
        }
        
        private void OnSelectedRoomChanged() {
            this.OnEventHandler(SelectedRoomChanged);
        }

        private void OnSelectedBookingChanged() {
            this.OnEventHandler(SelectedBookingChanged);
        }

        private void OnMaxWidthExceeded() {
            Trace.TraceWarning("The Max Width of the control was exceeded and will be truncated.");
            this.OnEventHandler(MaxWidthExceeded);
        }

        private void OnMaxHeightExceeded() {
            Trace.TraceWarning("The Max Height of the control was exceeded and will be truncated.");
            this.OnEventHandler(MaxHeightExceeded);
        }
    }
}
