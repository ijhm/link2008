﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OneSpace.OutlookPlugin.UI.Controls {
    public class TimePicker : System.Windows.Forms.ComboBox {
        private static readonly TimeSpan MAX_TIME = TimeSpan.FromHours(24);

        private TimeSpan _selected_time;

        public event EventHandler<TimePickerValidateTimeChangeEventArgs> ValidateTimeChange;
        public event EventHandler<TimePickerTimeChangeEventArgs> TimeChanged;

        public TimePicker() {
            InitializeComponent();
        }

        private void InitializeComponent() {
            /* Note: This method must be called InitializeComponent or the VS Desiger will copy the following
             *       code into the designer class for any form or control that uses this control.
             * */

            this.SuspendLayout();

            /* Todo: Determine if this works for languages like Simplified Chinese */
            this.Items.AddRange(new object[] {
                "00:00",
                "00:30",
                "01:00",
                "01:30",
                "02:00",
                "02:30",
                "03:00",
                "03:30",
                "04:00",
                "04:30",
                "05:00",
                "05:30",
                "06:00",
                "06:30",
                "07:00",
                "07:30",
                "08:00",
                "08:30",
                "09:00",
                "09:30",
                "10:00",
                "10:30",
                "11:00",
                "11:30",
                "12:00",
                "12:30",
                "13:00",
                "13:30",
                "14:00",
                "14:30",
                "15:00",
                "15:30",
                "16:00",
                "16:30",
                "17:00",
                "17:30",
                "18:00",
                "18:30",
                "19:00",
                "19:30",
                "20:00",
                "20:30",
                "21:00",
                "21:30",
                "22:00",
                "22:30",
                "23:00",
                "23:30"
            });

            this.SelectedIndex = 0;

            this.ResumeLayout();
        }

        protected override void OnLostFocus(EventArgs e) {
            base.OnLostFocus(e);

            TimeSpan result;

            if (TimeSpan.TryParse(Text, out result)) {
                SelectedTime = result;
            }
            else {
                SelectedTime = TimeSpan.Zero;
            }
        }
        
        private TimeSpan OnValidateTimeChange(TimeSpan new_value) {
            var event_args = new TimePickerValidateTimeChangeEventArgs();
            ((ITimePickerTimeChangeOldTime)event_args).OldTime = _selected_time;

            /* Note: Ensure the time value is never 24:00 or above */
            if (new_value >= MAX_TIME) {
                new_value = _selected_time;
            }

            event_args.NewTime = new_value;

            this.OnEventHandler(ValidateTimeChange, event_args);

            /* Note: Ensure the time value is never 24:00 or above */
            if (event_args.NewTime >= MAX_TIME) {
                event_args.NewTime = _selected_time;
            }

            return event_args.NewTime;
        }

        public TimeSpan SelectedTime {
            get {
                return _selected_time;
            }
            set {
                var has_time_changed = false;
                var old_time = _selected_time;
                var new_time = OnValidateTimeChange(value);

                if (new_time == _selected_time) {
                    /* DO NOTHING - NO CHANGE */
                }
                else if (new_time >= MAX_TIME || new_time < TimeSpan.Zero) {
                    /* DO NOTHING - OUTSIDE OF VAID RANGE */
                }
                else {
                    _selected_time = new TimeSpan(new_time.Hours, new_time.Minutes, 0);
                    has_time_changed = true;
                }

                Text = _selected_time.ToString("hh\\:mm");

                if (has_time_changed) {
                    OnTimeChanged(old_time);
                }
            }
        }

        private void OnTimeChanged(TimeSpan old_time) {
            var event_args = new TimePickerTimeChangeEventArgs();
            ((ITimePickerTimeChangeOldTime)event_args).OldTime = old_time;

            this.OnEventHandler(TimeChanged, event_args);
        }

        private interface ITimePickerTimeChangeOldTime {
            TimeSpan OldTime { set; }
        }

        public class TimePickerTimeChangeEventArgs : EventArgs, ITimePickerTimeChangeOldTime {
            private TimeSpan _old_time;

            TimeSpan ITimePickerTimeChangeOldTime.OldTime { set { _old_time = value; } }
            public TimeSpan OldTime;
        }

        public class TimePickerValidateTimeChangeEventArgs : TimePickerTimeChangeEventArgs {
            /// <summary>
            /// The time this control will represent.  Should this value not be suitable change it either to OldTime or another suitable value.
            /// </summary>
            public TimeSpan NewTime { get; set; }
        }
    }
}
