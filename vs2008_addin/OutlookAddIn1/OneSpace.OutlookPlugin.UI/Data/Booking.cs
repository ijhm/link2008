﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.UI.Data {
    public class Booking {
        public Room Room { get; set; }
        public DateTimeOffset BeginsOn { get; set; }
        public DateTimeOffset EndsOn { get; set; }
        public object ReferenceItem { get; set; }
        public bool IsPartOfALinkedBooking { get; set; }
        public bool IsBufferTime { get; set; }

    }
}
