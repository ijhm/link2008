﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.UI.Data {
    public class Location {
        private Location _parent = null;

        public string Name { get; set; }

        public Location Parent {
            get { return _parent; }
            set {
                if(value != _parent) {
                    if(_parent != null) {
                        _parent.Children.Remove(this);
                    }

                    _parent = value;
                    _parent.Children.Add(this);
                }
            }
        }

        public Image Layout { get; set; }
        public List<Location> Children { get; set; }
        public GraphicsPath Path { get; set; }
        public object ReferenceItem { get; set; }
    }
}
