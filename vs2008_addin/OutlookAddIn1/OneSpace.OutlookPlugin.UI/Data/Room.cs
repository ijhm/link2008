﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.UI.Data {
    public class Room {
        private readonly static TimeSpan ONE_DAY = TimeSpan.FromDays(1);

        public string Name { get; set; }
        public Location Parent { get; set; }
        public List<Booking> Bookings { get; set; }
        public GraphicsPath Path { get; set; }
        public object ReferenceItem { get; set; }

        /// <summary>
        /// If you set the LinkedRooms list, also set the Path to null
        /// </summary>
        public List<Room> LinkedRooms { get; set; }

        public string GetLocationPath() {
            var parent = Parent;
            var parent_names = new List<string>();

            while (parent != null) {
                if(string.IsNullOrWhiteSpace(parent.Name) == false) {
                    parent_names.Add(parent.Name);
                }
                parent = parent.Parent;
            }

            parent_names.Reverse();

            return string.Join(" / ", parent_names);
        }

        public List<Booking> GetFreeSlots(DateTimeOffset begins_on, DateTimeOffset ends_on) {
            var free_slots = new List<Booking>();

            var working_date_time = begins_on;
            var bookings = Bookings.OrderBy(e => e.BeginsOn).ToList();

            var is_terminating_booking_required = !(
                from b in bookings
                where b.EndsOn > ends_on &&
                      b.BeginsOn < ends_on
                select b
            ).Any();

            if (is_terminating_booking_required) {
                bookings.Add(new Booking() {
                    BeginsOn = ends_on,
                    EndsOn = ends_on
                });
            }

            foreach (var booking in bookings) {
                var slot_begins_on = working_date_time;
                var slot_ends_on = booking.BeginsOn;

                if (slot_begins_on < booking.BeginsOn) {
                    var begin_date = slot_begins_on.Date;
                    var end_date = slot_ends_on.Date;

                    if (begin_date != end_date) {
                        DateTimeOffset working_date = begin_date;
                        var exit_loop_date = (ends_on < end_date) ? ends_on : end_date;

                        do {
                            working_date += ONE_DAY;

                            if(working_date > ends_on) {
                                working_date = ends_on;
                            }

                            free_slots.Add(new Booking() {
                                BeginsOn = slot_begins_on,
                                EndsOn = working_date
                            });

                            slot_begins_on = working_date;
                        } while (working_date < exit_loop_date);
                    }

                    if (slot_begins_on < ends_on) {
                        if (slot_ends_on > ends_on) {
                            slot_ends_on = ends_on;
                        }

                        free_slots.Add(new Booking() {
                            BeginsOn = slot_begins_on,
                            EndsOn = slot_ends_on
                        });
                    }
                }

                working_date_time = booking.EndsOn;

                if(working_date_time >= ends_on) {
                    break;
                }
            }
            
            return free_slots;
        }

        public bool IsAvaliable(DateTimeOffset begins_on, DateTimeOffset ends_on) {

            foreach (var booking in Bookings) {
                if (begins_on == booking.BeginsOn && ends_on == booking.EndsOn) {
                    return false;
                }

                if (begins_on >= booking.BeginsOn && begins_on < booking.EndsOn) {
                    return false;
                }

                if (ends_on > booking.BeginsOn && ends_on <= booking.EndsOn) {
                    return false;
                }
            }

            return true;
        }
    }
}
