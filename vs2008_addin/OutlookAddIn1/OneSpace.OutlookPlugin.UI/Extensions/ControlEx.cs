﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;

namespace System.Windows.Forms {
    internal static class ControlEx {
        public static void Invalidate(this Control control, RectangleF area, float extra) {
            var area_simple = new Rectangle(
                x: (int)Math.Floor(area.X - extra),
                y: (int)Math.Floor(area.Y - extra),
                height: (int)Math.Ceiling(area.Height + (extra * 2)),
                width: (int)Math.Ceiling(area.Width + (extra * 2))
            );
            control.Invalidate(area_simple);
        }

        public static void Invalidate(this Control control, RectangleF area) {
            var area_simple = new Rectangle(
                x: (int)Math.Floor(area.X),
                y: (int)Math.Floor(area.Y),
                height: (int)Math.Ceiling(area.Height),
                width: (int)Math.Ceiling(area.Width)
            );
            control.Invalidate(area_simple);
        }

        [DebuggerStepThrough]
        public static void OnEventHandler(this Control control, EventHandler<EventArgs> event_handler) {
            if (event_handler != null) {
                control.OnEventHandler(event_handler, EventArgs.Empty);
            }
        }

        [DebuggerStepThrough]
        public static void OnEventHandler<T>(this Control control, EventHandler<T> event_handler, T event_args) {
            if (event_handler != null) {
                var callbacks = event_handler.GetInvocationList();

                /* Note: We iterate through the invocation list so that the subscribers get called on the UI
                 *       thread and if any subscriber throws an exception the remaining subscribers get called.
                 * */
                foreach (EventHandler<T> callback in callbacks) {
                    try {
                        callback(control, event_args);
                    }
                    catch (Exception ex) {
                        Trace.TraceWarning(ex.Message);
                    }
                }
            }
        }

    }
}
