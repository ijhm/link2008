﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.UI.Extentions {
    public static class DateTimeEx {
        public static DateTime? GetValueOrDefault(this List<DateTime> list, int index) {
            if(list == null) {
                return null;
            }

            if (index < 0) {
                return null;
            }

            if (index >= list.Count) {
                return null;
            }

            return list[index];
        }
    }
}
