﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Drawing {
    internal static class GraphicsEx {
        public static void DrawRectangle(this Graphics graphics, Pen pen, RectangleF rectangle) {
            graphics.DrawRectangle(pen, rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }
    }
}
