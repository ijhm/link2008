﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Windows.Forms {
    internal static class PictureBoxEx {
        public static Point TransalateToOriginalImageCoord(this PictureBox picture_box, Point coord) {
            var result = picture_box.TransalateToOriginalImageCoord((PointF)coord);
            return new Point(
                x: (int)result.X,
                y: (int)result.Y
            );
        }

        public static PointF TransalateToOriginalImageCoord(this PictureBox picture_box, PointF coord) {

            if (picture_box.SizeMode == PictureBoxSizeMode.Zoom) {
                RectangleF c_rect = picture_box.ClientRectangle;
                SizeF i_size = picture_box.Image.Size;

                var scale = Math.Min(
                    c_rect.Width / i_size.Width,
                    c_rect.Height / i_size.Height
                );

                var x_offset = (c_rect.Width - (i_size.Width * scale)) / 2;
                var y_offset = (c_rect.Height - (i_size.Height * scale)) / 2;

                var result = new PointF(
                    x: ((coord.X - x_offset) / scale),
                    y: ((coord.Y - y_offset) / scale)
                );

                return result;
            }

            throw new NotSupportedException("This method currently only supports the PictureBox.SizeMode == PictureBoxSizeMode.Zoom");
        }
    }
}
