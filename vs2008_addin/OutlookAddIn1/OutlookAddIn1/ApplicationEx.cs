﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using Microsoft.Office.Interop.Outlook;
using OneSpace.OutlookPlugin.Data;

namespace OneSpace.OutlookPlugin.Extentions
{
    internal static class ApplicationEx
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        private struct CREDUI_INFO
        {
            public int cbSize;
            public IntPtr hwndParent;
            public string pszMessageText;
            public string pszCaptionText;
            public IntPtr hbmBanner;
        }

        private enum CredUIReturnCodes
        {
            NO_ERROR = 0,
            ERROR_CANCELLED = 1223,
            ERROR_NO_SUCH_LOGON_SESSION = 1312,
            ERROR_NOT_FOUND = 1168,
            ERROR_INVALID_ACCOUNT_NAME = 1315,
            ERROR_INSUFFICIENT_BUFFER = 122,
            ERROR_INVALID_PARAMETER = 87,
            ERROR_INVALID_FLAGS = 1004,
            ERROR_BAD_ARGUMENTS = 160
        }

        [Flags]
        private enum CREDUI_FLAGS
        {
            ALWAYS_SHOW_UI = 0x00080,
            COMPLETE_USERNAME = 0x00800,
            DO_NOT_PERSIST = 0x00002,
            EXCLUDE_CERTIFICATES = 0x00008,
            EXPECT_CONFIRMATION = 0x20000,
            GENERIC_CREDENTIALS = 0x40000,
            INCORRECT_PASSWORD = 0x00001,
            KEEP_USERNAME = 0x100000,
            PASSWORD_ONLY_OK = 0x00200,
            PERSIST = 0x01000,
            REQUEST_ADMINISTRATOR = 0x00004,
            REQUIRE_CERTIFICATE = 0x00010,
            REQUIRE_SMARTCARD = 0x00100,
            SERVER_CREDENTIAL = 0x04000,
            SHOW_SAVE_CHECK_BOX = 0x00040,
            USERNAME_TARGET_CREDENTIALS = 0x80000,
            VALIDATE_USERNAME = 0x00400,
        }

        [DllImport("credui", CharSet = CharSet.Unicode)]
        private static extern CredUIReturnCodes CredUIPromptForCredentialsW(
            ref CREDUI_INFO creditUR,
            string targetName,
            IntPtr reserved1,
            int iError,
            StringBuilder userName,
            int maxUserName,
            StringBuilder password,
            int maxPassword,
            [MarshalAs(UnmanagedType.Bool)] ref bool pfSave,
            CREDUI_FLAGS flags
        );
    }
}
