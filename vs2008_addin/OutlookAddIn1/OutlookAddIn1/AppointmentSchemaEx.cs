﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin {
    internal static class AppointmentSchemaEx
    {
        public const string RoomBookingRegion       = "RoomBookingRegion";
        public const string RoomBookingLocation     = "RoomBookingLocation";
        public const string RoomBookingRoom         = "RoomBookingRoom";
        public const string RoomBookingLinkedRooms  = "RoomBookingLinkedRooms";
        public const string RoomBookingCapacity     = "RoomBookingCapacity";
        public const string RoomDataUpdated         = "RoomDataUpdated";
        public const string RoomDataId              = "RoomDataId";
        public const string RoomBookingLocationText = "RoomBookingLocationText";
        public const string OneSpaceStart           = "OneSpaceStart";
        public const string OneSpaceEnd             = "OneSpaceEnd";
        public const string RoomBookingDataUpdated  = "RoomBookingDataUpdated";
    }
}
