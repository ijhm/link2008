﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data.Admin
{
    public class AdminTool
    {
        public static bool IsInstalled
        {
            get
            {
                return true;
            }
        }

        public static string ProcessName
        {
            get
            {
                return "FakeAdminApp";
            }
        }

        public static string FilePath
        {
            get
            {
                string result = "C:\\git\\onespace.outlookplugin\\FakeAdminWinForms\\FakeAdminApp\\bin\\Debug\\FakeAdminApp.exe";
                string path = Path.GetTempPath() + "CibOutlookPlugin\\admintool.inf";
                if (System.IO.File.Exists(path))
                {
                    result = System.IO.File.ReadAllText(path);
                }
                return result;
            }
        }

    }
}
