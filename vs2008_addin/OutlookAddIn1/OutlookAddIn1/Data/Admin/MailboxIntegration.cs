﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OneSpace.OutlookPlugin.Data.Admin
{
    public class MailboxIntegration
    {
        public static void PublishUpdate(string id)
        {
            Outlook.MailItem msg = (Outlook.MailItem)Globals.ThisAddIn.OutlookApp.CreateItem(Outlook.OlItemType.olMailItem);
            string configPath = string.Format("{0}\\{1}\\config.json", Globals.ThisAddIn.AdminSession.sharedDirectory, id);
            msg.Attachments.Add(configPath, Outlook.OlAttachmentType.olByValue);
            var files = System.IO.Directory.GetFiles(string.Format("{0}\\{1}",Globals.ThisAddIn.AdminSession.sharedDirectory,id), "*.png");
            foreach (var file in files)
            {
                msg.Attachments.Add(file, Outlook.OlAttachmentType.olByValue);
            }
            msg.To = Globals.ThisAddIn.DeligateAccount;
            msg.Subject = "MasterData2";
            msg.Send();
        }
    }
}
