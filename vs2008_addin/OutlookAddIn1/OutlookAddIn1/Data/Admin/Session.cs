﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data.Admin
{

    public class Session
    {
        [DllImport("User32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        public enum State
        {
            SESSIONSTART,
            PUBLISHFILE,
            ENDSESSION
        }

        public Guid Id;
        private FileSystemWatcher watcher;
        private bool publishing;

        /// <summary>
        /// Starts a new admin session by creating a new id value and creating a file with this id
        /// that will then be monitored for publishing
        /// </summary>
        public Session(bool startSession)
        {

            Id = Guid.NewGuid();
            if (startSession)
            {
                using (var file = new FileStream(SessionFile, FileMode.Create))
                {
                    StreamWriter writer = new StreamWriter(file);
                    writer.WriteLine(State.SESSIONSTART);
                    writer.Close();
                    file.Close();
                }
                //Clone everything
                System.IO.Directory.CreateDirectory(string.Format("{0}\\{1}", sharedDirectory, Id.ToString()));
                File.Copy(string.Format("{0}\\OutlookLink.json", sharedDirectory), string.Format("{0}\\{1}\\OutlookLink.json", sharedDirectory, Id.ToString()));
                var files = System.IO.Directory.GetFiles(sharedDirectory, "*.png");
                foreach (var file in files)
                {
                    int lastSlash = file.LastIndexOf("\\");
                    string fileName = file.Substring(lastSlash + 1);
                    string targetFile = string.Format("{0}\\{1}\\{2}", sharedDirectory, Id.ToString(), fileName);
                    File.Copy(file, targetFile);
                }
                watcher = new FileSystemWatcher(string.Format("{0}\\{1}", sharedDirectory, Id.ToString()), "*.publish");
                watcher.EnableRaisingEvents = true;
                watcher.Changed += Watcher_Changed;
                publishing = false;
            }
        }

        public string sharedDirectory
        {
            get
            {
                return string.Format("{0}\\CibOutlookPlugin", Path.GetTempPath());
            }
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (!publishing)
            {
                publishing = true;
                PublishFiles();
                publishing = false;
            }
        }

        public void PublishFiles()
        {
            //Clone everything
            System.IO.Directory.CreateDirectory(string.Format("{0}\\{1}", sharedDirectory, Id.ToString()));
            File.Copy(string.Format("{0}\\{1}\\OutlookLink.json", sharedDirectory, Id.ToString()), string.Format("{0}\\OutlookLink.json", sharedDirectory),true);
            File.Copy(string.Format("{0}\\{1}\\OutlookLink.json", sharedDirectory, Id.ToString()), string.Format("{0}\\{1}\\config.json", sharedDirectory,Id.ToString()), true);
            var files = System.IO.Directory.GetFiles(string.Format("{0}\\{1}", sharedDirectory, Id.ToString()), "*.png");
            foreach (var file in files)
            {
                int lastSlash = file.LastIndexOf("\\");
                string fileName = file.Substring(lastSlash + 1);
                string targetFile = string.Format("{0}\\{1}", sharedDirectory, fileName);
                File.Copy(file, targetFile,true);
            }
            MailboxIntegration.PublishUpdate(Id.ToString());
            Globals.ThisAddIn.LoadDataFromCache(sharedDirectory);
        }


        public string SessionFile
        {
            get
            {
                return string.Format("{0}\\{1}.session", sharedDirectory, Id.ToString());
            }
        }

        public bool IsRunning
        {
            get
            {
                return Process.GetProcessesByName(Admin.AdminTool.ProcessName).Any();
            }
        }

        public void BringToFront() {
            var proc = Process.GetProcessesByName(Admin.AdminTool.ProcessName).First();
            SetForegroundWindow(proc.MainWindowHandle);
        }

        public void LaunchAdmin()
        {
            var proc = new Process();
            var start = new ProcessStartInfo
            {
                FileName = Admin.AdminTool.FilePath,
                UseShellExecute = true,
                WindowStyle = ProcessWindowStyle.Normal,
                Arguments = "\"{sharedDirectory}\" {Id}"
            };
            proc.StartInfo = start;
            proc.Start();
        }
    }
}
