﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OneSpace.OutlookPlugin.Data.Booking
{
    public class AppointmentAnalysis
    {
        public Region Region;
        public int Hours;
        public int AttendeeCount;
        public List<BookingItem> BookingItems;

        public AppointmentAnalysis(Outlook.AppointmentItem appointment,Region region)
        {
            if(region != null)
            {
                Region = region;
                BookingItems = region.BookingItems.ToList();
            }
            float tempHours = (float)appointment.Duration / 60;
            Hours = (int)Math.Ceiling(tempHours);
            if (appointment.RequiredAttendees!=null)
            {
                AttendeeCount = appointment.RequiredAttendees.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Count();
            }
        }
        
        public BookingItem GetBookingItem(int itemId)
        {
            foreach (var item in BookingItems)
            {
                if (item.Id==itemId)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
