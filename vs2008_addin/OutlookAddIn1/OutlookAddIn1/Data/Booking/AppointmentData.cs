﻿using OneSpace.OutlookPlugin;
using OneSpace.OutlookPlugin.Settings;
using OneSpace.OutlookPlugin.Utilities;
using Microsoft.Office.Interop.Outlook;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSpace.OutlookPlugin.Logging;
using OneSpace.OutlookPlugin.Data.Booking;
using OneSpace.OutlookPlugin.ServiceCalendars;

namespace OneSpace.OutlookPlugin.Data
{
    public class AppointmentData
    {
        public enum Enum_ProcessAppointmentMode
        {
            SendNew,
            SendUpdates,
            DeleteAll
        }

        public DateTime LastUpdated;
        public List<OrderItem> Items;
        public string CostCentre;
        public DateTime Start;
        public DateTime End;
        public List<string> DeletedServiceIDs;

        [JsonIgnore]
        public string AlertEmail;



        public AppointmentData()
        {
            Items = new List<Data.OrderItem>();
            DeletedServiceIDs = new List<string>();
        }

        public AppointmentData(string jsonData)
        {
            var data = JsonConvert.DeserializeObject<AppointmentData>(jsonData);
            Items = data.Items;
            LastUpdated = data.LastUpdated;
            CostCentre = data.CostCentre;
            Start = data.Start;
            End = data.End;
            DeletedServiceIDs = new List<string>();
        }

        public static string GetDataText(AppointmentData obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public Currency TotalCost(AppointmentAnalysis analysis)
        {
            Currency result = new Currency() {
                Symbol = analysis.Region.CurrencySymbol,
                HasSymbolOnLeft = analysis.Region.HasSymbolOnLeft,
                Value = 0
            };
            
            foreach (OrderItem item in Items)
            {
                result += item.SubTotal(analysis);
            }
            return result;
        }



        public static bool Move(ServiceMailboxIntegration integration, AppointmentItem appointment)
        {
            if (!SystemConsts.EnableServiceMailboxIntegration)
            {
                return false;
            }

            ActivityLog.WriteToLog("    Processing moving appointment data:" + appointment.Subject);

            #region Local variables
            bool updated = false;
            string currentJson = "";
            string subject = "";
            BookingItem bookingItem;
            var newUpdate = new AppointmentData();
            AppointmentData updatedData = null;
            #endregion

            try
            {
                currentJson = (string)appointment.UserProperties[AppointmentSchemaEx.RoomBookingDataUpdated]?.Value;
                if (string.IsNullOrWhiteSpace(currentJson) == false) {
                    updatedData = new AppointmentData(currentJson);
                }
                else {
                    updatedData = new AppointmentData();
                }
            }
            catch (System.Exception)
            {
                updatedData = new AppointmentData();
                //Ignore a non OneSpace enabled booking
                ActivityLog.WriteToLog("        Non OneSpace booking, ignoring");
                return false;
            }
            var srvMsg = new ServiceMessages();

            //Delete existing app IDs deleted from the order item list
            foreach (var appId in updatedData.DeletedServiceIDs)
            {
                ActivityLog.WriteToLog("        Delete item deleted from order:" + appId);
                integration.DeleteInvitation(appId);
            }
            updatedData.DeletedServiceIDs = new List<string>();

            #region Delete existing IDs
            //Get existing app IDs to delete
            for (int i = 0; i < updatedData.Items.Count; i++)
            {
                if (updatedData.Items[i].ServiceAppointmentId != "" && updatedData.Items[i].ServiceAppointmentId != null)
                {
                    srvMsg.AddAppToDelete(updatedData.Items[i].ServiceAppointmentId);
                }
            }
            //Delete existing app IDs
            foreach (var appId in srvMsg.AppToDelete)
            {
                ActivityLog.WriteToLog("        Delete item:" + appId);
                integration.DeleteInvitation(appId);
            }
            //Remove all app IDs from existing items
            for (int i = 0; i < updatedData.Items.Count; i++)
            {
                updatedData.Items[i].ServiceAppointmentId = "";
                updatedData.Items[i].Start = appointment.Start;
                updatedData.Items[i].End = appointment.End;
            }
            #endregion

            #region Create message batch
            for (int i = 0; i < updatedData.Items.Count; i++)
            {
                bookingItem = Globals.ThisAddIn.OneSpaceData.GetAssetById(updatedData.Items[i].ItemId);
                if (bookingItem.AlertEmail!="")
                {
                    subject = string.Format(Globals.ThisAddIn.resourceManager.GetString("serviceSubject"), appointment.Location);
                    var lineItem = bookingItem.Name + " X " + updatedData.Items[i].Qty.ToString() + "\r\n" +
                            updatedData.Items[i].Notes;

                    srvMsg.AddMessage(i,
                        updatedData.Items[i].CalculatedDelivery,
                        updatedData.Items[i].CalculatedClearAway,
                        bookingItem.AlertEmail,
                        subject,
                        lineItem);
                }
            }
            #endregion

            foreach (var msg in srvMsg.Messages)
            {
                ActivityLog.WriteToLog("        Sending new service request for item:" + msg.Body);
                var appId = integration.SendInvitation(msg.ServiceMailbox,
                                                       msg.Subject,
                                                       msg.Body,
                                                       appointment.Location, 
                                                       msg.DeliveryDateTime,
                                                       msg.ClearAwayDateTime
                                                       );

                ActivityLog.WriteToLog("            Id:" + appId);

                foreach (int index in msg.ItemIndex)
                {
                    updatedData.Items[index].ServiceAppointmentId = appId;
                }
            }
            ActivityLog.WriteToLog("        Updating appointment data");
            
            //appointment.UserProperties[AppointmentSchemaEx.RoomBookingDataUpdated].Value = AppointmentData.GetDataText(updatedData);
            OutlookUtilities.SetPropertyText(appointment, AppointmentSchemaEx.RoomBookingDataUpdated, AppointmentData.GetDataText(updatedData));

            OutlookUtilities.SetPropertyText(appointment, "OneSpaceStart", appointment.Start.ToString(SystemConsts.DateTimeFormat));
            OutlookUtilities.SetPropertyText(appointment, "OneSpaceEnd", appointment.End.ToString(SystemConsts.DateTimeFormat));


            /* Todo: Find out why this call is being made.  I think the appointment should be saved when the user
             *       presses "Save" / "Send" as per the normal Outlook workflow
             * */
            //appointment.Save();

            ActivityLog.WriteToLog("    Finished");

            return updated;
        }

        public static void Delete(ServiceMailboxIntegration integration, AppointmentItem appointment)
        {
            if (!SystemConsts.EnableServiceMailboxIntegration)
            {
                return;
            }

            ActivityLog.WriteToLog("    Processing deleted appointment data:" + appointment.Subject);

            #region Local variables
            string currentJson = "";
            var newUpdate = new AppointmentData();
            AppointmentData updatedData = null;
            #endregion

            try
            {
                currentJson = (string)appointment.UserProperties[AppointmentSchemaEx.RoomBookingDataUpdated]?.Value;
                if(string.IsNullOrWhiteSpace(currentJson) == false) {
                    updatedData = new AppointmentData(currentJson);
                }
                else {
                    updatedData = new AppointmentData();
                }
            }
            catch (System.Exception)
            {
                updatedData = new AppointmentData();
                //Ignore a non OneSpace enabled booking
                ActivityLog.WriteToLog("        Non OneSpace booking, ignoring");
                return;
            }
            var srvMsg = new ServiceMessages();

            #region Delete existing IDs
            //Get existing app IDs to delete
            for (int i = 0; i < updatedData.Items.Count; i++)
            {
                if (updatedData.Items[i].ServiceAppointmentId != "" && updatedData.Items[i].ServiceAppointmentId != null)
                {
                    srvMsg.AddAppToDelete(updatedData.Items[i].ServiceAppointmentId);
                }
            }
            //Delete existing app IDs
            foreach (var appId in srvMsg.AppToDelete)
            {
                ActivityLog.WriteToLog("        Delete item:" + appId);
                integration.DeleteInvitation(appId);
            }
            #endregion
        }


        /// <summary>
        /// Handle the deletes of service invitations
        /// </summary>
        /// <param name="appointment"></param>
        public static void Delete(AppointmentItem appointment)
        {
//            if (!SystemConsts.EnableServiceMonitoring)
//            {
//                return;
//            }

            ActivityLog.WriteToLog("Processing delete appointment data");

            #region Local variables
            string currentJson = "";
            BookingItem bookingItem;
            var newUpdate = new AppointmentData();
            AppointmentData currentData = null;
            #endregion

            try
            {
                currentJson = (string)appointment.UserProperties[AppointmentSchemaEx.RoomBookingDataUpdated].Value;
                currentData = new AppointmentData(currentJson);
            }
            catch (System.Exception)
            {
                currentData = new AppointmentData();
            }

            for (int i = 0; i < currentData.Items.Count; i++)
            {
                bookingItem = Globals.ThisAddIn.OneSpaceData.GetAssetById(currentData.Items[i].ItemId);
                if (bookingItem.AlertEmail != "")
                {
                    if (currentData.Items[i].ServiceAppointmentId != "" && currentData.Items[i].ServiceAppointmentId != null)
                    {
                        //Send delete
                        ActivityLog.WriteToLog("        Deleting item:" + bookingItem.Name);
                        OutlookUtilities.DeleteAppointment(currentData.Items[i].ServiceAppointmentId, bookingItem.AlertEmail);
                    }
                }
            }
        }

        public static bool IsOneSpaceAppointment(AppointmentItem appointment)
        {
            try
            {
                var meetingRoomId = int.Parse(appointment.UserProperties["RoomBookingLocation"].Value.ToString());
                return true;
            }
            catch (System.Exception)
            {
            }
            return false;
        }

        /// <summary>
        /// Determine if the appointment has been updated to be different to what was stored against the room booing within the plug-in
        /// </summary>
        /// <param name="appointment"></param>
        /// <returns></returns>
        public static bool IsUpdated(AppointmentItem appointment)
        {
            if (appointment.UserProperties["OneSpaceStart"]!=null && appointment.UserProperties["OneSpaceEnd"] !=null)
            {
                DateTime date = DateTime.Parse(appointment.UserProperties["OneSpaceStart"].Value);
                if (date.ToString(SystemConsts.DateTimeFormat)!=appointment.Start.ToString(SystemConsts.DateTimeFormat))
                {
                    return true;
                }
                date = DateTime.Parse(appointment.UserProperties["OneSpaceEnd"].Value);
                if (date.ToString(SystemConsts.DateTimeFormat) != appointment.End.ToString(SystemConsts.DateTimeFormat))
                {
                    return true;
                }
                try
                {
                    var currentJson = (string)appointment.UserProperties[AppointmentSchemaEx.RoomBookingDataUpdated]?.Value;
                    if(string.IsNullOrWhiteSpace(currentJson) == false) {
                        var updatedData = new AppointmentData(currentJson);
                        foreach (var item in updatedData.Items) {
                            var bookingItem = Globals.ThisAddIn.OneSpaceData.GetAssetById(item.ItemId);
                            if (bookingItem.AlertEmail != "" && bookingItem.AlertEmail != null) {
                                if (item.ServiceAppointmentId == null || item.ServiceAppointmentId == "") {
                                    return true;
                                }
                            }
                        }

                        if (updatedData.DeletedServiceIDs.Count > 0) {
                            return true;
                        }
                    }
                }
                catch (System.Exception)
                {
                }

            }
            else
            {
                return true;
            }
            return false;
        }
    }
}
