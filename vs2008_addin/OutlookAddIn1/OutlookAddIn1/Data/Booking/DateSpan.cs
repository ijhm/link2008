﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class DateSpan
    {
        public DateTime Start;
        public DateTime End;

    }
}
