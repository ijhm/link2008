﻿using OneSpace.OutlookPlugin.Data.Booking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class OrderItem
    {
        public enum enum_OrderServiceState
        {
            Normal,
            PendingServiceInvitation,
            SentServiceInvitation,
            DeclinedServiceInvitation
        }

        public int ItemId;
        public Currency ItemCost;
        public int Qty;
        public string Notes;

        /// <summary>
        /// Datetime value used to see if it is different to the appointment value
        /// </summary>
        public DateTime Start;
        public DateTime End;
        /// <summary>
        /// The datetime calculated from the appointment date with the offsets applied
        /// </summary>
 //       public DateTime DeliveryTime;
 //       public DateTime ClearAwayTime;
        public int DeliveryTimeOffset;
        public int ClearAwayTimeOffset;
        public bool Scanned;
        public OrderItem()
        {
            ItemCost = new Data.Currency();
        }

        public DateTime CalculatedDelivery
        {
            get
            {
                return Start.AddMinutes(DeliveryTimeOffset);
            }
        }

        public DateTime CalculatedClearAway
        {
            get
            {
                return End.AddMinutes(ClearAwayTimeOffset);
            }
        }

        public string ServiceAppointmentId { get; set; }
        public enum_OrderServiceState ServiceState;

        public Currency SubTotal(AppointmentAnalysis analysis) {
            Currency result = new Currency() {
                Symbol = analysis.Region.CurrencySymbol,
                HasSymbolOnLeft = analysis.Region.HasSymbolOnLeft,
                Value = 0
            };

            try
            {
                if (analysis != null)
                {
                    var item = analysis.GetBookingItem(this.ItemId);
                    switch (item.QtyType)
                    {
                        case BookingItem.enum_QtyType.PerHour:
                            result = (Qty * analysis.Hours) * ItemCost;
                            break;

                        case BookingItem.enum_QtyType.PerSession:
                            result = Qty * ItemCost;
                            break;

                        case BookingItem.enum_QtyType.PerServing:
                            result = Qty * ItemCost;
                            break;
                    }
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public DateTime DeliveryDate(DateTime date)
        {
            return date.AddMinutes(DeliveryTimeOffset);
        }

        public DateTime ClearawayDate(DateTime date)
        {
            return date.AddMinutes(DeliveryTimeOffset);
        }

        public bool IsDifferent(OrderItem current)
        {
            return (Start != current.Start || End != current.End || DeliveryTimeOffset != current.DeliveryTimeOffset || ClearAwayTimeOffset != current.ClearAwayTimeOffset);
        }

    }
}
