﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class OrderItemDisplay : OrderItem
    {
        public List<BookingItem> BookingItems;

        public BookingItem BookingItem
        {
            get
            {
                foreach (BookingItem item in BookingItems)
                {
                    if (item.Id == ItemId)
                    {
                        return item;
                    }
                }
                return null;
            }
        }

        public bool IsTooLate(DateTime startDate)
        {
            bool result = false;
            if (BookingItem.MinLeadTime == 0)
            {
                return false;
            }
            if (startDate <= DateTime.Now)
            {
                return true;
            }
            TimeSpan time = startDate - DateTime.Now;
            if (time.TotalMinutes < BookingItem.MinLeadTime)
            {
                result = true;
            }
            return result;
        }


    }
}
