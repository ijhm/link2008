﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSpace.OutlookPlugin.Extentions;

namespace OneSpace.OutlookPlugin.Data
{
    public class RoomBooking
    {
        private AppointmentData _data = null;
        public GraphicsPath Path = new GraphicsPath();

        public string Subject;
        public Room Room;
        public DateTime DateFrom;
        public DateTime DateTo;
        public AppointmentData Data {
            get {
                if(_data == null && appointmentItem != null) {
                    var data_json = (appointmentItem.UserProperties[AppointmentSchemaEx.RoomBookingDataUpdated]?.Value as string) ?? string.Empty;
                    if(string.IsNullOrWhiteSpace(data_json) == false) {
                        var data = new AppointmentData(data_json);
                        _data = data;
                    }
                }

                return _data;
            }
            set { _data = value; }
        }
        public string Attendees;
        public string Id;
        public int Index;
        public int RoomId;

        public double UpdatedDuration;
        public DateTime UpdatedDateFrom;
        public DateTime UpdatedDateTo;

        public string OrganiserName;
        public string OrganiserEmail;
        public string OrganiserPhone;
        public string Location;
        public int LocationId;

        [JsonIgnore]
        public string LocationName;

        public bool IsSelected;
        public bool IsInValid;
        public bool IsFree;
        public bool IsBuffer;

        public int X;

        public int x1;
        public int y1;
        public int x2;
        public int y2;

        public Microsoft.Office.Interop.Outlook.AppointmentItem appointmentItem;
        public string RoomDataId;

        /// <summary>
        /// Returns the calculated buffer pre appointment buffer:
        /// If there is no pre buffer it returns null
        /// </summary>
        public DateSpan PreBuffer
        {
            get
            {
                if(this.Room.SetupTime > 0 || Data?.Items.Count > 0) {
                    var has_pre_buffer = false;
                    var appointment_start = appointmentItem?.Start ?? DateFrom;
                    var appointment_end = appointmentItem?.End ?? DateTo;

                    var buffer_start = appointment_start;
                    var buffer_end = appointment_start;

                    if(this.Room.SetupTime > 0) {
                        buffer_start = appointment_start.Subtract(TimeSpan.FromMinutes(this.Room.SetupTime));
                        has_pre_buffer = true;
                    }
                
                    if (this.Data?.Items.Count > 0) {
                        var region = Globals.ThisAddIn.OneSpaceData.GetRegionByRoomId(Room.Id);

                        foreach (var item in Data.Items) {
                            var booking_item = Globals.ThisAddIn.OneSpaceData.GetBookingItem(region.Id, item.ItemId);
                            if (booking_item!=null)
                            {
                                if (booking_item.SetupTime > 0)
                                {
                                    var setup_time = TimeSpan.FromMinutes(booking_item.SetupTime);
                                    buffer_start = buffer_start.Subtract(setup_time);
                                    has_pre_buffer = true;
                                }
                            }
                        }
                    }

                    if (has_pre_buffer) {
                        return new DateSpan() { Start = buffer_start, End = buffer_end };
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Returns the calculated buffer post appointment buffer:
        /// If there is no pre buffer it returns null
        /// </summary>
        public DateSpan PostBuffer {
            get {
                if (this.Room.ClearDownTime > 0 || this.Data?.Items.Count > 0) {
                    var has_post_buffer = false;
                    var appointment_start = appointmentItem?.Start ?? DateFrom;
                    var appointment_end = appointmentItem?.End ?? DateTo;

                    var buffer_start = appointment_end;
                    var buffer_end = appointment_end;

                    if (this.Room.ClearDownTime > 0) {
                        buffer_end = appointment_end.Add(TimeSpan.FromMinutes(this.Room.ClearDownTime));
                        has_post_buffer = true;
                    }

                    if (this.Data?.Items.Count > 0) {
                        var region = Globals.ThisAddIn.OneSpaceData.GetRegionByRoomId(Room.Id);

                        foreach (var item in this.Data.Items) {
                            var booking_item = Globals.ThisAddIn.OneSpaceData.GetBookingItem(region.Id, item.ItemId);
                            if (booking_item!=null)
                            {
                                if (booking_item.ClearDownTime > 0)
                                {
                                    var clear_away_time = TimeSpan.FromMinutes(booking_item.ClearDownTime);
                                    buffer_end = buffer_end.Add(clear_away_time);
                                    has_post_buffer = true;
                                }
                            }
                        }
                    }

                    if (has_post_buffer) {
                        return new DateSpan() { Start = buffer_start, End = buffer_end };
                    }
                }

                return null;
            }
        }

        public double Duration
        {
            get
            {
                double i = 0;
                i = (DateTo.Subtract(DateFrom)).TotalMinutes;
                return i;
            }
        }

        public string DurationString
        {
            get
            {
                DateTime d1 = new DateTime(200, 1, 1);
                TimeSpan ts = (d1.AddMinutes(Duration)) - d1;
                return ts.ToFriendlyString();
            }
        }
    }
}
