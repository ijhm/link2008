﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class RoomWithBookings : RoomMailbox
    {
        public List<RoomBooking> Bookings = new List<RoomBooking>();
        public List<RoomBooking> FreeSlots = new List<RoomBooking>();
        public List<RoomBooking> Schedule = new List<RoomBooking>();
        public List<RoomBooking> SlotList = new List<RoomBooking>();

        /// <summary>
        /// Calculate if the proposed meeting can fit inside the slot defined.
        /// Looks at the room buffer times
        /// </summary>
        /// <param name="adjustedFreeSlots"></param>
        /// <param name="appointment"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static BufferResult CanBookWithBuffer(List<RoomBooking> bufferedSlots, 
            AppointmentData appointment, 
            DateTime start, 
            DateTime end)
        {
            var result = new BufferResult();

            int freeSlots = 0;
            foreach (var slot in bufferedSlots)
            {
                if (slot.IsFree)
                {
                    freeSlots++;
                }
            }
                
            if (freeSlots == 0)
            {
                result.BufferOk = true;
                return result;
            }

            var room = new RoomBooking
            {
                DateFrom = start,
                DateTo = end,
                Room = bufferedSlots[0].Room,
                Data = appointment
            };
            var pre_buffer = room.PreBuffer;
            var post_buffer = room.PostBuffer;

            result.Before = (pre_buffer != null) ? start - pre_buffer.Start : TimeSpan.Zero;
            result.After = (post_buffer != null) ? end - post_buffer.End : TimeSpan.Zero;

            foreach (var slot in bufferedSlots)
            {
                if (slot.IsFree 
                    && (pre_buffer?.Start ?? start) >= slot.DateFrom
                    && (post_buffer?.End ?? end) <= slot.DateTo)
                {
                    result.BufferOk = true;
                    return result;
                }
            }

            result.BufferOk = false;
            return result;
        }

        public void ReCalculateSchedule(int roomId, DateTime start, DateTime end)
        {
            Schedule = new List<RoomBooking>();
           RoomBooking freeSlot;
            Bookings = Bookings.OrderBy(d => d.DateFrom).ToList();
            foreach (RoomBooking booking in Bookings)
            {
                if (start < booking.DateFrom)
                {
                    freeSlot = new RoomBooking
                    {
                        DateFrom = start,
                        DateTo = booking.DateFrom,
                        Subject = "",
                        Id = "",
                        RoomId = roomId
                    };
                    Schedule.Add(freeSlot);
                    Schedule.Add(booking);
                }
                else
                {
                    Schedule.Add(booking);
                }
                start = booking.DateTo;
            }
            if (start < end)
            {
                freeSlot = new RoomBooking
                {
                    DateFrom = start,
                    DateTo = end,
                    Subject = "",
                    Id = "",
                    RoomId = roomId
                };
                Schedule.Add(freeSlot);
            }
            Schedule = Schedule.OrderBy(d => d.DateFrom).ToList();
        }
    }

    public class BufferResult
    {
        public bool BufferOk;
        public TimeSpan Before;
        public TimeSpan After;
    }
}
