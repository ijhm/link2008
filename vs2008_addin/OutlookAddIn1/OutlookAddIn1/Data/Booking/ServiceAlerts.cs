﻿using OneSpace.OutlookPlugin.Logging;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Utilities;
using Microsoft.Office.Interop.Outlook;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data.Booking
{
    public class ServiceAlerts
    {
        private AppointmentItem appointment;
        private AppointmentData data;
        public enum Enum_ProcessAppointmentMode
        {
            DoNothing,
            SendNew,
            SendUpdates,
            DeleteAll
        }
 //       private Enum_ProcessAppointmentMode mode = Enum_ProcessAppointmentMode.DoNothing;
        private BookingItem bookingItem;
        private List<int> newOrderItems = new List<int>();
        private List<AlertDeletes> existingAppointmentIds = new List<AlertDeletes>();
        private bool addNewAlertEmail;
        private bool processPending;
        private List<string> appointmentIds = new List<string>();

        public ServiceAlerts(AppointmentItem _appointment)
        {
            appointment = _appointment;
            try
            {
                string currentJson = (string)appointment.UserProperties[AppointmentSchemaEx.RoomBookingDataUpdated].Value;
                data = new AppointmentData(currentJson);

                string lastJson = string.Empty;
                try
                {
                    lastJson = (string)appointment.UserProperties["RoomBookingDataLast"].Value;
                }
                catch (System.Exception)
                {
                }

                try
                {
                    existingAppointmentIds = (List<AlertDeletes>)appointment.UserProperties["RoomBookingServiceAppointments"].Value;
                }
                catch (System.Exception)
                {
                }

                OutlookUtilities.SetPropertyText(appointment, "RoomBookingServiceAppointments", JsonConvert.SerializeObject(appointmentIds));

                if (lastJson==currentJson)
                {
                    processPending = false;
                }
                else
                {
                    processPending = true;
                }

                //Determine if appointments have already been sent
                for (int i = 0; i < data.Items.Count; i++)
                {
                    data.Items[i].Start = appointment.Start;
                    data.Items[i].End = appointment.End;
                    newOrderItems.Add(i);
                }

            }
            catch (System.Exception)
            {
            }
        }

        public AppointmentData ProcessAlerts()
        {
            if (processPending)
            {
                //Send appointment deletes for existing Ids
                foreach (var item in existingAppointmentIds)
                {
                    OutlookUtilities.DeleteAppointment(item.AppointmentId, item.AlertEmail);
                }

                ProcessNew();
                OutlookUtilities.SetPropertyText(appointment, "RoomBookingDataLast", AppointmentData.GetDataText(data));
            }
            return data;
        }

        private void ProcessNew()
        {
            //Break down the new order items by start/end/appointmentId
            var emailAlerts = new List<AppointmentData>();

            ActivityLog.WriteToLog("Process new");

            foreach (int itemId in newOrderItems)
            {
                bookingItem = Globals.ThisAddIn.OneSpaceData.GetAssetById(data.Items[itemId].ItemId);
                addNewAlertEmail = true;

                ActivityLog.WriteToLog("    Item:" + itemId.ToString());

                for (int j = 0; j < emailAlerts.Count; j++)
                {
                    ActivityLog.WriteToLog(string.Format("    Comparing:{0},{1} > {2},{3}   {4}",   data.Items[itemId].CalculatedDelivery, 
                                                                                                    data.Items[itemId].CalculatedClearAway,
                                                                                                    emailAlerts[j].Start,
                                                                                                    emailAlerts[j].End,
                                                                                                    bookingItem.AlertEmail));
                    if (emailAlerts[j].Start == data.Items[itemId].CalculatedDelivery &&
                        emailAlerts[j].End == data.Items[itemId].CalculatedClearAway &&
                        emailAlerts[j].AlertEmail == bookingItem.AlertEmail)
                    {
                        ActivityLog.WriteToLog("    Add to existing");
                        emailAlerts[j].Items.Add(data.Items[itemId]);
                        addNewAlertEmail = false;
                    }
                }
                if (addNewAlertEmail)
                {
                    var newAlertEmail = new AppointmentData
                    {
                        Start = data.Items[itemId].CalculatedDelivery,
                        End = data.Items[itemId].CalculatedClearAway,
                        AlertEmail = bookingItem.AlertEmail
                    };
                    newAlertEmail.Items.Add(data.Items[itemId]);
                    emailAlerts.Add(newAlertEmail);
                }
            }

            foreach (AppointmentData email in emailAlerts)
            {
                var subject = String.Format(Globals.ThisAddIn.resourceManager.GetString("serviceAlertSubject"),appointment.Location);
                var body = Globals.ThisAddIn.resourceManager.GetString("serviceAlertBody");
                body += "----------------------------------------------------------------<br />";
                for (int i = 0; i < email.Items.Count; i++)
                {
                    bookingItem = Globals.ThisAddIn.OneSpaceData.GetAssetById(email.Items[i].ItemId);
                    body += bookingItem.Name + " X " + email.Items[i].Qty.ToString() + "<br />n";
                    body += email.Items[i].Notes + "<br />";
                    body += "----------------------------------------------------------------<br />";
                }

                if (email.AlertEmail != string.Empty && email.AlertEmail != null)
                {
                    //Send new
                    ActivityLog.WriteToLog("        Sending new service request " + data.AlertEmail);
                    ActivityLog.WriteToLog(body);
                    var serviceAppointmentId = OutlookUtilities.CreateAppointment(email.AlertEmail,
                                                                                  appointment.Location,
                                                                                  email.Start,
                                                                                  email.End,
                                                                                  subject,
                                                                                  body);

                    existingAppointmentIds.Add(new AlertDeletes {
                           AlertEmail= email.AlertEmail,
                           AppointmentId= serviceAppointmentId
                    });

                    /*
                    for (int i = 0; i < email.Items.Count; i++)
                    {
                        bookingItem = Globals.ThisAddIn.OneSpaceData.GetAssetById(email.Items[i].ItemId);
                        for (int j = 0; j < newOrderItems.Count; j++)
                        {
                            if (data.Items[j].Start == email.Items[i].CalculatedDelivery &&
                                data.Items[j].End == email.Items[i].CalculatedClearAway &&
                                email.AlertEmail == bookingItem.AlertEmail)
                            {
                                data.Items[j].ServiceAppointmentId = serviceAppointmentId;
                            }
                        }
                    }
                    */

                }

            }

            OutlookUtilities.SetPropertyText(appointment, "RoomBookingServiceAppointments", JsonConvert.SerializeObject(existingAppointmentIds));

        }

    }

    public class AlertDeletes
    {
        public string AlertEmail;
        public string AppointmentId;
    }
}
