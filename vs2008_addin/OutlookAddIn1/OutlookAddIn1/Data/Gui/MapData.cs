﻿using OneSpace.OutlookPlugin.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public enum enum_MapAreaStatus
    {
        Free,
        Booked,
        NotSuitable,
        Editing
    }

    public class MapArea
    {
        public GraphicsPath Path = new GraphicsPath();
        public int Id;
        public string Name;
        public List<Point> Poly = new List<Point>();
        public enum_MapAreaStatus Status;
        public float Zoom;
        private Brush Fill_Free;
        private Brush Fill_Booked;
        private Brush Fill_NotSuitable;

        public MapArea()
        {
            Fill_Free = new SolidBrush(Color.FromArgb(70, 100, 200, 100));
            Fill_Booked = new SolidBrush(Color.FromArgb(70, 200, 100, 100));
            Fill_NotSuitable = new SolidBrush(Color.FromArgb(70, 150, 150, 150));
        }


        public void Setup()
        {
            Path = new GraphicsPath();
            Path.AddPolygon(Poly.ToArray());
        }

        public void AddPoint(int x, int y)
        {
            Poly.Add(new Point(x, y));
        }

        public List<Point> ZoomPoly
        {
            get
            {
                List<Point> result = new List<Point>();
                for (int i=0;i< Poly.Count;i++)
                {
                    result.Add(ZoomPoint(Poly[i]));
                }
                return result;
            }
        }

        private Point ZoomPoint(Point point)
        {
            point.X = (int)(Zoom * (float)point.X);
            point.Y = (int)(Zoom * (float)point.Y);
            return point;
        }

        public string StatusName
        {
            get
            {
                switch (Status)
                {
                    case enum_MapAreaStatus.Free:
                        return Globals.ThisAddIn.resourceManager.GetString("enumMapAreaStatusFree");

                    case enum_MapAreaStatus.Booked:
                        return Globals.ThisAddIn.resourceManager.GetString("enumMapAreaStatusBooked");

                    case enum_MapAreaStatus.NotSuitable:
                        return Globals.ThisAddIn.resourceManager.GetString("enumMapAreaStatusNotSuitable");

                    default:
                        return string.Empty;
                }
            }
        }

        public Brush StatusBrush
        {
            get
            {
                switch (Status)
                {
  //                  case enum_MapAreaStatus.Selected:
  //                      return Fill_Free;

                    case enum_MapAreaStatus.Free:
                        return Fill_Free;

                    case enum_MapAreaStatus.Booked:
                        return Fill_Booked;

                    case enum_MapAreaStatus.NotSuitable:
                        return Fill_Booked;

                        //                  case enum_MapAreaStatus.NotAvailable:
                    //                      return Fill_Booked;

                    default:
                        return null;
                }
            }
        }
    }

}
