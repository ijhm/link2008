﻿using OneSpace.OutlookPlugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OneSpace.OutlookPlugin.Data
{
    public class NodeId
    {
        public string Type;
        public int LocationId;
        public int FloorId;
        public int RoomNumber;
        public int AssetNumber;
        public int CateringNumber;
        public string Name;

        public string NodeKey
        {
            get
            {
                return String.Format("{0} {1} {2} {3} {4} {5}", Type, LocationId, FloorId, RoomNumber, AssetNumber, CateringNumber);
            }
        }
    }

    public class UIUtilities
    {
        [DllImport("User32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

        const int LVM_FIRST = 0x1000;
        const int LVM_SETICONSPACING = LVM_FIRST + 53;

        public static void SetListViewSpacing(ListView lst, int x, int y)
        {
            SendMessage(lst.Handle, LVM_SETICONSPACING, false, x * 65536 + y);
        }

        public enum enum_ButtonType
        {
            Normal,
            Main
        }

        public static void SetControls(Control parent)
        {
            parent.BackColor = SystemConsts.Background;
            switch (parent.GetType().Name)
            {

                case "GroupBox":
                    var group = (GroupBox)parent;
                    group.Font = SystemConsts.MainLabel;
                    break;

                case "Panel":
                    var panel = (Panel)parent;
                    panel.Font = SystemConsts.MainLabel;
                    break;
            }

            if (parent.HasChildren)
            {
                foreach (var child in parent.Controls)
                {
                    switch (child.GetType().Name)
                    {

                        case "Button":
                            var but = (Button)child;
                            but.BackColor = SystemConsts.NormalButton;
                            if (but.Tag!=null)
                            {
                                if (but.Tag.ToString() == "main")
                                {
                                    but.BackColor = SystemConsts.MainButton;
                                }
                            }
                            but.ForeColor = SystemConsts.ButtonText;
                            but.FlatStyle = FlatStyle.Flat;
                            but.FlatAppearance.BorderSize = 0;
                            break;

                        case "Label":
                            var label = (Label)child;
                            if (label.Tag!=null)
                            {
                                var tag = label.Tag.ToString();
                                if (tag == "Smaller")
                                {
                                    label.Font = SystemConsts.SmallerLabel;
                                }
                                else
                                {
                                    label.Font = SystemConsts.MainLabel;
                                }
                            }
                            else
                            {
                                label.Font = SystemConsts.MainLabel;
                            }
                            break;
                    }
                }
            }
        }

        public static void DockWithParent(Control ctrl)
        {
            ctrl.Width = ctrl.Parent.Width;
            ctrl.Height = ctrl.Parent.Height;
            ctrl.Left = 0;
            ctrl.Top = 0;
        }

        public static void SetButton(Button button, bool enabled)
        {
            enum_ButtonType type = enum_ButtonType.Normal;
            if (button.Tag != null)
            {
                if (button.Tag.ToString() == "main")
                {
                    type = enum_ButtonType.Main;
                }
            }
            switch (type)
            {
                case enum_ButtonType.Normal:
                    if (enabled)
                    {
                        button.BackColor = SystemConsts.NormalButton;
                    }
                    else
                    {
                        button.BackColor = SystemConsts.DisabledButton;
                    }
                    break;
                case enum_ButtonType.Main:
                    if (enabled)
                    {
                        button.BackColor = SystemConsts.MainButton;
                    }
                    else
                    {
                        button.BackColor = SystemConsts.DisabledButton;
                    }
                    break;
            }
        }
    }

}
