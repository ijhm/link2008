﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class Floor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public Image MapAreaImage { get; set; }
        public List<Point> Poly = new List<Point>();
        [JsonIgnore]
        public List<Point> ScaledPoly = new List<Point>();

        public string Notes { get; set; }

        public List<RoomMailbox> Rooms { get; set; }
        public List<RoomLinked> LinkedRooms { get; set; }

        public override string ToString() { return Name; }

        public Floor()
        {
            Rooms = new List<Data.RoomMailbox>();
            LinkedRooms = new List<Data.RoomLinked>();
        }
    }
}
