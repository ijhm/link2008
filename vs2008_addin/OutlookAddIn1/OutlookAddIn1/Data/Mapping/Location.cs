﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class Location
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Floor> Floors { get; set; }

        [Obsolete("Booking Items now reside at the region level.", false)]
        public List<BookingItem> BookingItems { get; set; }

        [JsonIgnore]
        public Image MapAreaImage { get; set; }

        public List<Point> Poly = new List<Point>();
        [JsonIgnore]
        public List<Point> ScaledPoly = new List<Point>();

        public string Address { get; set; }
        public string Notes { get; set; }

        public override string ToString()
        {
            return Name;
        }
        public Location()
        {
            Floors = new List<Data.Floor>();
            BookingItems = new List<BookingItem>();
            Poly = new List<Point>();
        }

        [Obsolete("This method dosn't have an implementation", true)]
        public static Location Get(int id)
        {
            return null;
        }

        public List<Pin> GetFloorPins
        {
            get
            {
                List<Pin> pins = new List<Data.Pin>();
                foreach (Floor floor in this.Floors)
                {
                    Pin pin = new Pin
                    {
                        Poly = floor.Poly,
                        MetaId = floor.Id,
                        SelectionType = Pin.enum_SelectionType.Floor
                    };
                    pins.Add(pin);
                }
                return pins;
            }
        }

        [Obsolete("Booking Items now reside at the region level.", false)]
        public string GetFixedAssetList(List<int> itemIds)
        {
            string result = "";
            for (int i = 0; i < BookingItems.Count; i++)
            {
                if (itemIds.Contains(BookingItems[i].Id))
                {
                    result += BookingItems[i].Name + ", ";
                }
            }
            if (result.Length > 0)
            {
                result = result.Substring(0, result.Length - 2);
            }
            return result;
        }
        
    }
}
