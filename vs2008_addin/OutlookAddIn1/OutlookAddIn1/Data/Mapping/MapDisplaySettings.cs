﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data.Mapping
{
    public class MapDisplaySettings
    {
        public bool InvisiblePoly { get; set; }
    }
}
