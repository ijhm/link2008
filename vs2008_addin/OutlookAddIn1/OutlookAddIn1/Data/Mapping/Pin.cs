﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class Pin
    {
        public enum enum_SelectionType
        {
            Estate,
            Location,
            Floor
        }
        public enum_SelectionType SelectionType { get; set; }
        public int MetaId { get; set; }

        [JsonIgnore]
        public GraphicsPath Path = new GraphicsPath();

        public int X { get; set; }
        public int Y { get; set; }
        public int ScaledX { get; set; }
        public int ScaledY { get; set; }
        public List<Point> Poly = new List<Point>();
        [JsonIgnore]
        public List<Point> ScaledPoly = new List<Point>();

        public Point[] PinPoly
        {
            get
            {
                return ScaledPoly.ToArray();
            }
        }

        private Point P(int dx,int dy)
        {
            return new Point(ScaledX + dx, ScaledY + dy);
        }
    }
}
