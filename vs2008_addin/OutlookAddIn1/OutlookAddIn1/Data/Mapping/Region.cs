﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class Region
    {
        private ObservableCollection<BookingItem> _booking_items = new ObservableCollection<BookingItem>();

        public Region() {
            Locations = new List<Data.Location>();
            Poly = new List<Point>();

            _booking_items.CollectionChanged += _booking_items_CollectionChanged;
        }

        private void _booking_items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
            foreach (BookingItem item in e.NewItems) {
                item.Cost = new Currency();
                item.Cost.Symbol = CurrencySymbol;
                item.Cost.HasSymbolOnLeft = HasSymbolOnLeft;
            }
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string CurrencySymbol { get; set; }
        public bool HasSymbolOnLeft { get; set; }
        public List<Location> Locations { get; set; }
        public IList<BookingItem> BookingItems { get { return _booking_items; } }

        [JsonIgnore]
        public GraphicsPath Path = new GraphicsPath();

        public List<Point> Poly = new List<Point>();
        [JsonIgnore]
        public List<Point> ScaledPoly = new List<Point>();
        
        [JsonIgnore]
        public Image MapAreaImage { get; set; }


        public string Address { get; set; }
        public string Notes { get; set; }

        public override string ToString()
        {
            return Name;
        }

        [Obsolete("This method no longer has an implentation", true)]
        public static Region Get(int id)
        {
            return null;
        }

        public List<Pin> GetLocationPins
        {
            get
            {
                List<Pin> pins = new List<Data.Pin>();
                foreach (Location loc in this.Locations)
                {
                    Pin pin = new Pin
                    {
                        Poly = loc.Poly,
                        MetaId=loc.Id,
                        SelectionType = Pin.enum_SelectionType.Location
                    };
                    pins.Add(pin);
                }
                return pins;
            }
        }

        public string GetFixedAssetList(List<int> itemIds)
        {
            string result = string.Empty;
            for (int i = 0; i < BookingItems.Count; i++)
            {
                if (itemIds.Contains(BookingItems[i].Id))
                {
                    result += BookingItems[i].Name + ", ";
                }
            }
            if (result.Length > 0)
            {
                result = result.Substring(0, result.Length - 2);
            }
            return result;
        }
    }
}
