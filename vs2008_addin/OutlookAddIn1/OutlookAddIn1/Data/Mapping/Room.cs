﻿using Newtonsoft.Json;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public abstract class Room
    {
        private Brush Fill_Free;
        private Brush Fill_Booked;

        [JsonIgnore]
        public GraphicsPath Path = new GraphicsPath();
        public int Id { get; set; }
        public string Name { get; set; }
        public string Attendee { get; set; }
        public int Capacitiy { get; set; }
        public int SetupTime { get; set; }
        public int ClearDownTime { get; set; }
        public string AlertEmail { get; set; }
        public List<int> Features { get; set; }
        public List<int> Facilities { get; set; }
        public List<int> Catering { get; set; }
        public List<int> Layout { get; set; }
        public int MinLeadTime { get; set; }

        public List<Point> Poly = new List<Point>();
        [JsonIgnore]
        public List<Point> ScaledPoly = new List<Point>();

        [JsonIgnore]
        public enum_MapAreaStatus Status { get; set; }
        public string Notes { get; set; }
        public string Description { get; set; }
        [JsonIgnore]
        public string FloorName { get; set; }

        public Room()
        {
            Fill_Free = new SolidBrush(Color.FromArgb(70, 100, 200, 100));
            Fill_Booked = new SolidBrush(Color.FromArgb(70, 200, 100, 100));

            Features = new List<int>();
            Facilities = new List<int>();
        }
        public override string ToString()
        {
            return Name;
        }

        public void AddPoint(int x, int y)
        {
            Poly.Add(new Point(x, y));
        }

        public void Setup()
        {
            if (ScaledPoly!=null)
            {
                Path = new GraphicsPath();
                if (ScaledPoly.Count>0)
                {
                    Path.AddPolygon(ScaledPoly.ToArray());
                }
            }
        }


        public string StatusName
        {
            get
            {
                switch (Status)
                {
                    case enum_MapAreaStatus.Free:
                        return Globals.ThisAddIn.resourceManager.GetString("enumMapAreaStatusFree");

                    case enum_MapAreaStatus.Booked:
                        return Globals.ThisAddIn.resourceManager.GetString("enumMapAreaStatusBooked");
                    default:
                        return string.Empty;
                }
            }
        }

        public Brush StatusBrush
        {
            get
            {
                switch (Status)
                {
                    //                    case enum_MapAreaStatus.Selected:
                    //                        return Fill_Free;

                    case enum_MapAreaStatus.Free:
                        return Fill_Free;

                    case enum_MapAreaStatus.Booked:
                        return Fill_Booked;

                    //                    case enum_MapAreaStatus.NotAvailable:
                    //                        return Fill_Booked;

                    default:
                        return null;
                }
            }
        }

        public bool IsTooLate(DateTime startDate)
        {
            bool result = false;
            if (MinLeadTime == 0)
            {
                return false;
            }
            if (startDate <= DateTime.Now)
            {
                return true;
            }
            TimeSpan time = startDate - DateTime.Now;
            if (time.TotalMinutes < MinLeadTime)
            {
                result = true;
            }
            return result;
        }


    }
}
