﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data {
    public class RoomMailbox : Room {
        public string MailBox { get; set; }
    }
}
