﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data {
    [JsonConverter(typeof(SchemaVersionConverter))]
    public class SchemaVersion {

        public SchemaVersion(int major, int minor, int patch) {
            Major = major;
            Minor = minor;
            Patch = patch;
        }

        public int Major { get; set; }
        public int Minor { get; set; }
        public int Patch { get; set; }

        public override bool Equals(object obj) {
            if(object.ReferenceEquals(this, obj)) {
                return true;
            }

            var other = obj as SchemaVersion;
            return Equals(other);
        }

        public bool Equals(SchemaVersion other) {
            if(other == null) {
                return false;
            }

            return (
                this.Major == other.Major &&
                this.Minor == other.Minor &&
                this.Patch == other.Patch
            );
        }

        public override int GetHashCode() {
            return ToString().GetHashCode();
        }

        public override string ToString() {
            return "{Major}.{Minor}.{Patch}";
        }

        public static bool TryParse(string value, out SchemaVersion version) {
            version = null;

            if (string.IsNullOrWhiteSpace(value)) {
                return false;
            }

            var parts = value.Split('.');

            if(parts.Length != 3) {
                return false;
            }

            int major;
            if (int.TryParse(parts[0], out major) == false) {
                return false;
            }

            int minor;
            if (int.TryParse(parts[1], out minor) == false) {
                return false;
            }

            int patch;
            if (int.TryParse(parts[2], out patch) == false) {
                return false;
            }

            version = new SchemaVersion(major, minor, patch);
            return true;
        }

        public static bool operator ==(SchemaVersion left, SchemaVersion right) {
            if(object.ReferenceEquals(left, right)) {
                return true;
            }

            return left.Equals(right);
        }

        public static bool operator !=(SchemaVersion left, SchemaVersion right) {
            if (object.ReferenceEquals(left, right)) {
                return false;
            }

            return !left.Equals(right);
        }

        public static bool operator <(SchemaVersion left, SchemaVersion right) {
            if(left.Major != right.Major) {
                return (left.Major < right.Major);
            }

            if (left.Minor != right.Minor) {
                return (left.Minor < right.Minor);
            }

            if (left.Patch != right.Patch) {
                return (left.Patch < right.Patch);
            }
            
            return false;
        }

        public static bool operator >(SchemaVersion left, SchemaVersion right) {
            if (left.Major != right.Major) {
                return (left.Major > right.Major);
            }

            if (left.Minor != right.Minor) {
                return (left.Minor > right.Minor);
            }

            if (left.Patch != right.Patch) {
                return (left.Patch > right.Patch);
            }

            return false;
        }

        public static bool operator >=(SchemaVersion left, SchemaVersion right) {
            if(left == right) {
                return true;
            }

            return (left > right);
        }

        public static bool operator <=(SchemaVersion left, SchemaVersion right) {
            if (left == right) {
                return true;
            }

            return (left < right);
        }
    }

    public class SchemaVersionConverter : JsonConverter {
        public override bool CanConvert(Type object_type) {
            return (object_type == typeof(SchemaVersion));
        }

        public override object ReadJson(JsonReader reader, Type object_type, object existing_value, JsonSerializer serializer) {
            if((object_type != typeof(SchemaVersion))) {
                return null;
            }

            var version_str = reader.Value as string;

            SchemaVersion version;
            if(SchemaVersion.TryParse(version_str, out version) == false) {
                return null;
            }

            return version;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            var version = value as SchemaVersion;

            if(version == null) {
                return;
            }

            writer.WriteValue(version.ToString());
        }
    }
}
