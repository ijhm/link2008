﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data.Security
{
    public class CIBSecurity
    {
        private static int _iterations = 2;
        private static int _keySize = 256;

        private static string _hash = "SHA1";
        private static string _salt; 
        private static string _vector; 
        private static string _password;

        public CIBSecurity()
        {
            _salt = "kifej56hfycndhr3";
            _vector = "kmvn453hdufnwq89";
            _password = "xsd4982d";
        }


        public string EncryptWebUrlFriendly(string value)
        {
            string result = Encrypt(value);
            var bytes = Encoding.UTF8.GetBytes(result);
            string output = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                output += bytes[i].ToString("D3");
            }
            return output;
        }

        public string DecryptWebUrlFriendly(string value)
        {
            string raw = "";
            for (int i = 0; i < value.Length; i += 3)
            {
                string temp = value.Substring(i, 1);
                temp += value.Substring(i + 1, 1);
                temp += value.Substring(i + 2, 1);
                int tempValue = int.Parse(temp);
                raw += (char)tempValue;
            }
            return Decrypt<AesManaged>(raw);
        }

        public string EncryptBase64(string value)
        {
            string result = Encrypt(value);
            return Base64Encode(result);
        }

        private string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public string Encrypt(string value)
        {
            return Encrypt<AesManaged>(value);
        }
        public string Encrypt<T>(string value)
                where T : SymmetricAlgorithm, new()
        {
            byte[] vectorBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_vector);
            byte[] saltBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_salt);
            byte[] valueBytes = System.Text.UTF8Encoding.UTF8.GetBytes(value);

            byte[] encrypted;
            using (T cipher = new T())
            {
                PasswordDeriveBytes _passwordBytes =
                    new PasswordDeriveBytes(_password, saltBytes, _hash, _iterations);
                byte[] keyBytes = _passwordBytes.GetBytes(_keySize / 8);

                cipher.Mode = CipherMode.CBC;

                using (ICryptoTransform encryptor = cipher.CreateEncryptor(keyBytes, vectorBytes))
                {
                    using (MemoryStream to = new MemoryStream())
                    {
                        using (CryptoStream writer = new CryptoStream(to, encryptor, CryptoStreamMode.Write))
                        {
                            writer.Write(valueBytes, 0, valueBytes.Length);
                            writer.FlushFinalBlock();
                            encrypted = to.ToArray();
                        }
                    }
                }
                cipher.Clear();
            }
            return Convert.ToBase64String(encrypted);
        }

        public string DecryptBase64(string value)
        {
            value = Base64Decode(value);
            return Decrypt<AesManaged>(value);
        }

        public string Decrypt(string value)
        {
            return Decrypt<AesManaged>(value);
        }
        public string Decrypt<T>(string value) where T : SymmetricAlgorithm, new()
        {
            byte[] vectorBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_vector);
            byte[] saltBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_salt);
            byte[] valueBytes = Convert.FromBase64String(value);

            byte[] decrypted;
            int decryptedByteCount = 0;

            using (T cipher = new T())
            {
                PasswordDeriveBytes _passwordBytes = new PasswordDeriveBytes(_password, saltBytes, _hash, _iterations);
                byte[] keyBytes = _passwordBytes.GetBytes(_keySize / 8);

                cipher.Mode = CipherMode.CBC;

                try
                {
                    using (ICryptoTransform decryptor = cipher.CreateDecryptor(keyBytes, vectorBytes))
                    {
                        using (MemoryStream from = new MemoryStream(valueBytes))
                        {
                            using (CryptoStream reader = new CryptoStream(from, decryptor, CryptoStreamMode.Read))
                            {
                                decrypted = new byte[valueBytes.Length];
                                decryptedByteCount = reader.Read(decrypted, 0, decrypted.Length);
                            }
                       }
                    }
                }
                catch (Exception)
                {
                    return String.Empty;
                }

                cipher.Clear();
            }
            return Encoding.UTF8.GetString(decrypted, 0, decryptedByteCount);
        }
    }
}
