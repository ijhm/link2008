﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace OneSpace.OutlookPlugin.Data.Security
{
    public class LicenseKey
    {
        public string ServiceAccount;
        public string Domain;
        public DateTime ExpiryDate;
        public bool ShowEqupimentPrice;

        private const string KeyName = "LICENSEKEY";
        /// <summary>
        /// Returns the AppTitle as the reg key root for the application
        /// </summary>
        private static string SubKey
        {
            get
            {
                return "SOFTWARE\\{Branding.Resource.licensePath}.OutlookPlugin";
            }
        } 
            //= "SOFTWARE\\RedstoneConnectLabs.OutlookPlugin";

        public bool IsInLicense
        {
            get
            {
                return (this.ExpiryDate > DateTime.Now);
            }
        }

        public static LicenseKey ParseKey(string keyJson)
        {
            LicenseKey key = null;
            try
            {
                var settings = new JsonSerializerSettings
                {
                    Error = (sender, args) =>
                    {
                        if (System.Diagnostics.Debugger.IsAttached)
                        {
                            System.Diagnostics.Debugger.Break();
                        }
                    }
                };

                key = Newtonsoft.Json.JsonConvert.DeserializeObject<LicenseKey>(keyJson, settings);
                if (key.Domain!="")
                {
                    return key;
                }
            }
            catch (Exception)
            {
            }
            return key;
        }

        public static LicenseKey GetKey
        {
            get
            {
                string keyJson = LicenseKey.KeyText;
                try
                {
                    var settings = new JsonSerializerSettings
                    {
                        Error = (sender, args) =>
                        {
                            if (System.Diagnostics.Debugger.IsAttached)
                            {
                                System.Diagnostics.Debugger.Break();
                            }
                        }
                    };
                    var key = Newtonsoft.Json.JsonConvert.DeserializeObject<LicenseKey>(keyJson,settings);

                    //ToDo: Figure out configuraiton for show price options
//                    Globals.ThisAddIn._showEquipmentPrice = key.ShowEqupimentPrice;
                    Globals.ThisAddIn._showEquipmentPrice = true;

                    return key;
                }
                catch (Exception)
                {
                }
                return null;
            }
        }

        public static string KeyText
        {
            get
            {

                RegistryKey rk = Registry.CurrentUser;
                RegistryKey sk1 = rk.OpenSubKey(SubKey);
                if (sk1 == null)
                {
                    return null;
                }
                else
                {
                    try {
                        var decodeSecure = new Data.Security.CIBSecurity();
                        var keyJson = (string)sk1.GetValue(KeyName);
                        return decodeSecure.DecryptBase64(keyJson);
                    }
                    catch (Exception)
                    {
                    }
                }
                return null;
            }
        }

        public static bool SetKey(LicenseKey key)
        {
            try
            {
                var decodeSecure = new Data.Security.CIBSecurity();
                RegistryKey rk = Registry.CurrentUser;
                RegistryKey sk1 = rk.CreateSubKey(SubKey);
                var keyJson = Newtonsoft.Json.JsonConvert.SerializeObject(key);

                sk1.SetValue(KeyName, decodeSecure.EncryptBase64(keyJson));

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ClearKey()
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser;
                rk.DeleteSubKey(SubKey);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool InLicense
        {
            get
            {
                var key = GetKey;
                if (key!=null)
                {
                    if (key.ExpiryDate>DateTime.Now)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

    }
}
