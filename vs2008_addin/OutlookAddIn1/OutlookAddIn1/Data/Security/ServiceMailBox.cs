﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class ServiceMailbox
    {
        public string Name;

        /// <summary>
        /// This property is intended to be the email address and will be changed to Email when we schedule in a breaking change.
        /// </summary>
        public string Username;

        public override string ToString()
        {
            return this.Name;
        }
    }
}
