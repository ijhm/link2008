﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Settings
{
    public class SystemConsts
    {
        public static bool EnableServiceMailboxIntegration
        {
            get
            {
                return true;
            }
        }
        
        public static string DateTimeFormat { get { return  "dd MMMM yyyy HH:mm:00"; } }
        public static string OutlookSchema_Image { get {return  "http://schemas.microsoft.com/mapi/proptag/0x37010102"; } }

        //public static Color MainButton => Color.FromArgb(141, 41, 51);
        //public static Color DisabledButton => Color.FromArgb(219, 219, 211);
        //public static Color NormalButton => Color.FromArgb(175, 169, 165);
        //public static Color ActiveButton => Color.FromArgb(198, 59, 73);
        public static Color MainButton => Branding.Resource.PrimaryColour;
        public static Color DisabledButton => Color.FromArgb(219, 219, 211);
        public static Color ActivatedButton => Color.FromArgb(150, 150, 150);
        public static Color NormalButton => Branding.Resource.PrimaryColour;
        public static Color ActiveButton => Color.FromArgb(198, 59, 73);

        public static Color Background => Color.White;
        public static Color ButtonText => Color.White;

        public static Font MainLabel => new Font(new FontFamily("Arial"), 11, FontStyle.Regular, GraphicsUnit.Point);
        public static Font SmallerLabel => new Font(new FontFamily("Arial"), 9, FontStyle.Regular, GraphicsUnit.Point);

        internal const string OVERRIDE_PATH = @"C:\OneSpaceLink\";

        public static string TempPath
        {
            get
            {
                if (Directory.Exists(OVERRIDE_PATH)) {
                    return OVERRIDE_PATH;
                }

                return Path.Combine(Path.GetTempPath(), "CibOutlookPlugin");
            }
        }

        public static bool WriteToDebugLog {
            get { return File.Exists(Path.Combine(TempPath, "debug.inf")); }
        }

    }
}
