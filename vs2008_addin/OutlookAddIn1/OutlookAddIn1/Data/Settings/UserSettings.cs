﻿using OneSpace.OutlookPlugin.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class UserSettings
    {
        public List<string> PreviousCostCentres = new List<string>();

        public static UserSettings Get
        {
            get
            {
                var result = new UserSettings();
                string filePath = Path.Combine(SystemConsts.TempPath, "usersettings.json");
                if (File.Exists(filePath))
                {
                    var json = File.ReadAllText(filePath);
                    result = Newtonsoft.Json.JsonConvert.DeserializeObject<UserSettings>(json);
                }
                return result;
            }
        }

        public static void Set(UserSettings settings)
        {
            string filePath = Path.Combine(SystemConsts.TempPath, "usersettings.json");
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            File.AppendAllText(filePath, settings.ToString());
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
