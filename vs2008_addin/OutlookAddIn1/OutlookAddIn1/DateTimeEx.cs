﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Extentions
{
    internal static class DateTimeEx
    {
        public static string ToOutlookFilter(this DateTime value)
        {

            /* Ref: https://msdn.microsoft.com/en-us/library/office/microsoft.office.interop.outlook._items.restrict.aspx */
            /* Ref: https://msdn.microsoft.com/en-us/VBA/language-reference-vba/articles/format-function-visual-basic-for-applications */
            /* Ref: https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-date-and-time-format-strings */
            /* Ref: https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings */

            /* Note: Based on the above documentation Outlook uses the systems short
             *       date format + the US time format.  I would have prefered them use
             *       the General date/time (Short) format for consistancy.
             * */

            return $"{value:d} {value:h:mm tt}";
        }
    }
}
