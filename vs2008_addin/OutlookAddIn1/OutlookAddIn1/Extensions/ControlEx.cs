﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OneSpace.OutlookPlugin.Extentions {
    internal static class ControlEx {
        public static void Invoke(this Control ctrl, Action action) {
            if (ctrl.InvokeRequired) {
                ctrl.Invoke(action);
            }
            else {
                action();
            }
        }

        public static T Invoke<T>(this Control ctrl, Func<T> func) {
            if (ctrl.InvokeRequired) {
                return (T)ctrl.Invoke(func);
            }
            else {
                return func();
            }
        }
    }
}
