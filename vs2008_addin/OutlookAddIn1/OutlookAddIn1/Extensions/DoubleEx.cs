﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Extentions {
    internal static class DoubleEx {
        public static int ToFlooredInt32(this double value) {
            return (int)Math.Floor(value);
        }
    }
}
