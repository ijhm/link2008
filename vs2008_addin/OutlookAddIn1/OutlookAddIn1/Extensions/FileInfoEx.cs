﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Extentions {
    internal static class FileInfoEx {
        public static Image SafeLoadImage(this FileInfo file_info) {
            if (file_info.Exists) {
                using(var fs = file_info.Open(FileMode.Open)) {
                    return Image.FromStream(fs);
                }
            }

            return null;
        }
    }
}
