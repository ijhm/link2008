﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Extentions {
    internal static class StringEx {
        public static int[] FromJsonToInt32Array(this string value) {
            if (string.IsNullOrWhiteSpace(value)) {
                return new int[] { };
            }

            return JsonConvert.DeserializeObject<int[]>(value) ?? new int[] { };
        }

        public static System.Net.Mail.MailAddress TryConvertToMailAddress(this string value) {
            try {
                if(value?.IndexOf('@') > 0) {
                    return new System.Net.Mail.MailAddress(value);
                }
            }
            catch { }

            return null;
        }
    }
}
