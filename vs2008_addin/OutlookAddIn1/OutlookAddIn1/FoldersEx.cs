﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;

namespace OneSpace.OutlookPlugin.Extentions
{
    internal static class FoldersEx
    {
        public static bool TryGetFolder(this Folders folders, string name, out MAPIFolder folder)
        {

            foreach (MAPIFolder sub_folder in folders)
            {
                if (string.Equals(sub_folder.Name, name, StringComparison.OrdinalIgnoreCase))
                {
                    folder = sub_folder;
                    return true;
                }
            }

            folder = null;
            return false;
        }
    }
}
