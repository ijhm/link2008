﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OneSpace.OutlookPlugin.Settings;

namespace OneSpace.OutlookPlugin.Logging
{
    public class ActivityLog
    {
        public static void WriteToLog(string logEntry)
        {
            if (Globals.ThisAddIn.WriteToDebugLog)
            {
                try
                {

                Trace.WriteLine("[" + logEntry + "]");
                string path = SystemConsts.TempPath;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                    string filePath = path + "\\activity_" + DateTime.Now.ToString("dd-MM-yyyy") + ".log";
                    File.AppendAllText(filePath, DateTime.Now.ToString("HH:mm") + "\t" + logEntry + "\r\n");
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
