﻿using OneSpace.OutlookPlugin.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Reports
{
    public class ReportCollection
    {
        public List<ReportCollectionItem> Items { get; set; }

        public ReportCollection()
        {
            Items = new List<ReportCollectionItem>();
        }

        public void AddKey(string keyValue)
        {
            bool found = false;
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Key == keyValue)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                //Add new item
                Items.Add(new ReportCollectionItem
                {
                    Key = keyValue
                });
            }
        }

        public void AddBooking(string keyValue, string Location, RoomBooking data)
        {
            bool found = false;
            data.LocationName = Location;
            for (int i=0;i<Items.Count;i++)
            {
                if (Items[i].Key==keyValue)
                {
                    Items[i].Meetings.Add(data);
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                //Add new item
                Items.Add(new ReportCollectionItem {
                    Key = keyValue
                });
                Items[0].Meetings.Add(data);
            }
        }
    }

    public class ReportCollectionItem
    {
        public string Key { get; set; }
        public List<RoomBooking> Meetings { get; set; }
        public ReportCollectionItem()
        {
            Meetings = new List<RoomBooking>();
        }

    }
}
