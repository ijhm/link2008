﻿using OneSpace.OutlookPlugin.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Reports
{
    public class ReportEventsFilter1Args
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Region Region { get; set; }
        public Location Location { get; set; }
        public Floor Floor { get; set; }
        public RoomMailbox MeetingRoom { get; set; }
        public string Filter { get; set; }

    }


}
