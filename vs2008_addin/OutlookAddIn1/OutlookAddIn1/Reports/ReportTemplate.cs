﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class ReportTemplates
    {
        public List<ReportTemplate> Templates;
    }

    public class ReportTemplate
    {
        public string Name;
        public string Template;
        public List<TemplateData> Fragments;

        public ReportTemplate()
        {
            Fragments = new List<TemplateData>();
        }

        /*
        public string Render()
        {
            string result = Template;
            foreach (TemplateData data in Fragments)
            {
                result = result.Replace(data.Name, data.Template);
            }
            return result;
        }

        public void MergeField(string fragmentName, string value)
        {
            string fragment = "";
            foreach (var frag in Fragments)
            {
                if (frag.Name == fragmentName)
                {

                }
            }
        }
        */

        public string GetFragment(string fragmentName)
        {
            foreach (var frag in Fragments)
            {
                if (frag.Name == fragmentName)
                {
                    return frag.Template;
                }
            }
            return "";
        }

    }

    public class TemplateData
    {
        public string Name;
        public string Template;
        public string Temp;
    }
}
