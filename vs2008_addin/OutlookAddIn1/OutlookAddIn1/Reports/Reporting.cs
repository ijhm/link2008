﻿using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;
using OneSpace.OutlookPlugin.Extentions;
using OneSpace.OutlookPlugin.Data.Booking;
using OneSpace.OutlookPlugin.Properties;
using System.Threading;
using System.Runtime.InteropServices;

namespace OneSpace.OutlookPlugin.Reports
{
    public class Reporting
    {
//        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Tools.Office.ProgrammingModel.dll", "14.0.0.0")]
//        internal Microsoft.Office.Interop.Outlook.Application Application;
        private string[] templateTags = { "[building_header]", "[floor_header]", "[room_detail]", "[meeting]" };

        public List<RoomWithBookings> roomList;
        public DateTime dateStart;
        public DateTime dateEnd;
        public Location location;
        public Floor floor;
        public RoomMailbox room;
        public ReportTemplate printTemplate;
        public string meeting;
        public string tempPath;
        public string report = string.Empty;
        public string html = string.Empty;
        public string filterText = string.Empty;
        public string xlsx = string.Empty;
        protected string xlsx_error = string.Empty;

        public virtual void GenerateReport(Location _location,Floor _floor,RoomMailbox _room,DateTime fromDate,DateTime toDate,string filter)
        {
            location = _location;
            floor = _floor;
            room = _room;
            dateStart = fromDate;
            dateEnd = toDate;
            filterText = filter;
        }

        public void SaveXlsx(string new_file_path) {
            if (string.IsNullOrWhiteSpace(xlsx)) {
                throw new Exception("{Globals.ThisAddIn.resourceManager.GetString(errorGenerateReportFirst)}\r\n{xlsx_error}");
            }
            try
            {
                File.Copy(xlsx, new_file_path, true);
            }
            catch (Exception ex)
            {
                ErrorDialog dialog;
                if (ex.Message.Contains("because it is being used by another process."))
                {
                    dialog = new ErrorDialog(
                        Globals.ThisAddIn.resourceManager.GetString("fileDialogInUseTitle"),
                        Globals.ThisAddIn.resourceManager.GetString("fileDialogInUseError")
                        );
                }
                else
                {
                    dialog = new ErrorDialog(
                        Globals.ThisAddIn.resourceManager.GetString("fileDialogErrorTitle"),
                        Globals.ThisAddIn.resourceManager.GetString("fileDialogErrorMessage")
                        );
                }
                dialog.ShowDialog();
            }
        }

        public void ProcessTemplate()
        {
            for (int i = 0; i < templateTags.Length; i++)
            {
                var fragment = ReportingHelper.GetFragment(printTemplate.Template, templateTags[i], templateTags[i].Replace("[", "[/"));
                printTemplate.Fragments.Add(new TemplateData { Name = templateTags[i], Template = fragment });
            }
        }

        public void ProcessFilters()
        {
            roomList = new List<RoomWithBookings>();

            if (floor == null)
            {
                foreach (Floor floor in location.Floors)
                {
                    foreach (RoomMailbox room in floor.Rooms)
                    {
                        roomList.Add(new RoomWithBookings { Id = room.Id, Name = room.Name, MailBox = room.MailBox });
                    }
                }
            }
            else
            {
                if (room == null)
                {
                    foreach (RoomMailbox room in floor.Rooms)
                    {
                        roomList.Add(new RoomWithBookings { Id = room.Id, Name = room.Name, MailBox = room.MailBox });
                    }
                }
                else
                {
                    roomList.Add(new RoomWithBookings { Id = room.Id, Name = room.Name, MailBox = room.MailBox });
                }
            }
        }

        public void ScanRooms(bool returnOnlyBookedRooms) {
            var rooms_appointments = GetAppointments(roomList, dateStart, dateEnd);

            foreach(var room in roomList) {
                var appointments = rooms_appointments[room];
                foreach(var appointment in appointments) {
                    int location_id;
                    var location_id_str = OutlookUtilities.GetPropertyText(appointment, AppointmentSchemaEx.RoomBookingLocationText);
                    if (int.TryParse(location_id_str, out location_id) == false) {
                        location_id = 0;
                    }
                    
                    var order = new AppointmentData();
                    try {
                        var temp_order = new AppointmentData((string)appointment.UserProperties[AppointmentSchemaEx.RoomBookingDataUpdated].Value);
                        for (int n = 0; n < temp_order.Items.Count; n++) {
                            var bookingItem = Globals.ThisAddIn.OneSpaceData.GetBookingItem(location_id, temp_order.Items[n].ItemId);
                            if (!Globals.ThisAddIn.ShowEquipmentPrice && bookingItem.ItemType == BookingItem.enum_ItemType.Facilities) {
                                temp_order.Items[n].ItemCost.Value = 0;
                            }
                            else
                            {
                                temp_order.Items[n].ItemCost = bookingItem.Cost;
                            }
                        }

                        order = temp_order;
                    }
                    catch {
                    }

                    var booking = new RoomBooking {
                        Id = appointment.GlobalAppointmentID,
                        DateFrom = appointment.Start,
                        DateTo = appointment.End,
                        Subject = appointment.Subject,
                        Data = order,
                        LocationId = location_id,
                        UpdatedDateFrom = appointment.Start,
                        UpdatedDateTo = appointment.End,
                        UpdatedDuration = appointment.Duration, /* <-- Note: This value is in Minutes already :) */
                        Location = appointment.Location,
                        OrganiserName = appointment.Organizer ?? string.Empty,
                        appointmentItem = appointment,
                        Attendees = appointment.RequiredAttendees /* Todo: (Rob) - Varify if this value excludes the organiser. */
                    };

            //        Marshal.ReleaseComObject(appointment);

                    room.Bookings.Add(booking);
                }
            }
            
            if (returnOnlyBookedRooms) {
                List<RoomWithBookings> filteredRoomList = new List<RoomWithBookings>();

                for (int i = 0; i < roomList.Count; i++) {
                    if (roomList[i].Bookings.Count > 0) {
                        filteredRoomList.Add(roomList[i]);
                    }
                }

                roomList = filteredRoomList;
            }

        }
        
        private Dictionary<Room, List<Outlook.AppointmentItem>> GetAppointments(List<RoomWithBookings> rooms, DateTime begins_on, DateTime ends_on)
        {
            var wait_obj = new object();
            var is_complete = false;
            var result = new Dictionary<Room, List<Outlook.AppointmentItem>>();
            var exceptions = new List<Exception>();

            var ex_handler = new Action<Exception, Room>((ex, room) => {
                var mailbox = (room is RoomMailbox) ? ((RoomMailbox)room)?.MailBox : "Linked Room";
                var message = @"An exception occured when processing '{room?.Name}'";
                exceptions.Add(new Exception(message, ex));
            });
            
            var ns = Globals.ThisAddIn.Application.Session;
            
            foreach (var room in rooms)
            {
                try
                {
                    var room_account = ns.CreateRecipient(room.MailBox);
                    var room_calendar = ns.GetSharedDefaultFolder(room_account, Outlook.OlDefaultFolders.olFolderCalendar);

                    if (room_calendar.InAppFolderSyncObject == false)
                    {
                        room_calendar.InAppFolderSyncObject = true;
                    }

                    Globals.ThisAddIn.Search(
                        room_calendar,
                        string.Format("(\"urn:schemas:calendar:dtstart\" >= '{0}' AND \"urn:schemas:calendar:dtend\" <= '{1}')",begins_on.ToOutlookFilter(),ends_on.ToOutlookFilter()),
                        false,
                        (s) =>
                        {
                            try
                            {
                                var room_calendar_items = s.Results;
                                room_calendar_items.Sort("[Start]", false);

                                var items = new List<Outlook.AppointmentItem>();
                                foreach (Outlook.AppointmentItem room_calendar_item in room_calendar_items)
                                {
                                    /* Note: For some reason it was quicker to get the properties here in this way and allow the Outlook API to cache the values then call it later when needed. */
                                    room_calendar_item.PropertyAccessor.GetProperties(new[] {
                                        "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/RoomBookingDataUpdated/0x0000001F",
                                        "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/RoomBookingLocationText/0x0000001F"
                                    });
                                    items.Add(room_calendar_item);
                                }

                                result.Add(room, items);
                            }
                            catch (Exception ex)
                            {
                                ex_handler(ex, room);
                            }

                            if (result.Count >= (rooms.Count - exceptions.Count))
                            {
                                lock (wait_obj)
                                {
                                    is_complete = true;
                                    Monitor.Pulse(wait_obj);
                                }
                            }
                        }
                    );
                }
                catch (Exception ex)
                {
                    ex_handler(ex, room);
                }
            }

            lock (wait_obj)
            {
                if (is_complete == false)
                {
                    Monitor.Wait(wait_obj);
                }
            }

            if (exceptions.Count > 0)
            {
                throw new AggregateException(
                    string.Format("{0} exception(s) occured whilst getting appointments for a number of rooms, please see the inner exception(s) for details", exceptions.Count), exceptions);
            }

            return result;
        }

        protected TemplateData GetOrderTable(string html, Location location, RoomBooking booking)
        {
            TemplateData result = new OneSpace.OutlookPlugin.Data.TemplateData();

            result.Temp = string.Empty;

            var region = Globals.ThisAddIn.OneSpaceData.GetRegionFromLocationId(location.Id);
            var analysis = new AppointmentAnalysis(booking.appointmentItem, region);

            var BookingItems = region.BookingItems;
            if (booking.Data.Items.Count==0)
            {
                result.Temp += result.Template;
                result.Temp = result.Temp.Replace("[service]", Globals.ThisAddIn.resourceManager.GetString("reportNoServicesOrdered"));
                result.Temp = result.Temp.Replace("[SERVICE]", Globals.ThisAddIn.resourceManager.GetString("reportNoServicesOrdered"));
                result.Temp = result.Temp.Replace("[cost]", string.Empty);
                result.Temp = result.Temp.Replace("[COST]", string.Empty);
                result.Temp = result.Temp.Replace("[qty]", string.Empty);
                result.Temp = result.Temp.Replace("[QTY]", string.Empty);
                result.Temp = result.Temp.Replace("[sub]", string.Empty);
                result.Temp = result.Temp.Replace("[SUB]", string.Empty);
            }
            foreach (var item in booking.Data.Items)
            {
                result.Temp += result.Template;
                foreach (BookingItem testItem in BookingItems)
                {
                    if (testItem.Id == item.ItemId)
                    {
                        result.Temp = result.Temp.Replace("[service]", testItem.Name);
                        result.Temp = result.Temp.Replace("[SERVICE]", testItem.Name);
                        result.Temp = result.Temp.Replace("[cost]", testItem.CostToCurrencyHtmlString());
                        result.Temp = result.Temp.Replace("[COST]", testItem.CostToCurrencyHtmlString());
                    }
                }
                result.Temp = result.Temp.Replace("[qty]", item.Qty.ToString());
                result.Temp = result.Temp.Replace("[QTY]", item.Qty.ToString());

                result.Temp = result.Temp.Replace("[sub]", item.SubTotal(analysis).ToHtmlString());
                result.Temp = result.Temp.Replace("[SUB]", item.SubTotal(analysis).ToHtmlString());
            }
            return result;
        }
    }
}
