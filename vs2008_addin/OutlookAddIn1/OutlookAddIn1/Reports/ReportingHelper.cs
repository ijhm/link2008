﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Data
{
    public class ReportingHelper
    {
        public static string MakeTempFile(string fileName, string fileContents)
        {
            string tempPath = Path.GetTempPath() + fileName + ".mht";
            if (System.IO.File.Exists(tempPath))
            {
                System.IO.File.Delete(tempPath);
            }
            return tempPath;
        }

        /// <summary>
        /// Find a tag in some HTML and find the enclosing 'p /p' tags then replace that with the tag name provided to allow for
        /// more accurate replacing of data
        /// </summary>
        /// <param name="template"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public static string FilterTemplate(string template, string tagName)
        {
            int pos = template.IndexOf(tagName);
            if (pos > -1)
            {
                int start = 0;
                int end = 0;
                while (pos > 0)
                {
                    if (template.Substring(pos, 2) == "<p" || template.Substring(pos, 2) == "<P")
                    {
                        start = pos;
                        break;
                    }
                    pos--;
                }

                while (pos > 0)
                {
                    if (template.Substring(pos, 3) == "</p" || template.Substring(pos, 3) == "</P")
                    {
                        end = pos;
                        break;
                    }
                    pos++;
                }

                string row = template.Substring(start, end - start + 5);
                template = template.Replace(row, tagName);
            }
            return template;
        }

        /// <summary>
        /// Extract the fragment of HTML from the template bounding by start and end tags
        /// </summary>
        /// <param name="template"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public static string GetFragment(string template, string startTag, string endTag)
        {
            int start = template.IndexOf(startTag);
            int end = template.IndexOf(endTag);
            string fragment = string.Empty;
            if (start > -1 && end > start)
            {
                fragment = template.Substring(start, (end - start) + endTag.Length);
            }
            fragment = fragment.Replace(startTag, string.Empty);
            fragment = fragment.Replace(endTag, string.Empty);
            return fragment;
        }

        public static string ClearBetweenTags(string html, string startTag, string endTag)
        {
            string result = html;
            int pos = html.IndexOf(startTag);
            if (pos > -1)
            {
                pos += startTag.Length;
                int pos2 = html.IndexOf(endTag);
                pos2 += endTag.Length;
                result = result.Substring(0, pos) + result.Substring(pos2);
            }
            return result;
        }


    }
}
