﻿using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSpace.OutlookPlugin.Data.Booking;
using OneSpace.OutlookPlugin.Settings;
using OneSpace.OutlookPlugin.Properties;
using OneSpace.OutlookPlugin.Extentions;

namespace OneSpace.OutlookPlugin.Reports {
    public class Reporting_Bookings : Reporting {
        public override void GenerateReport(Location _location, Floor _floor, RoomMailbox _room, DateTime fromDate, DateTime toDate, string filter) {
            location = _location;
            floor = _floor;
            room = _room;
            dateStart = fromDate;
            dateEnd = toDate.AddMinutes(1439);

            tempPath = Path.Combine(SystemConsts.TempPath, "OneSpace_PrintTemplate_" + Guid.NewGuid().ToString() + ".htm");
            xlsx = Path.ChangeExtension(tempPath, "xlsx");

            if (System.IO.File.Exists(tempPath)) {
                System.IO.File.Delete(tempPath);
            }

            if (System.IO.File.Exists(xlsx)) {
                System.IO.File.Delete(xlsx);
            }

            printTemplate = new ReportTemplate();
            printTemplate.Template = Branding.Resource.reportBookings;

            ProcessFilters();
            ScanRooms(true);
            ProcessTemplate();

            GenerateHtml();
            GenerateXlsx();
        }

        private void GetFloorsAndRooms(out List<Floor> floors, out List<Room> rooms) {
            floors = new List<Floor>();
            rooms = new List<Data.Room>();

            if (floor == null) {
                foreach (Floor floor in location.Floors) {
                    floors.Add(floor);
                    foreach (RoomMailbox room in floor.Rooms) {
                        foreach (var temp in roomList) {
                            if (temp.Id == room.Id) {
                                rooms.Add(room);
                                break;
                            }
                        }
                    }
                }
            }
            else {
                floors.Add(floor);
                if (room == null) {
                    foreach (RoomMailbox room in floor.Rooms) {
                        foreach (var temp in roomList) {
                            if (temp.Id == room.Id) {
                                rooms.Add(room);
                                break;
                            }
                        }
                    }
                }
                else {
                    foreach (var temp in roomList) {
                        if (temp.Id == room.Id) {
                            rooms.Add(room);
                            break;
                        }
                    }
                }
            }

            floors = floors.OrderBy(e => e.Name).ToList();
        }

        private void GenerateHtml() {

            var buildingFrag = printTemplate.GetFragment("[building_header]");
            var floorFrag = printTemplate.GetFragment("[floor_header]");
            var roomFrag = printTemplate.GetFragment("[room_detail]");
            var meetingFrag = printTemplate.GetFragment("[meeting]");

            printTemplate.Template = ReportingHelper.ClearBetweenTags(printTemplate.Template, "[building_header]", "[/meeting]");

            html += buildingFrag.Replace("[building]", location.Name);

            List<Floor> floorList = null;
            List<Room> rooms = null;
            GetFloorsAndRooms(out floorList, out rooms);

            string itemRow = @"<tr>
                <td>[service]</td>
                <td>[qtytype]</td>
                <td>[cost]</td>
                <td>[qty]</td>
                <td>[sub]</td>
                    </tr>";
            string deliveryRow = string.Format(@"<tr>
                <td colspan='4' style='padding-left:50px;font-size: 11.0pt;'>{0}:[delivery] {1}:[clearaway]</td>
                <td>&nbsp;</td>
                    </tr>",Globals.ThisAddIn.resourceManager.GetString("reportLableDelivery"),Globals.ThisAddIn.resourceManager.GetString("reportLableClearaway"));
            string notesRow = @"<tr>
                <td colspan='4' style='padding-left:100px;font-size: 10.0pt;'>[notes]</td>
                <td>&nbsp;</td>
                    </tr>";

            #region All floors
            bool firstMeeting = true;
            foreach (Floor floor in floorList) {
                html += floorFrag;
                html = html.Replace("[floor]", floor.Name);

                foreach (RoomMailbox room in floor.Rooms) {
                    html += roomFrag;
                    html = html.Replace("[room]", room.Name);

                    foreach (var bookingSet in roomList) {
                        if (bookingSet.Id == room.Id) {
                            if (bookingSet.Bookings.Count == 0) {
                                html = ReportingHelper.ClearBetweenTags(html, "[meeting]", "[/meeting]");
                                html = html.Replace("[meeting]", string.Empty);
                                //html += "<p>No bookings found within the selected period</p>";
                            }
                            else {
                                firstMeeting = true;
                                foreach (var booking in bookingSet.Bookings) {

                                    var region = Globals.ThisAddIn.OneSpaceData.GetRegionFromLocationId(location.Id);
                                    var analysis = new AppointmentAnalysis(booking.appointmentItem, region);
                                    var BookingItems = region.BookingItems;

                                    html += meetingFrag;
                                    html = html.Replace("[from]", booking.DateFrom.ToString("dd MMMM yyyy HH:mm"));
                                    html = html.Replace("[to]", booking.DateTo.ToString("dd MMMM yyyy HH:mm"));
                                    if (firstMeeting) {
                                        html = html.Replace("[meeting_top]", string.Empty);
                                        firstMeeting = false;
                                    }
                                    else {
                                        html = html.Replace("[meeting_top]", "padding-top:50px;");
                                    }
                                    TimeSpan ts = booking.DateTo - booking.DateFrom;
                                    html = html.Replace("[duration]", ts.ToFriendlyString());
                                    html = html.Replace("[organiser]", booking.OrganiserName);
                                    if (booking.Attendees == string.Empty) {
                                        html = html.Replace("[attendees]", Globals.ThisAddIn.resourceManager.GetString("reportLableNone"));
                                    }
                                    else {
                                        html = html.Replace("[attendees]", booking.Attendees);
                                    }
                                    html = html.Replace("[costcentre]", booking.Data.CostCentre);
                                    html = html.Replace("[subject]", booking.Subject);

                                    string rows = string.Empty;

                                    foreach (var item in booking.Data.Items) {
                                        rows += itemRow;
                                        foreach (BookingItem testItem in BookingItems) {
                                            if (testItem.Id == item.ItemId) {
                                                rows = rows.Replace("[service]", testItem.Name);
                                                rows = rows.Replace("[qtytype]", testItem.CostUnit);
                                                rows = rows.Replace("[cost]", testItem.CostToCurrencyHtmlString());
                                            }
                                        }


                                        rows = rows.Replace("[qty]", item.Qty.ToString());
                                        rows = rows.Replace("[sub]", item.SubTotal(analysis).ToString());

                                        if (item.CalculatedDelivery != booking.DateFrom || item.CalculatedClearAway != booking.DateTo) {
                                            rows += deliveryRow;
                                            rows = rows.Replace("[delivery]", item.CalculatedDelivery.ToString("HH:mm"));
                                            rows = rows.Replace("[clearaway]", item.CalculatedClearAway.ToString("HH:mm"));
                                        }

                                        if (item.Notes != string.Empty) {
                                            rows += notesRow.Replace("[notes]", item.Notes);
                                        }
                                    }



                                    string total = string.Format("<tr><td colspan=\"4\">{0}</td><td>{1}</td>",Globals.ThisAddIn.resourceManager.GetString("reportLableTotal"),booking.Data.TotalCost(analysis).ToHtmlString());
                                    if (booking.Data.Items.Count > 0) 
                                    {
                                        html = html.Replace("[total]", total);
                                    }
                                    else 
                                    {
                                        html = html.Replace("[total]",string.Format("<tr><td>{0}</td></tr>",Globals.ThisAddIn.resourceManager.GetString("reportNoServicesAdded")));
                                    }

                                    html = html.Replace("[rows]", rows);


                                    html = html.Replace("[total]", booking.Data.TotalCost(analysis).ToHtmlString());
                                    html = html.Replace("[TOTAL]", booking.Data.TotalCost(analysis).ToHtmlString());

                                }
                            }
                        }
                    }
                }
            }
            #endregion


            printTemplate.Template = printTemplate.Template.Replace("[building_header]", html);
            printTemplate.Template = printTemplate.Template.Replace("[/room_detail]", string.Empty);

            printTemplate.Template = printTemplate.Template.Replace("_detail]or_header]r_header]lding_header]>", string.Empty);
            printTemplate.Template = printTemplate.Template.Replace("[building_header]", string.Empty);

            System.IO.File.AppendAllText(tempPath, printTemplate.Template);
        }

        private void GenerateXlsx() {
            #region [ SQL_INSERT_REPORT_DATA ]

            const string SQL_INSERT_REPORT_DATA =
@"INSERT INTO
    [Report$] ([Report], [Building], [Floor], [Room], [Subject], [Scheduled From], [Scheduled To], [Organiser], [Attendees], [Cost Centre], [Service], [Delivery], [Clear away], [Notes], [Qty Type], [Cost], [Qty], [Sub Total])
VALUES
    (@Report, @Building, @Floor, @Room, @Subject, @ScheduledFrom, @ScheduledTo, @Organiser, @Attendees, @CostCentre, @Service, @Delivery, @Clearaway, @Notes, @QtyType, @Cost, @Qty, @SubTotal)";

            #endregion

            try {
                var temp_file = Path.Combine(SystemConsts.TempPath, "OneSpace_PrintTemplate_" + Guid.NewGuid().ToString() + ".xlsx");

                if (System.IO.File.Exists(temp_file)) {
                    System.IO.File.Delete(temp_file);
                }

                var temp_file_info = new FileInfo(temp_file);
                using (var temp_file_stream = temp_file_info.Create()) {
                    temp_file_stream.Write(
                        Properties.Resources.BookingSummaryTemplate,
                        0,
                        Properties.Resources.BookingSummaryTemplate.Length
                    );
                }

                List<Floor> floors = null;
                List<Room> rooms = null;
                GetFloorsAndRooms(out floors, out rooms);

                var region = Globals.ThisAddIn.OneSpaceData.GetRegionFromLocationId(location.Id);

                var single_blank_order_items = new List<OrderItem>(new[] {
                    new OrderItem() {
                        ItemId = -1,
                        ServiceAppointmentId = string.Empty,
                        ServiceState = OrderItem.enum_OrderServiceState.Normal,
                        Start = DateTime.MinValue,
                        End = DateTime.MinValue,
                        DeliveryTimeOffset = 0,
                        ClearAwayTimeOffset = 0,
                        ItemCost = new Currency() { Symbol = region.CurrencySymbol, HasSymbolOnLeft = region.HasSymbolOnLeft, Value = 0.0m },
                        Qty = 0,
                        Notes = string.Empty,
                    }
                });

                var blank_booking_item = new BookingItem() {
                    Id = -1,
                    Name = string.Empty,
                    Description = string.Empty,
                    AlertEmail = string.Empty,
                    ItemType = BookingItem.enum_ItemType.None,
                    Icon = string.Empty,
                    SetupTime = 0,
                    ClearDownTime = 0,
                    MinLeadTime = 0,
                    QtyType = BookingItem.enum_QtyType.PerSession,
                    MaxQty = 0,
                    Cost = new Currency() { Symbol = region.CurrencySymbol, HasSymbolOnLeft = region.HasSymbolOnLeft, Value = 0.0m },
                };

                var report_name = Branding.Resource.xlsReportSummary;
                var building_name = location.Name ?? string.Empty;


                using (var excel_data = new ExcelData(temp_file_info)) {
                    foreach (var floor in floors) {
                        var floor_name = floor.Name;
                        var floor_rooms = floor.Rooms.OrderBy(e => e.Name).ToList();

                        foreach (var room in floor_rooms) {
                            var room_name = room.Name;

                            var room_with_booking = (
                                from r in roomList
                                where r.Id == room.Id
                                select r
                            ).FirstOrDefault();

                            if (room_with_booking != null) {
                                foreach (var booking in room_with_booking.Bookings) {

                                    var subject = booking.Subject ?? string.Empty;
                                    var scheduled_from = $"{booking.DateFrom:F}";
                                    var scheduled_to = $"{booking.DateTo:F}";
                                    var organiser = booking.OrganiserName ?? string.Empty;
                                    var attendees = booking.Attendees ?? string.Empty;
                                    var cost_centre = booking.Data.CostCentre ?? string.Empty;

                                    var order_items = (booking.Data.Items.Count > 0) ? booking.Data.Items : single_blank_order_items;

                                    foreach (var order_item in order_items) {
                                        var analysis = new AppointmentAnalysis(booking.appointmentItem, region);

                                        var booking_item = (
                                            from b in region.BookingItems
                                            where b.Id == order_item.ItemId
                                            select b
                                        ).FirstOrDefault() ?? blank_booking_item;

                                        var is_not_blank_order_item = (booking_item != null);

                                        var delivery_time = (is_not_blank_order_item) ? order_item.CalculatedDelivery.ToString("HH:mm") : string.Empty;
                                        var clear_away_time = (is_not_blank_order_item) ? order_item.CalculatedClearAway.ToString("HH:mm") : string.Empty;
                                        var item_cost = (is_not_blank_order_item) ? booking_item.CostToCurrencyString() : string.Empty;
                                        var qty_type = (is_not_blank_order_item) ? booking_item.QtyType.ToString() : string.Empty;
                                        var qty = (is_not_blank_order_item) ? order_item.Qty.ToString() : string.Empty;
                                        var sub_total_str = string.Empty;

                                        if (is_not_blank_order_item) {
                                            var sub_total = order_item.SubTotal(analysis);
                                            sub_total_str = sub_total.ToString();
                                        }

                                        /* Note: Keep the parameters in the same order as the query */
                                        /* Ref: https://blogs.msdn.microsoft.com/wriju/2008/01/24/ado-net-oledbcommand-parameterized-query-sequence-rule/ */
                                        excel_data.ExecuteNonQuery(
                                            SQL_INSERT_REPORT_DATA,
                                            System.Data.CommandType.Text,
                                            excel_data.CreateParameter("@Report", report_name),
                                            excel_data.CreateParameter("@Building", building_name),
                                            excel_data.CreateParameter("@Floor", floor_name),
                                            excel_data.CreateParameter("@Room", room_name),
                                            excel_data.CreateParameter("@Subject", subject),
                                            excel_data.CreateParameter("@ScheduledFrom", scheduled_from),
                                            excel_data.CreateParameter("@ScheduledTo", scheduled_to),
                                            excel_data.CreateParameter("@Organiser", organiser),
                                            excel_data.CreateParameter("@Attendees", attendees),
                                            excel_data.CreateParameter("@CostCentre", cost_centre),
                                            excel_data.CreateParameter("@Service", booking_item.Name),
                                            excel_data.CreateParameter("@Delivery", delivery_time),
                                            excel_data.CreateParameter("@Clearaway", clear_away_time),
                                            excel_data.CreateParameter("@Notes", order_item.Notes),
                                            excel_data.CreateParameter("@QtyType", qty_type),
                                            excel_data.CreateParameter("@Cost", item_cost),
                                            excel_data.CreateParameter("@Qty", qty),
                                            excel_data.CreateParameter("@SubTotal", sub_total_str)
                                        );
                                    }
                                }
                            }
                        }
                    }
                }

                /* Rename the file only once the data has been completely added so users don't get partial data if somthing goes wrong. */
                File.Move(temp_file, xlsx);
            }
            catch (Exception ex) {
                xlsx = string.Empty;
                xlsx_error = ex.Message;
            }
        }
    }
}