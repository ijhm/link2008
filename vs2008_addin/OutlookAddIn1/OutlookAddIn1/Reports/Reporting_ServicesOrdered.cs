﻿using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Data.Booking;
using OneSpace.OutlookPlugin.Properties;
using OneSpace.OutlookPlugin.Settings;
using OneSpace.OutlookPlugin.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSpace.OutlookPlugin.Extentions;

namespace OneSpace.OutlookPlugin.Reports
{
    public class Reporting_ServicesOrdered : Reporting
    {
        public override void GenerateReport(Location _location, Floor _floor, RoomMailbox _room, DateTime fromDate, DateTime toDate, string filter) {
            location = _location;
            floor = _floor;
            room = _room;
            dateStart = fromDate;
            dateEnd = toDate.AddMinutes(1439);

            tempPath = Path.Combine(SystemConsts.TempPath, "OneSpace_PrintTemplate_" + Guid.NewGuid().ToString() + ".htm");
            xlsx = Path.ChangeExtension(tempPath, "xlsx");

            if (System.IO.File.Exists(tempPath)) {
                System.IO.File.Delete(tempPath);
            }

            if (System.IO.File.Exists(xlsx)) {
                System.IO.File.Delete(xlsx);
            }

            ProcessFilters();
            ScanRooms(true);

            var report_collection = GetReportCollection(filter);

            GenerateHtml(report_collection);
            GenerateXlsx(report_collection);
        }
        
        private ReportCollection GetReportCollection(string filter) {
            var filter_alert_email = string.Empty;
            if (string.IsNullOrWhiteSpace(filter)) {
                filter_alert_email = (
                    from m in Globals.ThisAddIn.OneSpaceData.ServiceMailboxes
                    where m.Name.Equals(filter, StringComparison.InvariantCultureIgnoreCase)
                    select m.Username
                ).FirstOrDefault() ?? string.Empty;
            }

            var collection = new ReportCollection();
            foreach (var item in roomList) {
                foreach (var booking in item.Bookings) {
                    foreach (var orderItem in booking.Data.Items) {
                        var region = Globals.ThisAddIn.OneSpaceData.GetRegionFromLocationId(booking.LocationId);
                        var bookableItem = Globals.ThisAddIn.OneSpaceData.GetBookingItem(region.Id, orderItem.ItemId);
                        if (bookableItem != null) {
                            bool addProvider = true;
                            if (bookableItem.AlertEmail == string.Empty) {
                                addProvider = false;
                            }
                            if (filter_alert_email != string.Empty) {
                                if (filter_alert_email != bookableItem.AlertEmail) {
                                    addProvider = false;
                                }
                            }
                            if (addProvider) {
                                collection.AddKey(bookableItem.AlertEmail);
                            }
                        }
                    }
                }
            }

            for (int c = 0; c < collection.Items.Count; c++) {
                foreach (var item in roomList) {
                    foreach (var booking in item.Bookings) {
                        List<OrderItem> items = new List<Data.OrderItem>();
                        foreach (var orderItem in booking.Data.Items) {
                            var region = Globals.ThisAddIn.OneSpaceData.GetRegionFromLocationId(booking.LocationId);
                            var bookableItem = Globals.ThisAddIn.OneSpaceData.GetBookingItem(region.Id, orderItem.ItemId);
                            if (bookableItem != null) {
                                if (collection.Items[c].Key == bookableItem.AlertEmail) {
                                    items.Add(orderItem);
                                }
                            }
                        }
                        if (items.Count > 0) {
                            booking.Data.Items = items;
                            collection.Items[c].Meetings.Add(booking);
                        }
                    }
                }
            }

            return collection;
        }

        private void GenerateHtml(ReportCollection report_collection) {
            var html = Branding.Resource.reportServicesOrdered;
            var header = ReportingHelper.GetFragment(html, "[service_detail]", "[/service_detail]");
            var meetingFrag = ReportingHelper.GetFragment(html, "[meeting]", "[/meeting]");
            html = html.Replace(header, "[html]");
            html = html.Replace(meetingFrag, string.Empty);

            html = html.Replace("[service_detail]", string.Empty);
            html = html.Replace("[/service_detail]", string.Empty);
            html = html.Replace("[meeting]", string.Empty);
            html = html.Replace("[/meeting]", string.Empty);

            var result = string.Empty;

            for (int c = 0; c < report_collection.Items.Count; c++) {
                foreach (var item in roomList) {
                    foreach (var booking in item.Bookings) {
                        List<OrderItem> items = new List<Data.OrderItem>();
                        foreach (var orderItem in booking.Data.Items) {
                            var region = Globals.ThisAddIn.OneSpaceData.GetRegionFromLocationId(booking.LocationId);
                            var bookableItem = Globals.ThisAddIn.OneSpaceData.GetBookingItem(region.Id, orderItem.ItemId);
                            if (bookableItem != null) {
                                if (report_collection.Items[c].Key == bookableItem.AlertEmail) {
                                    items.Add(orderItem);
                                }
                            }
                        }
                        if (items.Count > 0) {
                            booking.Data.Items = items;
                            report_collection.Items[c].Meetings.Add(booking);
                        }
                    }
                }
            }

            string itemRow = @"<tr>
                <td>[service]</td>
                <td>[cost]</td>
                <td>[qty]</td>
                <td>[sub]</td>
                    </tr>";
            string deliveryRow = @"<tr>
                <td colspan='3' style='padding-left:50px;font-size: 11.0pt;'>{Globals.ThisAddIn.resourceManager.GetString(reportLableDelivery)}:[delivery] {Globals.ThisAddIn.resourceManager.GetString(reportLableClearaway)}:[clearaway]</td>
                <td>&nbsp;</td>
                    </tr>";
            string notesRow = @"<tr>
                <td colspan='3' style='padding-left:100px;font-size: 10.0pt;'>[notes]</td>
                <td>&nbsp;</td>
                    </tr>";


            foreach (var provider in report_collection.Items) {
                if (provider.Meetings.Count > 0) {
                    result += header.Replace("[service_provider]", provider.Key);

                    foreach (var booking in provider.Meetings) {

                        var region = Globals.ThisAddIn.OneSpaceData.GetRegionFromLocationId(location.Id);
                        var analysis = new AppointmentAnalysis(booking.appointmentItem, region);
                        var BookingItems = region.BookingItems;
                        var meetingHtml = meetingFrag;
                        var boolFoundServices = false;
                        meetingHtml = meetingHtml.Replace("[from]", booking.DateFrom.ToString("dd MMMM yyyy HH:mm"));
                        meetingHtml = meetingHtml.Replace("[to]", booking.DateTo.ToString("dd MMMM yyyy HH:mm"));

                        TimeSpan ts = booking.DateTo - booking.DateFrom;
                        meetingHtml = meetingHtml.Replace("[location]", booking.Location);
                        meetingHtml = meetingHtml.Replace("[duration]", ts.ToFriendlyString());
                        meetingHtml = meetingHtml.Replace("[organiser]", booking.OrganiserName);
                        if (booking.Attendees == string.Empty) {
                            meetingHtml = meetingHtml.Replace("[attendees]", Globals.ThisAddIn.resourceManager.GetString("reportLableNone"));
                        }
                        else {
                            meetingHtml = meetingHtml.Replace("[attendees]", booking.Attendees);
                        }
                        if (!string.IsNullOrWhiteSpace(booking.Data.CostCentre)) {
                            meetingHtml = meetingHtml.Replace("[costcentre]", booking.Data.CostCentre);
                        }
                        else {
                            meetingHtml = meetingHtml.Replace("[costcentre]", "-");
                        }
                        meetingHtml = meetingHtml.Replace("[subject]", booking.Subject);

                        string rows = string.Empty;

                        foreach (var item in booking.Data.Items) {
                            rows += itemRow;
                            foreach (BookingItem testItem in BookingItems) {
                                if (testItem.Id == item.ItemId) {
                                    boolFoundServices = true;
                                    rows = rows.Replace("[service]", testItem.Name);
                                    rows = rows.Replace("[qtytype]", testItem.CostUnit);
                                    rows = rows.Replace("[cost]", testItem.CostToCurrencyHtmlString());
                                }
                            }

                            rows = rows.Replace("[qty]", item.Qty.ToString());
                            rows = rows.Replace("[sub]", item.SubTotal(analysis).ToHtmlString());

                            if (item.CalculatedDelivery != booking.DateFrom || item.CalculatedClearAway != booking.DateTo) {
                                rows += deliveryRow;
                                rows = rows.Replace("[delivery]", item.CalculatedDelivery.ToString("HH:mm"));
                                rows = rows.Replace("[clearaway]", item.CalculatedClearAway.ToString("HH:mm"));
                            }

                            if (item.Notes != string.Empty) {
                                rows += notesRow.Replace("[notes]", item.Notes);
                            }
                        }

                        string total = "<tr><td colspan=\"3\">{Globals.ThisAddIn.resourceManager.GetString(reportLableTotal)}</td><td>{booking.Data.TotalCost(analysis).ToHtmlString()}</td>";
                        if (booking.Data.Items.Count > 0) {
                            meetingHtml = meetingHtml.Replace("[total]", total);
                        }
                        else {
                            meetingHtml = meetingHtml.Replace("[total]", "<tr><td>{Globals.ThisAddIn.resourceManager.GetString(reportNoServicesAdded)}</td></tr>");
                        }
                        meetingHtml = meetingHtml.Replace("[rows]", rows);
                        meetingHtml = meetingHtml.Replace("[total]", booking.Data.TotalCost(analysis).ToHtmlString());
                        meetingHtml = meetingHtml.Replace("[TOTAL]", booking.Data.TotalCost(analysis).ToHtmlString());

                        if (boolFoundServices) {
                            result += meetingHtml;
                        }
                    }
                }
            }

            html = html.Replace("[html]", result);

            System.IO.File.AppendAllText(tempPath, html);
        }

        private void GenerateXlsx(ReportCollection report_collection) {
            #region [ SQL_INSERT_REPORT_DATA ]

            const string SQL_INSERT_REPORT_DATA =
@"INSERT INTO
    [Report$] ([Report], [Service Delivery Provider], [Location], [Scheduled From], [Scheduled To], [Organiser], [Cost Centre], [Service], [Delivery], [Clear away], [Notes], [Qty Type], [Cost], [Qty], [Sub Total])
VALUES
    (@Report, @ServiceDeliveryProvider, @Location, @ScheduledFrom, @ScheduledTo, @Organiser, @CostCentre, @Service, @Delivery, @Clearaway, @Notes, @QtyType, @Cost, @Qty, @SubTotal)";

            #endregion

            try {
                var temp_file = Path.Combine(SystemConsts.TempPath, "OneSpace_PrintTemplate_" + Guid.NewGuid().ToString() + ".xlsx");

                if (System.IO.File.Exists(temp_file)) {
                    System.IO.File.Delete(temp_file);
                }

                var temp_file_info = new FileInfo(temp_file);
                using (var temp_file_stream = temp_file_info.Create()) {
                    temp_file_stream.Write(
                        Properties.Resources.OrderTemplate,
                        0,
                        Properties.Resources.OrderTemplate.Length
                    );
                }
                var region = Globals.ThisAddIn.OneSpaceData.GetRegionFromLocationId(location.Id);

                var single_blank_order_items = new List<OrderItem>(new[] {
                    new OrderItem() {
                        ItemId = -1,
                        ServiceAppointmentId = string.Empty,
                        ServiceState = OrderItem.enum_OrderServiceState.Normal,
                        Start = DateTime.MinValue,
                        End = DateTime.MinValue,
                        DeliveryTimeOffset = 0,
                        ClearAwayTimeOffset = 0,
                        ItemCost = new Currency() { Symbol = region.CurrencySymbol, HasSymbolOnLeft = region.HasSymbolOnLeft, Value = 0.0m },
                        Qty = 0,
                        Notes = string.Empty,
                    }
                });

                var blank_booking_item = new BookingItem() {
                    Id = -1,
                    Name = string.Empty,
                    Description = string.Empty,
                    AlertEmail = string.Empty,
                    ItemType = BookingItem.enum_ItemType.None,
                    Icon = string.Empty,
                    SetupTime = 0,
                    ClearDownTime = 0,
                    MinLeadTime = 0,
                    QtyType = BookingItem.enum_QtyType.PerSession,
                    MaxQty = 0,
                    Cost = new Currency() { Symbol = region.CurrencySymbol, HasSymbolOnLeft = region.HasSymbolOnLeft, Value = 0.0m },
                };

                var report_name = Branding.Resource.xlsReportServices;

                using (var excel_data = new ExcelData(temp_file_info)) {
                    foreach (var provider in report_collection.Items) {
                        foreach (var meeting in provider.Meetings) {
                            var analysis = new AppointmentAnalysis(meeting.appointmentItem, region);
                            var booking_items = region.BookingItems;

                            var meeting_location = meeting.Location ?? string.Empty;
                            var scheduled_from = "{meeting.DateFrom:F}";
                            var scheduled_to = "{meeting.DateTo:F}";
                            var organiser = meeting.OrganiserName ?? string.Empty;
                            var cost_centre = meeting.Data.CostCentre ?? string.Empty;

                            var order_items = (meeting.Data.Items.Count > 0) ? meeting.Data.Items : single_blank_order_items;

                            foreach (var order_item in order_items) {

                                var booking_item = (
                                    from b in region.BookingItems
                                    where b.Id == order_item.ItemId
                                    select b
                                ).FirstOrDefault() ?? blank_booking_item;

                                var is_not_blank_order_item = (booking_item != null);

                                var delivery_time = (is_not_blank_order_item) ? order_item.CalculatedDelivery.ToString("HH:mm") : string.Empty;
                                var clear_away_time = (is_not_blank_order_item) ? order_item.CalculatedClearAway.ToString("HH:mm") : string.Empty;
                                var item_cost = (is_not_blank_order_item) ? booking_item.CostToCurrencyString() : string.Empty;
                                var qty_type = (is_not_blank_order_item) ? booking_item.QtyType.ToString() : string.Empty;
                                var qty = (is_not_blank_order_item) ? order_item.Qty.ToString() : string.Empty;
                                var sub_total_str = string.Empty;

                                if (is_not_blank_order_item) {
                                    var sub_total = order_item.SubTotal(analysis);
                                    sub_total_str = sub_total.ToString();
                                }

                                /* Note: Keep the parameters in the same order as the query */
                                /* Ref: https://blogs.msdn.microsoft.com/wriju/2008/01/24/ado-net-oledbcommand-parameterized-query-sequence-rule/ */
                                excel_data.ExecuteNonQuery(
                                    SQL_INSERT_REPORT_DATA,
                                    System.Data.CommandType.Text,
                                    excel_data.CreateParameter("@Report", report_name),
                                    excel_data.CreateParameter("@ServiceDeliveryProvider", provider.Key),
                                    excel_data.CreateParameter("@Location", meeting_location),
                                    excel_data.CreateParameter("@ScheduledFrom", scheduled_from),
                                    excel_data.CreateParameter("@ScheduledTo", scheduled_to),
                                    excel_data.CreateParameter("@Organiser", organiser),
                                    excel_data.CreateParameter("@CostCentre", cost_centre),
                                    excel_data.CreateParameter("@Service", booking_item.Name),
                                    excel_data.CreateParameter("@Delivery", delivery_time),
                                    excel_data.CreateParameter("@Clearaway", clear_away_time),
                                    excel_data.CreateParameter("@Notes", order_item.Notes),
                                    excel_data.CreateParameter("@QtyType", qty_type),
                                    excel_data.CreateParameter("@Cost", item_cost),
                                    excel_data.CreateParameter("@Qty", qty),
                                    excel_data.CreateParameter("@SubTotal", sub_total_str)
                                );
                            }
                        }
                    }
                }

                /* Rename the file only once the data has been completely added so users don't get partial data if somthing goes wrong. */
                File.Move(temp_file, xlsx);
            }
            catch (Exception ex) {
                xlsx = string.Empty;
                xlsx_error = ex.Message;
            }
        }
    }
}
