﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using OneSpace.OutlookPlugin.Logging;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Settings;
using System.Runtime.InteropServices;

namespace OneSpace.OutlookPlugin.ServiceCalendars
{
    /// <summary>
    /// Handles the invitation of calender items for service users via the hidden Plugin folder
    /// </summary>
    public class ServiceMailboxIntegration
    {
        private string OrganiserEmail;
        private MAPIFolder CalendarFolder = null;
        private MAPIFolder HiddenFolder = null;
        private MAPIFolder DeletedFolder = null;
        private ItemsEvents_Event CalendarEvents = null;

        //        public event EventHandler<EventArgs<AppointmentItem>> AppointmentAdded;

        public ServiceMailboxIntegration(MAPIFolder calendarFolder, MAPIFolder hiddenFolder, MAPIFolder deletedFolder , string orgEmail)
        {
            CalendarFolder = calendarFolder;
            HiddenFolder = hiddenFolder;
            DeletedFolder = deletedFolder;
            OrganiserEmail = orgEmail;
            CalendarEvents = CalendarFolder.Items;
            CalendarEvents.ItemChange += Items_ItemChange;
            CalendarEvents.ItemAdd += CalendarEvents_ItemAdd;
            CalendarEvents.ItemRemove += CalendarEvents_ItemRemove;
            ((MAPIFolderEvents_12_Event)CalendarFolder).BeforeItemMove +=
                      new MAPIFolderEvents_12_BeforeItemMoveEventHandler(Calendar_BeforeItemMove);

        }

        private void Calendar_BeforeItemMove(object Item, MAPIFolder MoveTo, ref bool Cancel)
        {
            if ((MoveTo == null) || (IsDeletedItemsFolder(MoveTo)))
            {
                Logging.ActivityLog.WriteToLog("Calendar_BeforeItemMove - Deleted item");
                AppointmentItem appointment = (Item as AppointmentItem);
                if (appointment != null)
                {

                    try
                    {
                        if (Globals.ThisAddIn.YouAreTheOwner(appointment))
                        {
                            AppointmentData.Delete(this, appointment);
                            ActivityLog.WriteToLog("    Deleting.");
                        }
                        else
                        {
                            ActivityLog.WriteToLog("    Not the owner. Ignoring.");
                        }
                    }
                    finally
                    {
                        Marshal.ReleaseComObject(appointment);
                        appointment = null;
                    }
                }
            }
            else
            {
                Logging.ActivityLog.WriteToLog("Calendar_BeforeItemMove - Not deleted folder, ignore");
            }

        }

        private void CalendarEvents_ItemRemove()
        {

        }

        private void CalendarEvents_ItemAdd(object Item)
        {
            Logging.ActivityLog.WriteToLog("CalendarEvents_ItemAdd");
            AppointmentItem appointment = (Item as AppointmentItem);
            if (appointment != null)
            {

                try
                {
                    if (Globals.ThisAddIn.YouAreTheOwner(appointment))
                    {
                        ActivityLog.WriteToLog("    Start:" + appointment.Start.ToString(SystemConsts.DateTimeFormat));
                        ActivityLog.WriteToLog("    End:" + appointment.End.ToString(SystemConsts.DateTimeFormat));

                        var par = appointment.Parent;

                        if (AppointmentData.IsUpdated(appointment))
                        {
                            AppointmentData.Move(this, appointment);
                        }
                        else
                        {
                            ActivityLog.WriteToLog("    Not updated. Ignoring.");
                        }
                    }
                    else
                    {
                        ActivityLog.WriteToLog("    Not the owner. Ignoring.");
                    }
                }
                finally
                {
                    Marshal.ReleaseComObject(appointment);
                    appointment = null;
                }
            }

        }

        /// <summary>
        /// Fires when an appointment item is changed
        /// </summary>
        /// <param name="Item"></param>
        private void Items_ItemChange(object Item)
        {
            Logging.ActivityLog.WriteToLog("CalendarItems_ItemChange");
            AppointmentItem appointment = (Item as AppointmentItem);
            if (appointment != null)
            {

                try
                {
                    if (Globals.ThisAddIn.YouAreTheOwner(appointment))
                    {
                        ActivityLog.WriteToLog("    Start:" + appointment.Start.ToString(SystemConsts.DateTimeFormat));
                        ActivityLog.WriteToLog("    End:" + appointment.End.ToString(SystemConsts.DateTimeFormat));

                        var par = appointment.Parent;

                        if (AppointmentData.IsUpdated(appointment))
                        {
                            AppointmentData.Move(this, appointment);
                        }
                        else
                        {
                            ActivityLog.WriteToLog("    Not updated. Ignoring.");
                        }
                    }
                    else
                    {
                        ActivityLog.WriteToLog("    Not the owner. Ignoring.");
                    }
                }
                finally
                {
                    Marshal.ReleaseComObject(appointment);
                    appointment = null;
                }
            }
        }

        private void Items_ItemAdd(object Item)
        {
            Logging.ActivityLog.WriteToLog("CalendarItems_ItemAdd");
        }

        public string SendInvitation(string serviceMailbox, 
                                     string subject, 
                                     string body, 
                                     string location, 
                                     DateTime start, 
                                     DateTime end)
        {
            var now = DateTime.Now;

            var appointment = HiddenFolder.Items.Add(Outlook.OlItemType.olAppointmentItem) as Outlook.AppointmentItem;
            appointment.Subject = subject;
            appointment.Body = body;
            appointment.Location = location;
            appointment.Start = start;
            appointment.End = end;
            appointment.MeetingStatus = Outlook.OlMeetingStatus.olMeeting;

            var organizer = appointment.Recipients.Add(OrganiserEmail);
            organizer.Type = (int)Outlook.OlMeetingRecipientType.olRequired;

            var servieUser = appointment.Recipients.Add(serviceMailbox);
            servieUser.Type = (int)Outlook.OlMeetingRecipientType.olRequired;

            string appGuid = Guid.NewGuid().ToString();
            appointment.UserProperties.Add("OneSpaceServiceId", OlUserPropertyType.olText);
            appointment.UserProperties["OneSpaceServiceId"].Value = appGuid;

            appointment.Send();

            //            return appointment.GlobalAppointmentID;
            return appGuid;
        }

        public void DeleteInvitation(string appId)
        {
            Outlook.AppointmentItem appointmentItem;
            var item = HiddenFolder.Items.Find(String.Format("[OneSpaceServiceId] = '{0}'", appId));
            if (item!=null)
            {
                appointmentItem = (Outlook.AppointmentItem)item;
                appointmentItem.MeetingStatus = OlMeetingStatus.olMeetingCanceled;
                appointmentItem.Send();
            }
        }

        public void DeleteInvitations(List<string> appointmentIds)
        {
            foreach (string id in appointmentIds)
            {
                DeleteInvitation(id);
            }
        }

        private bool IsUsersCalendar(MAPIFolder aFolder)
        {
            //
            // This is based purely on my observations so far - a better way?
            //
            return (aFolder.Store != null);
        }

        private bool IsDeletedItemsFolder(MAPIFolder aFolder)
        {
            return (aFolder.EntryID == DeletedFolder.EntryID);
        }


    }
}
