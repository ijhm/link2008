﻿using OneSpace.OutlookPlugin.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.ServiceCalendars
{
    public class ServiceMessages
    {
        public List<ServiceMessage> Messages { get; set; }
        public List<string> AppToDelete { get; set; }

        public ServiceMessages()
        {
            Messages = new List<ServiceCalendars.ServiceMessage>();
            AppToDelete = new List<string>();
        }

        public void AddMessage(int itemIndex,DateTime delivery,DateTime clearaway,string serviceMailbox,string subject,string itemLine)
        {
            ServiceMessage item;
            for (int i=0;i<Messages.Count;i++)
            {
                if (Messages[i].DeliveryDateTime.Equals(delivery) &&
                    Messages[i].ClearAwayDateTime.Equals(clearaway) &&
                    Messages[i].ServiceMailbox == serviceMailbox)
                {
                    Messages[i].Body += string.Format(Globals.ThisAddIn.resourceManager.GetString("serviceMessageProvideTheFollowing"), itemLine);
                    Messages[i].ItemIndex.Add(itemIndex);
                }
                else
                {
                    item = new ServiceMessage
                    {
                        ServiceMailbox = serviceMailbox,
                        DeliveryDateTime = delivery,
                        ClearAwayDateTime = clearaway,
                        Subject = subject
                    };
                    item.Body += itemLine;
                    item.ItemIndex.Add(itemIndex);
                    Messages.Add(item);
                }
            }
            if (Messages.Count==0)
            {
                item = new ServiceMessage
                {
                    ServiceMailbox = serviceMailbox,
                    DeliveryDateTime = delivery,
                    ClearAwayDateTime = clearaway,
                    Subject = subject
                };
                item.Body += itemLine;
                item.ItemIndex.Add(itemIndex);
                Messages.Add(item);
            }
        }

        public void AddAppToDelete(string appId)
        {
            bool addNew = true;
            for (int i = 0; i < AppToDelete.Count; i++)
            {
                if (appId == AppToDelete[i])
                {
                    addNew = false;
                }
            }
            if (addNew)
            {
                AppToDelete.Add(appId);
            }
        }

    }

    public class ServiceMessage
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string ServiceMailbox { get; set; }
        public DateTime DeliveryDateTime { get; set; }
        public DateTime ClearAwayDateTime { get; set; }
        public List<int> ItemIndex { get; set; }

        public ServiceMessage()
        {
            ItemIndex = new List<int>();
        }
    }
}
