﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Threading;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Watcher;
using OneSpace.OutlookPlugin.Settings;
using System.IO;
using System.Diagnostics;
using System.Drawing;
using OneSpace.OutlookPlugin.Extentions;
using OneSpace.OutlookPlugin.Utilities;
using OneSpace.OutlookPlugin.ServiceCalendars;
using Newtonsoft.Json;
using System.Resources;
using System.Reflection;

/* Todo: Update the Data stored against each AppointmentItem to use a MAPI Namespace so each property can be set indervidually and a common way to access the data in either Outlook COM or EWS */
/* Ref: https://msdn.microsoft.com/en-us/library/microsoft.office.interop.outlook._propertyaccessor.getproperty.aspx */
/* Ref: https://msdn.microsoft.com/en-us/library/microsoft.office.interop.outlook._propertyaccessor.setproperty.aspx */
/* Ref: https://msdn.microsoft.com/en-us/library/bb147567.aspx */
/* Ref: https://msdn.microsoft.com/en-us/library/bb147573.aspx */
/* Note: The EWS Rest API provides access to the properties speciifed in the namespace and allows any search to be filtered on those values */
/* Ref: https://msdn.microsoft.com/EN-US/library/office/dd633691(v=exchg.80).aspx */
/* Ref: https://msdn.microsoft.com/en-us/library/office/dd633654(v=exchg.80).aspx */

namespace OneSpace.OutlookPlugin
{
    public partial class ThisAddIn
    {
        #region Constants
   //     string SchemaPR_ATTACH_CONTENT_ID = @"http://schemas.microsoft.com/mapi/proptag/0x3712001E";
        private const string PR_ATTR_HIDDEN = "http://schemas.microsoft.com/mapi/proptag/0x10F4000B";
        private const string HIDDEN_CALENDER = "OneMeeting-74057B58";
        private static readonly List<Outlook.OlResponseStatus> RESPONSE_STATUS_NO_RESPONSE = new List<Outlook.OlResponseStatus>(new[] { Outlook.OlResponseStatus.olResponseNone, Outlook.OlResponseStatus.olResponseNotResponded });
        #endregion

        #region Variables
        private Dictionary<string, Action<Outlook.Search>> _search_callbacks = new Dictionary<string, Action<Outlook.Search>>();
        public Outlook.Application OutlookApp;
        public ServiceMailboxIntegration serviceMailboxIntegration;
        public string ErrorMsg { get; set; }
        public string Domain = "";
        public List<TempImageArea> MapAreaImages = new List<TempImageArea>();
        public UserSettings Settings { get; set; }
        public string smtpAddress { get; set; }
        public Outlook.AddressEntry CurrentUser;
        public Outlook.MAPIFolder Inbox;
        public Outlook.MAPIFolder Calendar;
        public Outlook.Items Calendar_Items;
        public Outlook.StorageItem LocalData;
        public bool _showEquipmentPrice { get; set; }
        public string _DeligateAccount { get; set; }
        public bool WriteToDebugLog => SystemConsts.WriteToDebugLog;
        public ResourceManager resourceManager { get; set; }
        public Data.Admin.Session AdminSession { get; set; }
        public int LoadingCount { get; set; }
        public int LoadingTotal { get; set; }

        /// <summary>
        /// Cached values used by RoomAsset region to improve performance
        /// </summary>
        public static List<RoomBooking> BufferedSlots;
        
        private bool? _isAdmin { get; set; }
        private bool? _isReporting { get; set; }

        public string DeligateAccount
        {
            get
            {
                if (_DeligateAccount != null && _DeligateAccount != "")
                {
                    return _DeligateAccount;
                }
                else
                {
                    string result = "";
                    var key = Data.Security.LicenseKey.GetKey;
                    if (key != null)
                    {
                        if (key.IsInLicense)
                        {
                            if (key.Domain == Domain)
                            {
                                result = key.ServiceAccount + "@" + key.Domain;
                            }
                        }
                        else
                        {
                            Logging.ActivityLog.WriteToLog("   Not in license");
                        }
                    }
                    else
                    {
                        Logging.ActivityLog.WriteToLog("   Not in license");
                    }
                    _DeligateAccount = result;
                    return result;
                }
            }
        }
        private OneSpace.OutlookPlugin.Data.RoomBookingData _OneSpaceData = null;
        public ReportTemplate PrintoutTemplate;
        public ReportTemplate DailyReportTemplate;

        public OneSpace.OutlookPlugin.Data.RoomBookingData OneSpaceData
        {
            set
            {
                _OneSpaceData = value;
            }
            get
            {
                if (_OneSpaceData == null)
                {
                    //ToDo: Property fetch load commented out!
                    //                    LoadSystemDataInForeground();
                    //                    LoadInBackground();
                }
                return _OneSpaceData;
            }
        }

        public enum enum_LoadingState
        {
            Loading,
            Loaded,
            FailedToLoad,
            Cleared
        }
        
        private enum_LoadingState LoadingState = enum_LoadingState.Loading;
        #endregion

        #region System constants
        private string[] templateTags = { "[building_header]", "[/building_header]", "[floor_header]", "[/floor_header]", "[room_detail]", "[/room_detail]" };
        public const string DataRecordSubject = "MasterData";
        public const string PrintoutTemplateSubject = "PrintTemplate";
        private Outlook.MAPIFolder _hidden_folder;
        #endregion

        #region Settings and state
        public bool ShowEquipmentPrice
        {
            get
            {
                return _showEquipmentPrice;
            }
        }

        public enum_LoadingState LoadState
        {
            get
            {
                return LoadingState;
            }
        }

        public void AddCentre(string centre)
        {
            if (centre != null && centre != "")
            {
                if (!Settings.PreviousCostCentres.Contains(centre))
                {
                    List<string> list = new List<string>();
                    list.Add(centre);
                    int counter = 0;
                    foreach (var item in Settings.PreviousCostCentres)
                    {
                        if (counter < 5)
                        {
                            list.Add(item);
                        }
                        counter++;
                    }
                    Settings.PreviousCostCentres = list;
                }
                UserSettings.Set(Settings);
//                LocalData.Save();
            }
        }

        #endregion

        #region Plug-in life cycle

        private static string GetOutlookVersion() {
            // HKEY_CLASSES_ROOT\Outlook.Application\CurVer
            var outlook_current_version_progid = (string)Microsoft.Win32.Registry.GetValue(
                                                     "HKEY_CLASSES_ROOT\\Outlook.Application\\CurVer",
                                                     string.Empty,
                                                     string.Empty
                                                 ) ?? string.Empty;

            var outlook_current_version = outlook_current_version_progid.Replace("Outlook.Application.", string.Empty);

            return string.Format("{0}.0", outlook_current_version);
        }

        private void ThisAddIn_Startup(object sender, System.EventArgs e) {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Application.AdvancedSearchComplete += Application_AdvancedSearchComplete;
            Application.AdvancedSearchStopped += Application_AdvancedSearchStopped;

            InitBranding();
            SetPluginResilianceKey();

            /* Todo: Remove the resourceManager and change all calls to Properties.Lang.xxx */
            resourceManager = Properties.Lang.ResourceManager;

            Logging.ActivityLog.WriteToLog("\r\nLaunching plug-in\r\n");
            LoadingState = enum_LoadingState.Loading;
            OutlookApp = this.Application;

            Reload();

            StartServiceMailboxIntegration();
            Settings = UserSettings.Get;

            Application.ItemSend += Application_ItemSend;

            Calendar_Items = Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar).Items;
            Calendar_Items.ItemChange += Calendar_Items_ItemChange;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
            if(e.ExceptionObject != null) {
                var ex = JsonConvert.SerializeObject(e.ExceptionObject);
                Logging.ActivityLog.WriteToLog(@"An unhandled exception occured");
            }

#if DEBUG
            if (e.IsTerminating) {
                System.Windows.MessageBox.Show("An unhandled exception occured and Outlook will be terminated, please see the activity log for details.");
            }
#endif
        }

        private void InitBranding() {
            try {
#if REDSTONE
                Branding.Resource.Initialise("OneSpace.OutlookPlugin.Redstone.dll");
#elif EVOKO
                Branding.Resource.Initialise("OneSpace.OutlookPlugin.Evoko.dll");
#endif
            }
            catch (Exception ex) {
                Logging.ActivityLog.WriteToLog("An error occured whilst loading the Brand Resources: {ex.Message}");
            }
        }

        private void SetPluginResilianceKey()
        {
            var reg_outlook_resiliance = string.Format(
                "HKEY_CURRENT_USER\\Software\\Microsoft\\Office\\{0}\\Outlook\\Resiliency\\DoNotDisableAddinList",
                GetOutlookVersion()
            );

            Microsoft.Win32.Registry.SetValue(reg_outlook_resiliance, "OneSpace.OutlookPlugin", 1);
        }

        private void Application_ItemSend(object Item, ref bool Cancel) {
            /* Objective: Check service restriction and if the restrictions are not met alert the user and set Cancel to true to stop the appointment being sent */
            /* Ref: https://msdn.microsoft.com/en-us/vba/outlook-vba/articles/application-itemsend-event-outlook */
            try {
                var now = DateTime.Now;
                var restriction_notices = new List<string>();
                
                var meeting = Item as Outlook.MeetingItem;
                var appointment = meeting?.GetAssociatedAppointment(false);
                if (appointment != null) {
                    Data.Region region = null;
                    AppointmentData order = null;

                    var location_id = (int?)appointment.UserProperties["RoomBookingLocation"]?.Value;

                    if (location_id != null) {
                        region = OneSpaceData.GetRegionByLocationId(location_id ?? 0);

                        try {
                            string appointJson = (string)appointment.UserProperties[AppointmentSchemaEx.RoomBookingDataUpdated]?.Value;
                            if (string.IsNullOrWhiteSpace(appointJson) == false) {
                                order = new AppointmentData(appointJson);
                            }
                        }
                        catch { }
                    }
                    
                    if (region != null && order != null) {
                        var analysis = new OneSpace.OutlookPlugin.Data.Booking.AppointmentAnalysis(appointment, region);

                        var order_items = (
                            from o in order.Items
                            select new {
                                Order = o,
                                Item = analysis.GetBookingItem(o.ItemId)
                            }
                        );

                        Func<int, int> AbsNegNumOrZero = (number) => {
                            if(number > 0) {
                                return 0;
                            }

                            return Math.Abs(number);
                        };

                        var provider_lead_time = (
                            from o in order_items
                            where string.IsNullOrWhiteSpace(o.Item.AlertEmail) == false
                            group o by o.Item.AlertEmail into items
                            select new {
                                Provider = items.Key,
                                LeadTime = items.Sum(e => e.Item.MinLeadTime + AbsNegNumOrZero(e.Order.DeliveryTimeOffset))
                            }
                        );

                        var service_lead_time = TimeSpan.FromMinutes(
                            provider_lead_time.Max(e => e.LeadTime)
                        );
                        var must_book_on_or_before = appointment.Start.Subtract(service_lead_time);

                        if (now > must_book_on_or_before) {
                            restriction_notices.Add(string.Format(
                                Globals.ThisAddIn.resourceManager.GetString("bookServiceLeadTime"),
                                service_lead_time.ToFriendlyString()
                            ));
                        }

                        var layout_count = (
                            from o in order_items
                            where o.Item.ItemType == BookingItem.enum_ItemType.Layout
                            select o
                        ).Count();

                        if (layout_count > 1) {
                            restriction_notices.Add(Globals.ThisAddIn.resourceManager.GetString("bookOnlyOneLayoutAllowed"));
                        }
                    }

                    if (restriction_notices.Count > 0) {
                        Cancel = true;
                        System.Windows.Forms.MessageBox.Show(
                            string.Join(Environment.NewLine, restriction_notices),
                            Globals.ThisAddIn.resourceManager.GetString("bookRestrictionErrorsTitle"),
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Information
                        );
                    }
                }
            }
            catch (Exception ex) {
                Logging.ActivityLog.WriteToLog("Error processing restriction on send: {ex.Message}");
            }
        }

        private void Calendar_Items_ItemChange(object Item) {
            try {
                Trace.WriteLine("{nameof(Calendar_Items_ItemChange)}({Microsoft.VisualBasic.Information.TypeName(Item)} {nameof(Item)}) {{");

                var appointment_item = Item as Outlook.AppointmentItem;
                if (appointment_item != null) {
                    Trace.WriteLine(" 'Subject == {appointment_item.Subject}");
                    Trace.WriteLine(" 'Schedule == {appointment_item.Start} - {appointment_item.End}");
                    Trace.WriteLine(" 'MeetingStatus == {appointment_item.MeetingStatus}");
                    /* No need to process cancelled meetings or meetings with only the organiser */
                    if (appointment_item.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled || appointment_item.Recipients.Count == 1) {
                        Trace.WriteLine(" Meeting Canceled, no processing required");
                        return;
                    }

                    var room_recipients = new List<Outlook.Recipient>();
                    var rooms = new List<Room>();
                    var linked_room_ids = JsonConvert.DeserializeObject<List<int>>(OutlookUtilities.GetPropertyText(appointment_item, "RoomBookingLinkedRooms"));

                    if(linked_room_ids != null) {
                        foreach (var linked_room_id in linked_room_ids) {
                            var linked_room = OneSpaceData.GetRoomById(linked_room_id);
                            if (linked_room != null) {
                                rooms.Add(linked_room);
                            }
                        }
                    }

                    var has_any_rooms_declined = false;
                    var has_all_rooms_responded = true;
                    foreach (Outlook.Recipient recipient in appointment_item.Recipients) {
                        Trace.WriteLine(" '{recipient.Name}'.MeetingStatus == {recipient.MeetingResponseStatus}");
                        if (recipient.Type == (int)Outlook.OlMeetingRecipientType.olResource) {

                            /* Todo: Find a reliable way to determine if the resource is a room */
                            var is_room = (
                                from r in rooms
                                where r.Attendee.Equals(recipient.Name, StringComparison.InvariantCultureIgnoreCase)
                                select r
                            ).Any();

                            if (is_room) {
                                if (RESPONSE_STATUS_NO_RESPONSE.Contains(recipient.MeetingResponseStatus)) {
                                    has_all_rooms_responded = false;
                                }

                                room_recipients.Add(recipient);
                                if (recipient.MeetingResponseStatus == Outlook.OlResponseStatus.olResponseDeclined) {
                                    has_any_rooms_declined = true;
                                }
                            }
                        }
                    }

                    if (has_any_rooms_declined && has_all_rooms_responded) {
                        Trace.WriteLine(" Removing rooms from meeting");
                        OutlookUtilities.SetPropertyText(appointment_item, "RoomBookingLinkedRooms", string.Empty);

                        if (appointment_item.Recipients.Count - room_recipients.Count == 1) {
                            appointment_item.MeetingStatus = Outlook.OlMeetingStatus.olMeetingCanceled;
                        }
                        else {
                            foreach (var recipient in room_recipients) {
                                appointment_item.Recipients.Remove(recipient.Index);
                            }
                        }

                        appointment_item.Save();
                        appointment_item.Send();

                        if(appointment_item.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled) {
                            appointment_item.Delete();
                        }
                    }
                }
            }
            catch(Exception ex) {
                Trace.WriteLine(" Error: {ex.Message}");
                Trace.WriteLine(" Statck Trace: \r\n\r\n{ex.StackTrace}\r\n\r\n");
            }
            finally {
                Trace.WriteLine("}");
            }
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see http://go.microsoft.com/fwlink/?LinkId=506785
            Logging.ActivityLog.WriteToLog("Shutting down...");
        }

        #endregion

        #region User security
        private void LoadAccountDetails()
        {
            smtpAddress = string.Empty;
            var primary_user_x400_address = Application.Session.CurrentUser.Address;

            var accounts = Application.Session.Accounts;
            foreach (Outlook.Account account in accounts)
            {
                if (account.CurrentUser.Address.Equals(primary_user_x400_address, StringComparison.InvariantCultureIgnoreCase))
                {
                    smtpAddress = account.SmtpAddress;
                    break;
                }
            }

            if (string.IsNullOrWhiteSpace(smtpAddress))
            {
                CurrentUser = Application.Session.CurrentUser.AddressEntry;
                Inbox = Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);
                var exUser = CurrentUser.GetExchangeUser();
                smtpAddress = exUser?.PrimarySmtpAddress ?? "onespace@UNABLE-TO-RESOLVE_DOMAIN";
            }

            int i = smtpAddress.IndexOf("@");
            Domain = smtpAddress.Substring(i + 1);
        }

        public void Reload() {
            LoadingState = enum_LoadingState.Loading;

            try
            {
                string path = Path.Combine(SystemConsts.TempPath, "debug.inf");
                bool local_cache_only = (WriteToDebugLog && System.IO.File.ReadAllText(path) == "local-cache-only");

                LoadAccountDetails();
                if (local_cache_only == false)
                {
                    PopulateLocalDataUsingOutlookApiLocal();
                }
                LoadDataFromCache(SystemConsts.TempPath);
                LoadingState = enum_LoadingState.Loaded;
            }
            catch (Exception ex)
            {
                Logging.ActivityLog.WriteToLog("   Load Error: {ex.Message}");
                LoadingState = enum_LoadingState.FailedToLoad;
                ErrorMsg = ex.Message;
            }
        }

        public void Deactivate() {
            LoadingState = enum_LoadingState.Loading;
        }

        public bool IsAdmin
        {
            get
            {
                if (_isAdmin == null)
                {
                    _isAdmin = false;
                    try
                    {
                        var user = Application.Session.CurrentUser.AddressEntry;
                        var exUser = user.GetExchangeUser();
                        string smtpAddress = exUser.PrimarySmtpAddress;
                        foreach (var userAccount in OneSpaceData.SecurityRoles.AdminUsers)
                        {
                            if (userAccount.Equals(smtpAddress, StringComparison.InvariantCultureIgnoreCase))
                            {
                                _isAdmin = true;
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                return (bool)_isAdmin;
            }
        }

        public bool IsReporting
        {
            get
            {
                if (_isReporting == null)
                {
                    _isReporting = false;
                    try
                    {
                        var user = Application.Session.CurrentUser.AddressEntry;
                        var exUser = user.GetExchangeUser();
                        string smtpAddress = exUser.PrimarySmtpAddress;
                        foreach (var userAccount in OneSpaceData.SecurityRoles.ReportUsers)
                        {
                            if (userAccount.Equals(smtpAddress, StringComparison.InvariantCultureIgnoreCase))
                            {
                                _isReporting = true;
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                return (bool)_isReporting;
            }
        }

        #endregion

        #region Data management methods

        public void ClearCache()
        {
            System.Windows.Forms.Application.DoEvents();

            if (Inbox != null) {
                var textLocalData = Inbox.GetStorage("RoomBooking", Outlook.OlStorageIdentifierType.olIdentifyBySubject);
                if (textLocalData.Size > 0) {
                    textLocalData.Delete();
                }
            }

            _OneSpaceData = null;

            if (!Directory.Exists(SystemConsts.TempPath))
            {
                Directory.CreateDirectory(SystemConsts.TempPath);
            }
            foreach (string file in Directory.GetFiles(SystemConsts.TempPath))
            {
                if (!file.Contains("debug.inf"))
                {
                    File.Delete(file);
                }
            }

            LoadingState = enum_LoadingState.Cleared;
        }

        #endregion

        #region Config scan

        /// <summary>
        /// Load system data from a local cache
        /// </summary>
        public void LoadDataFromCache(string path)
        {
            string filePath = Path.Combine(path, "config.json");

            if (File.Exists(filePath))
            {
                string fileJson = File.ReadAllText(filePath);
                _OneSpaceData = new RoomBookingData(fileJson);

                if(_OneSpaceData.SchemaVersion.Major <= 1) {
                    _OneSpaceData.MapAreaImage = new FileInfo(Path.Combine(path, "estate.png")).SafeLoadImage();
                    for (int r = 0; r < _OneSpaceData.Regions.Count; r++) {
                        _OneSpaceData.Regions[r].MapAreaImage = new FileInfo(Path.Combine(path, string.Format("r{0}.png", _OneSpaceData.Regions[r].Id))).SafeLoadImage();
                        for (int l = 0; l < _OneSpaceData.Regions[r].Locations.Count; l++) {
                            _OneSpaceData.Regions[r].Locations[l].MapAreaImage = new FileInfo(Path.Combine(path, string.Format("l{0}.png", _OneSpaceData.Regions[r].Locations[l].Id))).SafeLoadImage();
                            for (int f = 0; f < _OneSpaceData.Regions[r].Locations[l].Floors.Count; f++) {
                                _OneSpaceData.Regions[r].Locations[l].Floors[f].MapAreaImage = new FileInfo(Path.Combine(path, string.Format("f{0}.png", _OneSpaceData.Regions[r].Locations[l].Floors[f].Id))).SafeLoadImage();
                            }
                        }
                    }
                    LoadingState = enum_LoadingState.Loaded;
                }
                else {
                    _OneSpaceData = null;
                    ErrorMsg += "Load Error: Unsupported Config Schema Version\r\n";
                    LoadingState = enum_LoadingState.FailedToLoad;
                }
            }
        }
        
        private void PopulateLocalDataUsingOutlookApiLocal() {
            Logging.ActivityLog.WriteToLog("   Triggered background scan - Local file cache");

            if (DeligateAccount == "") {
                Logging.ActivityLog.WriteToLog("   Missing DeligateAccount.  Loading Local file cache stopped.");
                return;
            }

            //Bind to the deleted items folder looking for OneSpace items that are owned
            var deleted_items = Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderDeletedItems);
            deleted_items.Items.ItemAdd += new Outlook.ItemsEvents_ItemAddEventHandler(Items_DeletedItems);

            System.Windows.Forms.Application.DoEvents();

            Logging.ActivityLog.WriteToLog("   Check data online");
            var ns = Globals.ThisAddIn.Application.GetNamespace("MAPI");

            var service_account = ns.CreateRecipient(DeligateAccount);
            var service_inbox = ns.GetSharedDefaultFolder(service_account, Outlook.OlDefaultFolders.olFolderInbox);

            System.Windows.Forms.Application.DoEvents();

            /* Note: Ensure Outlook syncs these folders to improve performance. */
            if (service_inbox.InAppFolderSyncObject == false) {
                service_inbox.InAppFolderSyncObject = true;
            }
            
            var service_inbox_items = service_inbox.Items;
            service_inbox_items.Sort(Property: "LastModificationTime", Descending: true);

            var master_data_item = service_inbox_items.Find("[Subject] = 'MasterData'") as Outlook.MailItem;

            System.Windows.Forms.Application.DoEvents();

            if (master_data_item == null) {
                Logging.ActivityLog.WriteToLog("   Unable to locate MasterData.  Loading Local file cache stopped.");
                return;
            }

            var room_data_updated = FileUtilities.GetLastUpdated("OutlookLink.inf");
            Logging.ActivityLog.WriteToLog("   MasterData: {room_data_updated:dd MM yyyy HH:mm}");
            if (master_data_item.LastModificationTime != room_data_updated) {
                ClearCache();
                PopulateMasterDataToLocal(master_data_item);
                FileUtilities.SetLastUpdated("OutlookLink.inf", master_data_item.LastModificationTime);
                Logging.ActivityLog.WriteToLog("   Updated");
            }
            else {
                Logging.ActivityLog.WriteToLog("   Not updated");
            }
        }

        private void PopulateMasterDataToLocal(Outlook.MailItem master_data) {
            if (Directory.Exists(SystemConsts.TempPath) == false) {
                Directory.CreateDirectory(SystemConsts.TempPath);
            }
            
            var master_data_attachment_count = master_data.Attachments.Count;

            if (master_data_attachment_count == 0) {
                Logging.ActivityLog.WriteToLog("MasterData missing attachments, can't replace current config");
                return;
            }

            Globals.ThisAddIn.LoadingTotal = master_data_attachment_count;
            Globals.ThisAddIn.LoadingCount = 0;

            System.Windows.Forms.Application.DoEvents();

            var has_config = false;

            foreach (Outlook.Attachment a in master_data.Attachments) {
                Globals.ThisAddIn.LoadingCount++;
                if (a.FileName.Equals("Config.json", StringComparison.InvariantCultureIgnoreCase)) {
                    has_config = true;
                }
                a.SaveAsFile(Path.Combine(SystemConsts.TempPath, a.FileName));
                System.Windows.Forms.Application.DoEvents();
            }

            if (has_config == false) {
                var config_json = master_data.Body;
                try {
                    Newtonsoft.Json.JsonConvert.SerializeObject(config_json);
                    has_config = true;
                }
                catch {
                    has_config = false;
                }

                if (has_config) {
                    File.WriteAllText(Path.Combine(SystemConsts.TempPath, "Config.json"), config_json, Encoding.UTF8);
                }
            }
        }

        public bool YouAreTheOwner(Outlook.AppointmentItem appointment)
        {
            return appointment.Organizer == Application.Session.CurrentUser.AddressEntry.Name;
        }

        private void Items_DeletedItems(object Item)
        {
            try
            {
                if (Item is Outlook.AppointmentItem)
                {
                    var appointment = (Outlook.AppointmentItem)Item;
                    if (appointment.Organizer == Application.Session.CurrentUser.AddressEntry.Name)
                    {
                        Logging.ActivityLog.WriteToLog("Deleted appointment detected and you are the owner");
                        if (AppointmentData.IsOneSpaceAppointment(appointment))
                        {
                            AppointmentData.Delete(appointment);
                        }
                        else
                        {
                            Logging.ActivityLog.WriteToLog("    Ignoring as it is not a OneSpace item");
                        }
                    }
                    else
                    {
                        Logging.ActivityLog.WriteToLog("Deleted appointment detected, you are not the owner so ignore");
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion
        
        #region Hidden folder methods
        private void StartServiceMailboxIntegration()
        {
            if (!SystemConsts.EnableServiceMailboxIntegration)
            {
                Logging.ActivityLog.WriteToLog("   StartServiceMailboxIntegration - Skipped");
                return;
            }

            Logging.ActivityLog.WriteToLog("   StartServiceMailboxIntegration - Starting");

            var ipm_root = Application.Session.DefaultStore.GetRootFolder();
            var root_folders = ipm_root.Folders;

            if (root_folders.TryGetFolder(HIDDEN_CALENDER, out _hidden_folder) == false)
            {
                _hidden_folder = root_folders.Add(HIDDEN_CALENDER, Outlook.OlDefaultFolders.olFolderCalendar);
                _hidden_folder.PropertyAccessor.SetProperty(PR_ATTR_HIDDEN, true);
            }

            var item = Globals.ThisAddIn.OutlookApp.CreateItem(Outlook.OlItemType.olAppointmentItem);
            var ns = Globals.ThisAddIn.OutlookApp.GetNamespace("MAPI");
            try
            {
                var rec = ns.CreateRecipient(smtpAddress);
                rec.Resolve();
                if (rec.Resolved)
                {
                    Calendar = ns.GetSharedDefaultFolder(rec, Outlook.OlDefaultFolders.olFolderCalendar);
                    Logging.ActivityLog.WriteToLog("   Monitoring calendar " + smtpAddress);
                }
            }
            catch (Exception ex)
            {
                Logging.ActivityLog.WriteToLog("   Error connecting to calendar:" + ex.Message);
            }

            Outlook.MAPIFolder deletedItemsFolder;
            try
            {
                deletedItemsFolder = ns.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderDeletedItems);
            }
            finally
            {
            }

            serviceMailboxIntegration = new ServiceMailboxIntegration(Calendar, 
                                                                      _hidden_folder, 
                                                                      deletedItemsFolder, 
                                                                      smtpAddress);

        }

        #endregion

        #region Search Methods
        
        public void Search(Outlook.MAPIFolder folder, string filter, bool search_sub_folders, Action<Outlook.Search> callback) {
            var tag = Guid.NewGuid().ToString("N");

            _search_callbacks.Add(tag, callback);

            var folder_path = folder.FolderPath;

            Application.AdvancedSearch("'{folder_path}'",filter,search_sub_folders,tag);
        }

        private void Application_AdvancedSearchComplete(Outlook.Search search_object) {
            var tag = search_object.Tag;
            Action<Outlook.Search> callback;

            try {
                if (_search_callbacks.ContainsKey(tag)) {
                    callback = _search_callbacks[tag];
                    _search_callbacks.Remove(tag);

                    callback(search_object);
                }
            }
            catch {
            }
        }

        private void Application_AdvancedSearchStopped(Outlook.Search search_object) {
            try {
                var tag = search_object.Tag;
                if (_search_callbacks.ContainsKey(tag)) {
                    _search_callbacks.Remove(tag);
                }
            }
            catch {
            }
        }

        #endregion

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }

}
