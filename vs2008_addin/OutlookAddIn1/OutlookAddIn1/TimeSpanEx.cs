﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneSpace.OutlookPlugin.Properties;

namespace OneSpace.OutlookPlugin.Extentions {
    internal static class TimeSpanEx {
        public static string ToFriendlyString(this TimeSpan time_span_value, bool show_seconds_if_less_then_one_min = false) {
            var result = new StringBuilder();

            var time_span = new TimeSpan(Math.Abs(time_span_value.Days),
                                         Math.Abs(time_span_value.Hours),
                                         Math.Abs(time_span_value.Minutes),
                                         Math.Abs(time_span_value.Seconds));
 
            /* Ref: https://connectib.atlassian.net/browse/OUTPLUG-213 */
            /* Todo: Expand to allow the caller to specify what unit to go down to. */

            if (time_span.TotalDays > 1.0d) {
                result.AppendTimeUnit(time_span.TotalDays.ToFlooredInt32(), Globals.ThisAddIn.resourceManager.GetString("timeUnitDay"), Globals.ThisAddIn.resourceManager.GetString("timeUnitDays"));

                if (time_span.Hours > 0 && time_span.Minutes > 0) {
                    result.AppendTimeUnit(time_span.Hours, Globals.ThisAddIn.resourceManager.GetString("timeUnitHour"), Globals.ThisAddIn.resourceManager.GetString("timeUnitHours"));
                }

                if(time_span.Minutes > 0) {
                    result.AppendTimeUnit(time_span.Minutes, Globals.ThisAddIn.resourceManager.GetString("timeUnitMinute"), Globals.ThisAddIn.resourceManager.GetString("timeUnitMinutes"));
                }
            }
            else if (time_span.TotalHours <= 24.0d && time_span.TotalHours > 1.0d) {
                result.AppendTimeUnit(time_span.TotalHours.ToFlooredInt32(), Globals.ThisAddIn.resourceManager.GetString("timeUnitHour"), Globals.ThisAddIn.resourceManager.GetString("timeUnitHours"));

                if (time_span.Minutes > 0) {
                    result.AppendTimeUnit(time_span.Minutes, Globals.ThisAddIn.resourceManager.GetString("timeUnitMinute"), Globals.ThisAddIn.resourceManager.GetString("timeUnitMinutes"));
                }
            }
            else if (time_span.TotalMinutes <= 60.0d && time_span.TotalMinutes >= 1.0d) {
                result.AppendTimeUnit(time_span.TotalMinutes.ToFlooredInt32(), Globals.ThisAddIn.resourceManager.GetString("timeUnitMinute"), Globals.ThisAddIn.resourceManager.GetString("timeUnitMinutes"));
            }
            else {
                if (show_seconds_if_less_then_one_min) {
                    result.AppendTimeUnit(time_span.TotalSeconds.ToFlooredInt32(), Globals.ThisAddIn.resourceManager.GetString("timeUnitSecond"), Globals.ThisAddIn.resourceManager.GetString("timeUnitSeconds"));
                }
                else {
                    result.Append(Globals.ThisAddIn.resourceManager.GetString("timeLessThenAMinute"));
                }
            }
            
            return result.ToString().Trim();
        }

        private static void AppendTimeUnit(this StringBuilder result, int value, string unit_singular, string unit_plural) {
            if (value == 1) {
                result.Append($"{value} {unit_singular} ");
            }
            else {
                result.Append($"{value} {unit_plural} ");
            }
        }
    }
}
