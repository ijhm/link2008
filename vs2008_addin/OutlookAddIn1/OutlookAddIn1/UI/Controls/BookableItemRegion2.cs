﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.Data;

namespace OneSpace.OutlookPlugin.Controls
{
    public partial class BookableItemRegion2 : UserControl
    {
        public event EventHandler ToggleEvent;
        public event EventHandler<ItemSelectedEventArgs> ItemSelectedEvent;
        public bool IsHidden;
        public int CollapsedHeight;

        public BookableItemRegion2()
        {
            InitializeComponent();
            CollapsedHeight = butShowHide.Height;
            IsHidden = true;
            butShowHide.Image = Resource1.bullet_arrow_down;
            this.AutoScroll = false;
            this.Height = CollapsedHeight;
            listView.Columns[0].Width = this.Width / 2;
            listView.Columns[1].Width = this.Width / 6;
            listView.Columns[2].Width = this.Width / 6;

       //     UIUtilities.SetListViewSpacing(listView, 64 + 16, 100 + 60);

        }

        public string Title
        {
            set
            {
                labelPanelLabel.Text = value;
            }
        }

        private void butShowHide_Click(object sender, EventArgs e)
        {
            Toggle();
            if (this.ToggleEvent != null)
            {
                this.ToggleEvent(this, e);
            }
        }

        private void ShowHide()
        {
        }

        public void ClearItems()
        {
            listView.Items.Clear();
        }

        public void AddItem(Data.BookingItem item)
        {
            string[] itemValues = { " " + item.Name, item.CostToCurrencyString(), item.CostUnit + "\r\n" };
            var newItem = new ListViewItem(itemValues);
            newItem.Tag = item;
            listView.Items.Add(newItem);
/*
 * 
 * Removed the configurable show equipment price
            if (item.ItemType == 1 && !Globals.ThisAddIn.ShowEquipmentPrice)
            {

                string[] itemValues = { " " + item.Name, "", item.TypeName + "\r\n" };
                var newItem = new ListViewItem(itemValues);
                newItem.Tag = item;
                listView.Items.Add(newItem);
            }
            else
            {
                string[] itemValues = { " " + item.Name, Utilities.OutlookUtilities.FormatMoney(item.Cost), item.TypeName + "\r\n" };
                var newItem = new ListViewItem(itemValues);
                newItem.Tag = item;
                listView.Items.Add(newItem);
            }
            */
        }

        public void Toggle()
        {
            if (IsHidden)
            {
                this.Height = 250;
                IsHidden = false;
                butShowHide.Image = Resource1.bullet_arrow_up;
            }
            else
            {
                this.Height = CollapsedHeight;
                IsHidden = true;
                butShowHide.Image = Resource1.bullet_arrow_down;
            }
        }

        public void Collapse()
        {
            this.Height = CollapsedHeight;
            IsHidden = true;
            butShowHide.Image = Resource1.bullet_arrow_down;
        }


        private void labelPanellabel_Click(object sender, EventArgs e)
        {
            Toggle();
            if (this.ToggleEvent != null)
            {
                this.ToggleEvent(this, e);
            }
        }

        private void listView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {

            /*
            if (e.IsSelected)
            {
                if (this.ItemSelectedEvent != null)
                {
                    this.ItemSelectedEvent(this, new ItemSelectedEventArgs { Item = (Data.BookingItem)e.Item.Tag });
                }
            }
            else
            {
                if (this.ItemSelectedEvent != null)
                {
                    this.ItemSelectedEvent(this, null);
                }
            }
            */

        }

        private void BookableItemRegion2_Paint(object sender, PaintEventArgs e)
        {
            labelPanelLabel.Left = 0;
            labelPanelLabel.Width = this.Width - CollapsedHeight;
            labelPanelLabel.Top = 0;
            listView.Top = CollapsedHeight+1;
            listView.Left = 0;
            listView.Width = this.Width;
            listView.Height = this.Height - CollapsedHeight;
            butShowHide.Left = this.Width - butShowHide.Width-1;
            butShowHide.Top = 0;
            butShowHide.Width = CollapsedHeight;
        }

        private void listView_Click(object sender, EventArgs e)
        {

        }

        private void listView_Click_1(object sender, EventArgs e)
        {
            ListView view = (ListView)sender;

            if (view.SelectedItems.Count > 0)
            {
                if (this.ItemSelectedEvent != null)
                {
                    this.ItemSelectedEvent(this, new ItemSelectedEventArgs { Item = (Data.BookingItem)view.SelectedItems[0].Tag });
                }
            }
            else
            {
                if (this.ItemSelectedEvent != null)
                {
                    this.ItemSelectedEvent(this, null);
                }
            }
        }

        private void listView_DoubleClick(object sender, EventArgs e)
        {
            ListView view = (ListView)sender;

            if (view.SelectedItems.Count > 0)
            {
                if (this.ItemSelectedEvent != null)
                {
                    this.ItemSelectedEvent(this, new ItemSelectedEventArgs { Item = (Data.BookingItem)view.SelectedItems[0].Tag, DoubleClicked=true });
                }
            }
            else
            {
                if (this.ItemSelectedEvent != null)
                {
                    this.ItemSelectedEvent(this, null);
                }
            }
        }
    }



}
