﻿namespace OneSpace.OutlookPlugin.Controls
{
    partial class BookableItemRegion2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookableItemRegion2));
            this.labelPanelLabel = new System.Windows.Forms.Label();
            this.listView = new System.Windows.Forms.ListView();
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colUnit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.butShowHide = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelPanelLabel
            // 
            resources.ApplyResources(this.labelPanelLabel, "labelPanelLabel");
            this.labelPanelLabel.Name = "labelPanelLabel";
            this.labelPanelLabel.Click += new System.EventHandler(this.labelPanellabel_Click);
            // 
            // listView
            // 
            this.listView.Activation = System.Windows.Forms.ItemActivation.OneClick;
            resources.ApplyResources(this.listView, "listView");
            this.listView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.colCost,
            this.colUnit});
            this.listView.FullRowSelect = true;
            this.listView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listView.MultiSelect = false;
            this.listView.Name = "listView";
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listView_ItemSelectionChanged);
            this.listView.Click += new System.EventHandler(this.listView_Click_1);
            this.listView.DoubleClick += new System.EventHandler(this.listView_DoubleClick);
            // 
            // colName
            // 
            resources.ApplyResources(this.colName, "colName");
            // 
            // colCost
            // 
            resources.ApplyResources(this.colCost, "colCost");
            // 
            // colUnit
            // 
            resources.ApplyResources(this.colUnit, "colUnit");
            // 
            // butShowHide
            // 
            resources.ApplyResources(this.butShowHide, "butShowHide");
            this.butShowHide.BackColor = System.Drawing.Color.Transparent;
            this.butShowHide.Name = "butShowHide";
            this.butShowHide.UseVisualStyleBackColor = false;
            this.butShowHide.Click += new System.EventHandler(this.butShowHide_Click);
            // 
            // BookableItemRegion2
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.listView);
            this.Controls.Add(this.butShowHide);
            this.Controls.Add(this.labelPanelLabel);
            this.Name = "BookableItemRegion2";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.BookableItemRegion2_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelPanelLabel;
        private System.Windows.Forms.Button butShowHide;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colCost;
        private System.Windows.Forms.ColumnHeader colUnit;
    }
}
