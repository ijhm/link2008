﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OneSpace.OutlookPlugin
{
    public partial class CIBDateTimeControl : UserControl
    {
        public event EventHandler DateAndTimeChanged;

        public CIBDateTimeControl()
        {
            InitializeComponent();
        }

        public DateTime DateAndTime
        {
            get
            {
                return datePicker.Value.Date + timePicker.Value.TimeOfDay; 
            }

            set
            {
                datePicker.Value = value.Date;
                timePicker.Value = value;
            }
        }

        private void datePicker_ValueChanged(object sender, EventArgs e)
        {
            if (this.DateAndTimeChanged != null)
            {
                this.DateAndTimeChanged(this, e);
            }
        }

        private void timePicker_ValueChanged(object sender, EventArgs e)
        {
            if (this.DateAndTimeChanged != null)
            {
                this.DateAndTimeChanged(this, e);
            }
        }
    }
}
