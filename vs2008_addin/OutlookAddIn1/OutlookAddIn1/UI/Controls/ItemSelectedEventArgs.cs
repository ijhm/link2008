﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Controls {
    public class ItemSelectedEventArgs : EventArgs {
        public OneSpace.OutlookPlugin.Data.BookingItem Item { get; set; }
        public bool DoubleClicked { get; set; }
    }
}
