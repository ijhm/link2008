﻿namespace OneSpace.OutlookPlugin.UI.Controls.Reports
{
    partial class ReportFilterCostCentres
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportFilterCostCentres));
            this.dateFrom = new OneSpace.OutlookPlugin.CIBDateTimeControl();
            this.dateTo = new OneSpace.OutlookPlugin.CIBDateTimeControl();
            this.regionBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelHeading = new System.Windows.Forms.Label();
            this.butPrint = new System.Windows.Forms.Button();
            this.butReport = new System.Windows.Forms.Button();
            this.labelLocation = new System.Windows.Forms.Label();
            this.locationBox = new System.Windows.Forms.ComboBox();
            this.floorBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.boxCostCentre = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // dateFrom
            // 
            this.dateFrom.DateAndTime = new System.DateTime(2017, 4, 19, 0, 0, 0, 0);
            resources.ApplyResources(this.dateFrom, "dateFrom");
            this.dateFrom.Name = "dateFrom";
            // 
            // dateTo
            // 
            this.dateTo.DateAndTime = new System.DateTime(2017, 4, 19, 0, 0, 0, 0);
            resources.ApplyResources(this.dateTo, "dateTo");
            this.dateTo.Name = "dateTo";
            // 
            // regionBox
            // 
            this.regionBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.regionBox.FormattingEnabled = true;
            resources.ApplyResources(this.regionBox, "regionBox");
            this.regionBox.Name = "regionBox";
            this.regionBox.SelectedIndexChanged += new System.EventHandler(this.regionBox_SelectedIndexChanged);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // labelHeading
            // 
            resources.ApplyResources(this.labelHeading, "labelHeading");
            this.labelHeading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Tag = "Heading";
            // 
            // butPrint
            // 
            resources.ApplyResources(this.butPrint, "butPrint");
            this.butPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            this.butPrint.ForeColor = System.Drawing.Color.White;
            this.butPrint.Name = "butPrint";
            this.butPrint.Tag = "main";
            this.butPrint.UseVisualStyleBackColor = false;
            this.butPrint.Click += new System.EventHandler(this.butPrint_Click);
            // 
            // butReport
            // 
            this.butReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            resources.ApplyResources(this.butReport, "butReport");
            this.butReport.ForeColor = System.Drawing.Color.White;
            this.butReport.Name = "butReport";
            this.butReport.Tag = "main";
            this.butReport.UseVisualStyleBackColor = false;
            this.butReport.Click += new System.EventHandler(this.butReport_Click);
            // 
            // labelLocation
            // 
            resources.ApplyResources(this.labelLocation, "labelLocation");
            this.labelLocation.Name = "labelLocation";
            // 
            // locationBox
            // 
            this.locationBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.locationBox.FormattingEnabled = true;
            resources.ApplyResources(this.locationBox, "locationBox");
            this.locationBox.Name = "locationBox";
            this.locationBox.SelectedIndexChanged += new System.EventHandler(this.locationBox_SelectedIndexChanged);
            // 
            // floorBox
            // 
            this.floorBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.floorBox.FormattingEnabled = true;
            resources.ApplyResources(this.floorBox, "floorBox");
            this.floorBox.Name = "floorBox";
            this.floorBox.SelectedIndexChanged += new System.EventHandler(this.floorBox_SelectedIndexChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // labelDate
            // 
            resources.ApplyResources(this.labelDate, "labelDate");
            this.labelDate.Name = "labelDate";
            // 
            // boxCostCentre
            // 
            this.boxCostCentre.FormattingEnabled = true;
            resources.ApplyResources(this.boxCostCentre, "boxCostCentre");
            this.boxCostCentre.Name = "boxCostCentre";
            // 
            // ReportFilterCostCentres
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.boxCostCentre);
            this.Controls.Add(this.dateFrom);
            this.Controls.Add(this.dateTo);
            this.Controls.Add(this.regionBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelHeading);
            this.Controls.Add(this.butPrint);
            this.Controls.Add(this.butReport);
            this.Controls.Add(this.labelLocation);
            this.Controls.Add(this.locationBox);
            this.Controls.Add(this.floorBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelDate);
            this.Name = "ReportFilterCostCentres";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CIBDateTimeControl dateFrom;
        private CIBDateTimeControl dateTo;
        private System.Windows.Forms.ComboBox regionBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelHeading;
        public System.Windows.Forms.Button butPrint;
        private System.Windows.Forms.Button butReport;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.ComboBox locationBox;
        private System.Windows.Forms.ComboBox floorBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.ComboBox boxCostCentre;
    }
}
