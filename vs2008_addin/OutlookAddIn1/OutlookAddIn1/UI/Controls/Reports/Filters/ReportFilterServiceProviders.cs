﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.Reports;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Properties;

namespace OneSpace.OutlookPlugin.UI.Controls.Reports
{
    public partial class ReportFilterServiceProviders : UserControl
    {
        public event EventHandler<ReportEventsFilter1Args> RunReportEvent;
        public event EventHandler PrintReportEvent;

        public bool enablePrint;
        public bool enableReport;

        public ReportFilterServiceProviders()
        {
            InitializeComponent();
            enableReport = false;
            UIUtilities.SetButton(butReport, enableReport);
            enablePrint = false;
            UIUtilities.SetButton(butPrint, enablePrint);
            locationBox.Enabled = false;
            providersBox.Items.Add(Lang.filterAllServiceProviders);
            try
            {
                foreach (var mailbox in Globals.ThisAddIn.OneSpaceData.ServiceMailboxes)
                {
                    providersBox.Items.Add(mailbox);
                }
                foreach (OneSpace.OutlookPlugin.Data.Region region in Globals.ThisAddIn.OneSpaceData.Regions)
                {
                    regionBox.Items.Add(region);
                }
            }
            catch (Exception)
            {
            }

            DateTime now = DateTime.Now;
            dateFrom.DateAndTime = new DateTime(now.Year, now.Month, now.Day, now.Hour, 0, 0);
            dateTo.DateAndTime = dateFrom.DateAndTime.AddDays(1);
            floorBox.Items.Add(Lang.filterSelectFloor);
            floorBox.Enabled = false;
            floorBox.SelectedIndex = 0;
            labelHeading.ForeColor = Branding.Resource.TextColour;

        }

        #region Control filters
        private OneSpace.OutlookPlugin.Data.Region SelectedRegion
        {
            get
            {
                int regionId = regionBox.SelectedIndex;
                return Globals.ThisAddIn.OneSpaceData.Regions[regionId];
            }
        }
        private Location SelectedLocation
        {
            get
            {
                int regionId = regionBox.SelectedIndex;
                return Globals.ThisAddIn.OneSpaceData.Regions[regionId].Locations[locationBox.SelectedIndex - 1];
            }
        }

        private Floor SelectedFloor
        {
            get
            {
                if (floorBox.SelectedIndex > 0)
                {
                    return SelectedLocation.Floors[floorBox.SelectedIndex - 1];
                }
                else
                {
                    return null;
                }
            }
        }

        private void locationBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedLocation = locationBox.SelectedIndex;
            if (selectedLocation > 0)
            {
                floorBox.Items.Clear();
                floorBox.Items.Add(Globals.ThisAddIn.resourceManager.GetString("filterSelectFloor"));
                foreach (Floor floor in SelectedLocation.Floors)
                {
                    floorBox.Items.Add(floor);
                }
                floorBox.Enabled = true;
                enableReport = true;
            }
            else
            {
                floorBox.Enabled = false;
                enableReport = false;
            }
            UIUtilities.SetButton(butReport, enableReport);

        }

        private void floorBox_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void regionBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int regionId = regionBox.SelectedIndex;
            locationBox.Items.Clear();
            locationBox.Items.Add(Globals.ThisAddIn.resourceManager.GetString("filterSelectLocation"));
            locationBox.SelectedIndex = 0;
            foreach (Location loc in Globals.ThisAddIn.OneSpaceData.Regions[regionId].Locations)
            {
                locationBox.Items.Add(loc);
            }
            locationBox.Enabled = true;
        }
        #endregion

        private void butPrint_Click(object sender, EventArgs e)
        {
            if (!enablePrint)
            {
                return;
            }
            if (PrintReportEvent != null)
            {
                this.PrintReportEvent(this, e);
            }
            //            webView.ShowPrintDialog();
        }

        private void butReport_Click(object sender, EventArgs e)
        {
            if (this.RunReportEvent != null)
            {
                this.RunReportEvent(this, ParamList);
            }
        }

        public ReportEventsFilter1Args ParamList
        {
            get
            {
                var result = new ReportEventsFilter1Args
                {
                    From = dateFrom.DateAndTime,
                    To = dateTo.DateAndTime,
                    Region = SelectedRegion,
                    Location = SelectedLocation,
                    Floor = SelectedFloor
                };
                if (providersBox.SelectedIndex==0)
                {
                    result.Filter = string.Empty;
                }
                else
                {
                    result.Filter = providersBox.Text;
                }
                return result;
            }

        }

    }
}
