﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.Reports;
using System.Threading;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Extentions;

namespace OneSpace.OutlookPlugin.UI.Controls.Reports
{
    public partial class ReportServicesOrdered : UserControl
    {

        public event EventHandler ReportFinished;
        public delegate void ActionFinished();
        private Reporting report;
        public int LocationId;
        public DateTime From;
        public DateTime To;


        public ReportServicesOrdered()
        {
            InitializeComponent();
            activityBar.Visible = false;
            labelReporting.Text = Globals.ThisAddIn.resourceManager.GetString("reportDefineFilters");
            labelReporting.ForeColor = Color.Gray;
        }

        private void report_Filter11_RunReportEvent(object sender, OutlookPlugin.Reports.ReportEventsFilter1Args e)
        {
            activityBar.MarqueeAnimationSpeed = 30;
            activityBar.Visible = true;
            labelReporting.Visible = true;
            labelReporting.Text = Globals.ThisAddIn.resourceManager.GetString("reportGeneratingReport");
            this.Refresh();
            FilterServiceProviders.enablePrint = false;
            button_download.Enabled = false;
            UIUtilities.SetButton(FilterServiceProviders.butPrint, FilterServiceProviders.enablePrint);
            webView.Visible = false;
            Thread t = new Thread(() =>
            {
                report = new Reporting_ServicesOrdered();
                report.GenerateReport(e.Location,e.Floor,e.MeetingRoom,e.From,e.To,e.Filter);
                LocationId = e.Location.Id;
                From = e.From;
                To = e.To;
                webView.Invoke(ActionFinishedUI);
            });
            t.Start();
        }

        private void ActionFinishedUI()
        {
            webView.Navigate("file:///" + report.tempPath + "?" + DateTime.Now.ToString("HH_mm_ss_ff"));
/*
            enableReport = true;
            enablePrint = true;
            UIUtilities.SetButton(butReport, enableReport);
            UIUtilities.SetButton(butPrint, enableReport);
            */
            activityBar.MarqueeAnimationSpeed = 0;
            activityBar.Visible = false;
            labelReporting.Visible = false;
            FilterServiceProviders.enablePrint = true;
            UIUtilities.SetButton(FilterServiceProviders.butPrint, FilterServiceProviders.enablePrint);
            button_download.Enabled = true;
            webView.Visible = true;
        }

        private void reportFilter11_PrintReportEvent(object sender, EventArgs e)
        {
            if (!FilterServiceProviders.enablePrint)
            {
                return;
            }
            webView.ShowPrintDialog();
        }

        private void button_download_Click(object sender, EventArgs e) {
            var save_dialog = new SaveFileDialog();
            save_dialog.Filter = "Excel (*.xlsx)|*.xlsx";
            var location = Globals.ThisAddIn.OneSpaceData.GetLocationById(LocationId);
            var region = Globals.ThisAddIn.OneSpaceData.GetRegionByLocationId(LocationId);

            save_dialog.FileName = string.Format(Branding.Resource.defaultXlsReportFileName, region.Name, location.Name, From.ToString("dd MMM yyyy"), To.ToString("dd MMM yyyy"));

            save_dialog.RestoreDirectory = true;
            var result = save_dialog.ShowDialog();

            if (result == DialogResult.OK) {
                report.SaveXlsx(save_dialog.FileName);
            }
        }
    }
}
