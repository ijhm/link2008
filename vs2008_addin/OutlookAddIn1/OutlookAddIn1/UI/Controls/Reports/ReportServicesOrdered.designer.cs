﻿namespace OneSpace.OutlookPlugin.UI.Controls.Reports
{
    partial class ReportServicesOrdered
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportServicesOrdered));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_download = new System.Windows.Forms.Button();
            this.labelReporting = new System.Windows.Forms.Label();
            this.activityBar = new System.Windows.Forms.ProgressBar();
            this.webView = new System.Windows.Forms.WebBrowser();
            this.FilterServiceProviders = new OneSpace.OutlookPlugin.UI.Controls.Reports.ReportFilterServiceProviders();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.button_download);
            this.groupBox1.Controls.Add(this.labelReporting);
            this.groupBox1.Controls.Add(this.activityBar);
            this.groupBox1.Controls.Add(this.webView);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // button_download
            // 
            resources.ApplyResources(this.button_download, "button_download");
            this.button_download.Image = global::OneSpace.OutlookPlugin.Resource1.Download_x16;
            this.button_download.Name = "button_download";
            this.button_download.UseVisualStyleBackColor = true;
            this.button_download.Click += new System.EventHandler(this.button_download_Click);
            // 
            // labelReporting
            // 
            resources.ApplyResources(this.labelReporting, "labelReporting");
            this.labelReporting.BackColor = System.Drawing.Color.White;
            this.labelReporting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.labelReporting.Name = "labelReporting";
            this.labelReporting.Tag = "Heading";
            // 
            // activityBar
            // 
            resources.ApplyResources(this.activityBar, "activityBar");
            this.activityBar.Name = "activityBar";
            this.activityBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // webView
            // 
            this.webView.AllowWebBrowserDrop = false;
            resources.ApplyResources(this.webView, "webView");
            this.webView.CausesValidation = false;
            this.webView.IsWebBrowserContextMenuEnabled = false;
            this.webView.Name = "webView";
            this.webView.ScriptErrorsSuppressed = true;
            this.webView.WebBrowserShortcutsEnabled = false;
            // 
            // FilterServiceProviders
            // 
            this.FilterServiceProviders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.FilterServiceProviders, "FilterServiceProviders");
            this.FilterServiceProviders.Name = "FilterServiceProviders";
            this.FilterServiceProviders.RunReportEvent += new System.EventHandler<OneSpace.OutlookPlugin.Reports.ReportEventsFilter1Args>(this.report_Filter11_RunReportEvent);
            this.FilterServiceProviders.PrintReportEvent += new System.EventHandler(this.reportFilter11_PrintReportEvent);
            // 
            // ReportServicesOrdered
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.FilterServiceProviders);
            this.Controls.Add(this.groupBox1);
            this.Name = "ReportServicesOrdered";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ProgressBar activityBar;
        private System.Windows.Forms.WebBrowser webView;
        private ReportFilterServiceProviders FilterServiceProviders;
        public System.Windows.Forms.Label labelReporting;
        private System.Windows.Forms.Button button_download;
    }
}
