﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.Data;

namespace OneSpace.OutlookPlugin.Controls
{
    public partial class SelectionRegion2 : UserControl
    {
        public event EventHandler ToggleEvent;
        public int CollapsedHeight;

        public bool IsHidden;

        public SelectionRegion2()
        {
            InitializeComponent();
            CollapsedHeight = butShowHide.Height;
            IsHidden = true;
            butShowHide.Image = Resource1.bullet_arrow_down;
            this.AutoScroll = false;
            this.Height = CollapsedHeight;
            butShowHide.Left = this.Width - butShowHide.Width - 1;
        }

        public void DeselectAll()
        {
            for (int i = 0; i < selectList.Items.Count; i++)
            {
                selectList.SetItemChecked(i, false);
            }
        }

        public string Title
        {
            set
            {
                labelPanelPanel.Text = value;
            }
        }

        private void butShowHide_Click(object sender, EventArgs e)
        {
            Toggle();
            if (this.ToggleEvent != null)
            {
                this.ToggleEvent(this, e);
            }
        }

        public void ClearItems()
        {
            selectList.Items.Clear();
        }

        public void AddItem(object obj)
        {
            selectList.Items.Add(obj);
        }

        public void Toggle()
        {
            if (IsHidden)
            {
                this.Height = 200;
                IsHidden = false;
                butShowHide.Image = Resource1.bullet_arrow_up;
            }
            else
            {
                this.Height = CollapsedHeight;
                IsHidden = true;
                butShowHide.Image = Resource1.bullet_arrow_down;
            }
        }

        public void Collapse()
        {
            this.Height = CollapsedHeight;
            IsHidden = true;
            butShowHide.Image = Resource1.bullet_arrow_down;
        }

        private void labelPanelPanel_Click(object sender, EventArgs e)
        {
            Toggle();
            if (this.ToggleEvent != null)
            {
                this.ToggleEvent(this, e);
            }
        }

        public bool IsAllIncluded(List<int> idList)
        {
            bool result = false;
            int matchCount = 0;
            int selectCount = 0;
            for (int i = 0; i < selectList.Items.Count; i++)
            {
                if (selectList.GetItemChecked(i))
                {
                    selectCount++;
                    BookingItem item = (BookingItem)selectList.Items[i];
                    if (idList.Contains(item.Id))
                    {
                        matchCount++;
                    }
                }
            }
            if (matchCount == selectCount)
            {
                result = true;
            }
            return result;
        }

        public void EnableList()
        {
            selectList.Enabled = true;
        }
        public void DisableList()
        {
            selectList.Enabled = false;
        }

        private void BookableItemRegion2_Paint(object sender, PaintEventArgs e)
        {
        }

        private void SelectionRegion2_SizeChanged(object sender, EventArgs e)
        {
            labelPanelPanel.Left = 0;
            labelPanelPanel.Width = this.Width - CollapsedHeight;
            labelPanelPanel.Top = 0;
            selectList.Top = CollapsedHeight + 1;
            selectList.Left = 0;
            selectList.Width = this.Width;
            selectList.Height = this.Height - CollapsedHeight;
            butShowHide.Left = this.Width - butShowHide.Width - 1;
      //      butShowHide.Top = 1;
            butShowHide.Width = CollapsedHeight;

        }
    }
}
