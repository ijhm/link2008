﻿namespace OneSpace.OutlookPlugin.Controls
{
    partial class SelectionRegion2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectionRegion2));
            this.labelPanelPanel = new System.Windows.Forms.Label();
            this.selectList = new System.Windows.Forms.CheckedListBox();
            this.butShowHide = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelPanelPanel
            // 
            resources.ApplyResources(this.labelPanelPanel, "labelPanelPanel");
            this.labelPanelPanel.Name = "labelPanelPanel";
            this.labelPanelPanel.Click += new System.EventHandler(this.labelPanelPanel_Click);
            // 
            // selectList
            // 
            resources.ApplyResources(this.selectList, "selectList");
            this.selectList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.selectList.CheckOnClick = true;
            this.selectList.FormattingEnabled = true;
            this.selectList.Name = "selectList";
            // 
            // butShowHide
            // 
            resources.ApplyResources(this.butShowHide, "butShowHide");
            this.butShowHide.Image = global::OneSpace.OutlookPlugin.Resource1.bullet_arrow_up;
            this.butShowHide.Name = "butShowHide";
            this.butShowHide.UseVisualStyleBackColor = true;
            this.butShowHide.Click += new System.EventHandler(this.butShowHide_Click);
            // 
            // SelectionRegion2
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.butShowHide);
            this.Controls.Add(this.selectList);
            this.Controls.Add(this.labelPanelPanel);
            this.Name = "SelectionRegion2";
            this.SizeChanged += new System.EventHandler(this.SelectionRegion2_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.BookableItemRegion2_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelPanelPanel;
        private System.Windows.Forms.CheckedListBox selectList;
        private System.Windows.Forms.Button butShowHide;
    }
}
