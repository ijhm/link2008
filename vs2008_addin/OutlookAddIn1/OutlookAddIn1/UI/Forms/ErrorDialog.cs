﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OneSpace.OutlookPlugin
{
    public partial class ErrorDialog : Form
    {

        public ErrorDialog()
        {
            InitializeComponent();
        }

        public ErrorDialog(string title,string message)
        {
            InitializeComponent();
            labelTitle.Text = title;
            labelMessage.Text = message;
        }

        private void butClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
