﻿using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OneSpace.OutlookPlugin
{
    public partial class FormAbout : Form
    {
        Data.Security.LicenseKey key;
        ToolTip tip;

        public FormAbout()
        {
            InitializeComponent();
            linkServiceData.Visible = Globals.ThisAddIn.WriteToDebugLog;
            this.Icon = Branding.Resource.AboutIcon;
            labelTitle.Text = Branding.Resource.AboutTitle;
            this.Text = Branding.Resource.AboutTitle;
            pictureBox1.Image = Branding.Resource.Banner;

            UIUtilities.SetControls(this);
            labelAccountWarning.Visible = false;

#if DEBUG
            Logging.ActivityLog.WriteToLog("Debug mode - Enabling service data link");
#endif
            tip = new ToolTip();
            string version = Application.ProductVersion;
            linkOpenRedstone.Text = Branding.Resource.LinkUrl;

            labelAbout.Text = labelAbout.Text.Replace("[version]", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            labelAbout.Text = labelAbout.Text.Replace("[build]", version);
            key = Data.Security.LicenseKey.GetKey;
            if (key!=null)
            {
                //Is licensed
                labelLicenseState.Text = string.Format(Globals.ThisAddIn.resourceManager.GetString("licenseValidState"), key.ExpiryDate.ToString("dd MMMM yyyy"), key.Domain);
                if (!key.IsInLicense)
                {
                    labelLicenseState.ForeColor = Color.Red;
                    tip.SetToolTip(this.labelLicenseState, Globals.ThisAddIn.resourceManager.GetString("msgLicenseExpired"));
                }
                else
                {
                    labelLicenseState.ForeColor = Color.Black;
                    TimeSpan days = key.ExpiryDate - DateTime.Now;
                    string msg = string.Format(Globals.ThisAddIn.resourceManager.GetString("msgLicenseRemaining"), days.TotalDays);
                    tip.SetToolTip(this.labelLicenseState, msg);

                    if (key.Domain!=Globals.ThisAddIn.Domain)
                    {
                        labelAccountWarning.Text = string.Format(Globals.ThisAddIn.resourceManager.GetString("pluginInactive"), Globals.ThisAddIn.Domain);
                        labelAccountWarning.Visible = true;
                    }
                }
                butSetKey.Text = Globals.ThisAddIn.resourceManager.GetString("licenseButtonRemoveKey");
                linkEmail.Visible = false;
            }
            else
            {
                //Not licensed
                labelLicenseState.Text = Globals.ThisAddIn.resourceManager.GetString("licenseInvalidState");
                butSetKey.Text = Globals.ThisAddIn.resourceManager.GetString("licenseButtonAddKey");
                linkEmail.Visible = true;
            }
        }

        private void butClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        [Obsolete("Update to white labled URL", true)]
        private void linkOpenRedstone_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var proc = new ProcessStartInfo("https://" + Branding.Resource.LinkUrl);
            Process.Start(proc);
        }

        [Obsolete("Update to white labled mail", true)]
        private void linkEmail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var proc = new ProcessStartInfo("mailto:sales@redstoneconnectplc.com?subject=Outlook%20Plugin%20License%20Key");
            Process.Start(proc);
        }

        private void butSetKey_Click(object sender, EventArgs e)
        {
            if (key==null)
            {
                LicenseKeyForm licenseForm = new LicenseKeyForm();
                licenseForm.ShowDialog();
                if (licenseForm.key != null)
                {
                    key = licenseForm.key;
                    labelLicenseState.Text = string.Format(Globals.ThisAddIn.resourceManager.GetString("licenseValidState"), key.ExpiryDate.ToString("dd MMMM yyyy"), key.Domain);
                    if (!key.IsInLicense)
                    {
                        labelLicenseState.ForeColor = Color.Red;
                        tip.SetToolTip(this.labelLicenseState, Globals.ThisAddIn.resourceManager.GetString("msgLicenseExpired"));
                    }
                    else
                    {
                        labelLicenseState.ForeColor = Color.Black;
                        TimeSpan days = key.ExpiryDate - DateTime.Now;
                        string msg = string.Format(Globals.ThisAddIn.resourceManager.GetString("msgLicenseRemaining"),days.TotalDays);
                        tip.SetToolTip(this.labelLicenseState, msg);
                        //                        MessageBox.Show(Properties.Resources.licenseKeyAssignedCaption, Properties.Resources.licenseKeyAssignedTitle, MessageBoxButtons.OK,MessageBoxIcon.Information);
                        //                        Application.Exit();
            //            Globals.ThisAddIn.ClearCache();
            //            Globals.ThisAddIn.LoadState = ThisAddIn.enum_LoadingState.Loading;
                    }
                    butSetKey.Text = Globals.ThisAddIn.resourceManager.GetString("licenseButtonRemoveKey");
                    linkEmail.Visible = false;
                }
                else
                {
                    labelLicenseState.ForeColor = Color.Black;
                    labelLicenseState.Text = Globals.ThisAddIn.resourceManager.GetString("licenseInvalidState");
                    butSetKey.Text = Globals.ThisAddIn.resourceManager.GetString("licenseButtonAddKey");
                    linkEmail.Visible = true;
                }
            }
            else
            {
                if (MessageBox.Show(Globals.ThisAddIn.resourceManager.GetString("licenseRemoveKeyCaption"), Globals.ThisAddIn.resourceManager.GetString("licenseRemoveKeyTitle"), MessageBoxButtons.OKCancel,MessageBoxIcon.Question)==DialogResult.OK)
                {
                    Data.Security.LicenseKey.ClearKey();
                    labelLicenseState.Text = Globals.ThisAddIn.resourceManager.GetString("licenseInvalidState");
                    butSetKey.Text = Globals.ThisAddIn.resourceManager.GetString("licenseButtonAddKey");
                    linkEmail.Visible = true;
                    labelLicenseState.ForeColor = Color.Black;
                    key = null;
                    Globals.ThisAddIn.Deactivate();
                }
            }
        }

        private void butClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormAbout_Load(object sender, EventArgs e)
        {

        }

        private void linkServiceData_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var dlg = new SystemAdminForm();
            dlg.Show();
        }
    }
}
