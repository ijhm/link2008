﻿namespace OneSpace.OutlookPlugin
{
    partial class FormAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAbout));
            this.butSetKey = new System.Windows.Forms.Button();
            this.butClose = new System.Windows.Forms.Button();
            this.linkEmail = new System.Windows.Forms.LinkLabel();
            this.labelLicenseState = new System.Windows.Forms.Label();
            this.labelCurrentLicense = new System.Windows.Forms.Label();
            this.linkOpenRedstone = new System.Windows.Forms.LinkLabel();
            this.labelAbout = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelAccountWarning = new System.Windows.Forms.Label();
            this.labelTitle = new System.Windows.Forms.Label();
            this.linkServiceData = new System.Windows.Forms.LinkLabel();
            this.cibHorizontalLine2 = new OneSpace.OutlookPlugin.Controls.CIBHorizontalLine();
            this.cibHorizontalLine1 = new OneSpace.OutlookPlugin.Controls.CIBHorizontalLine();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // butSetKey
            // 
            this.butSetKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            resources.ApplyResources(this.butSetKey, "butSetKey");
            this.butSetKey.ForeColor = System.Drawing.Color.White;
            this.butSetKey.Name = "butSetKey";
            this.butSetKey.Tag = "";
            this.butSetKey.UseVisualStyleBackColor = false;
            this.butSetKey.Click += new System.EventHandler(this.butSetKey_Click);
            // 
            // butClose
            // 
            this.butClose.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.butClose, "butClose");
            this.butClose.Name = "butClose";
            this.butClose.Tag = "main";
            this.butClose.UseVisualStyleBackColor = false;
            this.butClose.Click += new System.EventHandler(this.butClose_Click_1);
            // 
            // linkEmail
            // 
            resources.ApplyResources(this.linkEmail, "linkEmail");
            this.linkEmail.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(41)))), ((int)(((byte)(51)))));
            this.linkEmail.Name = "linkEmail";
            this.linkEmail.TabStop = true;
            this.linkEmail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkEmail_LinkClicked);
            // 
            // labelLicenseState
            // 
            resources.ApplyResources(this.labelLicenseState, "labelLicenseState");
            this.labelLicenseState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.labelLicenseState.Name = "labelLicenseState";
            this.labelLicenseState.Tag = "Smaller";
            // 
            // labelCurrentLicense
            // 
            resources.ApplyResources(this.labelCurrentLicense, "labelCurrentLicense");
            this.labelCurrentLicense.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.labelCurrentLicense.Name = "labelCurrentLicense";
            // 
            // linkOpenRedstone
            // 
            this.linkOpenRedstone.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(41)))), ((int)(((byte)(51)))));
            resources.ApplyResources(this.linkOpenRedstone, "linkOpenRedstone");
            this.linkOpenRedstone.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(41)))), ((int)(((byte)(51)))));
            this.linkOpenRedstone.Name = "linkOpenRedstone";
            this.linkOpenRedstone.TabStop = true;
            this.linkOpenRedstone.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkOpenRedstone_LinkClicked);
            // 
            // labelAbout
            // 
            resources.ApplyResources(this.labelAbout, "labelAbout");
            this.labelAbout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.labelAbout.Name = "labelAbout";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::OneSpace.OutlookPlugin.Properties.Resources.plugin_logo;
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // labelAccountWarning
            // 
            resources.ApplyResources(this.labelAccountWarning, "labelAccountWarning");
            this.labelAccountWarning.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(41)))), ((int)(((byte)(51)))));
            this.labelAccountWarning.Name = "labelAccountWarning";
            // 
            // labelTitle
            // 
            resources.ApplyResources(this.labelTitle, "labelTitle");
            this.labelTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.labelTitle.Name = "labelTitle";
            // 
            // linkServiceData
            // 
            resources.ApplyResources(this.linkServiceData, "linkServiceData");
            this.linkServiceData.Name = "linkServiceData";
            this.linkServiceData.TabStop = true;
            this.linkServiceData.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkServiceData_LinkClicked);
            // 
            // cibHorizontalLine2
            // 
            resources.ApplyResources(this.cibHorizontalLine2, "cibHorizontalLine2");
            this.cibHorizontalLine2.Name = "cibHorizontalLine2";
            // 
            // cibHorizontalLine1
            // 
            resources.ApplyResources(this.cibHorizontalLine1, "cibHorizontalLine1");
            this.cibHorizontalLine1.Name = "cibHorizontalLine1";
            // 
            // FormAbout
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.linkServiceData);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.labelAccountWarning);
            this.Controls.Add(this.cibHorizontalLine2);
            this.Controls.Add(this.cibHorizontalLine1);
            this.Controls.Add(this.labelAbout);
            this.Controls.Add(this.linkOpenRedstone);
            this.Controls.Add(this.labelLicenseState);
            this.Controls.Add(this.linkEmail);
            this.Controls.Add(this.labelCurrentLicense);
            this.Controls.Add(this.butClose);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.butSetKey);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FormAbout";
            this.Load += new System.EventHandler(this.FormAbout_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button butSetKey;
        private System.Windows.Forms.Label labelLicenseState;
        private System.Windows.Forms.Label labelCurrentLicense;
        private System.Windows.Forms.LinkLabel linkOpenRedstone;
        private System.Windows.Forms.Label labelAbout;
        private System.Windows.Forms.Button butClose;
        private System.Windows.Forms.LinkLabel linkEmail;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Controls.CIBHorizontalLine cibHorizontalLine1;
        private Controls.CIBHorizontalLine cibHorizontalLine2;
        private System.Windows.Forms.Label labelAccountWarning;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.LinkLabel linkServiceData;
    }
}