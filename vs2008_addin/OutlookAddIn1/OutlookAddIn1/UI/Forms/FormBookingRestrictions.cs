﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.Properties;
using OneSpace.OutlookPlugin.Utilities;
using OneSpace.OutlookPlugin.Extentions;

namespace OneSpace.OutlookPlugin.UI.Forms
{
    public partial class FormBookingRestrictions : Form
    {
        public FormBookingRestrictions()
        {
            InitializeComponent();
        }

        public FormBookingRestrictions(TimeSpan before,TimeSpan after)
        {
            InitializeComponent();
            labelCaption.Text = Globals.ThisAddIn.resourceManager.GetString("meetingBufferErrorTitle");
            if (before.ToString()=="00:00:00" && after.ToString()=="00:00:00")
            {
                labelSetup.Text = Globals.ThisAddIn.resourceManager.GetString("meetingTimeNotFree");
                labelClearaway.Text = "";
            }
            else
            {
                labelSetup.Text = string.Format(Globals.ThisAddIn.resourceManager.GetString("meetingBufferErrorBefore"), before.ToFriendlyString(show_seconds_if_less_then_one_min: true).ToLower());
                labelClearaway.Text = string.Format(Globals.ThisAddIn.resourceManager.GetString("meetingBufferErrorAfter"), after.ToFriendlyString(show_seconds_if_less_then_one_min: true).ToLower());
            }
            labelMessage.Text = Globals.ThisAddIn.resourceManager.GetString("meetingBufferErrorMessage");
        }

        private void butBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
