﻿namespace OneSpace.OutlookPlugin.UI.Forms
{
    partial class FormBookingRestrictions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBookingRestrictions));
            this.labelCaption = new System.Windows.Forms.Label();
            this.labelSetup = new System.Windows.Forms.Label();
            this.labelClearaway = new System.Windows.Forms.Label();
            this.labelMessage = new System.Windows.Forms.Label();
            this.butBack = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelCaption
            // 
            resources.ApplyResources(this.labelCaption, "labelCaption");
            this.labelCaption.Name = "labelCaption";
            // 
            // labelSetup
            // 
            resources.ApplyResources(this.labelSetup, "labelSetup");
            this.labelSetup.Name = "labelSetup";
            // 
            // labelClearaway
            // 
            resources.ApplyResources(this.labelClearaway, "labelClearaway");
            this.labelClearaway.Name = "labelClearaway";
            // 
            // labelMessage
            // 
            resources.ApplyResources(this.labelMessage, "labelMessage");
            this.labelMessage.Name = "labelMessage";
            // 
            // butBack
            // 
            this.butBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(51)))), ((int)(((byte)(41)))));
            resources.ApplyResources(this.butBack, "butBack");
            this.butBack.ForeColor = System.Drawing.Color.White;
            this.butBack.Name = "butBack";
            this.butBack.UseVisualStyleBackColor = false;
            this.butBack.Click += new System.EventHandler(this.butBack_Click);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Image = global::OneSpace.OutlookPlugin.Resource1.warning;
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // FormBookingRestrictions
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.butBack);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.labelClearaway);
            this.Controls.Add(this.labelSetup);
            this.Controls.Add(this.labelCaption);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormBookingRestrictions";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelCaption;
        private System.Windows.Forms.Label labelSetup;
        private System.Windows.Forms.Label labelClearaway;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Button butBack;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}