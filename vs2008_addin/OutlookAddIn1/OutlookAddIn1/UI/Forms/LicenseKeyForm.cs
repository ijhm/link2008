﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.Data.Security;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Settings;

namespace OneSpace.OutlookPlugin
{
    public partial class LicenseKeyForm : Form
    {
        public Data.Security.LicenseKey key;

        public LicenseKeyForm()
        {
            InitializeComponent();
            this.Icon = Resource1.admin1;
            butApply.BackColor = SystemConsts.NormalButton;
            butApply.ForeColor = SystemConsts.ButtonText;
            butApply.FlatStyle = FlatStyle.Flat;
            butApply.FlatAppearance.BorderSize = 0;
            textLicenseKey.Text = Data.Security.LicenseKey.KeyText;
        }

        private void butCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void butApply_Click(object sender, EventArgs e)
        {
            var decodeSecure = new Data.Security.CIBSecurity();
            var keyText = textLicenseKey.Text;
            if (keyText!=null)
            {
                try
                {
                    keyText = keyText.Trim();
                    var keyJson = decodeSecure.DecryptBase64(textLicenseKey.Text);
                    key = Data.Security.LicenseKey.ParseKey(keyJson);
                }
                catch (Exception)
                {
                    key = null;
                }
                if (key != null)
                {
                    Data.Security.LicenseKey.SetKey(key);
                    //Now licensed so activate
                    Globals.ThisAddIn.Reload();
                    this.Hide();
                }
                else
                {
                    Data.Security.LicenseKey.ClearKey();
                    MessageBox.Show(Globals.ThisAddIn.resourceManager.GetString("licenseNotValidCaption"), Globals.ThisAddIn.resourceManager.GetString("licenseNotValidTitle"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Hide();
                }
            }
            else
            {
                Data.Security.LicenseKey.ClearKey();
                MessageBox.Show(Globals.ThisAddIn.resourceManager.GetString("licenseCopyAndPasteCaption"), Globals.ThisAddIn.resourceManager.GetString("licenseCopyAndPasteTitle"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
        }
    }
}
