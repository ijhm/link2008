﻿namespace OneSpace.OutlookPlugin
{
    partial class LicenseKeyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LicenseKeyForm));
            this.groupLicense = new System.Windows.Forms.GroupBox();
            this.textLicenseKey = new System.Windows.Forms.TextBox();
            this.labelLicenseKeyNotes = new System.Windows.Forms.Label();
            this.butCancel = new System.Windows.Forms.Button();
            this.butApply = new System.Windows.Forms.Button();
            this.groupLicense.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupLicense
            // 
            this.groupLicense.Controls.Add(this.textLicenseKey);
            this.groupLicense.Controls.Add(this.labelLicenseKeyNotes);
            this.groupLicense.Controls.Add(this.butCancel);
            this.groupLicense.Controls.Add(this.butApply);
            resources.ApplyResources(this.groupLicense, "groupLicense");
            this.groupLicense.Name = "groupLicense";
            this.groupLicense.TabStop = false;
            // 
            // textLicenseKey
            // 
            resources.ApplyResources(this.textLicenseKey, "textLicenseKey");
            this.textLicenseKey.Name = "textLicenseKey";
            // 
            // labelLicenseKeyNotes
            // 
            resources.ApplyResources(this.labelLicenseKeyNotes, "labelLicenseKeyNotes");
            this.labelLicenseKeyNotes.Name = "labelLicenseKeyNotes";
            // 
            // butCancel
            // 
            this.butCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            resources.ApplyResources(this.butCancel, "butCancel");
            this.butCancel.ForeColor = System.Drawing.Color.White;
            this.butCancel.Name = "butCancel";
            this.butCancel.UseVisualStyleBackColor = false;
            this.butCancel.Click += new System.EventHandler(this.butCancel_Click);
            // 
            // butApply
            // 
            this.butApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(51)))), ((int)(((byte)(41)))));
            resources.ApplyResources(this.butApply, "butApply");
            this.butApply.ForeColor = System.Drawing.Color.White;
            this.butApply.Name = "butApply";
            this.butApply.Tag = "";
            this.butApply.UseVisualStyleBackColor = false;
            this.butApply.Click += new System.EventHandler(this.butApply_Click);
            // 
            // LicenseKeyForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupLicense);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "LicenseKeyForm";
            this.groupLicense.ResumeLayout(false);
            this.groupLicense.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupLicense;
        private System.Windows.Forms.TextBox textLicenseKey;
        private System.Windows.Forms.Label labelLicenseKeyNotes;
        private System.Windows.Forms.Button butCancel;
        private System.Windows.Forms.Button butApply;
    }
}