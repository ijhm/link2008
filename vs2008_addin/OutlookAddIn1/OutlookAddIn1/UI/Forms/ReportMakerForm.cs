﻿using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Settings;
using OneSpace.OutlookPlugin.UI.Controls.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OneSpace.OutlookPlugin.UI.Forms
{
    public partial class ReportMakerForm : Form
    {
        public ReportMakerForm()
        {
            InitializeComponent();
            this.Icon = Branding.Resource.ReportsIcon;
            labelHeading.ForeColor = Branding.Resource.TextColour;
            this.Text = Branding.Resource.ReportingTitle;
            EnableButtons(true);
        }

        private void EnableButtons(bool state)
        {
            butReport1.BackColor = SystemConsts.MainButton;
            butReport2.BackColor = SystemConsts.MainButton;
            butReport3.BackColor = SystemConsts.MainButton;
        }

        private void butReport2_Click(object sender, EventArgs e)
        {
//            EnableButtons(false);
            panelReport.Controls.Clear();
            var report = new ReportServicesOrdered();
            report.Dock = DockStyle.Fill;
            panelReport.Controls.Add(report);
            butReport1.BackColor = SystemConsts.MainButton;
            butReport2.BackColor = SystemConsts.ActivatedButton;
            butReport3.BackColor = SystemConsts.MainButton;
        }

        private void butReport3_Click(object sender, EventArgs e)
        {
            panelReport.Controls.Clear();
            var report = new ReportCostCentreSummary();
            report.Dock = DockStyle.Fill;
            panelReport.Controls.Add(report);
            butReport1.BackColor = SystemConsts.MainButton;
            butReport2.BackColor = SystemConsts.MainButton;
            butReport3.BackColor = SystemConsts.ActivatedButton;
        }

        private void butReport1_Click(object sender, EventArgs e)
        {
            panelReport.Controls.Clear();
            var report = new ReportSummary();
            report.Dock = DockStyle.Fill;
            panelReport.Controls.Add(report);
            butReport1.BackColor = SystemConsts.ActivatedButton;
            butReport2.BackColor = SystemConsts.MainButton;
            butReport3.BackColor = SystemConsts.MainButton;
        }
    }
}
