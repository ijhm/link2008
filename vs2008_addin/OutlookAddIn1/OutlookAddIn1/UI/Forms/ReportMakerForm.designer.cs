﻿namespace OneSpace.OutlookPlugin.UI.Forms
{
    partial class ReportMakerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportMakerForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelHeading = new System.Windows.Forms.Label();
            this.butReport3 = new System.Windows.Forms.Button();
            this.butReport2 = new System.Windows.Forms.Button();
            this.butReport1 = new System.Windows.Forms.Button();
            this.panelReport = new System.Windows.Forms.Panel();
            this.LabelStartReport = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panelReport.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.labelHeading);
            this.groupBox1.Controls.Add(this.butReport3);
            this.groupBox1.Controls.Add(this.butReport2);
            this.groupBox1.Controls.Add(this.butReport1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // labelHeading
            // 
            resources.ApplyResources(this.labelHeading, "labelHeading");
            this.labelHeading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Tag = "Heading";
            // 
            // butReport3
            // 
            this.butReport3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            resources.ApplyResources(this.butReport3, "butReport3");
            this.butReport3.ForeColor = System.Drawing.Color.White;
            this.butReport3.Name = "butReport3";
            this.butReport3.Tag = "main";
            this.butReport3.UseVisualStyleBackColor = false;
            this.butReport3.Click += new System.EventHandler(this.butReport3_Click);
            // 
            // butReport2
            // 
            this.butReport2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            resources.ApplyResources(this.butReport2, "butReport2");
            this.butReport2.ForeColor = System.Drawing.Color.White;
            this.butReport2.Name = "butReport2";
            this.butReport2.Tag = "main";
            this.butReport2.UseVisualStyleBackColor = false;
            this.butReport2.Click += new System.EventHandler(this.butReport2_Click);
            // 
            // butReport1
            // 
            this.butReport1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            resources.ApplyResources(this.butReport1, "butReport1");
            this.butReport1.ForeColor = System.Drawing.Color.White;
            this.butReport1.Name = "butReport1";
            this.butReport1.Tag = "main";
            this.butReport1.UseVisualStyleBackColor = false;
            this.butReport1.Click += new System.EventHandler(this.butReport1_Click);
            // 
            // panelReport
            // 
            resources.ApplyResources(this.panelReport, "panelReport");
            this.panelReport.Controls.Add(this.LabelStartReport);
            this.panelReport.Name = "panelReport";
            // 
            // LabelStartReport
            // 
            resources.ApplyResources(this.LabelStartReport, "LabelStartReport");
            this.LabelStartReport.Name = "LabelStartReport";
            // 
            // ReportMakerForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelReport);
            this.Controls.Add(this.groupBox1);
            this.Name = "ReportMakerForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelReport.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panelReport;
        private System.Windows.Forms.Button butReport3;
        private System.Windows.Forms.Button butReport2;
        private System.Windows.Forms.Button butReport1;
        private System.Windows.Forms.Label LabelStartReport;
        private System.Windows.Forms.Label labelHeading;
    }
}