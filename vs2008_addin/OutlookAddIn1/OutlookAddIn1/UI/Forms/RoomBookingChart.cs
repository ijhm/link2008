﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Threading;
using OneSpace.OutlookPlugin;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Utilities;
using OneSpace.OutlookPlugin.Extentions;
using System.Diagnostics;
using OneSpace.OutlookPlugin.Properties;

namespace OneSpace.OutlookPlugin
{
    public partial class RoomBookingChart : Form
    {
        private static readonly TimeSpan MAX_SCEDULE_DURATION = TimeSpan.FromDays(28);
        private class BuildingFloor
        {
            public Location Building { get; set; }
            public Floor Floor { get; set; }
        }

        //        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Tools.Office.ProgrammingModel.dll", "14.0.0.0")]
        //        internal Microsoft.Office.Interop.Outlook.Application Application;
        private int roomsFound;

        List<RoomWithBookings> Rooms = new List<RoomWithBookings>();
        List<RoomWithBookings> UpdatedRooms = new List<RoomWithBookings>();
        RoomBooking selectedBooking;
        string selectedId = "";
        public delegate void EventUpdateUI();
        public delegate void EventUpdateUIFinished();
        private Cursor lastCursor;
        private List<Floor> floorList;
        public bool scanning;
        private object scanning_lock = new object();
        private bool hiddenControls = false;

        private List<UI.Data.Room> RoomsData = new List<UI.Data.Room>();
        private UI.Data.Location LocationData;
        public List<RoomMailbox> filteredRoomList;
        private OneSpace.OutlookPlugin.UI.Data.Location map_estate_location = null;
        private int padding;

        public RoomBookingChart()
        {
            InitializeComponent();
            this.Icon = Branding.Resource.BookingsIcon;
            butToday.BackColor = Branding.Resource.PrimaryColour;
            butUpdate.BackColor = Branding.Resource.PrimaryColour;

            labelViewControls.ForeColor = Branding.Resource.TextColour;
            label4.ForeColor = Branding.Resource.TextColour;

            progUpdates.Visible = false;
            //            UIUtilities.SetControls(groupControls);
            //            UIUtilities.SetControls(groupDetails);
            //            UIUtilities.SetControls(groupUpdates);
            //            UIUtilities.SetControls(groupChart);
            labelSelectOption.Visible = true;
            schedule.Visible = false;

            foreach (OneSpace.OutlookPlugin.Data.Region region in Globals.ThisAddIn.OneSpaceData.Regions)
            {
                regionBox.Items.Add(region);
            }

            DateTime now = DateTime.Now;
            dateFrom.Value = new DateTime(now.Year, now.Month, now.Day, now.Hour, 0, 0);
            dateFrom.Value = dateFrom.Value.AddHours(1);
            dateTo.Value = dateFrom.Value.AddHours(48);
 //           cibBookingSchedule.OnBookingClick += cibBookingSchedule_OnBookingClick;

            Rectangle res = Screen.PrimaryScreen.Bounds;

            //if (res.Width == 3000 && res.Height == 2000)
            //{
            //    cibBookingSchedule.Width = groupChart.Width;
            //    cibBookingSchedule.Height = groupChart.Height;
            //}
            UIUtilities.SetButton(butUpdate, false);
            UIUtilities.SetButton(butToday, false);

            /* Note: This is here for the Map. */
            map_estate_location = new UI.Data.Location()
            {
                Layout = Globals.ThisAddIn.OneSpaceData.MapAreaImage
            };

            padding = groupDetails.Left - (groupChart.Left + groupChart.Width);
            this.Text = Branding.Resource.BookingChartTitle;

            // UpdateBookings();
        }

        private int SelectedLocationIndex
        {
            get
            {
                var selected_index = regionBox.Invoke(() => locationBox.SelectedIndex);
                return selected_index;
            }
        }
		
        private Location SelectedLocation
        {
            get
            {
                var selected_index = regionBox.Invoke(() => locationBox.SelectedIndex);
                return Globals.ThisAddIn.OneSpaceData.Regions[SelectedRegion].Locations[SelectedLocationIndex];
            }
        }

        private void butUpdate_Click(object sender, EventArgs e) {
            var begins_on = dateFrom.Value.Date + timeFrom.Value.TimeOfDay;
            var ends_on = dateTo.Value.Date + timeTo.Value.TimeOfDay;

            var search_duration = (ends_on - begins_on);

            if(search_duration > MAX_SCEDULE_DURATION) {
                MessageBox.Show(
                    string.Format(
                        Globals.ThisAddIn.resourceManager.GetString("maxSheduledSearchTime"),
                        MAX_SCEDULE_DURATION.ToFriendlyString(),
                        search_duration.ToFriendlyString()
                    )
                );
                return;
            }

            if (butUpdate.BackColor == Settings.SystemConsts.DisabledButton) {
                return;
            }

            UpdateBookings();

        }

        public void UpdateBookings()
        {
            lock (scanning_lock)
            {
                if (!scanning)
                {
                    schedule.Visible = false;
                    Cursor.Current = Cursors.WaitCursor;
                    this.Refresh();

                    filteredRoomList = new List<Data.RoomMailbox>();

                    floorList = new List<Floor>();
                    int region_index = regionBox.SelectedIndex;
                    int location_index = 0;
                    if (locationBox.Items.Count == 0)
                    {
                        return;
                    }

                    progUpdates.Visible = true;

                    location_index = locationBox.SelectedIndex;

                    floorList = Globals.ThisAddIn.OneSpaceData.Regions[region_index].Locations[location_index].Floors;

                    foreach (var floor in floorList)
                    {
                        foreach (var room in floor.Rooms)
                        {
                            filteredRoomList.Add(room);
                        }
                    }

                    var main_search_ews = new Thread(MainSearchEWS);
                    main_search_ews.Start();
                    scanning = true;
                }
            }
        }

        public void UpdateUI()
        {

      //      cibBookingSchedule.Width = (int)ts.TotalHours * 60 + 200;
        //    cibBookingSchedule.Refresh();
        }

        public void UpdateUIFinished()
        {
            Cursor.Current = lastCursor;
            scanning = false;
            labelSelectOption.Visible = false;
            schedule.Visible = true;
            progUpdates.Visible = false;

            var report_from = dateFrom.Value.Date + timeFrom.Value.TimeOfDay;
            var report_to = dateTo.Value.Date + timeTo.Value.TimeOfDay;

            schedule.Visible = true;

            this.Refresh();
        }


        private void MainSearchEWS()
        {
            try
            {
                Logging.ActivityLog.WriteToLog("Getting Appointments for the filtered room list");
                Action increment_progress = () => {
                    roomsFound++;
                };

                var appointments = GetAppointments(filteredRoomList, increment_progress);

                var room_floor_building_map = GetRoomFloorBuildingMap();
                var buildings = new Dictionary<Location, OneSpace.OutlookPlugin.UI.Data.Location>();
                var floors = new Dictionary<Floor, OneSpace.OutlookPlugin.UI.Data.Location>();

                var begins_on = dateFrom.Value.Date + timeFrom.Value.TimeOfDay;
                var ends_on = dateTo.Value.Date + timeTo.Value.TimeOfDay;

                var rooms = new List<OneSpace.OutlookPlugin.UI.Data.Room>();

                foreach (var room in filteredRoomList)
                {
                    BuildingFloor building_floor;

                    if (room_floor_building_map.TryGetValue(room, out building_floor) == true)
                    {
                        var ui_floor = (
                            from r in map_estate_location.Children
                            from b in r.Children
                            from f in b.Children
                            where f.ReferenceItem == building_floor.Floor
                            select f
                        ).FirstOrDefault();

                        var ui_room = new OneSpace.OutlookPlugin.UI.Data.Room()
                        {
                            Name = room.Name,
                            Parent = ui_floor,
                            ReferenceItem = room
                        };

                        ui_room.Path = new System.Drawing.Drawing2D.GraphicsPath();
                        ui_room.Path.AddPolygon(room.Poly.ToArray());

                        var room_appointments = appointments[room];
                        var bookings = (
                            from a in room_appointments
                            select new OneSpace.OutlookPlugin.UI.Data.Booking
                            {
                                Room = ui_room,
                                BeginsOn = a.Start,
                                EndsOn = a.End,
                                ReferenceItem = a
                            }
                        ).ToList();

                        ui_room.Bookings = bookings;

                        rooms.Add(ui_room);
                    }

                    increment_progress();
                }

                this.Invoke(() => {
                    schedule.Setup(rooms, begins_on, ends_on, map_estate_location);
                    UpdateUIFinished();
                });
            }
            catch (Exception ex)
            {
                /* Todo: Add Finish UI Call and alert the user to the error */
                Logging.ActivityLog.WriteToLog(ex.ToString());
            }
        }

        private DateTime FromDateTime
        {
            get
            {
                return dateFrom.Value.Date + timeFrom.Value.TimeOfDay;
            }
        }

        private DateTime ToDateTime
        {
            get
            {
                return dateTo.Value.Date + timeTo.Value.TimeOfDay;
            }
        }

        private Dictionary<RoomMailbox, List<Outlook.AppointmentItem>> GetAppointments(List<RoomMailbox> rooms, Action progress_callback)
        {
            var result = new Dictionary<RoomMailbox, List<Outlook.AppointmentItem>>();
            var exceptions = new List<Exception>();

            var begins_on = dateFrom.Value.Date;
            var ends_on = dateTo.Value.Date.AddDays(1);

            var ns = Globals.ThisAddIn.Application.Session;

            foreach (var room in rooms)
            {
                try
                {
                    System.Windows.Forms.Application.DoEvents();

                    var room_account = ns.CreateRecipient(room.MailBox);
                    var room_calendar = ns.GetSharedDefaultFolder(room_account, Outlook.OlDefaultFolders.olFolderCalendar);
                    
                    if(room_calendar.InAppFolderSyncObject == false) {
                        room_calendar.InAppFolderSyncObject = true;
                    }

                    var room_calendar_items = room_calendar.Items;
                    room_calendar_items.Restrict("[Start] >= '{begins_on.ToOutlookFilter()}' AND [End] <= '{ends_on.ToOutlookFilter()}'");
                    room_calendar_items.Sort("[Start]", false);

                    var items = new List<Outlook.AppointmentItem>();
                    foreach (Outlook.AppointmentItem room_calendar_item in room_calendar_items)
                    {
                        items.Add(room_calendar_item);
                    }

                    result.Add(room, items);

                    progress_callback();
                }
                catch (Exception ex)
                {
                    var message = @"An exception occured when processing '{room.Name}'";
                    exceptions.Add(new Exception(message, ex));
                }
            }

            System.Windows.Forms.Application.DoEvents();

            if (exceptions.Count > 0)
            {
                throw new AggregateException("{exceptions.Count} exception(s) occured whilst getting appointments for a number of rooms, please see the inner exception(s) for details", exceptions);
            }

            return result;
        }

        private Dictionary<Room, BuildingFloor> GetRoomFloorBuildingMap()
        {
            var selected_region = SelectedRegion;
            var selected_location = SelectedLocation;
            Floor selected_floor = null;

            var room_floor_building_map = new Dictionary<Room, BuildingFloor>();

            if (selected_floor != null && selected_location != null)
            {
                var building_floor = new BuildingFloor() { Building = selected_location, Floor = selected_floor };
                foreach (var room in selected_floor.Rooms)
                {
                    room_floor_building_map.Add(room, building_floor);
                }
            }
            else if (selected_location != null)
            {
                foreach (var floor in selected_location.Floors)
                {
                    var building_floor = new BuildingFloor() { Building = selected_location, Floor = floor };
                    foreach (var room in floor.Rooms)
                    {
                        room_floor_building_map.Add(room, building_floor);
                    }
                }
            }
            else
            {
                var buildings = Globals.ThisAddIn.OneSpaceData.Regions[selected_region].Locations;
                foreach (var building in buildings)
                {
                    foreach (var floor in building.Floors)
                    {
                        var building_floor = new BuildingFloor() { Building = building, Floor = floor };
                        foreach (var room in floor.Rooms)
                        {
                            room_floor_building_map.Add(room, building_floor);
                        }
                    }
                }
            }

            return room_floor_building_map;
        }

        private void FinishSearch()
        {
            UpdateUIFinished();
        }

        private void butDelete_Click(object sender, EventArgs e)
        {
            if (selectedBooking != null)
            {
                selectedBooking.appointmentItem.MeetingStatus = Microsoft.Office.Interop.Outlook.OlMeetingStatus.olMeetingCanceled;
                selectedBooking.appointmentItem.ForceUpdateToAllAttendees = true;
//                selectedBooking.appointmentItem.Save();
                selectedBooking.appointmentItem.Send();
            }

        }

        private void labelCalendar_Click(object sender, EventArgs e)
        {

        }

        private void regionBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int regionId = regionBox.SelectedIndex;
            locationBox.Items.Clear();
            foreach (Location loc in Globals.ThisAddIn.OneSpaceData.Regions[regionId].Locations)
            {
                locationBox.Items.Add(loc);
            }
            UIUtilities.SetButton(butUpdate, false);
            UIUtilities.SetButton(butToday, false);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!scanning)
            {
                MainSearchEWS();
            }
        }

        private void butToday_Click(object sender, EventArgs e)
        {
            if(butToday.BackColor == Settings.SystemConsts.DisabledButton) {
                return;
            }

            var now = DateTime.Now;
            dateFrom.Value = now.Date;
            timeFrom.Value = DateTime.Parse("09:00");
            dateTo.Value = now.Date;
            timeTo.Value = DateTime.Parse("17:30");
            this.Refresh();
            UpdateBookings();

        }

        private void locationBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UIUtilities.SetButton(butUpdate, true);
            UIUtilities.SetButton(butToday, true);

        }

        private void timeFrom_ValueChanged(object sender, EventArgs e)
        {
            CheckDates(nameof(timeFrom_ValueChanged));
        }

        private void dateFrom_ValueChanged(object sender, EventArgs e)
        {
            CheckDates(nameof(dateFrom_ValueChanged));
        }

        private void dateTo_ValueChanged(object sender, EventArgs e)
        {
            CheckDates(nameof(dateTo_ValueChanged));
        }

        private void timeTo_ValueChanged(object sender, EventArgs e)
        {
            CheckDates(nameof(timeTo_ValueChanged));
        }

        private void CheckDates(string source) {
            try {
                dateFrom.ValueChanged -= dateFrom_ValueChanged;
                dateTo.ValueChanged -= dateTo_ValueChanged;
                timeFrom.ValueChanged -= timeFrom_ValueChanged;
                timeTo.ValueChanged -= timeTo_ValueChanged;

                if (dateFrom.Value.Date > dateTo.Value.Date) {
                    dateFrom.Value = dateTo.Value.Date;
                }

                if (dateFrom.Value.Date == dateTo.Value.Date) {
                    if (timeFrom.Value.TimeOfDay > timeTo.Value.TimeOfDay) {
                        var swap_time = timeFrom.Value;
                        timeFrom.Value = timeTo.Value;
                        timeTo.Value = swap_time;
                    }
                }

                var report_from = dateFrom.Value.Date + timeFrom.Value.TimeOfDay;
                var report_to = dateTo.Value.Date + timeTo.Value.TimeOfDay;

                TimeSpan ts = report_from - report_to;
                if (ts.TotalDays < 1) {
                    UIUtilities.SetButton(butUpdate, true);
                    UIUtilities.SetButton(butToday, true);
                }
                else {
                    UIUtilities.SetButton(butUpdate, false);
                    UIUtilities.SetButton(butToday, false);
                }
            }
            finally {
                dateFrom.ValueChanged += dateFrom_ValueChanged;
                dateTo.ValueChanged += dateTo_ValueChanged;
                timeFrom.ValueChanged += timeFrom_ValueChanged;
                timeTo.ValueChanged += timeTo_ValueChanged;
            }
        }

        private int SelectedRegion
        {
            get
            {
                var selected_index = regionBox.Invoke(() => regionBox.SelectedIndex);
                return selected_index;
            }
        }

        private void schedule_SelectedBookingChanged(object sender, EventArgs e)
        {
            const string PROP_ORGANISER_NAME = "http://schemas.microsoft.com/mapi/proptag/0x0042001F";

            var selectedBooking = schedule.SelectedBooking;

            labelServices.Text = "";

            if (selectedBooking == null)
            {
                labelRoomDetailLocation.Text = string.Empty;
                labelRoomOrganiser.Text = string.Empty;
                labelMeetingSubject.Text = string.Empty;
                labelMeetingDates_From.Text = string.Empty;
                labelMeetingDates_To.Text = string.Empty;
                return;
            }

            var appointment = selectedBooking.ReferenceItem as Outlook.AppointmentItem;
            var props = appointment.PropertyAccessor.GetProperties(new[] { PROP_ORGANISER_NAME });
            var organiser_name = props[0] as string ?? string.Empty;

            labelRoomDetailLocation.Text = appointment.Location;
            labelRoomOrganiser.Text = organiser_name;
            //    labelRoomDetailAttendees.Text = selectedBooking.Attendees;
            labelMeetingSubject.Text = appointment.Subject;

            labelMeetingDates_From.Text = appointment.End.ToString("dd MMMM yyyy HH:mm");
            labelMeetingDates_To.Text = appointment.Start.ToString("dd MMMM yyyy HH:mm");

            string orderDetails = "";

            var region = Globals.ThisAddIn.OneSpaceData.Regions[SelectedRegion];
            string bookingJson = OutlookUtilities.GetPropertyText(appointment, AppointmentSchemaEx.RoomBookingDataUpdated);
            if (string.IsNullOrWhiteSpace(bookingJson) == false)
            {
                var bookingData = new AppointmentData(bookingJson);
                if (region != null)
                {
                    foreach (OrderItem item in bookingData.Items)
                    {
                        OrderItemDisplay newItem = new OrderItemDisplay();
                        newItem.ItemId = item.ItemId;
                        newItem.ItemCost = item.ItemCost;
                        newItem.Qty = item.Qty;
                        newItem.Notes = item.Notes;
                        newItem.BookingItems = region.BookingItems.ToList();
                        newItem.Start = item.Start;
                        newItem.End = item.End;

                        orderDetails += string.Format("{0} x {1}\r\n", newItem.BookingItem.Name, item.Qty.ToString());

                        labelServices.Text = orderDetails;
                    }
                }
            }


            //selectedId = e.Booking.Id;
           // butDelete.Enabled = true;

        }

        private void schedule_SelectedLocationChanged(object sender, EventArgs e)
        {

        }

        private void schedule_SelectedRoomChanged(object sender, EventArgs e)
        {

        }

        private void labelHideControl_Click(object sender, EventArgs e)
        {
            hiddenControls = !hiddenControls;
            if (hiddenControls)
            {
                labelHideControl.Text = "Show Controls";
                labelHideControl.BorderStyle = BorderStyle.FixedSingle;
                groupControls.Visible = false;
                groupDetails.Visible = false;
                groupChart.Top = groupControls.Top;
                labelViewControls.Visible = false;
                groupChart.Width = this.Width - padding;
            }
            else
            {
                labelHideControl.Text = "Hide Controls";
                labelHideControl.BorderStyle = BorderStyle.None;
                groupControls.Visible = true;
                groupDetails.Visible = true;
                labelViewControls.Visible = true;
                groupChart.Top = 150;
                groupChart.Width = groupDetails.Left- groupChart.Left;
            }
        }
    }
}
