﻿namespace OneSpace.OutlookPlugin
{
    partial class RoomBookingChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Timer timer1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoomBookingChart));
            this.groupChart = new System.Windows.Forms.GroupBox();
            this.progUpdates = new System.Windows.Forms.ProgressBar();
            this.labelSelectOption = new System.Windows.Forms.Label();
            this.schedule = new OneSpace.OutlookPlugin.UI.Controls.ScheduleOnly();
            this.groupDetails = new System.Windows.Forms.GroupBox();
            this.labelMeetingDates_To = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelServices = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelMeetingDuration = new System.Windows.Forms.Label();
            this.butSub15Mins = new System.Windows.Forms.Button();
            this.butAdd15Mins = new System.Windows.Forms.Button();
            this.labelMeetingDates_From = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelMeetingSubject = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelRoomOrganiser = new System.Windows.Forms.Label();
            this.labelRoomDetailLocation = new System.Windows.Forms.Label();
            this.labelOrganiser = new System.Windows.Forms.Label();
            this.labelRoomLocation = new System.Windows.Forms.Label();
            this.groupControls = new System.Windows.Forms.GroupBox();
            this.butToday = new System.Windows.Forms.Button();
            this.timeTo = new System.Windows.Forms.DateTimePicker();
            this.timeFrom = new System.Windows.Forms.DateTimePicker();
            this.dateTo = new System.Windows.Forms.DateTimePicker();
            this.dateFrom = new System.Windows.Forms.DateTimePicker();
            this.butUpdate = new System.Windows.Forms.Button();
            this.regionBox = new System.Windows.Forms.ComboBox();
            this.locationBox = new System.Windows.Forms.ComboBox();
            this.labelLocation = new System.Windows.Forms.Label();
            this.labelTo = new System.Windows.Forms.Label();
            this.labelFrom = new System.Windows.Forms.Label();
            this.groupUpdates = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.butDelete = new System.Windows.Forms.Button();
            this.labelUpdates = new System.Windows.Forms.Label();
            this.labelViewControls = new System.Windows.Forms.Label();
            this.labelHideControl = new System.Windows.Forms.Label();
            timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupChart.SuspendLayout();
            this.groupDetails.SuspendLayout();
            this.groupControls.SuspendLayout();
            this.groupUpdates.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            timer1.Interval = 60000;
            timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupChart
            // 
            resources.ApplyResources(this.groupChart, "groupChart");
            this.groupChart.Controls.Add(this.progUpdates);
            this.groupChart.Controls.Add(this.labelSelectOption);
            this.groupChart.Controls.Add(this.schedule);
            this.groupChart.Name = "groupChart";
            this.groupChart.TabStop = false;
            // 
            // progUpdates
            // 
            resources.ApplyResources(this.progUpdates, "progUpdates");
            this.progUpdates.MarqueeAnimationSpeed = 30;
            this.progUpdates.Name = "progUpdates";
            this.progUpdates.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // labelSelectOption
            // 
            resources.ApplyResources(this.labelSelectOption, "labelSelectOption");
            this.labelSelectOption.Name = "labelSelectOption";
            // 
            // schedule
            // 
            resources.ApplyResources(this.schedule, "schedule");
            this.schedule.Name = "schedule";
            this.schedule.SelectedLocationChanged += new System.EventHandler<System.EventArgs>(this.schedule_SelectedLocationChanged);
            this.schedule.SelectedRoomChanged += new System.EventHandler<System.EventArgs>(this.schedule_SelectedRoomChanged);
            this.schedule.SelectedBookingChanged += new System.EventHandler<System.EventArgs>(this.schedule_SelectedBookingChanged);
            // 
            // groupDetails
            // 
            resources.ApplyResources(this.groupDetails, "groupDetails");
            this.groupDetails.Controls.Add(this.labelMeetingDates_To);
            this.groupDetails.Controls.Add(this.label4);
            this.groupDetails.Controls.Add(this.labelServices);
            this.groupDetails.Controls.Add(this.label5);
            this.groupDetails.Controls.Add(this.labelMeetingDuration);
            this.groupDetails.Controls.Add(this.butSub15Mins);
            this.groupDetails.Controls.Add(this.butAdd15Mins);
            this.groupDetails.Controls.Add(this.labelMeetingDates_From);
            this.groupDetails.Controls.Add(this.label3);
            this.groupDetails.Controls.Add(this.labelMeetingSubject);
            this.groupDetails.Controls.Add(this.label2);
            this.groupDetails.Controls.Add(this.labelRoomOrganiser);
            this.groupDetails.Controls.Add(this.labelRoomDetailLocation);
            this.groupDetails.Controls.Add(this.labelOrganiser);
            this.groupDetails.Controls.Add(this.labelRoomLocation);
            this.groupDetails.Name = "groupDetails";
            this.groupDetails.TabStop = false;
            // 
            // labelMeetingDates_To
            // 
            resources.ApplyResources(this.labelMeetingDates_To, "labelMeetingDates_To");
            this.labelMeetingDates_To.Name = "labelMeetingDates_To";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.label4.Name = "label4";
            this.label4.Tag = "Heading";
            // 
            // labelServices
            // 
            resources.ApplyResources(this.labelServices, "labelServices");
            this.labelServices.Name = "labelServices";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // labelMeetingDuration
            // 
            resources.ApplyResources(this.labelMeetingDuration, "labelMeetingDuration");
            this.labelMeetingDuration.Name = "labelMeetingDuration";
            // 
            // butSub15Mins
            // 
            resources.ApplyResources(this.butSub15Mins, "butSub15Mins");
            this.butSub15Mins.Name = "butSub15Mins";
            this.butSub15Mins.UseVisualStyleBackColor = true;
            // 
            // butAdd15Mins
            // 
            resources.ApplyResources(this.butAdd15Mins, "butAdd15Mins");
            this.butAdd15Mins.Name = "butAdd15Mins";
            this.butAdd15Mins.UseVisualStyleBackColor = true;
            // 
            // labelMeetingDates_From
            // 
            resources.ApplyResources(this.labelMeetingDates_From, "labelMeetingDates_From");
            this.labelMeetingDates_From.Name = "labelMeetingDates_From";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // labelMeetingSubject
            // 
            resources.ApplyResources(this.labelMeetingSubject, "labelMeetingSubject");
            this.labelMeetingSubject.Name = "labelMeetingSubject";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // labelRoomOrganiser
            // 
            resources.ApplyResources(this.labelRoomOrganiser, "labelRoomOrganiser");
            this.labelRoomOrganiser.Name = "labelRoomOrganiser";
            // 
            // labelRoomDetailLocation
            // 
            resources.ApplyResources(this.labelRoomDetailLocation, "labelRoomDetailLocation");
            this.labelRoomDetailLocation.Name = "labelRoomDetailLocation";
            // 
            // labelOrganiser
            // 
            resources.ApplyResources(this.labelOrganiser, "labelOrganiser");
            this.labelOrganiser.Name = "labelOrganiser";
            // 
            // labelRoomLocation
            // 
            resources.ApplyResources(this.labelRoomLocation, "labelRoomLocation");
            this.labelRoomLocation.Name = "labelRoomLocation";
            // 
            // groupControls
            // 
            resources.ApplyResources(this.groupControls, "groupControls");
            this.groupControls.Controls.Add(this.butToday);
            this.groupControls.Controls.Add(this.timeTo);
            this.groupControls.Controls.Add(this.timeFrom);
            this.groupControls.Controls.Add(this.dateTo);
            this.groupControls.Controls.Add(this.dateFrom);
            this.groupControls.Controls.Add(this.butUpdate);
            this.groupControls.Controls.Add(this.regionBox);
            this.groupControls.Controls.Add(this.locationBox);
            this.groupControls.Controls.Add(this.labelLocation);
            this.groupControls.Controls.Add(this.labelTo);
            this.groupControls.Controls.Add(this.labelFrom);
            this.groupControls.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupControls.Name = "groupControls";
            this.groupControls.TabStop = false;
            // 
            // butToday
            // 
            this.butToday.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(51)))), ((int)(((byte)(41)))));
            resources.ApplyResources(this.butToday, "butToday");
            this.butToday.ForeColor = System.Drawing.Color.White;
            this.butToday.Name = "butToday";
            this.butToday.Tag = "main";
            this.butToday.UseVisualStyleBackColor = false;
            this.butToday.Click += new System.EventHandler(this.butToday_Click);
            // 
            // timeTo
            // 
            resources.ApplyResources(this.timeTo, "timeTo");
            this.timeTo.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeTo.Name = "timeTo";
            this.timeTo.ShowUpDown = true;
            this.timeTo.Value = new System.DateTime(2017, 8, 25, 17, 30, 0, 0);
            this.timeTo.ValueChanged += new System.EventHandler(this.timeTo_ValueChanged);
            // 
            // timeFrom
            // 
            resources.ApplyResources(this.timeFrom, "timeFrom");
            this.timeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeFrom.Name = "timeFrom";
            this.timeFrom.ShowUpDown = true;
            this.timeFrom.Value = new System.DateTime(2017, 8, 25, 9, 0, 0, 0);
            this.timeFrom.ValueChanged += new System.EventHandler(this.timeFrom_ValueChanged);
            // 
            // dateTo
            // 
            resources.ApplyResources(this.dateTo, "dateTo");
            this.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTo.Name = "dateTo";
            this.dateTo.ValueChanged += new System.EventHandler(this.dateTo_ValueChanged);
            // 
            // dateFrom
            // 
            resources.ApplyResources(this.dateFrom, "dateFrom");
            this.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.ValueChanged += new System.EventHandler(this.dateFrom_ValueChanged);
            // 
            // butUpdate
            // 
            this.butUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(51)))), ((int)(((byte)(41)))));
            resources.ApplyResources(this.butUpdate, "butUpdate");
            this.butUpdate.ForeColor = System.Drawing.Color.White;
            this.butUpdate.Name = "butUpdate";
            this.butUpdate.Tag = "main";
            this.butUpdate.UseVisualStyleBackColor = false;
            this.butUpdate.Click += new System.EventHandler(this.butUpdate_Click);
            // 
            // regionBox
            // 
            this.regionBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.regionBox.FormattingEnabled = true;
            resources.ApplyResources(this.regionBox, "regionBox");
            this.regionBox.Name = "regionBox";
            this.regionBox.SelectedIndexChanged += new System.EventHandler(this.regionBox_SelectedIndexChanged);
            // 
            // locationBox
            // 
            this.locationBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.locationBox.FormattingEnabled = true;
            resources.ApplyResources(this.locationBox, "locationBox");
            this.locationBox.Name = "locationBox";
            this.locationBox.SelectedIndexChanged += new System.EventHandler(this.locationBox_SelectedIndexChanged);
            // 
            // labelLocation
            // 
            resources.ApplyResources(this.labelLocation, "labelLocation");
            this.labelLocation.Name = "labelLocation";
            // 
            // labelTo
            // 
            resources.ApplyResources(this.labelTo, "labelTo");
            this.labelTo.Name = "labelTo";
            // 
            // labelFrom
            // 
            resources.ApplyResources(this.labelFrom, "labelFrom");
            this.labelFrom.Name = "labelFrom";
            // 
            // groupUpdates
            // 
            resources.ApplyResources(this.groupUpdates, "groupUpdates");
            this.groupUpdates.Controls.Add(this.label6);
            this.groupUpdates.Controls.Add(this.butDelete);
            this.groupUpdates.Controls.Add(this.labelUpdates);
            this.groupUpdates.Name = "groupUpdates";
            this.groupUpdates.TabStop = false;
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.label6.Name = "label6";
            this.label6.Tag = "Heading";
            // 
            // butDelete
            // 
            resources.ApplyResources(this.butDelete, "butDelete");
            this.butDelete.Name = "butDelete";
            this.butDelete.UseVisualStyleBackColor = true;
            this.butDelete.Click += new System.EventHandler(this.butDelete_Click);
            // 
            // labelUpdates
            // 
            resources.ApplyResources(this.labelUpdates, "labelUpdates");
            this.labelUpdates.Name = "labelUpdates";
            // 
            // labelViewControls
            // 
            resources.ApplyResources(this.labelViewControls, "labelViewControls");
            this.labelViewControls.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.labelViewControls.Name = "labelViewControls";
            this.labelViewControls.Tag = "Heading";
            // 
            // labelHideControl
            // 
            resources.ApplyResources(this.labelHideControl, "labelHideControl");
            this.labelHideControl.Name = "labelHideControl";
            this.labelHideControl.Click += new System.EventHandler(this.labelHideControl_Click);
            // 
            // RoomBookingChart
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.labelHideControl);
            this.Controls.Add(this.labelViewControls);
            this.Controls.Add(this.groupUpdates);
            this.Controls.Add(this.groupControls);
            this.Controls.Add(this.groupDetails);
            this.Controls.Add(this.groupChart);
            this.Name = "RoomBookingChart";
            this.groupChart.ResumeLayout(false);
            this.groupDetails.ResumeLayout(false);
            this.groupDetails.PerformLayout();
            this.groupControls.ResumeLayout(false);
            this.groupControls.PerformLayout();
            this.groupUpdates.ResumeLayout(false);
            this.groupUpdates.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupChart;
        private System.Windows.Forms.GroupBox groupDetails;
        private System.Windows.Forms.GroupBox groupControls;
        private System.Windows.Forms.Button butUpdate;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.ComboBox locationBox;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.Button butAdd15Mins;
        private System.Windows.Forms.Button butSub15Mins;
        private System.Windows.Forms.Label labelMeetingDuration;
        private System.Windows.Forms.GroupBox groupUpdates;
        private System.Windows.Forms.Label labelUpdates;
        private System.Windows.Forms.Button butDelete;
        private System.Windows.Forms.Label labelViewControls;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelServices;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelMeetingDates_From;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelMeetingSubject;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelRoomOrganiser;
        private System.Windows.Forms.Label labelRoomDetailLocation;
        private System.Windows.Forms.Label labelOrganiser;
        private System.Windows.Forms.Label labelRoomLocation;
        private System.Windows.Forms.ComboBox regionBox;
        private System.Windows.Forms.DateTimePicker dateTo;
        private System.Windows.Forms.DateTimePicker dateFrom;
        private System.Windows.Forms.Label labelSelectOption;
        private System.Windows.Forms.ProgressBar progUpdates;
        private System.Windows.Forms.DateTimePicker timeTo;
        private System.Windows.Forms.DateTimePicker timeFrom;
        private System.Windows.Forms.Button butToday;
        private System.Windows.Forms.Label labelMeetingDates_To;
        private UI.Controls.ScheduleOnly schedule;
        private System.Windows.Forms.Label labelHideControl;
    }
}