﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Utilities;
using Microsoft.Office.Interop.Outlook;
using OneSpace.OutlookPlugin.Data.Booking;
using OneSpace.OutlookPlugin.Settings;
using OneSpace.OutlookPlugin.Properties;

namespace OneSpace.OutlookPlugin
{
    public partial class RoomBookingOrderPrintout : Form
    {
        private Image mapImg = null;
        private PrintDocument printDocument1 = new PrintDocument();
        private ReportTemplate printTemplate;

        public RoomBookingOrderPrintout()
        {
            InitializeComponent();
            this.Icon = Branding.Resource.GeneralIcon;
            this.Text = Branding.Resource.ReportingTitle;
            butPrint.BackColor = Branding.Resource.PrimaryColour;
            UIUtilities.SetButton(butPrint, true);

        }

        public RoomBookingOrderPrintout(AppointmentAnalysis analysis, Location location,Floor floor,Room room,RoomBooking booking)
        {
            InitializeComponent();
            this.Icon = Branding.Resource.GeneralIcon;
            this.Text = Branding.Resource.ReportingTitle;
            UIUtilities.SetButton(butPrint, true);
            string report = string.Empty;
            string tempPath = Path.Combine(SystemConsts.TempPath,"OneSpace_OrderPrintTemplate.htm");
            if (System.IO.File.Exists(tempPath))
            {
                System.IO.File.Delete(tempPath);
            }
            string itemRow= @"<tr>
                <td>[service]</td>
                <td>[qtytype]</td>
                <td>[cost]</td>
                <td>[qty]</td>
                <td>[sub]</td>
                    </tr>";
            string deliveryRow = @"<tr>
                <td colspan='4' style='padding-left:50px;font-size: 11.0pt;'>{Globals.ThisAddIn.resourceManager.GetString(reportLableDelivery)}:[delivery] {Globals.ThisAddIn.resourceManager.GetString(reportLableClearaway)}:[clearaway]</td>
                <td>&nbsp;</td>
                    </tr>";
            string notesRow = @"<tr>
                <td colspan='4' style='padding-left:50px;font-size: 11.0pt;'>[notes]</td>
                <td>&nbsp;</td>
                    </tr>";

            printTemplate = Globals.ThisAddIn.PrintoutTemplate;

            report = Branding.Resource.printTemplate;

            report = report.Replace("[meetingdatetime]", String.Format("{0} - {1}", booking.DateFrom.ToString("dddd, d MMMM yyyy, HH:mm"), booking.DateTo.ToString("HH:mm")));

            report = report.Replace("[building]", location.Name);
            report = report.Replace("[floor]", floor.Name);
            report = report.Replace("[room]", room.Name);
            report = report.Replace("[from]", booking.DateFrom.ToString("dd MMMM yyyy"));
            report = report.Replace("[duration]", booking.DurationString);
            report = report.Replace("[subject]", booking.Subject);
            report = report.Replace("[attendees]", booking.Attendees);
            string costCentre = string.Empty;

            try
            {
                costCentre = booking.Data.CostCentre;
            }
            catch (System.Exception)
            {
            }
            report = report.Replace("[cost_centre]", costCentre);

            mapImg = new Bitmap(floor.MapAreaImage);

            Graphics memoryGraphics = Graphics.FromImage(mapImg);
            memoryGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            var roomFill = new SolidBrush(Color.FromArgb(70, 100, 200, 100));

            if(room is RoomLinked) {
                var room_linked = room as RoomLinked;
                foreach(var sub_room_id in room_linked.Rooms) {
                    var sub_room = Globals.ThisAddIn.OneSpaceData.GetRoomById(sub_room_id);
                    memoryGraphics.FillPolygon(roomFill, sub_room.Poly.ToArray());
                }
            }
            else {
                memoryGraphics.FillPolygon(roomFill, room.Poly.ToArray());
            }
            
            string rows = string.Empty;
            var region = Globals.ThisAddIn.OneSpaceData.GetRegionFromLocationId(location.Id);
            var BookingItems = region.BookingItems;
            foreach (var item in booking.Data.Items)
            {
                rows += itemRow;
                foreach (BookingItem testItem in BookingItems)
                {
                    if (testItem.Id == item.ItemId)
                    {
                        item.ItemCost = testItem.Cost;
                        rows = rows.Replace("[service]", testItem.Name);
                        rows = rows.Replace("[qtytype]", testItem.CostUnit);
                        rows = rows.Replace("[cost]", testItem.CostToCurrencyHtmlString());
                    }
                }
                

                rows = rows.Replace("[qty]", item.Qty.ToString());
                rows = rows.Replace("[sub]", item.SubTotal(analysis).ToHtmlString());
                
                rows += deliveryRow;
                rows = rows.Replace("[delivery]", item.CalculatedDelivery.ToString("HH:mm"));
                rows = rows.Replace("[clearaway]", item.CalculatedClearAway.ToString("HH:mm"));

                if (item.Notes != string.Empty)
                {
                    rows += notesRow.Replace("[notes]",item.Notes);
                }
            }
            string total = string.Format("<tr><td colspan=\"4\">{0}</td><td>{1}</td>", Globals.ThisAddIn.resourceManager.GetString(reportLableTotal), booking.Data.TotalCost(analysis).ToHtmlString());
            if (booking.Data.Items.Count>0)
            {
                report = report.Replace("[total]", total );
            }
            else
            {
                report = report.Replace("[total]", "<tr><td>{Globals.ThisAddIn.resourceManager.GetString(reportNoServicesAdded)}</td></tr>");
            }
            report = report.Replace("[rows]", rows);
            
            System.IO.File.AppendAllText(tempPath, report);
            webView.Navigate("file:///" + tempPath);
        }

        private void webView_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (mapImg != null)
            {
                MemoryStream mapStream = new MemoryStream();
                mapImg.Save(mapStream, System.Drawing.Imaging.ImageFormat.Png);
                byte[] imageBytes = mapStream.ToArray();
                var doc = webView.Document;
                foreach (HtmlElement node in doc.GetElementsByTagName("b"))
                {
                    if (node.InnerHtml.Contains("[map]") || node.InnerHtml.Contains("[MAP]"))
                    {
                        node.InnerHtml = "<img id='map' src='data:image/png;base64," + Convert.ToBase64String(imageBytes, Base64FormattingOptions.None) + "' />";
                    }
                }
            }
        }

        private void butPrint_Click(object sender, EventArgs e)
        {
            butPrint.Visible = false;
            webView.ShowPrintDialog();
            butPrint.Visible = true;
        }
    }
}
