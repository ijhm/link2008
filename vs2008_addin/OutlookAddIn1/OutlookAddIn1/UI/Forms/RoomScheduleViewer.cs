﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OneSpace.OutlookPlugin
{
    public partial class RoomScheduleViewer : Form
    {
        public RoomScheduleViewer()
        {
            InitializeComponent();
            this.Icon = Branding.Resource.GeneralIcon;
        }
    }
}
