﻿namespace OneSpace.OutlookPlugin
{
    partial class SystemAdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemAdminForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.checkDebug = new System.Windows.Forms.CheckBox();
            this.textRoomReport = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labelDataLoadState = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.butClearCache = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.checkDebug);
            this.tabPage1.Controls.Add(this.textRoomReport);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.labelDataLoadState);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.butClearCache);
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // checkDebug
            // 
            resources.ApplyResources(this.checkDebug, "checkDebug");
            this.checkDebug.Name = "checkDebug";
            this.checkDebug.UseVisualStyleBackColor = true;
            this.checkDebug.CheckedChanged += new System.EventHandler(this.checkDebug_CheckedChanged);
            // 
            // textRoomReport
            // 
            resources.ApplyResources(this.textRoomReport, "textRoomReport");
            this.textRoomReport.Name = "textRoomReport";
            this.textRoomReport.ReadOnly = true;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // labelDataLoadState
            // 
            resources.ApplyResources(this.labelDataLoadState, "labelDataLoadState");
            this.labelDataLoadState.Name = "labelDataLoadState";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // butClearCache
            // 
            resources.ApplyResources(this.butClearCache, "butClearCache");
            this.butClearCache.Name = "butClearCache";
            this.butClearCache.UseVisualStyleBackColor = true;
            this.butClearCache.Click += new System.EventHandler(this.butClearCache_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SystemAdminForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "SystemAdminForm";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button butClearCache;
        private System.Windows.Forms.Label labelDataLoadState;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textRoomReport;
        private System.Windows.Forms.CheckBox checkDebug;
    }
}