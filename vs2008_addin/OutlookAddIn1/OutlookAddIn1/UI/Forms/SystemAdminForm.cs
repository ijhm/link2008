﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Properties;

namespace OneSpace.OutlookPlugin
{
    public partial class SystemAdminForm : Form
    {
        public SystemAdminForm()
        {
            InitializeComponent();
            this.Icon = Branding.Resource.GeneralIcon;

            checkDebug.Checked = Globals.ThisAddIn.WriteToDebugLog;
            BookingItem item;
            textRoomReport.Text = "{Globals.ThisAddIn.resourceManager.GetString(configReportDataReport)}:\r\n";
            foreach (var region in Globals.ThisAddIn.OneSpaceData.Regions)
            {
                textRoomReport.Text += region.Name + "\r\n";
                textRoomReport.Text += "    {Globals.ThisAddIn.resourceManager.GetString(configReportItems)}\r\n\r\n";
                foreach (var bookingItem in region.BookingItems) {
                    textRoomReport.Text += "        {bookingItem.Name},{bookingItem.TypeName},{bookingItem.AlertEmail}\r\n";
                }
                foreach (var location in region.Locations)
                {
                    textRoomReport.Text += "    {Globals.ThisAddIn.resourceManager.GetString(configReportLocations)}\r\n\r\n";
                    textRoomReport.Text += "        {location.Name}\r\n";
                    foreach (var floor in location.Floors)
                    {
                        textRoomReport.Text += "        {Globals.ThisAddIn.resourceManager.GetString(configReportFloors)}\r\n\r\n";
                        textRoomReport.Text += "            {floor.Name}\r\n";
                        foreach (var room in floor.Rooms)
                        {
                            textRoomReport.Text += "            {Globals.ThisAddIn.resourceManager.GetString(configReportRooms)}\r\n\r\n";
                            textRoomReport.Text += "                {room.Name}\r\n";
                            textRoomReport.Text += "                    {Globals.ThisAddIn.resourceManager.GetString(configReportAttendee)}:{room.Attendee}\r\n";
                            textRoomReport.Text += "                    {Globals.ThisAddIn.resourceManager.GetString(configReportMailbox)}:{room.MailBox}\r\n";
                            textRoomReport.Text += "                    {Globals.ThisAddIn.resourceManager.GetString(configReportFeatures)}\r\n";
                            foreach (var itemId in room.Features)
                            {
                                item = Globals.ThisAddIn.OneSpaceData.GetAssetById(itemId);
                                textRoomReport.Text += "                        {item.Name}\r\n";
                            }
                            textRoomReport.Text += "                    {Globals.ThisAddIn.resourceManager.GetString(configReportCartering)}\r\n";
                            foreach (var itemId in room.Catering)
                            {
                                item = Globals.ThisAddIn.OneSpaceData.GetAssetById(itemId);
                                textRoomReport.Text += "                        {item.Name}\r\n";
                            }
                            textRoomReport.Text += "                    {Globals.ThisAddIn.resourceManager.GetString(configReportFacilities)}\r\n";
                            foreach (var itemId in room.Facilities)
                            {
                                item = Globals.ThisAddIn.OneSpaceData.GetAssetById(itemId);
                                textRoomReport.Text += "                        {item.Name}\r\n";
                            }
                            textRoomReport.Text += "                    {Globals.ThisAddIn.resourceManager.GetString(configReportLayout)}\r\n";
                            foreach (var itemId in room.Layout)
                            {
                                item = Globals.ThisAddIn.OneSpaceData.GetAssetById(itemId);
                                textRoomReport.Text += "                        {item.Name}\r\n";
                            }
                        }

                    }

                }
            }
        }

        private void butClearCache_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.ClearCache();
            MessageBox.Show(Globals.ThisAddIn.resourceManager.GetString("messageLocalCacheCleared"));
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            var status = Globals.ThisAddIn.LoadState;
            switch (status)
            {
                case ThisAddIn.enum_LoadingState.Loading:
                    labelDataLoadState.Text = Globals.ThisAddIn.resourceManager.GetString("enumLocadingStateLoading");
                    break;

                case ThisAddIn.enum_LoadingState.FailedToLoad:
                    labelDataLoadState.Text = "{Globals.ThisAddIn.resourceManager.GetString(enumLocadingStateFailedToLoad)}\r\n{Globals.ThisAddIn.ErrorMsg}";
                    break;

                case ThisAddIn.enum_LoadingState.Loaded:
                    labelDataLoadState.Text = Globals.ThisAddIn.resourceManager.GetString("enumLocadingStateLoaded");
                    break;

                case ThisAddIn.enum_LoadingState.Cleared:
                    labelDataLoadState.Text = Globals.ThisAddIn.resourceManager.GetString("enumLocadingStateCleared");
                    break;
            }
        }

        private void checkDebug_CheckedChanged(object sender, EventArgs e)
        {
            string path = Path.GetTempPath() + "CibOutlookPlugin\\debug.inf";
            bool exists = System.IO.File.Exists(path);
            if (checkDebug.Checked)
            {
                if (!exists)
                {
                    System.IO.File.AppendAllText(path, "1");
                }
            }
            else
            {
                if (exists)
                {
                    System.IO.File.Delete(path);
                }
            }
        }
    }
}
