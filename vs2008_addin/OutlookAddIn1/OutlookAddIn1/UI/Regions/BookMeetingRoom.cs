﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Office = Microsoft.Office.Core;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Utilities;
using OneSpace.OutlookPlugin.Settings;
using OneSpace.OutlookPlugin.Extentions;
using OneSpace.OutlookPlugin.Controls;
using Newtonsoft.Json;
using OneSpace.OutlookPlugin.Properties;
using OneSpace.OutlookPlugin.UI.Forms;

namespace OneSpace.OutlookPlugin {
    partial class BookMeetingRoom {

        private class BuildingFloor {
            public Location Building { get; set; }
            public Floor Floor { get; set; }
        }
        
        public List<RoomBooking> BufferedSlots;

        protected override void OnLoad(EventArgs e) {
            //Manifest.Icons.Page = Branding.Resource.RoomsImage;
            group_search.ForeColor = Branding.Resource.TextColour;
            group_room_details.ForeColor = Branding.Resource.TextColour;
            group_room_schedule.ForeColor = Branding.Resource.TextColour;
            butFind.BackColor = Branding.Resource.TextColour;

            label_powered_by_onespace.Text = Branding.Resource.PoweredByText;
            label_powered_by_onespace.Font = new System.Drawing.Font("Arial", 11, System.Drawing.FontStyle.Bold);

            /* Note: I have put in 1 weeks worth of search flexability either side of the start date
             *       making the max posible search range 14 days.  The schedule view will support
             *       25 days on most monitors confotably and a reducing number of days with higher
             *       DPI's e.g. 250, 300.  This is caused by a scrollbar scale limit imposed by MS.
             *  */
            combo_box_for_plus_minus.ValueMember = "Item1";
            combo_box_for_plus_minus.DisplayMember = "Item2";
            var plus_minus_exact_match = new Tuple<int, string>(0, Globals.ThisAddIn.resourceManager.GetString("searchFilterDateExactMatch"));
            combo_box_for_plus_minus.Items.Add(plus_minus_exact_match);
            combo_box_for_plus_minus.Items.Add(new Tuple<int, string>(1, Globals.ThisAddIn.resourceManager.GetString("searchFilterPlusMinusOneDay")));
            combo_box_for_plus_minus.Items.Add(new Tuple<int, string>(2, string.Format(Globals.ThisAddIn.resourceManager.GetString("searchFilterPlusMinusDays"), 2)));
            combo_box_for_plus_minus.Items.Add(new Tuple<int, string>(3, string.Format(Globals.ThisAddIn.resourceManager.GetString("searchFilterPlusMinusDays"), 3)));
            combo_box_for_plus_minus.Items.Add(new Tuple<int, string>(4, string.Format(Globals.ThisAddIn.resourceManager.GetString("searchFilterPlusMinusDays"), 4)));
            combo_box_for_plus_minus.Items.Add(new Tuple<int, string>(5, string.Format(Globals.ThisAddIn.resourceManager.GetString("searchFilterPlusMinusDays"), 5)));
            combo_box_for_plus_minus.Items.Add(new Tuple<int, string>(6, string.Format(Globals.ThisAddIn.resourceManager.GetString("searchFilterPlusMinusDays"), 6)));
            combo_box_for_plus_minus.Items.Add(new Tuple<int, string>(7, string.Format(Globals.ThisAddIn.resourceManager.GetString("searchFilterPlusMinusDays"), 7)));
            combo_box_for_plus_minus.SelectedItem = plus_minus_exact_match;

            combo_box_search_capacity.ValueMember = "Item1";
            combo_box_search_capacity.DisplayMember = "Item2";

            /* Note: I use int literals for "itme2" to ensure the number symbol is printed using the local culture. */
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(1, "{1}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(2, "{2}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(3, "{3}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(4, "{4}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(5, "{5}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(6, "{6}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(7, "{7}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(8, "{8}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(9, "{9}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(10, "{10}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(11, "{11}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(12, "{12}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(13, "{13}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(14,  "{14}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(15, "{15}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(16, "{16}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(17, "{17}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(18, "{18}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(19, "{19}"));
            combo_box_search_capacity.Items.Add(new Tuple<int, string>(int.MaxValue, "{20}+"));

            textBoxSearch.Enabled = false;

            ArrangeRegions();

        }

        private void SetupMap()
        {
            /* Note: This is here for the Map. */
            map_estate_location = new UI.Data.Location()
            {
                Layout = Globals.ThisAddIn.OneSpaceData.MapAreaImage
            };

            var regions = Globals.ThisAddIn.OneSpaceData.Regions;
            foreach (var region in regions)
            {
                var ui_region = new UI.Data.Location()
                {
                    Name = region.Name,
                    Parent = map_estate_location,
                    Layout = region.MapAreaImage,
                    ReferenceItem = region
                };
                ui_region.Path = new System.Drawing.Drawing2D.GraphicsPath();
                ui_region.Path.AddPolygon(region.Poly.ToArray());

                foreach (var building in region.Locations)
                {
                    var ui_building = new UI.Data.Location()
                    {
                        Name = building.Name,
                        Parent = ui_region,
                        Layout = building.MapAreaImage,
                        ReferenceItem = building
                    };
                    ui_building.Path = new System.Drawing.Drawing2D.GraphicsPath();
                    ui_building.Path.AddPolygon(building.Poly.ToArray());

                    foreach (var floor in building.Floors)
                    {
                        var ui_floor = new UI.Data.Location()
                        {
                            Name = floor.Name,
                            Parent = ui_building,
                            Layout = floor.MapAreaImage,
                            ReferenceItem = floor
                        };
                        ui_floor.Path = new System.Drawing.Drawing2D.GraphicsPath();
                        ui_floor.Path.AddPolygon(floor.Poly.ToArray());
                    }
                }
            }
          //  schedule.Visible = false;
            schedule.Setup(map_estate_location);
        }

        #region Variables
        public System.Threading.Timer BackgroundTimer;
        private Cursor lastCursor;
        Outlook.AppointmentItem appointment;
        delegate void StringArgReturningVoidDelegate();
        public List<RoomWithBookings> roomList;
        public List<Room> filteredRoomList;
        public List<Room> eliminatedRoomList;
        public IList<BookingItem> assetItems;
        public List<RoomWithBookings> bookingRooms;
        public delegate void AddRoom(RoomMailbox room, RoomWithBookings roomWithBookings);
        public delegate void AddRoomTick();
        public delegate void AddRoomError(Exception ex, string lastMailBox);
        public delegate void AddRoomFinished();
        public delegate void DoStartup();
        private bool enableCancel;
        private bool enableFind;
        private bool enableSelectRoom;
        private string lastLocation;
        private string lastFloor;
        private object scanning_lock = new object();
        private bool scanning;
        private RoomWithBookings BlankRow;
        private int roomsFound;
        private System.Drawing.Font UnderScoreFont;
        private System.Drawing.Font DisabledFont;
        private string appointmentId;
        private OneSpace.OutlookPlugin.UI.Data.Location map_estate_location = null;
        private bool do_supress_map_location_update = false;
        private bool has_user_changed_capacity = false;

        public bool IsSearching { get; set; }
        public string lastCheckedMailbox { get; set; }
        #endregion

        #region Form Region Factory 

        [Microsoft.Office.Tools.Outlook.FormRegionMessageClass(Microsoft.Office.Tools.Outlook.FormRegionMessageClassAttribute.Appointment)]
        [Microsoft.Office.Tools.Outlook.FormRegionName("OneSpace.OutlookPlugin.BookMeetingRoom")]
        public partial class BookMeetingRoomFactory {
            // Occurs before the form region is initialized.
            // To prevent the form region from appearing, set e.Cancel to true.
            // Use e.OutlookItem to get a reference to the current Outlook item.
            private void BookMeetingRoomFactory_FormRegionInitializing(object sender, Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs e) {
            }
        }

        #endregion

        // Occurs before the form region is displayed.
        // Use this.OutlookItem to get a reference to the current Outlook item.
        // Use this.OutlookFormRegion to get a reference to the form region.
        private void BookMeetingRoom_FormRegionShowing(object sender, System.EventArgs e) {
            enableFind = false;
            enableCancel = false;
            enableSelectRoom = false;
            UIUtilities.SetButton(butFind, enableFind);
            UIUtilities.SetButton(butCancel, enableCancel);
            UIUtilities.SetButton(butSelectRoom, enableSelectRoom);
            
            UIUtilities.DockWithParent(labelSelectRoomWarning2);
            labelSelectRoomWarning2.Visible = true;
            label_room_details_value_restrictions.Visible = false;

            this.Refresh();
            LoadInBackground();
        }

        // Occurs when the form region is closed.
        // Use this.OutlookItem to get a reference to the current Outlook item.
        // Use this.OutlookFormRegion to get a reference to the form region.
        private void BookMeetingRoom_FormRegionClosed(object sender, System.EventArgs e) {
        }

        private void LoadInBackground() 
        {
                BackgroundTimer = new System.Threading.Timer(new System.Threading.TimerCallback(ScanForConfig), null, 0, 1000);
        }

        //ToDo: Startup error sometimes occuring that prevents the config from being loaded...
        private void ScanForConfig(object state) {
            if (Globals.ThisAddIn.LoadState == ThisAddIn.enum_LoadingState.Loaded) {
                this.Invoke(Startup);
                BackgroundTimer.Change(Timeout.Infinite, Timeout.Infinite);
            }
        }

   //     [Obsolete("Fix font selection", true)]
        public void Startup() {
            try {
                SetupMap();
                UnderScoreFont = new System.Drawing.Font("Microsoft Sans Serif", 8, System.Drawing.FontStyle.Underline);

                listSlots.Columns[0].Width = listSlots.Width / 2 - 10;
                listSlots.Columns[1].Width = listSlots.Width / 2 - 10;
                panel_meeting_details.Visible = false;
                panel_room_details.Visible = true;

                appointment = (Outlook.AppointmentItem)this.OutlookItem;

                appointment.PropertyChange += Appointment_PropertyChange;
                Appointment_PropertyChange("Start");
                Appointment_PropertyChange("RequiredAttendees");

                if ((appointment.End - appointment.Start).TotalDays == 1 && appointment.Start.Hour == 0) {
                    appointment.Start = new DateTime(appointment.Start.Year, appointment.Start.Month, appointment.Start.Day, 8, 0, 0);
                    appointment.End = new DateTime(appointment.Start.Year, appointment.Start.Month, appointment.Start.Day, 18, 0, 0);
                }

                time_picker_schedule_begins_on.SelectedTime = appointment.Start.TimeOfDay;
                if(appointment.End.TimeOfDay > appointment.Start.TimeOfDay) {
                    time_picker_schedule_ends_on.SelectedTime = appointment.End.TimeOfDay;
                }
                else {
                    time_picker_schedule_ends_on.SelectedTime = appointment.Start.TimeOfDay;
                }

                enableSelectRoom = false;
                UIUtilities.SetButton(butSelectRoom, enableSelectRoom);
                
                locationBox.Enabled = false;

                floorBox.Items.Add(Globals.ThisAddIn.resourceManager.GetString("dropDownSelectFloor"));
                floorBox.Enabled = false;
                floorBox.SelectedIndex = 0;

                listFeatures.Title = Globals.ThisAddIn.resourceManager.GetString("dropDownFeatures");
                listFacilities.Title = Globals.ThisAddIn.resourceManager.GetString("dropDownEquipment");
                listCatering.Title = Globals.ThisAddIn.resourceManager.GetString("dropDownCatering");
                listLayout.Title = Globals.ThisAddIn.resourceManager.GetString("dropDownLayout");
                listFeatures.DisableList();
                listFacilities.DisableList();
                listCatering.DisableList();
                listLayout.DisableList();

                ClearRoomDetails();

                locationBox.Items.Add(Globals.ThisAddIn.resourceManager.GetString("dropDownSelectLocation"));
                SetLocationBoxIndex();

                regionBox.Items.Clear();
                regionBox.Items.Add(Globals.ThisAddIn.resourceManager.GetString("dropDownSelectRegion"));
                foreach (Region region in Globals.ThisAddIn.OneSpaceData.Regions) {
                    regionBox.Items.Add(region);
                }

                if (regionBox.Items.Count > 0) {
                    regionBox.SelectedIndex = 0;
                }

                this.Refresh();
                PopulateDefaults();
            }
            catch (Exception ex) {
                Logging.ActivityLog.WriteToLog(ex.Message);
                throw;
            }

        }

        private void Appointment_PropertyChange(string Name) {
            switch (Name) {
                case "Start":
                case "End":
                    label_appointment_start_end.Text = "{appointment.Start:dd MMM yyyy HH:mm} to {appointment.End:dd MMM yyyy HH:mm}";
                    time_picker_schedule_begins_on.SelectedTime = appointment.Start.TimeOfDay;
                    time_picker_schedule_ends_on.SelectedTime = appointment.End.TimeOfDay;
                    break;
                case "RequiredAttendees":
                    var attendee_count = appointment.Recipients.Count;
                    var selected_item = combo_box_search_capacity.SelectedItem as Tuple<int, string>;
                    var selected_value = selected_item?.Item1 ?? 0;

                    if (attendee_count > selected_value || has_user_changed_capacity == false)
                    {
                        var old_has_user_changed_capacity = has_user_changed_capacity;
                        var default_item = combo_box_search_capacity.Items[0] as Tuple<int, string>;
                        combo_box_search_capacity.SelectedItem = (
                            from Tuple<int, string> item in combo_box_search_capacity.Items
                            where item.Item1 >= attendee_count
                            orderby item.Item1
                            select item
                        ).FirstOrDefault() ?? default_item;

                        has_user_changed_capacity = (attendee_count > selected_value) ? false : old_has_user_changed_capacity;
                    }

                    break;
            }
        }

        private void ClearRoomDetails() {
            label_room_details_value_room_name.Text = string.Empty;
            label_room_details_value_restrictions.Text = string.Empty;
            label_room_details_value_capacity.Text = string.Empty;
            label_room_details_value_location.Text = string.Empty;
            label_room_details_value_description.Text = string.Empty;
            label_room_details_value_facilities.Text = string.Empty;
            label_room_details_value_linked_rooms.Text = string.Empty;

            listSlots.Items.Clear();

        }

        private void PopulateDefaults() {
            var regionId = OutlookUtilities.GetPropertyNumber(appointment, "RoomBookingRegion");
            var locationId = OutlookUtilities.GetPropertyNumber(appointment, "RoomBookingLocation");
            var roomId = OutlookUtilities.GetPropertyNumber(appointment, "RoomBookingRoom");
            if (roomId > 0) {
                labelSelectRoomWarning2.Visible = false;

                var room = Globals.ThisAddIn.OneSpaceData.GetRoomById(roomId);
                var floor = Globals.ThisAddIn.OneSpaceData.GetFloorFromRoomById(roomId);
                var location = Globals.ThisAddIn.OneSpaceData.GetLocationById(locationId);
                if (room != null) {
                    label_room_details_value_room_name.Text = room.Name;

                    if (location != null) {
                        label_room_details_value_facilities.Text = location.GetFixedAssetList(room.Features);
                        label_room_details_value_location.Text = location.Name + " - " + floor.Name;
                    }

                    label_room_details_value_description.Text = room.Description;
                }
            }
        }

        private int SelectedCapacitiy {
            get {
                var selected_capacity = combo_box_search_capacity.SelectedItem as Tuple<int, string>;

                if(selected_capacity == null) {
                    return -1;
                }

                return selected_capacity.Item1;
            }
        }

        private int SelectedDuration {
            get {
                var meeting = (Outlook.AppointmentItem)this.OutlookItem;
                return meeting.Duration;
            }
        }

        private int SelectedRegion {
            get {
                var selected_index = regionBox.Invoke(() => regionBox.SelectedIndex);
                return (selected_index == -1) ? selected_index : selected_index - 1;
            }
        }

        private int SelectedLocationIndex {
            get {
                var selected_index = locationBox.Invoke(() => locationBox.SelectedIndex);
                return (selected_index == -1) ? selected_index : selected_index - 1;
            }
        }

        private Location SelectedLocation {
            get {
                var selected_index = regionBox.Invoke(() => locationBox.SelectedIndex);
                if (selected_index > 0) {
                    return Globals.ThisAddIn.OneSpaceData.Regions[SelectedRegion].Locations[SelectedLocationIndex];
                }
                return null;
            }
        }

        private Floor SelectedFloor {
            get {
                var selected_index = regionBox.Invoke(() => floorBox.SelectedIndex);
                if (selected_index > 0) {
                    return SelectedLocation.Floors[selected_index - 1];
                }
                return null;
            }
        }
        
        private RoomWithBookings SelectedRoom {
            get {
                var selected_ui_room = schedule.SelectedRoom;

                if (selected_ui_room == null) {
                    return null;
                }

                var selected_room = selected_ui_room.ReferenceItem as Data.Room;
                var selected_room_mailbox = string.Empty;
                if(selected_room is Data.RoomMailbox) {
                    selected_room_mailbox = ((RoomMailbox)selected_room).MailBox;
                }

                List<Outlook.AppointmentItem> selected_room_appointments = (
                    from b in selected_ui_room.Bookings
                        where b.IsBufferTime == false
                    select b.ReferenceItem as Outlook.AppointmentItem
                ).ToList();

                var free_slots = selected_ui_room.GetFreeSlots(schedule.BeginsOn, schedule.EndsOn);

                var room_with_bookings = new Data.RoomWithBookings {
                    Id = selected_room.Id,
                    Name = selected_room.Name,
                    MailBox = selected_room_mailbox,
                    Attendee = selected_room.Attendee,
                    MinLeadTime = selected_room.MinLeadTime,
                    Bookings = (
                        from a in selected_room_appointments
                        select new Data.RoomBooking 
                        {
                            Id = a.EntryID,
                            DateFrom = a.Start,
                            DateTo = a.End,
                            Subject = a.Subject ?? string.Empty,
                            OrganiserName = GetDisplayName(a) ?? string.Empty,
                            OrganiserEmail = GetEmailAddress(a) ?? string.Empty,
                            appointmentItem = a,
                            IsFree = false,
                            IsBuffer = false,
                            Room = selected_room
                        }
                    ).ToList(),

                    FreeSlots = (
                        from s in free_slots
                        select new Data.RoomBooking {
//                            DateFrom = s.BeginsOn.LocalDateTime,
//                            DateTo = s.EndsOn.LocalDateTime,
                            //Offset to buffer times
                            DateFrom = s.BeginsOn.LocalDateTime,
                            DateTo = s.EndsOn.LocalDateTime,
                            IsFree = true,
                            Room = selected_room
                        }
                    ).ToList(),
                    Features = selected_room.Features,
                    Capacitiy = selected_room.Capacitiy,
                    Description = selected_room.Description
                };

                return room_with_bookings;
            }
        }


        private string GetDisplayName(AppointmentItem appointment)
        {
            var address = appointment.Organizer.TryConvertToMailAddress();
            if (address==null)
                return null;
             return address.DisplayName;
        }


        private string GetEmailAddress(AppointmentItem appointment)
        {
            var address = appointment.Organizer.TryConvertToMailAddress();
            if (address==null)
                return null;
            return address.Address;
        }


        private DateTime SearchAppointmentStart {
            get {
                var today = DateTime.Now.Date;
                var begins_on = appointment.Start.Date;

                var plus_minus_days = combo_box_for_plus_minus.Invoke(() => combo_box_for_plus_minus.SelectedItem as Tuple<int, string>);
                if (plus_minus_days.Item1 > 0) {
                    begins_on -= TimeSpan.FromDays(plus_minus_days.Item1);
                }

                if(begins_on < today) {
                    begins_on = today;
                }

                return begins_on;
            }
        }

        private DateTime SearchAppointmentEnd {
            get {
                var ends_on = appointment.End.Date.AddDays(1);

                var plus_minus_days = combo_box_for_plus_minus.Invoke(() => combo_box_for_plus_minus.SelectedItem as Tuple<int, string>);
                if (plus_minus_days.Item1 > 0) {
                    ends_on += TimeSpan.FromDays(plus_minus_days.Item1);
                }

                return ends_on;
            }
        }

        private void regionBox_SelectedIndexChanged(object sender, EventArgs e) {
            int selectedRegion = regionBox.SelectedIndex;
            floorBox.Items.Clear();
            floorBox.Items.Add(Globals.ThisAddIn.resourceManager.GetString("dropDownSelectFloor"));
            floorBox.Enabled = false;
            if (selectedRegion > 0) {
                textBoxSearch.Enabled = true;

                selectedRegion--;
                Region region = Globals.ThisAddIn.OneSpaceData.Regions[selectedRegion];

                listFacilities.DisableList();
                listCatering.DisableList();
                listFeatures.DisableList();
                listLayout.DisableList();
                listFacilities.ClearItems();
                listCatering.ClearItems();
                listFeatures.ClearItems();
                listLayout.ClearItems();

                foreach (BookingItem item in region.BookingItems) {
                    switch (item.ItemType) {
                        case BookingItem.enum_ItemType.Facilities:
                            listFacilities.AddItem(item);
                            listFacilities.EnableList();
                            break;
                        case BookingItem.enum_ItemType.Catering:
                            listCatering.AddItem(item);
                            listCatering.EnableList();
                            break;
                        case BookingItem.enum_ItemType.Features:
                            listFeatures.AddItem(item);
                            listFeatures.EnableList();
                            break;
                        case BookingItem.enum_ItemType.Layout:
                            listLayout.AddItem(item);
                            listLayout.EnableList();
                            break;
                    }
                }
                assetItems = region.BookingItems;

                enableFind = true;
                UIUtilities.SetButton(butFind, enableFind);
                butFind.Text = Globals.ThisAddIn.resourceManager.GetString("buttonTextFindRooms");

                locationBox.Items.Clear();
                locationBox.Items.Add(Globals.ThisAddIn.resourceManager.GetString("dropDownSelectLocation"));
                SetLocationBoxIndex();
                locationBox.Enabled = true;
                foreach (var item in region.Locations) {
                    locationBox.Items.Add(item);
                }
                SetLocationBoxIndex();
            }
            else {
                schedule.TrySelectLocation(map_estate_location);
                enableFind = false;
                UIUtilities.SetButton(butFind, enableFind);
                butFind.Text = Globals.ThisAddIn.resourceManager.GetString("buttonTextFindRooms");

                locationBox.Items.Clear();
                locationBox.Items.Add(Globals.ThisAddIn.resourceManager.GetString("dropDownSelectLocation"));
                SetLocationBoxIndex();
                locationBox.Enabled = false;
                textBoxSearch.Enabled = false;
            }

            SetMapLocation();
        }

        private void SetLocationBoxIndex()
        {
            locationBox.SelectedIndexChanged -= locationBox_SelectedIndexChanged;
            locationBox.SelectedIndex = 0;
            locationBox.SelectedIndexChanged += locationBox_SelectedIndexChanged;
        }

        private void SetFloorBoxIndex()
        {
            floorBox.SelectedIndexChanged -= floorBox_SelectedIndexChanged;
            floorBox.SelectedIndex = 0;
            floorBox.SelectedIndexChanged += floorBox_SelectedIndexChanged;
        }

        private void locationBox_SelectedIndexChanged(object sender, EventArgs e) {
            int selectedLocation = locationBox.SelectedIndex;
            int selectedRegion = regionBox.SelectedIndex;

            SetMapLocation();

            if (selectedRegion > 0 && selectedLocation > 0) {
                selectedRegion--;
                selectedLocation--;

                var selected_region = Globals.ThisAddIn.OneSpaceData.Regions[selectedRegion];
                floorBox.Items.Clear();
                floorBox.Items.Add(Globals.ThisAddIn.resourceManager.GetString("dropDownSelectFloor"));
                floorBox.Enabled = true;
                SetFloorBoxIndex();

                var selected_building = selected_region.Locations[selectedLocation];

                foreach (var item in selected_building.Floors) {
                    floorBox.Items.Add(item);
                }
                label_room_details_value_location.Text = SelectedLocation.Name;
            }
            else {
                floorBox.Items.Clear();
                floorBox.Items.Add(Globals.ThisAddIn.resourceManager.GetString("dropDownSelectFloor"));
                floorBox.Enabled = false;
                SetFloorBoxIndex();

                label_room_details_value_location.Text = string.Empty;
            }
        }

        private void floorBox_SelectedIndexChanged(object sender, EventArgs e) {
            SetMapLocation();
        }

        private void SetMapLocation() {
            if (do_supress_map_location_update) {
                return;
            }

            int selected_region_index = regionBox.SelectedIndex;
            int selected_building_index = locationBox.SelectedIndex;
            int selected_floor_index = floorBox.SelectedIndex;

            Data.Region selected_region = null;
            Data.Location selected_building = null;
            Data.Floor selected_floor = null;
            OneSpace.OutlookPlugin.UI.Data.Location ui_location = null;

            if (selected_region_index == 0)
            {
                SetupMap();
            }

            if (selected_region_index > 0) {
                selected_region_index--;
                selected_region = Globals.ThisAddIn.OneSpaceData.Regions[selected_region_index];
            }

            if (selected_region != null && selected_building_index > 0) {
                selected_building_index--;
                selected_building = selected_region.Locations[selected_building_index];
            }

            if (selected_building != null && selected_floor_index > 0) {
                selected_floor_index--;
                selected_floor = selected_building.Floors[selected_floor_index];
            }

            ui_location = (
                from l in schedule.Locations
                where l.ReferenceItem == ((object)selected_floor ?? (object)selected_building ?? (object)selected_region)
                select l
            ).FirstOrDefault();

            schedule.TrySelectLocation(ui_location);
        }

        private void butFind_Click(object sender, EventArgs e) {

            if (scanning) {
                butFind.Text = Globals.ThisAddIn.resourceManager.GetString("buttonTextFindRooms");
                Invoke(new AddRoomFinished(AddRoomFinishedUI));
                scanning = false;
                enableCancel = true;
                UIUtilities.SetButton(butCancel, enableCancel);
                return;
            }

            if (!enableFind) {
                return;
            }
            
            this.lastLocation = string.Empty;
            this.lastFloor = string.Empty;
            lastCursor = Cursor.Current;
            roomList = new List<Data.RoomWithBookings>();
            lastCheckedMailbox = string.Empty;
            progressScan.Visible = true;

            IsSearching = true;
            roomsFound = 0;

            combo_box_for_plus_minus.Enabled = false;
            combo_box_search_capacity.Enabled = false;
            check_box_show_booked_rooms.Enabled = false;
            regionBox.Enabled = false;
            locationBox.Enabled = false;
            floorBox.Enabled = false;

            int region = SelectedRegion;
            int min_capacity = SelectedCapacitiy;
            string lastFloor = string.Empty;
            string lastRoom = string.Empty;
            filteredRoomList = new List<Data.Room>();
            eliminatedRoomList = new List<Room>();
            bookingRooms = new List<Data.RoomWithBookings>();
            bool scanFloor;
            bool scanLocation;
            foreach (Location loc in Globals.ThisAddIn.OneSpaceData.Regions[region].Locations) {
                scanLocation = true;
                if (SelectedLocation != null) {
                    if (loc.Name != SelectedLocation.Name) {
                        scanLocation = false;
                        Logging.ActivityLog.WriteToLog(String.Format("Location:{0} eliminated as not selected", loc.Name));
                    }
                }
                if (scanLocation) {
                    foreach (Floor floor in loc.Floors) {
                        scanFloor = true;
                        if (SelectedFloor != null) {
                            if (floor.Name != SelectedFloor.Name) {
                                scanFloor = false;
                                Logging.ActivityLog.WriteToLog(String.Format("Floor:{0} eliminated as not selected", floor.Name));
                            }
                        }
                        if (scanFloor) {
                            lastFloor = floor.Name;

                            var rooms = new List<Room>();
                            rooms.AddRange(floor.Rooms);
                            rooms.AddRange(floor.LinkedRooms);

                            foreach (Room room in rooms) {
                                lastRoom = room.Name;
                                bool addRoom = true;

                                if(min_capacity > 0 && room.Capacitiy < min_capacity)
                                {
                                    addRoom = false;
                                    Logging.ActivityLog.WriteToLog(String.Format("Room:{0} eliminated due to not meeting the required capacity.  Room Capacity: {1}, Minimum Capacity: {2}", room.Name, room.Capacitiy, min_capacity));
                                }

                                if (!listFeatures.IsAllIncluded(room.Features)) {
                                    addRoom = false;
                                    Logging.ActivityLog.WriteToLog(String.Format("Room:{0} eliminated due to features not included", room.Name));
                                }

                                if (!listFacilities.IsAllIncluded(room.Facilities)) {
                                    addRoom = false;
                                    Logging.ActivityLog.WriteToLog(String.Format("Room:{0} eliminated due to features not included", room.Name));
                                }

                                if (!listCatering.IsAllIncluded(room.Catering)) {
                                    addRoom = false;
                                    Logging.ActivityLog.WriteToLog(String.Format("Room:{0} eliminated due to catering not included", room.Name));
                                }

                                if (!listLayout.IsAllIncluded(room.Layout)) {
                                    addRoom = false;
                                    Logging.ActivityLog.WriteToLog(String.Format("Room:{0} eliminated due to layout not included", room.Name));
                                }
                                
                                if (addRoom) {

                                    /* Note: OUTPLUG-179: This is a bad solution.  We should update the design to follow the rules of encapsulation */
                                    room.Status = enum_MapAreaStatus.Free;

                                    if (textBoxSearch.Text != string.Empty) {
                                        if (room.Name.ToLower().Contains(textBoxSearch.Text.ToLower())) {
                                            filteredRoomList.Add(room);
                                        }
                                    }
                                    else {
                                        filteredRoomList.Add(room);
                                    }
                                }
                                else {
                                    room.Status = enum_MapAreaStatus.NotSuitable;
                                    eliminatedRoomList.Add(room);
                                }
                            }
                        }
                    }
                }

                progressScan.Maximum = filteredRoomList.Count * 2;
            }

            butFind.Text = Globals.ThisAddIn.resourceManager.GetString("buttonTextCancelSearch");
            lock (scanning_lock) {
                if (!scanning) {
                    Cursor.Current = Cursors.WaitCursor;
                    this.Refresh();

                    var main_search_ews = new Thread(MainSearch);
                    main_search_ews.Start();
                    scanning = true;
                }
            }
        }

        private void MainSearch() {
            try {
                Logging.ActivityLog.WriteToLog("Getting Appointments for the filtered room list");
                Action increment_progress = () => {
                    roomsFound++;
                    if (roomsFound < progressScan.Maximum) {
                        progressScan.Invoke((Action)(() => { progressScan.Value = roomsFound; }));
                    }
                };

                var appointments = GetAppointments(filteredRoomList, increment_progress);

                var room_floor_building_map = GetRoomFloorBuildingMap();
                var buildings = new Dictionary<Location, OneSpace.OutlookPlugin.UI.Data.Location>();
                var floors = new Dictionary<Floor, OneSpace.OutlookPlugin.UI.Data.Location>();

                var all_rooms = new List<OneSpace.OutlookPlugin.UI.Data.Room>();

                /* Note: The appointments.Keys has all the rooms from the filtered list as well as any additional rooms the linked rooms bring */
                foreach (var room in appointments.Keys) {
                    BuildingFloor building_floor;

                    if (room_floor_building_map.TryGetValue(room, out building_floor) == true) {
                        var ui_floor = (
                            from r in map_estate_location.Children
                            from b in r.Children
                            from f in b.Children
                            where f.ReferenceItem == building_floor.Floor
                            select f
                        ).FirstOrDefault();

                        var ui_room = new OneSpace.OutlookPlugin.UI.Data.Room() {
                            Name = room.Name,
                            Parent = ui_floor,
                            ReferenceItem = room
                        };

                        var room_appointments = appointments[room];
                        
                        var bookings = (
                            from a in room_appointments
                            select new OneSpace.OutlookPlugin.UI.Data.Booking {
                                Room = ui_room,
                                BeginsOn = a.Start,
                                EndsOn = a.End,
                                ReferenceItem = a,
                                IsPartOfALinkedBooking = (OutlookUtilities.GetPropertyText(a, AppointmentSchemaEx.RoomBookingLinkedRooms) ?? string.Empty).FromJsonToInt32Array().Length > 0
                            }
                        ).ToList();

                        ui_room.Bookings = bookings;

                        #region Add start and end buffers
                        foreach (var appointment in room_appointments)
                        {
                            var startDate = appointment.Start;
                            var endDate = appointment.End;

                            string bookingJson = OutlookUtilities.GetPropertyText(appointment, AppointmentSchemaEx.RoomBookingDataUpdated);
                            AppointmentData data = null;
                            if (string.IsNullOrWhiteSpace(bookingJson) == false) {
                                data = new AppointmentData(bookingJson);
                            }

                            var booking = (
                                from b in ui_room.Bookings
                                where b.ReferenceItem == appointment
                                select b
                            ).FirstOrDefault();

                            var buffer_room = room;
                            if(booking?.IsPartOfALinkedBooking == true) {
                                var linked_room = (
                                    from r in appointments.Keys
                                    where r is RoomLinked &&
                                          ((RoomLinked)r).Rooms.Contains(room.Id)
                                    select r
                                ).FirstOrDefault();
                                buffer_room = linked_room ?? buffer_room;
                            }

                            var bufferAppointment = new RoomBooking()
                            {
                                appointmentItem = appointment,
                                Data = data,
                                DateFrom = startDate,
                                DateTo = endDate,
                                Room = buffer_room,
                                IsBuffer = true
                            };
                            //Add start buffer
                            var pre_buffer = bufferAppointment.PreBuffer;
                            if (pre_buffer != null)
                            {
                                var beforeItem = new OneSpace.OutlookPlugin.UI.Data.Booking
                                {
                                    Room = ui_room,
                                    BeginsOn = pre_buffer.Start,
                                    EndsOn = pre_buffer.End,
                                    ReferenceItem = appointment,
                                    IsBufferTime = true
                                };
                                ui_room.Bookings.Add(beforeItem);
                            }
                            //Add end buffer
                            var post_buffer = bufferAppointment.PostBuffer;
                            if (post_buffer != null)
                            {
                                var afterItem = new OneSpace.OutlookPlugin.UI.Data.Booking
                                {
                                    Room = ui_room,
                                    BeginsOn = post_buffer.Start,
                                    EndsOn = post_buffer.End,
                                    ReferenceItem = appointment,
                                    IsBufferTime = true
                                };
                                ui_room.Bookings.Add(afterItem);
                            }
                        }
                        #endregion

                        all_rooms.Add(ui_room);


                    }
                }

                var rooms = new List<OneSpace.OutlookPlugin.UI.Data.Room>();

                foreach (var room in filteredRoomList) {
                    var ui_room = (
                        from r in all_rooms
                        where r.ReferenceItem == room
                        select r
                    ).FirstOrDefault();

                    if(ui_room != null) {
                        var do_add_room_to_results = true;

                        if (room is RoomMailbox) {
                            ui_room.Path = new System.Drawing.Drawing2D.GraphicsPath();
                            ui_room.Path.AddPolygon(room.Poly.ToArray());
                        }
                        else {
                            var room_linked = room as RoomLinked;
                            ui_room.Path = null;
                            var linked_rooms = (
                                from region in Globals.ThisAddIn.OneSpaceData.Regions
                                from building in region.Locations
                                from floor in building.Floors
                                from room_a in floor.Rooms
                                where room_linked.Rooms.Contains(room_a.Id)
                                select room_a
                            ).ToList();

                            var linked_ui_rooms = (
                                from r in all_rooms
                                where linked_rooms.Contains(r.ReferenceItem)
                                select r
                            ).ToList();

                            foreach(var linked_ui_room in linked_ui_rooms) {
                                var linked_room = linked_ui_room.ReferenceItem as RoomMailbox;
                                if(linked_room != null) {
                                    linked_ui_room.Path = new System.Drawing.Drawing2D.GraphicsPath();
                                    linked_ui_room.Path.AddPolygon(linked_room.Poly.ToArray());
                                }
                            }

                            ui_room.LinkedRooms = linked_ui_rooms;

                            var bookings = (
                                from r in linked_ui_rooms
                                from b in r.Bookings
                                where b.IsPartOfALinkedBooking == false
                                      && b.IsBufferTime==false
                                select b
                            ).ToList();

                            var bookings_linked = (
                                from r in linked_ui_rooms
                                from b in r.Bookings
                                where b.IsPartOfALinkedBooking == true
                                      && b.IsBufferTime == false
                                group b by new { b.BeginsOn, b.EndsOn } into groub_b
                                select new UI.Data.Booking {
                                    BeginsOn = groub_b.Key.BeginsOn,
                                    EndsOn = groub_b.Key.EndsOn,
                                    Room = ui_room,
                                    ReferenceItem = groub_b.First().ReferenceItem,
                                    IsPartOfALinkedBooking = true,
                                }
                            ).ToList();



                            for (int i = 0; i < bookings_linked.Count; i++)
                            {
                                if (bookings_linked[i].Room.LinkedRooms.Count > 0)
                                {
                                    for (int j = 0; j < bookings_linked[i].Room.LinkedRooms.Count; j++)
                                    {
                                        for (int b=0;b< bookings_linked[i].Room.LinkedRooms[j].Bookings.Count;b++)
                                        {
                                            if (bookings_linked[i].Room.LinkedRooms[j].Bookings[b].IsBufferTime)
                                            {
                                                var bufferItemLink = new OneSpace.OutlookPlugin.UI.Data.Booking
                                                {
                                                    Room = bookings_linked[i].Room.LinkedRooms[j],
                                                    BeginsOn = bookings_linked[i].Room.LinkedRooms[j].Bookings[b].BeginsOn,
                                                    EndsOn = bookings_linked[i].Room.LinkedRooms[j].Bookings[b].EndsOn,
                                                    ReferenceItem = appointment,
                                                    IsBufferTime = true
                                                };
                                                ui_room.Bookings.Add(bufferItemLink);
                                            }
                                        }

                                    }
                                }
                            }

                            ui_room.Bookings.AddRange(bookings);
                            ui_room.Bookings.AddRange(bookings_linked);
                        }

                        if (check_box_show_booked_rooms.Checked == false) {
                            var begins_on = (DateTimeOffset)appointment.Start;
                            var ends_on = (DateTimeOffset)appointment.End;
                            
                            var plus_minus_days = combo_box_for_plus_minus.Invoke(() => combo_box_for_plus_minus.SelectedItem as Tuple<int, string>);

                            if(plus_minus_days.Item1 == 0 /* Exact Match */) {
                                do_add_room_to_results = !(
                                    from b in ui_room.Bookings
                                    where b.BeginsOn < ends_on &&
                                          b.EndsOn > begins_on
                                    select b
                                ).Any();
                            }
                            else {
                                var room_setup = TimeSpan.FromMinutes(room.SetupTime);
                                var room_cleardown = TimeSpan.FromMinutes(room.ClearDownTime);
                                var start = appointment.Start.Subtract(room_setup);
                                var end = appointment.End.Add(room_cleardown);
                                var duration = end - start;

                                var search_start = SearchAppointmentStart;
                                var search_end = SearchAppointmentEnd;

                                var bookings = (
                                    from b in ui_room.Bookings
                                    where b.BeginsOn >= search_start
                                    orderby b.BeginsOn
                                    select b
                                ).ToList();

                                var last_booking = bookings.LastOrDefault();
                                DateTimeOffset working_date = search_start;
                                foreach (var booking in bookings) {
                                    if((booking.BeginsOn - working_date) > duration) {
                                        /* Exit loop we have found a free slot for this room */
                                        break;
                                    }

                                    if(booking == last_booking) {
                                        if(end > booking.EndsOn && (end - booking.EndsOn) > duration){
                                            /* Exit loop we have found a free slot for this room */
                                            break;
                                        }

                                        /* We havn't found a free slot large enough, so exclude the room */
                                        do_add_room_to_results = false;
                                    }

                                    working_date = booking.EndsOn;
                                }
                            }
                        }

                        if (do_add_room_to_results == true) {
                            rooms.Add(ui_room);
                        }
                    }

                    increment_progress();
                }
                
                this.Invoke(() => {
                    var search_start = SearchAppointmentStart;
                    var search_end = SearchAppointmentEnd;

                    var number_of_days = (int)Math.Floor((search_end.Date - search_start.Date).TotalDays);
                    number_of_days = (number_of_days == 0) ? 1 : number_of_days;
                    var dates = (
                        from n in Enumerable.Range(0, number_of_days)
                        select search_start.Date.AddDays(n)
                    ).ToList();

                    date_selector_room_schedule.Setup(dates);
                    schedule.Setup(rooms, search_start, search_end, map_estate_location);
                    schedule.HighlightSection(appointment.Start, appointment.End);
//                    schedule.SwitchToSchedule();
                    schedule.SwitchToMap();
                    AddRoomFinishedUI();
                });
            }
            catch (Exception ex) {
                Logging.ActivityLog.WriteToLog(ex.ToString());
                var errorMsg = "An error occurred during the search, please see the log for details.";
                if (ex.InnerException != null) {
                    if (ex.InnerException.InnerException != null) {
                        if (ex.InnerException.InnerException.Message == "The SMTP address has no mailbox associated with it.") {
                            errorMsg = "One or more of the configured meeting rooms could not be contacted.";
                        }
                    }
                }
                MessageBox.Show(errorMsg);
                this.Invoke(AddRoomFinishedUI);
            }
        }

        private Dictionary<Room, List<Outlook.AppointmentItem>> GetAppointments(List<Room> rooms, Action progress_callback) {

            var mailbox_rooms = (
                from room in rooms
                where room is RoomMailbox
                select (RoomMailbox)room
            ).ToList();

            var linked_rooms = (
                from room in rooms
                where room is RoomLinked
                select (RoomLinked)room
            ).ToList();

            var linked_rooms_room_ids = (
                from room in linked_rooms
                from room_id in room.Rooms
                select room_id
            ).ToList();

            var linked_rooms_rooms = (
                from region in Globals.ThisAddIn.OneSpaceData.Regions
                from building in region.Locations
                from floor in building.Floors
                from room in floor.Rooms
                where linked_rooms_room_ids.Contains(room.Id)
                select room
            ).ToList();

            var mailbox_rooms_flat_list = mailbox_rooms.Union(linked_rooms_rooms).Distinct().ToList();

            var total_rooms = mailbox_rooms_flat_list.Count + linked_rooms.Count;
            var wait_obj = new object();
            var is_complete = (total_rooms == 0);
            var result = new Dictionary<Room, List<Outlook.AppointmentItem>>();
            var exceptions = new List<Exception>();

            var ex_handler = new Action<Exception, Room>((ex, room) => {
                var mailbox = (room is RoomMailbox) ? ((RoomMailbox)room)?.MailBox : "Linked Room";
                var message = @"An exception occured when processing '{room?.Name}'.";
                exceptions.Add(new Exception(message, ex));
            });

            var begins_on = SearchAppointmentStart;
            var ends_on = SearchAppointmentEnd.AddDays(7);

            var ns = Globals.ThisAddIn.Application.Session;
            var linked_room_appointments = new Dictionary<int, List<Outlook.AppointmentItem>>();

            foreach (var room in mailbox_rooms_flat_list) {
                try {
                    var room_account = ns.CreateRecipient(room.MailBox);
                    var room_calendar = ns.GetSharedDefaultFolder(room_account, Outlook.OlDefaultFolders.olFolderCalendar);

                    if(room_calendar.InAppFolderSyncObject == false) {
                        room_calendar.InAppFolderSyncObject = true;
                    }

                    Globals.ThisAddIn.Search(
                        room_calendar,
                        "(\"urn:schemas:calendar:dtstart\" >= '{begins_on.ToOutlookFilter()}' AND \"urn:schemas:calendar:dtend\" <= '{ends_on.ToOutlookFilter()}')",
                        false,
                        (s) =>
                        {
                            try {
                                var room_calendar_items = s.Results;
                                room_calendar_items.Sort("[Start]", false);
                        
                                var items = new List<Outlook.AppointmentItem>();
                                var linked_room_items = new List<Outlook.AppointmentItem>();
                                foreach (Outlook.AppointmentItem room_calendar_item in room_calendar_items)
                                {
                                    /* Note: For some reason it was quicker to get the properties here in this way and allow the Outlook API to cache the values then call it later when needed. */
                                    var props = room_calendar_item.PropertyAccessor.GetProperties(new[] {
                                        "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/RoomBookingDataUpdated/0x0000001F",
                                        "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/RoomBookingLinkedRooms/0x0000001F"
                                    });

                                    var appointment_linked_rooms = (props[1] as string ?? string.Empty).FromJsonToInt32Array();

                                    if (appointment_linked_rooms.Length > 0) {
                                        linked_room_items.Add(room_calendar_item);
                                    }

                                    items.Add(room_calendar_item);
                                }

                                result.Add(room, items);
                                linked_room_appointments.Add(room.Id, linked_room_items);
                            }
                            catch(Exception ex) {
                                ex_handler(ex, room);
                            }
                            
                            if(result.Count >= (mailbox_rooms_flat_list.Count - exceptions.Count)) {
                                lock (wait_obj) {
                                    is_complete = true;
                                    Monitor.Pulse(wait_obj);
                                }
                            }

                            progress_callback();
                        }
                    );
                }
                catch (Exception ex) {
                    ex_handler(ex, room);
                }
            }

            lock (wait_obj) {
                if(is_complete == false) {
                    Monitor.Wait(wait_obj);
                }
            }
            
            foreach (var room in linked_rooms) {
                var items = new List<Outlook.AppointmentItem>();

                var linked_rooms_results = (
                    from r in result
                    where room.Rooms.Contains(r.Key.Id)
                    select r
                );

                foreach(var room_result in linked_rooms_results) {
                    items.AddRange(room_result.Value);
                }
                
                result.Add(room, items);
                progress_callback();
            }
            
            if (exceptions.Count > 0) {
                throw new AggregateException("{exceptions.Count} exception(s) occured whilst getting appointments for a number of rooms, please see the inner exception(s) for details", exceptions);
            }

            return result;
        }

        private Dictionary<Room, BuildingFloor> GetRoomFloorBuildingMap() {
            var selected_region = SelectedRegion;
            var selected_location = SelectedLocation;
            var selected_floor = SelectedFloor;

            var room_floor_building_map = new Dictionary<Room, BuildingFloor>();

            if (selected_floor != null && selected_location != null) {
                var building_floor = new BuildingFloor() { Building = selected_location, Floor = selected_floor };
                foreach (var room in selected_floor.Rooms) {
                    room_floor_building_map.Add(room, building_floor);
                }
                foreach (var linked_room in selected_floor.LinkedRooms) {
                    room_floor_building_map.Add(linked_room, building_floor);
                }
            }
            else if (selected_location != null) {
                foreach (var floor in selected_location.Floors) {
                    var building_floor = new BuildingFloor() { Building = selected_location, Floor = floor };
                    foreach (var room in floor.Rooms) {
                        room_floor_building_map.Add(room, building_floor);
                    }
                    foreach (var linked_room in floor.LinkedRooms) {
                        room_floor_building_map.Add(linked_room, building_floor);
                    }
                }
            }
            else {
                var buildings = Globals.ThisAddIn.OneSpaceData.Regions[selected_region].Locations;
                foreach (var building in buildings) {
                    foreach (var floor in building.Floors) {
                        var building_floor = new BuildingFloor() { Building = building, Floor = floor };
                        foreach (var room in floor.Rooms) {
                            room_floor_building_map.Add(room, building_floor);
                        }
                        foreach (var linked_room in floor.LinkedRooms) {
                            room_floor_building_map.Add(linked_room, building_floor);
                        }
                    }
                }
            }

            return room_floor_building_map;
        }

        private void FinishSearch() {
            ReEnableControls();
        }

        private void ReEnableControls() {
            if (this.butFind.InvokeRequired) {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(FinishSearch);
                this.Invoke(d);
            }
            else {
                enableFind = true;
                UIUtilities.SetButton(butFind, enableFind);
                butFind.Text = Globals.ThisAddIn.resourceManager.GetString("buttonTextFindRooms");
                combo_box_for_plus_minus.Enabled = true;
                combo_box_search_capacity.Enabled = true;
                check_box_show_booked_rooms.Enabled = true;
                locationBox.Enabled = true;
            }
        }

        private Outlook.Items GetAppointmentsInRange(Outlook.Folder folder, DateTime from, DateTime to) {
            Outlook.Items result = null;
            string query = string.Format("[Start] >= '{0}' AND [End] <= '{1}'", from.ToString("g"), to.ToString("g"));
            try {
                Outlook.Items items = folder.Items;
                items.IncludeRecurrences = true;
                items.Sort("[Start]", Type.Missing);
                result = items.Restrict(query);
                if (result.Count == 0) {
                    result = null;
                }
            }
            catch (Exception) {
            }
            return result;
        }
        
        private void butSelectRoom_Click(object sender, EventArgs e) {
            if (!enableSelectRoom) {
#if DEBUG
                Logging.ActivityLog.WriteToLog("{nameof(enableSelectRoom)} == {enableSelectRoom}, Book button doing nothing.");
#endif
                return;
            }
            var selected_room = SelectedRoom;

            var selected_ui_room = schedule.SelectedRoom;

            if (selected_room != null && selected_ui_room != null) {

                var start_date = date_selector_room_schedule.SelectedDate ?? appointment.Start.Date;
                //var end_date = start_date.AddMinutes(appointment.Duration).Date;

                var appointment_start = start_date + time_picker_schedule_begins_on.SelectedTime;
                var appointment_end = start_date + time_picker_schedule_ends_on.SelectedTime;

                if (selected_room.MinLeadTime > 0) {
                    var book_after = DateTime.Now.AddMinutes(selected_room.MinLeadTime);
                    if (book_after > appointment_start) {
                        var lead_time = TimeSpan.FromMinutes(selected_room.MinLeadTime);
                        var message = string.Format(
                            Globals.ThisAddIn.resourceManager.GetString("constraintRoomMustBeBookedInAdvance"),
                            selected_room.Name,
                            lead_time.ToFriendlyString().ToLower()
                        );
                        MessageBox.Show(message);
                        Logging.ActivityLog.WriteToLog(message);
                        return;
                    }
                }

                var check = RoomWithBookings.CanBookWithBuffer(BufferedSlots, null, appointment_start, appointment_end);
                if (!check.BufferOk)
                {
                    var dialog = new FormBookingRestrictions(check.Before, check.After);
                    dialog.ShowDialog();
                    Logging.ActivityLog.WriteToLog(Globals.ThisAddIn.resourceManager.GetString("meetingBufferErrorTitle"));
                    return;
                }

                var the_region = Globals.ThisAddIn.OneSpaceData.GetRegionByRoomId(selected_room.Id);
                var the_location = Globals.ThisAddIn.OneSpaceData.GetLocationFromRoomById(selected_room.Id);

                appointment.Location = string.Format("{0} - {1} - {2}",the_region.Name, the_location.Name,selected_room.Name);
                
                //foreach(Outlook.Recipient recipient in appointment.Recipients) {
                //    if(recipient.Type == (int)Outlook.OlMeetingRecipientType.olResource) {
                //        /* Todo: Decide if we want to remove all Resources (Rooms & Equipment) before adding a new Room */
                //    }
                //}

                var linked_room_ids = new List<int>();
                if(selected_ui_room.LinkedRooms == null || selected_ui_room.LinkedRooms.Count == 0) {
                    var room_recipient = appointment.Recipients.Add(selected_room.MailBox);
                    room_recipient.Resolve();
                }
                else {
                    foreach(var ui_room in selected_ui_room.LinkedRooms) {
                        var room = ui_room.ReferenceItem as RoomMailbox;
                        if(room != null) {
                            var room_recipient = appointment.Recipients.Add(room.MailBox);
                            room_recipient.Resolve();
                            linked_room_ids.Add(room.Id);
                        }
                    }
                }

                appointment.Start = appointment_start;
                appointment.End = appointment_end;
                
                OutlookUtilities.SetPropertyNumber(appointment, "RoomBookingRegion", the_region.Id);
                Location location = Globals.ThisAddIn.OneSpaceData.GetLocationFromRoomById(selected_room.Id);
                OutlookUtilities.SetPropertyNumber(appointment, "RoomBookingLocation", location.Id);
                OutlookUtilities.SetPropertyNumber(appointment, "RoomBookingRoom", selected_room.Id);
                OutlookUtilities.SetPropertyText(appointment, "RoomBookingLinkedRooms", JsonConvert.SerializeObject(linked_room_ids));
                OutlookUtilities.SetPropertyNumber(appointment, "RoomBookingCapacity", SelectedCapacitiy);
                OutlookUtilities.SetPropertyNumber(appointment, "RoomDataUpdated", 1);
                OutlookUtilities.SetPropertyText(appointment, "RoomDataId", Guid.NewGuid().ToString());
                OutlookUtilities.SetPropertyText(appointment, "RoomBookingLocationText", location.Id.ToString());

                /* Note: Used as validation to ensure the OneSpace appointment data is updated when the Start & End are. */
                OutlookUtilities.SetPropertyText(appointment, "OneSpaceStart", appointment.Start.ToString(SystemConsts.DateTimeFormat));
                OutlookUtilities.SetPropertyText(appointment, "OneSpaceEnd", appointment.End.ToString(SystemConsts.DateTimeFormat));

                label_room_details_value_room_name.Text = selected_room.Name;
                label_room_details_value_facilities.Text = the_region.GetFixedAssetList(selected_room.Features);
                
                enableSelectRoom = false;
                UIUtilities.SetButton(butSelectRoom, enableSelectRoom);

                appointment.MeetingStatus = Microsoft.Office.Interop.Outlook.OlMeetingStatus.olMeeting;

                var inspector = Globals.ThisAddIn.Application.ActiveInspector();
                if (MessageBox.Show(Globals.ThisAddIn.resourceManager.GetString("dialogAddServicesText"), Globals.ThisAddIn.resourceManager.GetString("dialogAddServiceCaption"), MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    inspector.SetCurrentFormPage("OneSpace.OutlookPlugin.RoomAssets");
                }
                else {
                    inspector.SetCurrentFormPage("Appointment");
                }
            }
            else {
#if DEBUG
                Logging.ActivityLog.WriteToLog("{nameof(selected_room)} == null, 'Add Room to Meeting' button doing nothing.");
#endif
            }
        }

        public void AddRoomFinishedUI() {

            for (int i = 0; i < bookingRooms.Count; i++) {
                if (bookingRooms[i].FreeSlots.Count == 0) {
                    bookingRooms[i].Status = enum_MapAreaStatus.Booked;
                }
                else {
                    bookingRooms[i].Status = enum_MapAreaStatus.Free;
                }
            }

            scanning = false;
            Cursor.Current = lastCursor;
            IsSearching = false;
            progressScan.Visible = false;
            progressScan.Value = progressScan.Minimum;

            regionBox.Enabled = true;
            locationBox.Enabled = true;
            floorBox.Enabled = true;

            combo_box_for_plus_minus.Enabled = true;
            combo_box_search_capacity.Enabled = true;
            check_box_show_booked_rooms.Enabled = true;
            listFeatures.EnableList();
            listFacilities.EnableList();
            listCatering.EnableList();
            listLayout.EnableList();
            listRoomSearch.Visible = false;

            enableCancel = true;
            UIUtilities.SetButton(butCancel, enableCancel);

            enableFind = true;
            UIUtilities.SetButton(butFind, enableFind);
            butFind.Text = Globals.ThisAddIn.resourceManager.GetString("buttonTextFindRooms");

        }

        public void AddRoomTickUI() {
            this.Refresh();
        }

        private void butCancel_Click(object sender, EventArgs e) {
            listFacilities.ClearItems();
            listCatering.ClearItems();
            listFeatures.ClearItems();
            listLayout.ClearItems();
            regionBox.SelectedIndexChanged -= regionBox_SelectedIndexChanged;
            regionBox.SelectedIndex = 0;
            regionBox.SelectedIndexChanged += regionBox_SelectedIndexChanged;
            SetFloorBoxIndex();
            SetLocationBoxIndex();
            textBoxSearch.Text = string.Empty;
            enableCancel = false;
            UIUtilities.SetButton(butCancel, enableCancel);
            listFeatures.Collapse();
            listFacilities.Collapse();
            listCatering.Collapse();
            listLayout.Collapse();
            check_box_show_booked_rooms.Checked = false;
            combo_box_for_plus_minus.SelectedIndex = 0;
            combo_box_search_capacity.SelectedIndex = 0;
            textBoxSearch.Text = "";
            ArrangeRegions();
            SetupMap();
            schedule.ClearAll();
        }

        private void labelCapacity_Click(object sender, EventArgs e) {

        }

        private void BookMeetingRoom_Click(object sender, EventArgs e) {
            listFeatures.Collapse();
            listFacilities.Collapse();
            listCatering.Collapse();
            listLayout.Collapse();
            //ArrangeRegions();
        }

        //Form layout controls
        private void selectionRegion_ToggleEvent(object sender, EventArgs e) {
            ArrangeRegions();
        }

        private void ArrangeRegions() {
            listFacilities.Top = listFeatures.Top + listFeatures.Height + 13;
            labelServiceAvailiable.Top = listFacilities.Top + listFacilities.Height + 13;
            listCatering.Top = labelServiceAvailiable.Top + labelServiceAvailiable.Height + 13;
            listLayout.Top = listCatering.Top + listCatering.Height + 13;
            butCancel.Top = listLayout.Top + listLayout.Height + 13;
            butFind.Top = listLayout.Top + listLayout.Height + 13;
            progressScan.Top = butCancel.Top + butCancel.Height + 13;
            label_powered_by_onespace.Top = progressScan.Top + 13;
        }

        private void listSlots_SelectedIndexChanged(object sender, EventArgs e) {
            var is_avaliable_slot_selected = false;

            if (listSlots.SelectedItems.Count > 0) {
                var selected_slot = listSlots.SelectedItems[0];

                if (selected_slot.Tag is RoomBooking) {
                    var current_room = schedule.SelectedRoom;
                    schedule.TrySelectBooking(null);
                    schedule.TrySelectRoom(current_room);

                    is_avaliable_slot_selected = true;
                }
                else if (selected_slot.Tag is OneSpace.OutlookPlugin.UI.Data.Booking) {
                    /* This is a booking, update the UI to reflect the selected booking */
                    var booking = selected_slot.Tag as OneSpace.OutlookPlugin.UI.Data.Booking;
                    schedule.TrySelectBooking(booking);
                }

                //var selected_room = SelectedRoom;

                //if(selected_room?.MinLeadTime > 0) {
                //    var book_after = DateTime.Now.AddMinutes(selected_room.MinLeadTime);
                //    var start_date = date_selector_room_schedule.SelectedDate ?? appointment.Start.Date;
                //    var appointment_start = start_date + time_picker_schedule_begins_on.SelectedTime;

                //    if (book_after < appointment_start) {
                //        is_avaliable_slot_selected = false;
                //    }
                //}

                listSlots.FocusedItem = selected_slot;
            }
        }
        
        private void textBoxSearch_TextChanged(object sender, EventArgs e) {
            // textBoxSearch.AutoCompleteMode = AutoCompleteMode.None;
            if (textBoxSearch.Text != string.Empty) {
                var searchRoomList = new List<string>();
                listRoomSearch.Items.Clear();
                searchRoomList.AddRange(
                    from l in Globals.ThisAddIn.OneSpaceData.Regions[SelectedRegion].Locations
                    from f in l.Floors
                    from r in f.Rooms
                    where r.Name.ToLower().Contains(textBoxSearch.Text.ToLower())
                    select r.Name
            );
                /*
                                textBoxSearch.AutoCompleteCustomSource.Clear();
                                textBoxSearch.AutoCompleteCustomSource.AddRange(searchRoomList.ToArray());
                                textBoxSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
                                textBoxSearch.AutoCompleteMode = AutoCompleteMode.Suggest;
                                */

                Logging.ActivityLog.WriteToLog("Text search:" + searchRoomList.Count.ToString());
                foreach (var item in searchRoomList) {
                    listRoomSearch.Visible = true;
                    listRoomSearch.Items.Add(item);
                }
            }
            else {
                listRoomSearch.Visible = false;
            }
        }
        
        private void listRoomSearch_SelectedIndexChanged(object sender, EventArgs e) {
            textBoxSearch.Text = listRoomSearch.Text;
            listRoomSearch.Visible = false;
        }

        private void panelSearch_Click(object sender, EventArgs e) {
            listRoomSearch.Visible = false;
        }

        private void label_meeting_details_value_host_name_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            const string PROP_ORGANISER_ADDRESS = "http://schemas.microsoft.com/mapi/proptag/0x0C1F001F";

            var selected_booking = schedule.SelectedBooking;
            var appointment = selected_booking?.ReferenceItem as Outlook.AppointmentItem;

            var props = appointment.PropertyAccessor.GetProperties(new[] { PROP_ORGANISER_ADDRESS });
            var organiser_address = props[0] as string ?? string.Empty;

            if (appointment != null) {
                var gal = Globals.ThisAddIn.Application.Session.GetGlobalAddressList();
                foreach (Outlook.AddressEntry entry in gal.AddressEntries) {
                    if (entry.Address.Equals(organiser_address, StringComparison.OrdinalIgnoreCase)) {
                        entry.Details(this.Handle);
                        break;
                    }
                }
            }
        }

        private void schedule_SelectedLocationChanged(object sender, EventArgs e) {
            try {
                do_supress_map_location_update = true;

                if (schedule.SelectedRoom != null) {
                    return;
                }

                var selected_location = schedule.SelectedLocation;
                if (selected_location == null) {
                    regionBox.SelectedIndex = 0;
                }
                else {
                    var floor = selected_location.ReferenceItem as Data.Floor;
                    var building = selected_location.ReferenceItem as Data.Location;
                    var region = selected_location.ReferenceItem as Data.Region;

                    if (floor != null) {
                        region = selected_location?.Parent?.Parent?.ReferenceItem as Data.Region;
                        building = selected_location?.Parent?.ReferenceItem as Data.Location;
                    }
                    else if (building != null) {
                        region = selected_location?.Parent?.ReferenceItem as Data.Region;
                    }

                    if (region != null) {
                        regionBox.SelectedItem = region;

                        if (building != null) {
                            locationBox.SelectedItem = building;
                        }

                        if (floor != null) {
                            floorBox.SelectedItem = floor;
                        }
                    }
                    else {
                        //ToDo: Fixed run-time error that was being hidden. Need to check this has not broken something else...
                        if (regionBox.Items.Count > 0)
                        {
                            regionBox.SelectedIndex = 0;
                        }
                    }
                }
            }
            finally {
                do_supress_map_location_update = false;
            }
        }

        private void schedule_SelectedRoomChanged(object sender, EventArgs e) {
            var selected_ui_booking = schedule.SelectedBooking;
            var selected_ui_room = schedule.SelectedRoom;
            var selected_room = SelectedRoom;
            var selected_date = date_selector_room_schedule.SelectedDate ?? DateTime.Today;

            if (selected_ui_booking == null && selected_ui_room != null) {
                var onespace_data = Globals.ThisAddIn.OneSpaceData;
                var region = onespace_data.GetRegionByRoomId(selected_room.Id);
                var building = onespace_data.GetLocationFromRoomById(selected_room.Id);
                var floor = onespace_data.GetFloorFromRoomById(selected_room.Id);

                var restrinctions = string.Empty;

                if (selected_room.MinLeadTime > 0) {
                    restrinctions = string.Format(
                        Globals.ThisAddIn.resourceManager.GetString("roomRestrictionsLabel"),
                        TimeSpan.FromMinutes(selected_room.MinLeadTime).ToFriendlyString()
                    );
                }

                var linked_rooms = string.Empty;

                if(selected_ui_room?.LinkedRooms?.Count > 0) {
                    linked_rooms = string.Join(
                        Environment.NewLine,
                        (
                            from r in selected_ui_room.LinkedRooms
                            select r.Name
                        ).ToArray()
                    );
                }

                label_room_details_value_room_name.Text = selected_room.Name;
                label_room_details_value_restrictions.Text = restrinctions;
                label_room_details_value_location.Text = "{building?.Name} - {floor?.Name}";
                label_room_details_value_description.Text = selected_room.Description;
                label_room_details_value_facilities.Text = region.GetFixedAssetList(selected_room.Features);
                label_room_details_value_capacity.Text = "{selected_room.Capacitiy}";
                label_room_details_value_linked_rooms.Text = linked_rooms;
                listSlots.Items.Clear();

                panel_room_details.Visible = true;
                panel_room_details.BringToFront();
                label_room_details_value_room_name.Visible = true;
                label_room_details_value_restrictions.Visible = !string.IsNullOrWhiteSpace(restrinctions);
                label_room_details_value_location.Visible = true;
                label_room_details_value_description.Visible = true;
                label_room_details_value_facilities.Visible = true;
                label_room_details_value_capacity.Visible = true;
                label_room_details_value_linked_rooms.Visible = !string.IsNullOrWhiteSpace(linked_rooms);
                label_room_details_title_linked_rooms.Visible = label_room_details_value_linked_rooms.Visible;
                date_selector_room_schedule.Visible = true;
                enableSelectRoom = true;
                time_picker_schedule_begins_on.Visible = true;
                time_picker_schedule_ends_on.Visible = true;
            }
            else {
                label_room_details_value_room_name.Text = string.Empty;
                label_room_details_value_restrictions.Text = string.Empty;
                label_room_details_value_location.Text = string.Empty;
                label_room_details_value_description.Text = string.Empty;
                label_room_details_value_facilities.Text = string.Empty;
                label_room_details_value_capacity.Text = string.Empty;
                label_room_details_value_linked_rooms.Text = string.Empty;
                listSlots.Items.Clear();

                label_room_details_value_room_name.Visible = false;
                label_room_details_value_restrictions.Visible = false;
                label_room_details_value_location.Visible = false;
                label_room_details_value_description.Visible = false;
                label_room_details_value_facilities.Visible = false;
                label_room_details_value_capacity.Visible = false;
                label_room_details_value_linked_rooms.Visible = false;
                label_room_details_title_linked_rooms.Visible = false;
                date_selector_room_schedule.Visible = false;
                enableSelectRoom = false;
                time_picker_schedule_begins_on.Visible = false;
                time_picker_schedule_ends_on.Visible = false;
            }

            if(selected_room != null) {


                var slots = (
                   selected_room.Bookings.Union(selected_room.FreeSlots)
                ).ToList();

                //Failed attempt to add buffer times to normal rooms.
                //Linked rooms work and are shown in the slot list at the bottom
                /*
                foreach (var item in selected_ui_room.Bookings)
                {
                    if (item.IsBufferTime)
                    {
                        slots.Add(new RoomBooking
                        {
                            DateFrom = item.BeginsOn.DateTime,
                            DateTo = item.EndsOn.DateTime,
                            IsBuffer = true,
                            Room = selected_room,
                            appointmentItem = (Outlook.AppointmentItem)item.ReferenceItem
                        });
                    }
                }
                */

                slots = slots.OrderBy(s => s.DateFrom).ToList();



                //Condition free slots
                for (int i = 0; i < slots.Count; i++)
                {
                    //First item
                    if (i == 0 && slots[i].IsFree && slots.Count> 1 && !slots[i + 1].IsFree)
                    {
                        slots[i].DateTo = slots[i+1]?.PreBuffer?.Start ?? slots[i].DateTo;
                    }

                    //Middle item
                    if (i > 0 && slots[i].IsFree && 1>slots.Count-1)
                    {
                        if (!slots[i - 1].IsFree)
                        {
                            slots[i].DateFrom = slots[i - 1]?.PostBuffer?.End ?? slots[i].DateFrom;
                        }
                        if (!slots[i + 1].IsFree)
                        {
                            slots[i].DateTo = slots[i + 1]?.PreBuffer?.Start ?? slots[i].DateTo;
                        }
                    }

                    //Last item
                    if (i == slots.Count-1 && slots.Count > 1 && !slots[i-1].IsFree)
                    {
                        slots[i].DateFrom = slots[i-1]?.PostBuffer?.End ?? slots[i].DateFrom;
                    }
                }

                BufferedSlots = slots;
                ThisAddIn.BufferedSlots = slots;

                var bufferColour = System.Drawing.Color.FromArgb(255, 100, 100, 100);

                foreach (var slot in slots)
                {

                    ListViewItem setupItem = null;
                    ListViewItem clearAwayItem = null;

                    if (slot.DateFrom.Date == selected_date) {
                        var title = Globals.ThisAddIn.resourceManager.GetString("slotsTitleFree");
                        var font = UnderScoreFont;
                        var colour = System.Drawing.Color.FromArgb(255, 150, 150, 150);
                        ListViewItem item;

                        if (slot.IsFree == false) {
                            title = slot.Subject;
                            font = this.Font;
                            colour = this.ForeColor;
                            var pre_buffer = slot.PreBuffer;
                            if (pre_buffer != null)
                            {
                                setupItem = new ListViewItem(new[] { "{pre_buffer.Start:HH:mm} - {pre_buffer.End:HH:mm}", Globals.ThisAddIn.resourceManager.GetString("slotsTitleRoomSetup")});
                                setupItem.Name = setupItem.Text;
                            }
                            var post_buffer = slot.PostBuffer;
                            if (post_buffer != null)
                            {
                                clearAwayItem = new ListViewItem(new[] { "{post_buffer.Start:HH:mm} - {post_buffer.End:HH:mm}", Globals.ThisAddIn.resourceManager.GetString("slotsTitleRoomCleanup")});
                                clearAwayItem.Name = clearAwayItem.Text;
                            }
                        }
                        item = new ListViewItem(new[] { "{slot.DateFrom:HH:mm} - {slot.DateTo:HH:mm}", title });

                        var empty_array_json = "[]";
                        var room_booking_linked_rooms_json = OutlookUtilities.GetPropertyText(slot?.appointmentItem, AppointmentSchemaEx.RoomBookingLinkedRooms);
                        room_booking_linked_rooms_json = (string.IsNullOrWhiteSpace(room_booking_linked_rooms_json)) ? empty_array_json : room_booking_linked_rooms_json;
                        var room_booking_linked_rooms = JsonConvert.DeserializeObject<List<int>>(room_booking_linked_rooms_json);

                        try {
                            if (slot.appointmentItem != null) {
                                if (room_booking_linked_rooms.Count == 0 || selected_ui_room.LinkedRooms == null) {
                                    item.Tag = (
                                        from b in selected_ui_room.Bookings
                                        where b.ReferenceItem == slot.appointmentItem
                                        select b
                                    ).FirstOrDefault() ?? (object)slot;
                                }
                                else {
                                    item.Tag = (
                                        from r in selected_ui_room.LinkedRooms
                                        from b in r.Bookings
                                        where b.ReferenceItem == slot.appointmentItem
                                        select b
                                    ).FirstOrDefault() ?? (object)slot;
                                }
                            }
                            else {
                                item.Tag = slot;
                            }

                            if(setupItem != null) { setupItem.Tag = item.Tag; }
                            if(clearAwayItem != null) { clearAwayItem.Tag = item.Tag; }
                        }
                        catch (Exception ex) {
                            Debug.WriteLine(ex.Message);
                        }
                        item.Font = font;
                        item.ForeColor = colour;
                        item.Name = item.Text;


                        //Setup slot
                        if (setupItem!=null)
                        {
                            setupItem.Font = this.Font;
                            setupItem.ForeColor = bufferColour;
                            if (!listSlots.Items.ContainsKey(setupItem.Text))
                            {
                                listSlots.Items.Add(setupItem);
                            }
                        }

                        //Actual meeting slot
                        if (!listSlots.Items.ContainsKey(item.Text))
                        {
                            listSlots.Items.Add(item);
                        }

                        //Clearaway slot
                        if (clearAwayItem!=null)
                        {
                            clearAwayItem.Font = this.Font;
                            clearAwayItem.ForeColor = bufferColour;
                            if (!listSlots.Items.ContainsKey(clearAwayItem.Text))
                            {
                                listSlots.Items.Add(clearAwayItem);
                            }

                        }
                    }
                }

                panel_room_schedule_container.Visible = true;
                labelSelectRoomWarning2.Visible = false;
            }
            else {
                panel_room_schedule_container.Visible = false;
                labelSelectRoomWarning2.Visible = true;
            }

            if (selected_ui_booking == null) {
            }

            label_details_select_room.Visible = (selected_ui_room == null && selected_ui_booking == null);
            panel_room_details.Visible = (selected_ui_room != null && selected_ui_booking == null);
            panel_meeting_details.Visible = (selected_ui_room == null && selected_ui_booking != null);

            SetSelectRoomButton();
        }

        private void schedule_SelectedBookingChanged(object sender, EventArgs e) {
            const string PROP_ORGANISER_NAME = "http://schemas.microsoft.com/mapi/proptag/0x0042001F";
            const string PROP_ORGANISER_SMTP = "http://schemas.microsoft.com/mapi/proptag/0x5D02001F";

            var selected_booking = schedule?.SelectedBooking;
            var selected_room = selected_booking?.Room?.ReferenceItem as Room;
            var appointment = selected_booking?.ReferenceItem as Outlook.AppointmentItem;

            if (selected_booking != null && appointment != null) {

                var props = appointment.PropertyAccessor.GetProperties(new[] {
                        PROP_ORGANISER_NAME,
                        PROP_ORGANISER_SMTP
                });
                var organiser_name = props[0] as string ?? string.Empty;
                var organiser_smtp = props[1] as string ?? string.Empty;

                label_meeting_details_value_room_name.Text = selected_booking.Room.Name;
                label_meeting_details_value_subject.Text = appointment.Subject ?? string.Empty;
                label_meeting_details_value_scheduled.Text = "{appointment.Start:dddd, dd MMMM yyyy}, {appointment.Start:HH:mm} - {appointment.End:HH:mm}";
                label_meeting_details_value_host_name.Text = organiser_name;
                label_meeting_details_value_host_contact.Text = organiser_smtp;
                label_meeting_details_value_capacity.Text = "{selected_room?.Capacitiy}";
                
                label_meeting_details_value_room_name.Visible = true;
                label_meeting_details_value_subject.Visible = true;
                label_meeting_details_value_scheduled.Visible = true;
                label_meeting_details_value_host_name.Visible = true;
                label_meeting_details_value_host_contact.Visible = false;
                label_meeting_details_value_capacity.Visible = true;

                label_details_select_room.Visible = false;
                panel_room_details.Visible = false;
                panel_meeting_details.Visible = true;

                if (selected_booking.IsBufferTime)
                {
                    label_meeting_details_value_subject.Text += Globals.ThisAddIn.resourceManager.GetString("scheduleSetupClearDown");
                }
            }
            else {
                label_meeting_details_value_room_name.Text = string.Empty;
                label_meeting_details_value_subject.Text = string.Empty;
                label_meeting_details_value_scheduled.Text = string.Empty;
                label_meeting_details_value_host_name.Text = string.Empty;
                label_meeting_details_value_host_contact.Text = string.Empty;

                label_meeting_details_value_room_name.Visible = false;
                label_meeting_details_value_subject.Visible = false;
                label_meeting_details_value_scheduled.Visible = false;
                label_meeting_details_value_host_name.Visible = false;
                label_meeting_details_value_host_contact.Visible = false;
                label_meeting_details_value_capacity.Visible = false;

                label_details_select_room.Visible = true;
                panel_room_details.Visible = false;
                panel_meeting_details.Visible = false;
            }
        }

        private void date_selector_room_schedule_SelectedDateChanged(object sender, EventArgs e) {
            schedule_SelectedRoomChanged(sender, e);
        }

        private void time_picker_schedule_begins_on_TimeChanged(object sender, UI.Controls.TimePicker.TimePickerTimeChangeEventArgs e) {
            var time_diff = time_picker_schedule_begins_on.SelectedTime - e.OldTime;
            var new_value = time_picker_schedule_ends_on.SelectedTime + time_diff;

            if (new_value >= TimeSpan.FromDays(1)) {
                new_value = new TimeSpan(23, 59, 0);
            }

            time_picker_schedule_ends_on.SelectedTime = new_value;
            SetSelectRoomButton();
        }

        private void time_picker_schedule_ends_on_ValidateTimeChange(object sender, UI.Controls.TimePicker.TimePickerValidateTimeChangeEventArgs e) {
            if(time_picker_schedule_begins_on.SelectedTime > e.NewTime) {
                e.NewTime = time_picker_schedule_begins_on.SelectedTime;

                if (e.NewTime >= TimeSpan.FromDays(1)) {
                    e.NewTime = new TimeSpan(23, 59, 0);
                }
            }
        }

        private void time_picker_schedule_ends_on_TimeChanged(object sender, UI.Controls.TimePicker.TimePickerTimeChangeEventArgs e) {
            SetSelectRoomButton();
        }

        private void SetSelectRoomButton() {
            var selected_date = date_selector_room_schedule.SelectedDate ?? DateTime.Today;
            DateTimeOffset begins_on = selected_date + time_picker_schedule_begins_on.SelectedTime;
            DateTimeOffset ends_on = selected_date + time_picker_schedule_ends_on.SelectedTime;

            var do_enable_select_room_button = true;
            if(schedule?.SelectedRoom != null) {
                do_enable_select_room_button = schedule.SelectedRoom.IsAvaliable(begins_on, ends_on);
            }

           // enableSelectRoom = do_enable_select_room_button;
            UIUtilities.SetButton(butSelectRoom, true);
        }

        private void combo_box_search_capacity_SelectedValueChanged(object sender, EventArgs e)
        {
            has_user_changed_capacity = true;
        }
    }
}
