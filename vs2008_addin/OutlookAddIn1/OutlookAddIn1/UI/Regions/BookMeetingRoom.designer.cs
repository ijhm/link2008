﻿namespace OneSpace.OutlookPlugin {
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class BookMeetingRoom : Microsoft.Office.Tools.Outlook.FormRegionBase {
        public BookMeetingRoom(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            : base(Globals.Factory, formRegion) {
            this.InitializeComponent();
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookMeetingRoom));
            this.group_search = new System.Windows.Forms.GroupBox();
            this.panel_search_container = new System.Windows.Forms.Panel();
            this.label_powered_by_onespace = new System.Windows.Forms.Label();
            this.listRoomSearch = new System.Windows.Forms.ListBox();
            this.check_box_show_booked_rooms = new System.Windows.Forms.CheckBox();
            this.label_search_capacity = new System.Windows.Forms.Label();
            this.label_search_for_plus_minus = new System.Windows.Forms.Label();
            this.combo_box_search_capacity = new System.Windows.Forms.ComboBox();
            this.combo_box_for_plus_minus = new System.Windows.Forms.ComboBox();
            this.label_appointment_start_end = new System.Windows.Forms.Label();
            this.label_room_size_and_availability = new System.Windows.Forms.Label();
            this.listFeatures = new OneSpace.OutlookPlugin.Controls.SelectionRegion2();
            this.listFacilities = new OneSpace.OutlookPlugin.Controls.SelectionRegion2();
            this.listCatering = new OneSpace.OutlookPlugin.Controls.SelectionRegion2();
            this.listLayout = new OneSpace.OutlookPlugin.Controls.SelectionRegion2();
            this.labelFeaturesAndFacilities = new System.Windows.Forms.Label();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.labelSearchRoomsByName = new System.Windows.Forms.Label();
            this.labelLocation = new System.Windows.Forms.Label();
            this.regionBox = new System.Windows.Forms.ComboBox();
            this.floorBox = new System.Windows.Forms.ComboBox();
            this.locationBox = new System.Windows.Forms.ComboBox();
            this.progressScan = new System.Windows.Forms.ProgressBar();
            this.butCancel = new System.Windows.Forms.Button();
            this.butFind = new System.Windows.Forms.Button();
            this.labelServiceAvailiable = new System.Windows.Forms.Label();
            this.panel_room_details_schedule = new System.Windows.Forms.Panel();
            this.group_room_details = new System.Windows.Forms.GroupBox();
            this.label_details_select_room = new System.Windows.Forms.Label();
            this.panel_room_details = new System.Windows.Forms.TableLayoutPanel();
            this.label_room_details_value_linked_rooms = new System.Windows.Forms.Label();
            this.label_room_details_title_linked_rooms = new System.Windows.Forms.Label();
            this.label_room_details_value_facilities = new System.Windows.Forms.Label();
            this.label_room_details_title_facilities = new System.Windows.Forms.Label();
            this.label_room_details_value_description = new System.Windows.Forms.Label();
            this.label_room_details_title_description = new System.Windows.Forms.Label();
            this.label_room_details_value_location = new System.Windows.Forms.Label();
            this.label_room_details_title_location = new System.Windows.Forms.Label();
            this.label_room_details_value_capacity = new System.Windows.Forms.Label();
            this.label_room_details_title_capacity = new System.Windows.Forms.Label();
            this.label_room_details_value_restrictions = new System.Windows.Forms.Label();
            this.label_room_details_title_room_name = new System.Windows.Forms.Label();
            this.label_room_details_value_room_name = new System.Windows.Forms.Label();
            this.panel_meeting_details = new System.Windows.Forms.TableLayoutPanel();
            this.label_meeting_details_title_room_name = new System.Windows.Forms.Label();
            this.label_meeting_details_title_subject = new System.Windows.Forms.Label();
            this.label_meeting_details_title_scheduled = new System.Windows.Forms.Label();
            this.label_meeting_details_title_capacity = new System.Windows.Forms.Label();
            this.label_meeting_details_title_host_organiser = new System.Windows.Forms.Label();
            this.label_meeting_details_value_room_name = new System.Windows.Forms.Label();
            this.label_meeting_details_value_subject = new System.Windows.Forms.Label();
            this.label_meeting_details_value_capacity = new System.Windows.Forms.Label();
            this.label_meeting_details_value_scheduled = new System.Windows.Forms.Label();
            this.label_meeting_details_value_host_contact = new System.Windows.Forms.Label();
            this.label_meeting_details_value_host_name = new System.Windows.Forms.LinkLabel();
            this.group_room_schedule = new System.Windows.Forms.GroupBox();
            this.panel_room_schedule_container = new System.Windows.Forms.Panel();
            this.time_picker_schedule_ends_on = new OneSpace.OutlookPlugin.UI.Controls.TimePicker();
            this.time_picker_schedule_begins_on = new OneSpace.OutlookPlugin.UI.Controls.TimePicker();
            this.date_selector_room_schedule = new OneSpace.OutlookPlugin.UI.Controls.DateSelector();
            this.labelSelectRoomWarning2 = new System.Windows.Forms.Label();
            this.label_schedule_instructions = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.listSlots = new System.Windows.Forms.ListView();
            this.slotTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.slotState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelEnd = new System.Windows.Forms.Label();
            this.labelStart = new System.Windows.Forms.Label();
            this.butSelectRoom = new System.Windows.Forms.Button();
            this.schedule = new OneSpace.OutlookPlugin.UI.Controls.Schedule();
            this.group_search.SuspendLayout();
            this.panel_search_container.SuspendLayout();
            this.panel_room_details_schedule.SuspendLayout();
            this.group_room_details.SuspendLayout();
            this.panel_room_details.SuspendLayout();
            this.panel_meeting_details.SuspendLayout();
            this.group_room_schedule.SuspendLayout();
            this.panel_room_schedule_container.SuspendLayout();
            this.SuspendLayout();
            // 
            // group_search
            // 
            this.group_search.Controls.Add(this.panel_search_container);
            resources.ApplyResources(this.group_search, "group_search");
            this.group_search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.group_search.Name = "group_search";
            this.group_search.TabStop = false;
            // 
            // panel_search_container
            // 
            resources.ApplyResources(this.panel_search_container, "panel_search_container");
            this.panel_search_container.Controls.Add(this.label_powered_by_onespace);
            this.panel_search_container.Controls.Add(this.listRoomSearch);
            this.panel_search_container.Controls.Add(this.check_box_show_booked_rooms);
            this.panel_search_container.Controls.Add(this.label_search_capacity);
            this.panel_search_container.Controls.Add(this.label_search_for_plus_minus);
            this.panel_search_container.Controls.Add(this.combo_box_search_capacity);
            this.panel_search_container.Controls.Add(this.combo_box_for_plus_minus);
            this.panel_search_container.Controls.Add(this.label_appointment_start_end);
            this.panel_search_container.Controls.Add(this.label_room_size_and_availability);
            this.panel_search_container.Controls.Add(this.listFeatures);
            this.panel_search_container.Controls.Add(this.listFacilities);
            this.panel_search_container.Controls.Add(this.listCatering);
            this.panel_search_container.Controls.Add(this.listLayout);
            this.panel_search_container.Controls.Add(this.labelFeaturesAndFacilities);
            this.panel_search_container.Controls.Add(this.textBoxSearch);
            this.panel_search_container.Controls.Add(this.labelSearchRoomsByName);
            this.panel_search_container.Controls.Add(this.labelLocation);
            this.panel_search_container.Controls.Add(this.regionBox);
            this.panel_search_container.Controls.Add(this.floorBox);
            this.panel_search_container.Controls.Add(this.locationBox);
            this.panel_search_container.Controls.Add(this.progressScan);
            this.panel_search_container.Controls.Add(this.butCancel);
            this.panel_search_container.Controls.Add(this.butFind);
            this.panel_search_container.Controls.Add(this.labelServiceAvailiable);
            this.panel_search_container.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel_search_container.Name = "panel_search_container";
            // 
            // label_powered_by_onespace
            // 
            resources.ApplyResources(this.label_powered_by_onespace, "label_powered_by_onespace");
            this.label_powered_by_onespace.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.label_powered_by_onespace.Name = "label_powered_by_onespace";
            // 
            // listRoomSearch
            // 
            this.listRoomSearch.FormattingEnabled = true;
            resources.ApplyResources(this.listRoomSearch, "listRoomSearch");
            this.listRoomSearch.Name = "listRoomSearch";
            this.listRoomSearch.SelectedIndexChanged += new System.EventHandler(this.listRoomSearch_SelectedIndexChanged);
            // 
            // check_box_show_booked_rooms
            // 
            resources.ApplyResources(this.check_box_show_booked_rooms, "check_box_show_booked_rooms");
            this.check_box_show_booked_rooms.Name = "check_box_show_booked_rooms";
            this.check_box_show_booked_rooms.UseVisualStyleBackColor = true;
            // 
            // label_search_capacity
            // 
            resources.ApplyResources(this.label_search_capacity, "label_search_capacity");
            this.label_search_capacity.Name = "label_search_capacity";
            // 
            // label_search_for_plus_minus
            // 
            resources.ApplyResources(this.label_search_for_plus_minus, "label_search_for_plus_minus");
            this.label_search_for_plus_minus.Name = "label_search_for_plus_minus";
            // 
            // combo_box_search_capacity
            // 
            this.combo_box_search_capacity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_box_search_capacity.FormattingEnabled = true;
            resources.ApplyResources(this.combo_box_search_capacity, "combo_box_search_capacity");
            this.combo_box_search_capacity.Name = "combo_box_search_capacity";
            this.combo_box_search_capacity.SelectedValueChanged += new System.EventHandler(this.combo_box_search_capacity_SelectedValueChanged);
            // 
            // combo_box_for_plus_minus
            // 
            this.combo_box_for_plus_minus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_box_for_plus_minus.FormattingEnabled = true;
            resources.ApplyResources(this.combo_box_for_plus_minus, "combo_box_for_plus_minus");
            this.combo_box_for_plus_minus.Name = "combo_box_for_plus_minus";
            // 
            // label_appointment_start_end
            // 
            resources.ApplyResources(this.label_appointment_start_end, "label_appointment_start_end");
            this.label_appointment_start_end.Name = "label_appointment_start_end";
            // 
            // label_room_size_and_availability
            // 
            resources.ApplyResources(this.label_room_size_and_availability, "label_room_size_and_availability");
            this.label_room_size_and_availability.Name = "label_room_size_and_availability";
            // 
            // listFeatures
            // 
            this.listFeatures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.listFeatures, "listFeatures");
            this.listFeatures.Name = "listFeatures";
            this.listFeatures.ToggleEvent += new System.EventHandler(this.selectionRegion_ToggleEvent);
            // 
            // listFacilities
            // 
            this.listFacilities.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.listFacilities, "listFacilities");
            this.listFacilities.Name = "listFacilities";
            this.listFacilities.ToggleEvent += new System.EventHandler(this.selectionRegion_ToggleEvent);
            // 
            // listCatering
            // 
            this.listCatering.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.listCatering, "listCatering");
            this.listCatering.Name = "listCatering";
            this.listCatering.ToggleEvent += new System.EventHandler(this.selectionRegion_ToggleEvent);
            // 
            // listLayout
            // 
            this.listLayout.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.listLayout, "listLayout");
            this.listLayout.Name = "listLayout";
            this.listLayout.ToggleEvent += new System.EventHandler(this.selectionRegion_ToggleEvent);
            // 
            // labelFeaturesAndFacilities
            // 
            resources.ApplyResources(this.labelFeaturesAndFacilities, "labelFeaturesAndFacilities");
            this.labelFeaturesAndFacilities.Name = "labelFeaturesAndFacilities";
            // 
            // textBoxSearch
            // 
            resources.ApplyResources(this.textBoxSearch, "textBoxSearch");
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.TextChanged += new System.EventHandler(this.textBoxSearch_TextChanged);
            // 
            // labelSearchRoomsByName
            // 
            resources.ApplyResources(this.labelSearchRoomsByName, "labelSearchRoomsByName");
            this.labelSearchRoomsByName.Name = "labelSearchRoomsByName";
            // 
            // labelLocation
            // 
            resources.ApplyResources(this.labelLocation, "labelLocation");
            this.labelLocation.Name = "labelLocation";
            // 
            // regionBox
            // 
            this.regionBox.BackColor = System.Drawing.Color.White;
            this.regionBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.regionBox.FormattingEnabled = true;
            resources.ApplyResources(this.regionBox, "regionBox");
            this.regionBox.Name = "regionBox";
            this.regionBox.SelectedIndexChanged += new System.EventHandler(this.regionBox_SelectedIndexChanged);
            // 
            // floorBox
            // 
            this.floorBox.BackColor = System.Drawing.Color.White;
            this.floorBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.floorBox.FormattingEnabled = true;
            resources.ApplyResources(this.floorBox, "floorBox");
            this.floorBox.Name = "floorBox";
            this.floorBox.SelectedIndexChanged += new System.EventHandler(this.floorBox_SelectedIndexChanged);
            // 
            // locationBox
            // 
            this.locationBox.BackColor = System.Drawing.Color.White;
            this.locationBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.locationBox.FormattingEnabled = true;
            resources.ApplyResources(this.locationBox, "locationBox");
            this.locationBox.Name = "locationBox";
            this.locationBox.SelectedIndexChanged += new System.EventHandler(this.locationBox_SelectedIndexChanged);
            // 
            // progressScan
            // 
            resources.ApplyResources(this.progressScan, "progressScan");
            this.progressScan.Maximum = 10;
            this.progressScan.Name = "progressScan";
            // 
            // butCancel
            // 
            this.butCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            resources.ApplyResources(this.butCancel, "butCancel");
            this.butCancel.ForeColor = System.Drawing.Color.White;
            this.butCancel.Name = "butCancel";
            this.butCancel.UseVisualStyleBackColor = false;
            this.butCancel.Click += new System.EventHandler(this.butCancel_Click);
            // 
            // butFind
            // 
            this.butFind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(51)))), ((int)(((byte)(41)))));
            resources.ApplyResources(this.butFind, "butFind");
            this.butFind.ForeColor = System.Drawing.Color.White;
            this.butFind.Name = "butFind";
            this.butFind.Tag = "main";
            this.butFind.UseVisualStyleBackColor = false;
            this.butFind.Click += new System.EventHandler(this.butFind_Click);
            // 
            // labelServiceAvailiable
            // 
            resources.ApplyResources(this.labelServiceAvailiable, "labelServiceAvailiable");
            this.labelServiceAvailiable.Name = "labelServiceAvailiable";
            // 
            // panel_room_details_schedule
            // 
            this.panel_room_details_schedule.Controls.Add(this.group_room_details);
            this.panel_room_details_schedule.Controls.Add(this.group_room_schedule);
            resources.ApplyResources(this.panel_room_details_schedule, "panel_room_details_schedule");
            this.panel_room_details_schedule.Name = "panel_room_details_schedule";
            // 
            // group_room_details
            // 
            this.group_room_details.Controls.Add(this.label_details_select_room);
            this.group_room_details.Controls.Add(this.panel_room_details);
            this.group_room_details.Controls.Add(this.panel_meeting_details);
            resources.ApplyResources(this.group_room_details, "group_room_details");
            this.group_room_details.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.group_room_details.Name = "group_room_details";
            this.group_room_details.TabStop = false;
            // 
            // label_details_select_room
            // 
            resources.ApplyResources(this.label_details_select_room, "label_details_select_room");
            this.label_details_select_room.ForeColor = System.Drawing.Color.Black;
            this.label_details_select_room.Name = "label_details_select_room";
            // 
            // panel_room_details
            // 
            resources.ApplyResources(this.panel_room_details, "panel_room_details");
            this.panel_room_details.Controls.Add(this.label_room_details_value_linked_rooms, 1, 6);
            this.panel_room_details.Controls.Add(this.label_room_details_title_linked_rooms, 0, 6);
            this.panel_room_details.Controls.Add(this.label_room_details_value_facilities, 1, 5);
            this.panel_room_details.Controls.Add(this.label_room_details_title_facilities, 0, 5);
            this.panel_room_details.Controls.Add(this.label_room_details_value_description, 1, 4);
            this.panel_room_details.Controls.Add(this.label_room_details_title_description, 0, 4);
            this.panel_room_details.Controls.Add(this.label_room_details_value_location, 1, 3);
            this.panel_room_details.Controls.Add(this.label_room_details_title_location, 0, 3);
            this.panel_room_details.Controls.Add(this.label_room_details_value_capacity, 1, 2);
            this.panel_room_details.Controls.Add(this.label_room_details_title_capacity, 0, 2);
            this.panel_room_details.Controls.Add(this.label_room_details_value_restrictions, 0, 1);
            this.panel_room_details.Controls.Add(this.label_room_details_title_room_name, 0, 0);
            this.panel_room_details.Controls.Add(this.label_room_details_value_room_name, 1, 0);
            this.panel_room_details.ForeColor = System.Drawing.Color.Black;
            this.panel_room_details.Name = "panel_room_details";
            // 
            // label_room_details_value_linked_rooms
            // 
            resources.ApplyResources(this.label_room_details_value_linked_rooms, "label_room_details_value_linked_rooms");
            this.label_room_details_value_linked_rooms.Name = "label_room_details_value_linked_rooms";
            // 
            // label_room_details_title_linked_rooms
            // 
            resources.ApplyResources(this.label_room_details_title_linked_rooms, "label_room_details_title_linked_rooms");
            this.label_room_details_title_linked_rooms.Name = "label_room_details_title_linked_rooms";
            // 
            // label_room_details_value_facilities
            // 
            resources.ApplyResources(this.label_room_details_value_facilities, "label_room_details_value_facilities");
            this.label_room_details_value_facilities.Name = "label_room_details_value_facilities";
            // 
            // label_room_details_title_facilities
            // 
            resources.ApplyResources(this.label_room_details_title_facilities, "label_room_details_title_facilities");
            this.label_room_details_title_facilities.Name = "label_room_details_title_facilities";
            // 
            // label_room_details_value_description
            // 
            resources.ApplyResources(this.label_room_details_value_description, "label_room_details_value_description");
            this.label_room_details_value_description.Name = "label_room_details_value_description";
            // 
            // label_room_details_title_description
            // 
            resources.ApplyResources(this.label_room_details_title_description, "label_room_details_title_description");
            this.label_room_details_title_description.Name = "label_room_details_title_description";
            // 
            // label_room_details_value_location
            // 
            resources.ApplyResources(this.label_room_details_value_location, "label_room_details_value_location");
            this.label_room_details_value_location.Name = "label_room_details_value_location";
            // 
            // label_room_details_title_location
            // 
            resources.ApplyResources(this.label_room_details_title_location, "label_room_details_title_location");
            this.label_room_details_title_location.Name = "label_room_details_title_location";
            // 
            // label_room_details_value_capacity
            // 
            resources.ApplyResources(this.label_room_details_value_capacity, "label_room_details_value_capacity");
            this.label_room_details_value_capacity.Name = "label_room_details_value_capacity";
            // 
            // label_room_details_title_capacity
            // 
            resources.ApplyResources(this.label_room_details_title_capacity, "label_room_details_title_capacity");
            this.label_room_details_title_capacity.Name = "label_room_details_title_capacity";
            // 
            // label_room_details_value_restrictions
            // 
            resources.ApplyResources(this.label_room_details_value_restrictions, "label_room_details_value_restrictions");
            this.panel_room_details.SetColumnSpan(this.label_room_details_value_restrictions, 2);
            this.label_room_details_value_restrictions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label_room_details_value_restrictions.Name = "label_room_details_value_restrictions";
            // 
            // label_room_details_title_room_name
            // 
            resources.ApplyResources(this.label_room_details_title_room_name, "label_room_details_title_room_name");
            this.label_room_details_title_room_name.Name = "label_room_details_title_room_name";
            // 
            // label_room_details_value_room_name
            // 
            resources.ApplyResources(this.label_room_details_value_room_name, "label_room_details_value_room_name");
            this.label_room_details_value_room_name.Name = "label_room_details_value_room_name";
            // 
            // panel_meeting_details
            // 
            resources.ApplyResources(this.panel_meeting_details, "panel_meeting_details");
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_title_room_name, 0, 0);
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_title_subject, 0, 1);
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_title_scheduled, 0, 3);
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_title_capacity, 0, 2);
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_title_host_organiser, 0, 4);
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_value_room_name, 1, 0);
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_value_subject, 1, 1);
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_value_capacity, 1, 2);
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_value_scheduled, 1, 3);
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_value_host_contact, 1, 5);
            this.panel_meeting_details.Controls.Add(this.label_meeting_details_value_host_name, 1, 4);
            this.panel_meeting_details.ForeColor = System.Drawing.Color.Black;
            this.panel_meeting_details.Name = "panel_meeting_details";
            // 
            // label_meeting_details_title_room_name
            // 
            resources.ApplyResources(this.label_meeting_details_title_room_name, "label_meeting_details_title_room_name");
            this.label_meeting_details_title_room_name.Name = "label_meeting_details_title_room_name";
            // 
            // label_meeting_details_title_subject
            // 
            resources.ApplyResources(this.label_meeting_details_title_subject, "label_meeting_details_title_subject");
            this.label_meeting_details_title_subject.Name = "label_meeting_details_title_subject";
            // 
            // label_meeting_details_title_scheduled
            // 
            resources.ApplyResources(this.label_meeting_details_title_scheduled, "label_meeting_details_title_scheduled");
            this.label_meeting_details_title_scheduled.Name = "label_meeting_details_title_scheduled";
            // 
            // label_meeting_details_title_capacity
            // 
            resources.ApplyResources(this.label_meeting_details_title_capacity, "label_meeting_details_title_capacity");
            this.label_meeting_details_title_capacity.Name = "label_meeting_details_title_capacity";
            // 
            // label_meeting_details_title_host_organiser
            // 
            resources.ApplyResources(this.label_meeting_details_title_host_organiser, "label_meeting_details_title_host_organiser");
            this.label_meeting_details_title_host_organiser.Name = "label_meeting_details_title_host_organiser";
            // 
            // label_meeting_details_value_room_name
            // 
            resources.ApplyResources(this.label_meeting_details_value_room_name, "label_meeting_details_value_room_name");
            this.label_meeting_details_value_room_name.Name = "label_meeting_details_value_room_name";
            // 
            // label_meeting_details_value_subject
            // 
            resources.ApplyResources(this.label_meeting_details_value_subject, "label_meeting_details_value_subject");
            this.label_meeting_details_value_subject.Name = "label_meeting_details_value_subject";
            // 
            // label_meeting_details_value_capacity
            // 
            resources.ApplyResources(this.label_meeting_details_value_capacity, "label_meeting_details_value_capacity");
            this.label_meeting_details_value_capacity.Name = "label_meeting_details_value_capacity";
            // 
            // label_meeting_details_value_scheduled
            // 
            resources.ApplyResources(this.label_meeting_details_value_scheduled, "label_meeting_details_value_scheduled");
            this.label_meeting_details_value_scheduled.Name = "label_meeting_details_value_scheduled";
            // 
            // label_meeting_details_value_host_contact
            // 
            resources.ApplyResources(this.label_meeting_details_value_host_contact, "label_meeting_details_value_host_contact");
            this.label_meeting_details_value_host_contact.Name = "label_meeting_details_value_host_contact";
            // 
            // label_meeting_details_value_host_name
            // 
            resources.ApplyResources(this.label_meeting_details_value_host_name, "label_meeting_details_value_host_name");
            this.label_meeting_details_value_host_name.Name = "label_meeting_details_value_host_name";
            this.label_meeting_details_value_host_name.TabStop = true;
            this.label_meeting_details_value_host_name.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.label_meeting_details_value_host_name_LinkClicked);
            // 
            // group_room_schedule
            // 
            this.group_room_schedule.Controls.Add(this.panel_room_schedule_container);
            resources.ApplyResources(this.group_room_schedule, "group_room_schedule");
            this.group_room_schedule.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(25)))), ((int)(((byte)(43)))));
            this.group_room_schedule.Name = "group_room_schedule";
            this.group_room_schedule.TabStop = false;
            // 
            // panel_room_schedule_container
            // 
            this.panel_room_schedule_container.Controls.Add(this.time_picker_schedule_ends_on);
            this.panel_room_schedule_container.Controls.Add(this.time_picker_schedule_begins_on);
            this.panel_room_schedule_container.Controls.Add(this.date_selector_room_schedule);
            this.panel_room_schedule_container.Controls.Add(this.labelSelectRoomWarning2);
            this.panel_room_schedule_container.Controls.Add(this.label_schedule_instructions);
            this.panel_room_schedule_container.Controls.Add(this.labelDate);
            this.panel_room_schedule_container.Controls.Add(this.listSlots);
            this.panel_room_schedule_container.Controls.Add(this.labelEnd);
            this.panel_room_schedule_container.Controls.Add(this.labelStart);
            this.panel_room_schedule_container.Controls.Add(this.butSelectRoom);
            resources.ApplyResources(this.panel_room_schedule_container, "panel_room_schedule_container");
            this.panel_room_schedule_container.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel_room_schedule_container.Name = "panel_room_schedule_container";
            // 
            // time_picker_schedule_ends_on
            // 
            resources.ApplyResources(this.time_picker_schedule_ends_on, "time_picker_schedule_ends_on");
            this.time_picker_schedule_ends_on.Name = "time_picker_schedule_ends_on";
            this.time_picker_schedule_ends_on.SelectedTime = System.TimeSpan.Parse("00:00:00");
            this.time_picker_schedule_ends_on.ValidateTimeChange += new System.EventHandler<OneSpace.OutlookPlugin.UI.Controls.TimePicker.TimePickerValidateTimeChangeEventArgs>(this.time_picker_schedule_ends_on_ValidateTimeChange);
            this.time_picker_schedule_ends_on.TimeChanged += new System.EventHandler<OneSpace.OutlookPlugin.UI.Controls.TimePicker.TimePickerTimeChangeEventArgs>(this.time_picker_schedule_ends_on_TimeChanged);
            // 
            // time_picker_schedule_begins_on
            // 
            resources.ApplyResources(this.time_picker_schedule_begins_on, "time_picker_schedule_begins_on");
            this.time_picker_schedule_begins_on.Name = "time_picker_schedule_begins_on";
            this.time_picker_schedule_begins_on.SelectedTime = System.TimeSpan.Parse("00:00:00");
            this.time_picker_schedule_begins_on.TimeChanged += new System.EventHandler<OneSpace.OutlookPlugin.UI.Controls.TimePicker.TimePickerTimeChangeEventArgs>(this.time_picker_schedule_begins_on_TimeChanged);
            // 
            // date_selector_room_schedule
            // 
            resources.ApplyResources(this.date_selector_room_schedule, "date_selector_room_schedule");
            this.date_selector_room_schedule.Name = "date_selector_room_schedule";
            this.date_selector_room_schedule.SelectedDateChanged += new System.EventHandler<System.EventArgs>(this.date_selector_room_schedule_SelectedDateChanged);
            // 
            // labelSelectRoomWarning2
            // 
            resources.ApplyResources(this.labelSelectRoomWarning2, "labelSelectRoomWarning2");
            this.labelSelectRoomWarning2.Name = "labelSelectRoomWarning2";
            // 
            // label_schedule_instructions
            // 
            resources.ApplyResources(this.label_schedule_instructions, "label_schedule_instructions");
            this.label_schedule_instructions.Name = "label_schedule_instructions";
            // 
            // labelDate
            // 
            resources.ApplyResources(this.labelDate, "labelDate");
            this.labelDate.Name = "labelDate";
            // 
            // listSlots
            // 
            resources.ApplyResources(this.listSlots, "listSlots");
            this.listSlots.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.slotTime,
            this.slotState});
            this.listSlots.FullRowSelect = true;
            this.listSlots.GridLines = true;
            this.listSlots.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listSlots.HideSelection = false;
            this.listSlots.MultiSelect = false;
            this.listSlots.Name = "listSlots";
            this.listSlots.UseCompatibleStateImageBehavior = false;
            this.listSlots.View = System.Windows.Forms.View.Details;
            this.listSlots.SelectedIndexChanged += new System.EventHandler(this.listSlots_SelectedIndexChanged);
            // 
            // slotTime
            // 
            resources.ApplyResources(this.slotTime, "slotTime");
            // 
            // labelEnd
            // 
            resources.ApplyResources(this.labelEnd, "labelEnd");
            this.labelEnd.Name = "labelEnd";
            // 
            // labelStart
            // 
            resources.ApplyResources(this.labelStart, "labelStart");
            this.labelStart.Name = "labelStart";
            // 
            // butSelectRoom
            // 
            resources.ApplyResources(this.butSelectRoom, "butSelectRoom");
            this.butSelectRoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            this.butSelectRoom.ForeColor = System.Drawing.Color.White;
            this.butSelectRoom.Name = "butSelectRoom";
            this.butSelectRoom.Tag = "main";
            this.butSelectRoom.UseVisualStyleBackColor = false;
            this.butSelectRoom.Click += new System.EventHandler(this.butSelectRoom_Click);
            // 
            // schedule
            // 
            resources.ApplyResources(this.schedule, "schedule");
            this.schedule.BackColor = System.Drawing.Color.White;
            this.schedule.Name = "schedule";
            this.schedule.SelectedLocationChanged += new System.EventHandler<System.EventArgs>(this.schedule_SelectedLocationChanged);
            this.schedule.SelectedRoomChanged += new System.EventHandler<System.EventArgs>(this.schedule_SelectedRoomChanged);
            this.schedule.SelectedBookingChanged += new System.EventHandler<System.EventArgs>(this.schedule_SelectedBookingChanged);
            // 
            // BookMeetingRoom
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.schedule);
            this.Controls.Add(this.panel_room_details_schedule);
            this.Controls.Add(this.group_search);
            this.Name = "BookMeetingRoom";
            this.FormRegionShowing += new System.EventHandler(this.BookMeetingRoom_FormRegionShowing);
            this.FormRegionClosed += new System.EventHandler(this.BookMeetingRoom_FormRegionClosed);
            this.Click += new System.EventHandler(this.BookMeetingRoom_Click);
            this.group_search.ResumeLayout(false);
            this.panel_search_container.ResumeLayout(false);
            this.panel_search_container.PerformLayout();
            this.panel_room_details_schedule.ResumeLayout(false);
            this.group_room_details.ResumeLayout(false);
            this.panel_room_details.ResumeLayout(false);
            this.panel_room_details.PerformLayout();
            this.panel_meeting_details.ResumeLayout(false);
            this.panel_meeting_details.PerformLayout();
            this.group_room_schedule.ResumeLayout(false);
            this.panel_room_schedule_container.ResumeLayout(false);
            this.panel_room_schedule_container.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Form Region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private static void InitializeManifest(Microsoft.Office.Tools.Outlook.FormRegionManifest manifest, Microsoft.Office.Tools.Outlook.Factory factory) {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookMeetingRoom));
            manifest.FormRegionName = Branding.Resource.RoomsRegionName;
//            manifest.Icons.Page = global::OneSpace.OutlookPlugin.Resource1.rooms;
            manifest.Icons.Page = Branding.Resource.RoomsImage;
            manifest.ShowReadingPane = false;

        }

        #endregion

        private System.Windows.Forms.GroupBox group_search;
        private System.Windows.Forms.Panel panel_room_details_schedule;
        private System.Windows.Forms.GroupBox group_room_details;
        private System.Windows.Forms.GroupBox group_room_schedule;
        private System.Windows.Forms.Panel panel_search_container;
        private System.Windows.Forms.Panel panel_room_schedule_container;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.ListView listSlots;
        private System.Windows.Forms.ColumnHeader slotTime;
        private System.Windows.Forms.ColumnHeader slotState;
        private System.Windows.Forms.Label labelEnd;
        private System.Windows.Forms.Label labelStart;
        private System.Windows.Forms.Button butSelectRoom;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.ComboBox regionBox;
        private System.Windows.Forms.ComboBox floorBox;
        private System.Windows.Forms.ComboBox locationBox;
        private System.Windows.Forms.Label labelSearchRoomsByName;
        private System.Windows.Forms.TextBox textBoxSearch;
        private Controls.SelectionRegion2 listFacilities;
        private Controls.SelectionRegion2 listFeatures;
        private System.Windows.Forms.Label labelFeaturesAndFacilities;
        private System.Windows.Forms.Label labelServiceAvailiable;
        private Controls.SelectionRegion2 listLayout;
        private Controls.SelectionRegion2 listCatering;
        private System.Windows.Forms.Button butCancel;
        private System.Windows.Forms.Button butFind;
        private System.Windows.Forms.ProgressBar progressScan;
        private System.Windows.Forms.ListBox listRoomSearch;
        private UI.Controls.Schedule schedule;
        private System.Windows.Forms.Label label_appointment_start_end;
        private System.Windows.Forms.Label label_room_size_and_availability;
        private System.Windows.Forms.ComboBox combo_box_search_capacity;
        private System.Windows.Forms.ComboBox combo_box_for_plus_minus;
        private System.Windows.Forms.CheckBox check_box_show_booked_rooms;
        private System.Windows.Forms.Label label_search_capacity;
        private System.Windows.Forms.Label label_search_for_plus_minus;
        private System.Windows.Forms.Label label_room_details_value_restrictions;
        private System.Windows.Forms.Label label_room_details_title_capacity;
        private System.Windows.Forms.Label label_room_details_value_capacity;
        private System.Windows.Forms.Label label_room_details_title_description;
        private System.Windows.Forms.Label label_room_details_value_description;
        private System.Windows.Forms.Label label_room_details_title_facilities;
        private System.Windows.Forms.Label label_room_details_value_facilities;
        private System.Windows.Forms.Label label_room_details_title_linked_rooms;
        private System.Windows.Forms.Label label_room_details_value_linked_rooms;
        private System.Windows.Forms.Label label_room_details_title_location;
        private System.Windows.Forms.Label label_room_details_value_location;
        private System.Windows.Forms.TableLayoutPanel panel_room_details;
        private System.Windows.Forms.Label label_room_details_title_room_name;
        private System.Windows.Forms.Label label_room_details_value_room_name;
        private System.Windows.Forms.TableLayoutPanel panel_meeting_details;
        private System.Windows.Forms.Label label_meeting_details_title_room_name;
        private System.Windows.Forms.Label label_meeting_details_title_subject;
        private System.Windows.Forms.Label label_meeting_details_title_scheduled;
        private System.Windows.Forms.Label label_meeting_details_title_capacity;
        private System.Windows.Forms.Label label_meeting_details_title_host_organiser;
        private System.Windows.Forms.Label label_meeting_details_value_room_name;
        private System.Windows.Forms.Label label_meeting_details_value_subject;
        private System.Windows.Forms.Label label_meeting_details_value_capacity;
        private System.Windows.Forms.Label label_meeting_details_value_scheduled;
        private System.Windows.Forms.Label label_meeting_details_value_host_contact;
        private System.Windows.Forms.LinkLabel label_meeting_details_value_host_name;
        private System.Windows.Forms.Label label_details_select_room;
        private System.Windows.Forms.Label label_schedule_instructions;
        private System.Windows.Forms.Label labelSelectRoomWarning2;
        private UI.Controls.DateSelector date_selector_room_schedule;
        private UI.Controls.TimePicker time_picker_schedule_ends_on;
        private UI.Controls.TimePicker time_picker_schedule_begins_on;
        private System.Windows.Forms.Label label_powered_by_onespace;

        public partial class BookMeetingRoomFactory : Microsoft.Office.Tools.Outlook.IFormRegionFactory {
            public event Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler FormRegionInitializing;

            private Microsoft.Office.Tools.Outlook.FormRegionManifest _Manifest;

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public BookMeetingRoomFactory() {
                this._Manifest = Globals.Factory.CreateFormRegionManifest();
                BookMeetingRoom.InitializeManifest(this._Manifest, Globals.Factory);
                this.FormRegionInitializing += new Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler(this.BookMeetingRoomFactory_FormRegionInitializing);
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public Microsoft.Office.Tools.Outlook.FormRegionManifest Manifest {
                get {
                    return this._Manifest;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.IFormRegion Microsoft.Office.Tools.Outlook.IFormRegionFactory.CreateFormRegion(Microsoft.Office.Interop.Outlook.FormRegion formRegion) {
                BookMeetingRoom form = new BookMeetingRoom(formRegion);
                form.Factory = this;
                return form;
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            byte[] Microsoft.Office.Tools.Outlook.IFormRegionFactory.GetFormRegionStorage(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize) {
                throw new System.NotSupportedException();
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            bool Microsoft.Office.Tools.Outlook.IFormRegionFactory.IsDisplayedForItem(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize) {
                if (this.FormRegionInitializing != null) {
                    Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs cancelArgs = Globals.Factory.CreateFormRegionInitializingEventArgs(outlookItem, formRegionMode, formRegionSize, false);
                    this.FormRegionInitializing(this, cancelArgs);
                    return !cancelArgs.Cancel;
                }
                else {
                    return true;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.FormRegionKindConstants Microsoft.Office.Tools.Outlook.IFormRegionFactory.Kind {
                get {
                    return Microsoft.Office.Tools.Outlook.FormRegionKindConstants.WindowsForms;
                }
            }
        }
    }

    partial class WindowFormRegionCollection {
        internal BookMeetingRoom BookMeetingRoom {
            get {
                foreach (var item in this) {
                    if (item.GetType() == typeof(BookMeetingRoom))
                        return (BookMeetingRoom)item;
                }
                return null;
            }
        }
    }
}
