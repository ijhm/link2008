﻿using OneSpace.OutlookPlugin.Data;
using OneSpace.OutlookPlugin.Data.Booking;
using OneSpace.OutlookPlugin.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OneSpace.OutlookPlugin.Properties;
using Office = Microsoft.Office.Core;
using Outlook = Microsoft.Office.Interop.Outlook;
using OneSpace.OutlookPlugin.UI.Forms;
using OneSpace.OutlookPlugin.Extentions;

namespace OneSpace.OutlookPlugin
{
    partial class RoomAssets
    {
        

        Outlook.AppointmentItem appointment;
        List<OrderItemDisplay> Items;
        BookingItem ItemToAdd;
        AppointmentData order = null;

        private enum enum_Mode
        {
            Nothing,
            AddItem,
            UpdateItem
        }
        private enum_Mode Mode = enum_Mode.Nothing;

        #region Form Region Factory 

        [Microsoft.Office.Tools.Outlook.FormRegionMessageClass(Microsoft.Office.Tools.Outlook.FormRegionMessageClassAttribute.Appointment)]
        [Microsoft.Office.Tools.Outlook.FormRegionName("OneSpace.OutlookPlugin.RoomAssets")]
        public partial class RoomAssetsFactory
        {
            // Occurs before the form region is initialized.
            // To prevent the form region from appearing, set e.Cancel to true.
            // Use e.OutlookItem to get a reference to the current Outlook item.
            private void RoomAssetsFactory_FormRegionInitializing(object sender, Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs e)
            {
            }
        }

        #endregion
        
        // Occurs before the form region is displayed.
        // Use this.OutlookItem to get a reference to the current Outlook item.
        // Use this.OutlookFormRegion to get a reference to the form region.
        private void RoomAssets_FormRegionShowing(object sender, System.EventArgs e)
        {
            butSelectARoom.BackColor = Branding.Resource.PrimaryColour;
            butPrint.BackColor = Branding.Resource.PrimaryColour;

            labelCostUnit.Text = string.Empty;
            labelItemName.Text = string.Empty;
            labelItemCost.Text = string.Empty;
            labelItemDescription.Text = string.Empty;
            labelMeetingSummary.Text = string.Empty;

            appointment = (Outlook.AppointmentItem)this.OutlookItem;
            appointment.CustomPropertyChange += Appointment_CustomPropertyChange;
            appointment.Write += Appointment_Write;

            Items = new List<OrderItemDisplay>();

            itemsEquipment.Title = Globals.ThisAddIn.resourceManager.GetString("dropDownEquipment");
            itemsCatering.Title = Globals.ThisAddIn.resourceManager.GetString("dropDownCatering");
            itemsLayout.Title = Globals.ThisAddIn.resourceManager.GetString("dropDownLayout");
            itemsEquipment.Collapse();
            itemsCatering.Collapse();
            itemsLayout.Collapse();
            int colWidth = listBookingGrid.Width / 10;

            listBookingGrid.Columns[0].Width = colWidth - 1;
            listBookingGrid.Columns[1].Width = (colWidth* 3) - 1;
            listBookingGrid.Columns[2].Width = (colWidth* 2) - 1;
            listBookingGrid.Columns[3].Width = colWidth - 1;
            listBookingGrid.Columns[4].Width = colWidth - 1;
            listBookingGrid.Columns[5].Width = colWidth - 1;
            listBookingGrid.Columns[6].Width = colWidth - 1;
			
            order = AppointmentData;
            ArrangeRegions();

            if (!Data.Security.LicenseKey.InLicense)
            {
                labelWarningAssets.Visible = true;
                labelWarningServiceDetails.Visible = true;
                labelWarningSummary.Visible = true;
                labelWarningAssets.Text = Globals.ThisAddIn.resourceManager.GetString("licenseWarningMsg");
                labelWarningAssets.Left = 3;
                labelWarningAssets.Top = 3;
                labelWarningAssets.Height = 100;
                labelWarningAssets.AutoSize = false;
                ////labelWarningAssets.Width = groupAssets.Width - 10;
                //labelWarningAssets.Height = groupAssets.Height - 17;
                labelWarningServiceDetails.Left = 5;
                labelWarningServiceDetails.Top = 13;
                //labelWarningServiceDetails.Width = groupServiceDetails.Width - 10;
                //labelWarningServiceDetails.Height = groupServiceDetails.Height - 17;
                labelWarningSummary.Left = 5;
                labelWarningSummary.Top = 13;
                //labelWarningSummary.Width = groupSummary.Width - 10;
                //labelWarningSummary.Height = groupSummary.Height - 17;
                labelWarningAssets.BringToFront();
                labelWarningServiceDetails.BringToFront();
                labelWarningSummary.BringToFront();

                butSelectARoom.Visible = true;
                butSelectARoom.BringToFront();

                //group_services.Visible = false;
                group_service_details.Visible = false;
                group_meeting_summary.Visible = false;

                itemsEquipment.Visible = false;
                itemsCatering.Visible = false;
                itemsLayout.Visible = false;
                butSelectARoom.Visible = false;
            }
            else
            {
                itemsEquipment.Visible = true;
                itemsCatering.Visible = true;
                itemsLayout.Visible = true;
                butSelectARoom.Visible = true;
                PopulateDefaults();
            }
        }


        private void Appointment_Write(ref bool Cancel)
        {
 //           throw new NotImplementedException();
        }

        private void ArrangeRegions()
        {
            itemsLayout.BringToFront();
            itemsCatering.BringToFront();
            itemsEquipment.BringToFront();
            itemsCatering.Top = itemsEquipment.Top + itemsEquipment.Height + 80;
            itemsLayout.Top = itemsCatering.Top + itemsCatering.Height + 80;
        }

        private void Appointment_CustomPropertyChange(string Name)
        {
            if (Name == "RoomDataUpdated")
            {
                PopulateDefaults();
            }
        }

        #region Appointment properties
        private Data.Region SelectedRegion
        {
            get
            {
                var location = SelectedLocation;
                if (location!=null)
                {
                    return Globals.ThisAddIn.OneSpaceData.GetRegionFromLocationId(location.Id);
                }
                else
                {
                    return null;
                }
            }
        }

        private Location SelectedLocation
        {
            get
            {
                if (appointment.UserProperties["RoomBookingLocation"]!=null)
                {
                    int id = (int)appointment.UserProperties["RoomBookingLocation"].Value;
                    return Globals.ThisAddIn.OneSpaceData.GetLocationById(id);
                }
                else
                {
                    return null;
                }
            }
        }

        private Room SelectedRoom
        {
            get
            {
                if (appointment.UserProperties["RoomBookingRoom"] != null)
                {
                    int id = (int)appointment.UserProperties["RoomBookingRoom"].Value;
                    return Globals.ThisAddIn.OneSpaceData.GetRoomById(id);
                }
                else
                {
                    return null;
                }
            }
        }

        private int Capacity
        {
            get
            {
                int id = -1;
                if (appointment.UserProperties["RoomBookingCapacity"] != null)
                {
                    id = (int)appointment.UserProperties["RoomBookingCapacity"].Value;
                }
                return id;
            }
        }
		
        private AppointmentData AppointmentData {
            get {
                AppointmentData order = null;

                try {
                    var room_booking_data_updated = appointment?.UserProperties[AppointmentSchemaEx.RoomBookingDataUpdated]?.Value as string;

                    if (string.IsNullOrWhiteSpace(room_booking_data_updated) == false) {
                        order = new AppointmentData(room_booking_data_updated);

                        for (int i=0;i<order.Items.Count;i++)
                        {
                            if (order.Items[i].ItemCost==null)
                            {
                                order.Items[i].ItemCost = new Currency
                                {
                                    Value = 0,
                                    Symbol = SelectedRegion.CurrencySymbol,
                                    HasSymbolOnLeft = SelectedRegion.HasSymbolOnLeft
                                };
                            }
                        } 

                    }
                }
                catch (Exception) {
                    order = null;
                }

                return order;
            }
        }

        private AppointmentData AppointmentDataOrDefault {
            get {
                return AppointmentData ?? new AppointmentData();
            }
        }
        #endregion

        private void SaveBookingData()
        {
            order = new AppointmentData();
            order.LastUpdated = DateTime.Now;
            foreach (var item in Items)
            {
                order.Items.Add(item);
            }
            CalculateTotalCost();
            order.CostCentre = boxCostCentre.Text;
            OutlookUtilities.SetPropertyText(appointment, AppointmentSchemaEx.RoomBookingDataUpdated, AppointmentData.GetDataText(order));

        }

        private void PopulateDefaults()
        {
            var analysis = new AppointmentAnalysis(appointment, SelectedRegion);

            itemsEquipment.ClearItems();
            itemsCatering.ClearItems();
            itemsLayout.ClearItems();
            listBookedItems.Items.Clear();

            if (SelectedLocation!=null)
            {
                foreach (BookingItem item in SelectedRegion.BookingItems)
                {
                    switch (item.ItemType)
                    {
                        case BookingItem.enum_ItemType.Facilities:
                            if (SelectedRoom.Facilities.Contains(item.Id))
                            {
                                itemsEquipment.AddItem(item);
                            }
                            break;
                        case BookingItem.enum_ItemType.Catering:
                            if (SelectedRoom.Catering.Contains(item.Id))
                            {
                                itemsCatering.AddItem(item);
                            }
                            break;
                        case BookingItem.enum_ItemType.Layout:
                            if (SelectedRoom.Layout.Contains(item.Id))
                            {
                                itemsLayout.AddItem(item);
                            }
                            break;
                    }
                }

                if (SelectedRoom!=null)
                {
                    itemsEquipment.Visible = true;
                    itemsCatering.Visible = true;
                    itemsLayout.Visible = true;

                    //ToDo: Order dates
                    TimeSpan ts = appointment.End - appointment.Start;
                    labelMeetingSummary.Text = string.Format(Globals.ThisAddIn.resourceManager.GetString("roomAssetMeetingText"), SelectedRoom.Name, appointment.Start.ToString("dd MMM yyyy HH:mm"), ts.ToFriendlyString());
                    string assetReport = string.Empty;
                    foreach (int assetId in SelectedRoom.Features)
                    {
                        assetReport += SelectedRegion.GetFixedAssetList(SelectedRoom.Features);
                    }

                    Items = new List<OrderItemDisplay>();
                    foreach (OrderItem item in AppointmentDataOrDefault.Items)
                    {
                        OrderItemDisplay newItem = new OrderItemDisplay();
                        newItem.ItemId = item.ItemId;
                        newItem.ItemCost = item.ItemCost;
                        newItem.Qty = item.Qty;
                        newItem.Notes = item.Notes;
                        newItem.BookingItems = SelectedRegion.BookingItems.ToList();
                        newItem.DeliveryTimeOffset = item.DeliveryTimeOffset;
                        newItem.ClearAwayTimeOffset = item.ClearAwayTimeOffset;
                        newItem.ServiceAppointmentId = item.ServiceAppointmentId;
                        Items.Add(newItem);

                        if (Globals.ThisAddIn.ShowEquipmentPrice)
                        {
                            object[] cols = { newItem.BookingItem.IconImage, newItem.BookingItem.Name, newItem.BookingItem.TypeName, newItem.ItemCost.ToString(), item.Qty.ToString(), newItem.SubTotal(analysis).ToString() };
                            listBookingGrid.Rows.Add(cols);
                        }
                        else
                        {
                            object[] cols2 = { newItem.BookingItem.IconImage, newItem.BookingItem.Name, newItem.BookingItem.TypeName,string.Empty, item.Qty.ToString(), newItem.SubTotal(analysis).ToString() };
                            listBookingGrid.Rows.Add(cols2);
                        }

                        /*
                        listBookingGrid.Rows[listBookingGrid.Rows.Count - 1].Cells[0] = new DataGridViewImageCell();
                        listBookingGrid.Rows[listBookingGrid.Rows.Count - 1].Cells[0].Value = newItem.BookingItem.IconImage;
                        listBookingGrid.Rows[listBookingGrid.Rows.Count - 1].Cells[0].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                        */

                        //    listBookingGrid.Rows[listBookingGrid.Rows.Count - 1].Height = 32;
                    }
                }
                //group_services.Visible = true;
                group_service_details.Visible = true;
                group_meeting_summary.Visible = true;
         //       listBookingGrid.RowTemplate.Resizable = DataGridViewTriState.True;
         //       listBookingGrid.RowTemplate.Height = 64;
                boxCostCentre.Text = AppointmentDataOrDefault.CostCentre;
                foreach (var item in Globals.ThisAddIn.Settings.PreviousCostCentres)
                {
                    if (item!=string.Empty)
                    {
                        boxCostCentre.Items.Add(item);
                    }
                }
                butSelectARoom.Visible = false;

                labelWarningAssets.Visible = false;
                labelWarningServiceDetails.Visible = false;
                labelWarningSummary.Visible = false;
                
            }
            else
            {

                labelWarningAssets.Visible = true;
                labelWarningServiceDetails.Visible = true;
                //labelWarningAssets.Text = Properties.Resources.assetsWarningServiceDetails;
                labelWarningSummary.Visible = true;
                //labelWarningAssets.Left = 3;
                //labelWarningAssets.Top = 3;
                //labelWarningAssets.Width = groupAssets.Width-10;
                //labelWarningAssets.Height = groupAssets.Height-17;

                labelWarningServiceDetails.Left = 5;
                labelWarningServiceDetails.Top = 13;
                //labelWarningServiceDetails.Width = groupServiceDetails.Width-10;
                //labelWarningServiceDetails.Height = groupServiceDetails.Height-17;
                labelWarningSummary.Left = 5;
                labelWarningSummary.Top = 13;
                //labelWarningSummary.Width = groupSummary.Width-10;
                //labelWarningSummary.Height = groupSummary.Height-17;
                labelWarningAssets.BringToFront();
                labelWarningServiceDetails.BringToFront();
                labelWarningSummary.BringToFront();
                

                itemsEquipment.Visible = false;
                itemsCatering.Visible = false;
                itemsLayout.Visible = false;

                //group_services.Visible = false;
                group_service_details.Visible = false;
                group_meeting_summary.Visible = false;
                butSelectARoom.Visible = true;
                butSelectARoom.BringToFront();

            }
            CalculateTotalCost();
        }

        // Occurs when the form region is closed.
        // Use this.OutlookItem to get a reference to the current Outlook item.
        // Use this.OutlookFormRegion to get a reference to the form region.
        private void RoomAssets_FormRegionClosed(object sender, System.EventArgs e)
        {
            if (order!=null)
            {
                Globals.ThisAddIn.AddCentre(order.CostCentre);
                string location = appointment.Location;
                try
                {
                   // AppointmentData app;

                    OutlookUtilities.SetPropertyText(appointment, AppointmentSchemaEx.RoomBookingDataUpdated, AppointmentData.GetDataText(order));
                    order.Start = appointment.Start;
                    order.End = appointment.End;

                    //ToDo: Disabled service integration
                    /*
                    if (Globals.ThisAddIn.YouAreTheOwner(appointment))
                    {
                        var alert = new ServiceAlerts(appointment);
                        app = alert.ProcessAlerts();
                        OutlookUtilities.SetPropertyText(appointment, AppointmentSchemaEx.RoomBookingDataUpdated, AppointmentData.GetDataText(app));
                    }
                    */

                    /*
                    if (currentJson==string.Empty)
                    {
                        //New item
                        app = AppointmentData.Add(appointment);
                    }
                    else
                    {
                        //Updated item
                        app = AppointmentData.Process(appointment);
                    }
                    */

                    /* Todo: Find out why this call is being made.  I think the appointment should be saved when the user
                     *       presses "Save" / "Send" as per the normal Outlook workflow
                     * */
                    //appointment.Save();

                }
                catch (Exception ex)
                {
                    Logging.ActivityLog.WriteToLog("RoomAssets view closed error:" + ex.Message);
                }
            }
        }

        private void butAddAsset_Click(object sender, EventArgs e)
        {
            DoClick();
        }

        private void DoClick()
        {
            switch (Mode)
            {
                case enum_Mode.Nothing:
                    return;
                case enum_Mode.AddItem:
                    AddAsset();
                    break;
                case enum_Mode.UpdateItem:
                    UpdateAsset();
                    break;
            }

            UIUtilities.SetButton(butClose, true);
        }

        private void AddAsset() {
            var analysis = new AppointmentAnalysis(appointment, SelectedRegion);

            OrderItemDisplay item = new OrderItemDisplay();
            if (ItemToAdd != null) {
                if (ItemToAdd.ItemType == BookingItem.enum_ItemType.Layout) {
                    var has_layout = (
                        from i in Items
                        where analysis.GetBookingItem(i.ItemId).ItemType == BookingItem.enum_ItemType.Layout
                        select i
                    ).Any();
                    
                    if (has_layout) {
                        MessageBox.Show(
                            Globals.ThisAddIn.resourceManager.GetString("bookOnlyOneLayoutAllowed"),
                            Globals.ThisAddIn.resourceManager.GetString("bookOnlyOneLayoutAllowedTitle"),
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information
                        );
                        return;
                    }
                }

                item.BookingItems = SelectedRegion.BookingItems.ToList();
                item.ItemId = ItemToAdd.Id;
                if (item.IsTooLate(appointment.Start)) {
                    MessageBox.Show(
                        string.Format(
                            Globals.ThisAddIn.resourceManager.GetString("bookLeadtimeCaption"),
                            item.BookingItem.Name,
                            item.BookingItem.LeadTimeText
                        ),
                        Globals.ThisAddIn.resourceManager.GetString("bookLeadtimeTitle"),
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information
                    );
                    return;
                }
                item.ItemCost = ItemToAdd.Cost;
                item.Qty = (int)sizeBox.Value;
                item.Notes = textItemNotes.Text;

                item.DeliveryTimeOffset = OutlookUtilities.GetComboBoxValue(timeDelivery);
                item.ClearAwayTimeOffset = OutlookUtilities.GetComboBoxValue(timeClearAway);
                item.Start = appointment.Start;
                item.End = appointment.End;
                Items.Add(item);
                object[] cols = { item.BookingItem.IconImage, item.BookingItem.Name, item.BookingItem.TypeName, item.ItemCost.ToString(), item.Qty.ToString(), item.SubTotal(analysis).ToString() };
                listBookingGrid.Rows.Add(cols);
                
                SaveBookingData();

                UIUtilities.SetButton(butAddAsset, false);

            }
        }

        private void UpdateAsset()
        {
            int index = listBookingGrid.SelectedRows[0].Index;

            OrderItemDisplay item = Items[index];
            if (item != null)
            {
                var analysis = new AppointmentAnalysis(appointment, SelectedRegion);
                item.BookingItems = SelectedRegion.BookingItems.ToList();
                if (item.IsTooLate(appointment.Start))
                {
                    MessageBox.Show(string.Format(Globals.ThisAddIn.resourceManager.GetString("bookLeadtimeCaption"), item.BookingItem.Name, item.BookingItem.LeadTimeText), Globals.ThisAddIn.resourceManager.GetString("bookLeadtimeTitle"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                item.Qty = (int)sizeBox.Value;
                item.Notes = textItemNotes.Text;

                item.DeliveryTimeOffset = OutlookUtilities.GetComboBoxValue(timeDelivery);
                item.ClearAwayTimeOffset = OutlookUtilities.GetComboBoxValue(timeClearAway);
                item.Start = appointment.Start;
                item.End = appointment.End;
                Items[index] = item;

                listBookingGrid.Rows[index].Cells[4].Value = item.Qty.ToString();
                listBookingGrid.Rows[index].Cells[5].Value = item.SubTotal(analysis).ToString();

                SaveBookingData();

                UIUtilities.SetButton(butAddAsset, false);
            }
        }

        private void CalculateTotalCost()
        {
            try
            {
                var analysis = new AppointmentAnalysis(appointment, SelectedRegion);

                Currency totalCost = new Currency()
                {
                    Symbol = SelectedRegion.CurrencySymbol,
                    HasSymbolOnLeft = SelectedRegion.HasSymbolOnLeft,
                    Value = 0
                };

                foreach (OrderItem item in Items)
                {
                    totalCost += item.SubTotal(analysis);
                }

                labelTotalOrderCost.Text = totalCost.ToString();
            }
            catch (Exception)
            {
            }
        }

        private void RoomAssets_Enter(object sender, EventArgs e)
        {
            appointment = (Outlook.AppointmentItem)this.OutlookItem;
            Items = new List<OrderItemDisplay>();
            PopulateDefaults();
        }

        private void SetMaxCapacity(int maxValue, int defaultValue)
        {
            /* Note: We agreed -1 would mean no-limmit, but for the now if there is an error in the json no-limmit is fine. */
            if(maxValue < 0) {
                maxValue = int.MaxValue;
            }

            if(maxValue < defaultValue) {
                maxValue = defaultValue;
            }

            sizeBox.Maximum = (maxValue < 0) ? 1 : maxValue;
            sizeBox.Value = (defaultValue < 0) ? 1 : defaultValue;
        }

        private void butPrint_Click(object sender, EventArgs e)
        {
            /*
            if (!enablePrint)
            {
                return;
            }
            */
            var analysis = new AppointmentAnalysis(appointment, SelectedRegion);

            string orderReport = string.Empty;
            Currency cost = new Currency() {
                Symbol = SelectedRegion.CurrencySymbol,
                HasSymbolOnLeft = SelectedRegion.HasSymbolOnLeft,
                Value = 0
            };

            foreach (OrderItem item in AppointmentDataOrDefault.Items)
            {
                OrderItemDisplay newItem = new OrderItemDisplay();
                newItem.ItemId = item.ItemId;
                var bookingItem = Globals.ThisAddIn.OneSpaceData.GetBookingItem(SelectedLocation.Id, newItem.ItemId);
                newItem.ItemCost = bookingItem.Cost;
                newItem.Qty = item.Qty;
                newItem.Notes = item.Notes;
                newItem.Start = item.Start;
                newItem.End = item.End;
                newItem.BookingItems = SelectedRegion.BookingItems.ToList();

                orderReport += newItem.BookingItem.Name + " " + newItem.ItemCost.ToString() + " X " + newItem.Qty.ToString() + " = " + newItem.SubTotal(analysis).ToString() + "\n";

                if (newItem.BookingItem.ItemType == BookingItem.enum_ItemType.Catering)
                {
                    orderReport += string.Format(Globals.ThisAddIn.resourceManager.GetString("reportOrderItemDelivery"),newItem.DeliveryDate(newItem.Start).ToString("HH:mm"),newItem.ClearawayDate(newItem.End).ToString("HH:mm"));
                }
                cost += newItem.SubTotal(analysis);
            }
            orderReport += string.Format(Globals.ThisAddIn.resourceManager.GetString("reportOrderTotal"), cost.ToString());
            Floor selectedFloor = Globals.ThisAddIn.OneSpaceData.GetFloorFromRoomById(SelectedRoom.Id);
            string dates = appointment.Start.ToString("dd MMMM yyyy") + " - " + appointment.End.ToString("dd MMMM yyyy");
            RoomBooking booking = new RoomBooking();
            booking.DateFrom = appointment.Start;
            booking.DateTo = appointment.End;
            booking.Data = AppointmentDataOrDefault;
            booking.Subject = appointment.Subject;
            booking.Attendees = appointment.RequiredAttendees;

            RoomBookingOrderPrintout printout = new RoomBookingOrderPrintout(analysis,SelectedLocation, selectedFloor, SelectedRoom, booking);
            printout.ShowDialog();
        }

        private void selectionRegion_ToggleEvent(object sender, EventArgs e)
        {
            ArrangeRegions();
        }

        private void bookableItemRegion_ItemSelectedEvent(object sender, Controls.ItemSelectedEventArgs e)
        {

            if (e != null)
            {
                textItemNotes.Text = string.Empty;
                labelItemName.Text = e.Item.Name;

                if (!Globals.ThisAddIn.ShowEquipmentPrice && e.Item.ItemType == BookingItem.enum_ItemType.Facilities) {
                    labelCostUnit.Text = string.Empty;
                    labelItemCost.Text = string.Empty;
                    /* Note: I don't like this, the value shouldn't impact the UI logic. */
                    e.Item.Cost.Value = 0;
                }
                else {
                    labelCostUnit.Text = e.Item.CostUnit;
                    labelItemCost.Text = e.Item.CostToCurrencyString();
                }

                labelItemDescription.Text = e.Item.Description;
                ItemToAdd = e.Item;
                SetMaxCapacity(e.Item.MaxQty, e.Item.DefaultQty);
                Mode = enum_Mode.UpdateItem;
                sizeBox.Enabled = true;
                timeDelivery.Enabled = true;
                assetIcon.Image = e.Item.IconImageX2;
                Trace.WriteLine("Icon:" + e.Item.Icon);
                Mode = enum_Mode.AddItem;
                SetMode();

                OutlookUtilities.PopulateDeliveryDropDown(timeDelivery, appointment, ItemToAdd);
                OutlookUtilities.PopulateClearawayDropDown(timeClearAway, appointment, ItemToAdd);
                
                if (e.DoubleClicked)
                {
                    DoClick();
                }
            }
            else
            {
                textItemNotes.Text = string.Empty;
                labelItemName.Text = string.Empty;
                labelCostUnit.Text = string.Empty;
                labelItemCost.Text = string.Empty;
                labelItemDescription.Text = string.Empty;
                ItemToAdd = null;
                Mode = enum_Mode.Nothing;
                SetMode();
                timeDelivery.Enabled = false;
                timeClearAway.Enabled = false;
            }
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            itemsEquipment.Collapse();
            itemsCatering.Collapse();
            itemsLayout.Collapse();
            ArrangeRegions();

            labelItemName.Text = string.Empty;
            labelCostUnit.Text = string.Empty;
            labelItemCost.Text = string.Empty;
            labelItemDescription.Text = string.Empty;
            textItemNotes.Text = string.Empty;
            Mode = enum_Mode.Nothing;
            SetMode();
            sizeBox.Enabled = false;
            ItemToAdd = null;
        }

        private void listBookingGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textItemNotes.Text = string.Empty;
            if (e.ColumnIndex == 6)
            {
                var remove_service_results = MessageBox.Show(Globals.ThisAddIn.resourceManager.GetString("removeServiceConfirmation"), Globals.ThisAddIn.resourceManager.GetString("removeServiceConfirmationTitle"), MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (remove_service_results == DialogResult.OK) {
                    listBookingGrid.Rows.RemoveAt(e.RowIndex);
                    if (Items[e.RowIndex].ServiceAppointmentId != null && Items[e.RowIndex].ServiceAppointmentId != string.Empty) {
                        AddIdToDelete(Items[e.RowIndex].ServiceAppointmentId);
                    }
                    Items.RemoveAt(e.RowIndex);
                    SaveBookingData();
                    OutlookUtilities.SetComboBox(timeDelivery, 0);
                    OutlookUtilities.SetComboBox(timeClearAway, 0);
                }
            }
            else
            {
                ShowItem();
            }
        }

        private void AddIdToDelete(string newId)
        {
            bool addId = true;
            foreach (var id in order.DeletedServiceIDs)
            {
                if (id== newId)
                {
                    addId = false;
                }
            }
            if (addId)
            {
                order.DeletedServiceIDs.Add(newId);
            }
            for (int i=0;i<Items.Count;i++)
            {
                if (Items[i].ServiceAppointmentId==newId)
                {
                    Items[i].ServiceAppointmentId = string.Empty;
                }
            }
        }

        private void listBookingGrid_SelectionChanged(object sender, EventArgs e)
        {
            ShowItem();
        }

        private int SelectedIndex
        {
            get
            {
                if (listBookingGrid.SelectedRows.Count > 0)
                {
                    return listBookingGrid.SelectedRows[0].Index;
                }
                else
                {
                    return 0;
                }
            }
        }

        private void ShowItem()
        {
            if (listBookingGrid.SelectedRows.Count > 0)
            {
                int index = listBookingGrid.SelectedRows[0].Index;
                SetMaxCapacity(Items[index].BookingItem.MaxQty, Items[index].BookingItem.DefaultQty);
                SetCapacity(Items[index].Qty);
                var item = Items[index].BookingItem;
                textItemNotes.Text = Items[index].Notes;
                OutlookUtilities.SetComboBox(timeDelivery, Items[index].DeliveryTimeOffset);
                OutlookUtilities.SetComboBox(timeClearAway, Items[index].ClearAwayTimeOffset);
                sizeBox.Enabled = true;

                if (!Globals.ThisAddIn.ShowEquipmentPrice && item.ItemType == BookingItem.enum_ItemType.Facilities) {
                    labelCostUnit.Text = string.Empty;
                    labelItemCost.Text = string.Empty;
                    /* Note: I don't like this, the value shouldn't impact the UI logic. */
                    item.Cost.Value = 0;
                }
                else {
                    labelCostUnit.Text = item.CostUnit;
                    labelItemCost.Text = item.CostToCurrencyString();
                }

                labelItemName.Text = item.Name;
                labelItemDescription.Text = item.Description;
                Mode = enum_Mode.UpdateItem;
                SetMode();
                UIUtilities.SetButton(butAddAsset, false);
            }
        }

        private void SetCapacity(int value)
        {
            /* Note: This is here to handle the situation where the MaxQty changes between the time the order was made and the time it is loaded meaning the ordered value would be invalid. */
            if(sizeBox.Maximum < value) {
                sizeBox.Maximum = value;
            }

            sizeBox.Value = value;
        }

        private void textItemNotes_TextChanged(object sender, EventArgs e)
        {
            if (ComboBoxItem.TextboxNonUserUpdate(sender))
            {
                return;
            }
            SetMode();
        }
        
        private void butClose_Click(object sender, EventArgs e)
        {
            if (order!=null)
            {
                order.CostCentre = boxCostCentre.Text;
                /* Todo: (Rob) - Review how these properties are set and read */
                OutlookUtilities.SetPropertyText(appointment, AppointmentSchemaEx.RoomBookingDataUpdated, AppointmentData.GetDataText(order));
                bool hasSent = appointment.PropertyAccessor.GetProperty("http://schemas.microsoft.com/mapi/id/{00062002-0000-0000-C000-000000000046}/8229000B");
                if (!hasSent)
                {
                    var check = RoomWithBookings.CanBookWithBuffer(ThisAddIn.BufferedSlots, order, appointment.Start,
                        appointment.End);
                    if (!check.BufferOk)
                    {
                        var dialog = new FormBookingRestrictions(check.Before, check.After);
                        dialog.ShowDialog();
                        Logging.ActivityLog.WriteToLog(Globals.ThisAddIn.resourceManager.GetString("meetingBufferErrorTitle"));
                        return;
                    }
                }
            }
            var inspector = Globals.ThisAddIn.Application.ActiveInspector();
            inspector.SetCurrentFormPage("Appointment");
        }
        
        private void boxCostCentre_TextChanged(object sender, EventArgs e)
        {
            if (order==null)
            {
                order = new AppointmentData();
            }
            order.CostCentre = boxCostCentre.Text;
            OutlookUtilities.SetPropertyText(appointment, AppointmentSchemaEx.RoomBookingDataUpdated, AppointmentData.GetDataText(order));
        }

        private void timeDelivery_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxItem.NonUserUpdate(sender))
            {
                return;
            }
            SetMode();
        }

        private void SetMode()
        {
            switch (Mode)
            {
                case enum_Mode.Nothing:
                    UIUtilities.SetButton(butAddAsset, false);
                    break;
                case enum_Mode.AddItem:
                    butAddAsset.Text = Globals.ThisAddIn.resourceManager.GetString("addToMeeting");
                    UIUtilities.SetButton(butAddAsset, true);
                    break;
                case enum_Mode.UpdateItem:
                    butAddAsset.Text = Globals.ThisAddIn.resourceManager.GetString("update");
                    UIUtilities.SetButton(butAddAsset, true);
                    break;
            }
        }

        private void timeClearAway_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBoxItem.NonUserUpdate(sender))
            {
                return;
            }
            SetMode();
        }
        
        private void labelWarningServiceDetails_Click(object sender, EventArgs e)
        {

        }

        private void butSelectRoom_Click(object sender, EventArgs e)
        {
            var inspector = Globals.ThisAddIn.Application.ActiveInspector();
            inspector.SetCurrentFormPage("Appointment");
        }

        private void boxCostCentre_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void itemsEquipment_Load(object sender, EventArgs e)
        {

        }
        
        private void itemsLayout_Load_1(object sender, EventArgs e)
        {

        }

        private void butSelectARoom_Click(object sender, EventArgs e)
        {
            var inspector = Globals.ThisAddIn.Application.ActiveInspector();
//            inspector.SetCurrentFormPage("Appointment");
            inspector.SetCurrentFormPage("OneSpace.OutlookPlugin.BookMeetingRoom");
        }

        private void listBookingGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void sizeBox_ValueChanged(object sender, EventArgs e)
        {
            if (ComboBoxItem.UpdateDownNonUserUpdate(sender))
            {
                return;
            }
            SetMode();

        }
    }
}
