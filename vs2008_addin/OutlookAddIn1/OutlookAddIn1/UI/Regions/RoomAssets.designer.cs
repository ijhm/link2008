﻿namespace OneSpace.OutlookPlugin
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class RoomAssets : Microsoft.Office.Tools.Outlook.FormRegionBase
    {
        public RoomAssets(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            : base(Globals.Factory, formRegion)
        {
            this.InitializeComponent();
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoomAssets));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.group_services = new System.Windows.Forms.GroupBox();
            this.panel_services = new System.Windows.Forms.Panel();
            this.butSelectARoom = new System.Windows.Forms.Button();
            this.labelWarningAssets = new System.Windows.Forms.Label();
            this.itemsCatering = new OneSpace.OutlookPlugin.Controls.BookableItemRegion2();
            this.itemsLayout = new OneSpace.OutlookPlugin.Controls.BookableItemRegion2();
            this.itemsEquipment = new OneSpace.OutlookPlugin.Controls.BookableItemRegion2();
            this.group_service_details = new System.Windows.Forms.GroupBox();
            this.sizeBox = new System.Windows.Forms.NumericUpDown();
            this.labelBookingRestrictionWarning = new System.Windows.Forms.Label();
            this.assetIcon = new System.Windows.Forms.PictureBox();
            this.labelItemCost = new System.Windows.Forms.Label();
            this.labelCostUnit = new System.Windows.Forms.Label();
            this.timeClearAway = new System.Windows.Forms.ComboBox();
            this.labelItemName = new System.Windows.Forms.Label();
            this.timeDelivery = new System.Windows.Forms.ComboBox();
            this.butAddAsset = new System.Windows.Forms.Button();
            this.labelQuantity = new System.Windows.Forms.Label();
            this.labelWarningServiceDetails = new System.Windows.Forms.Label();
            this.labelClearAwayTime = new System.Windows.Forms.Label();
            this.labelItemNotes = new System.Windows.Forms.Label();
            this.textItemNotes = new System.Windows.Forms.TextBox();
            this.labelDeliveryTime = new System.Windows.Forms.Label();
            this.labelItemDescription = new System.Windows.Forms.Label();
            this.group_meeting_summary = new System.Windows.Forms.GroupBox();
            this.boxCostCentre = new System.Windows.Forms.ComboBox();
            this.labelMeetingSummary = new System.Windows.Forms.Label();
            this.labelTotalOrderCost = new System.Windows.Forms.Label();
            this.labelCostCentre = new System.Windows.Forms.Label();
            this.labelTotalCost = new System.Windows.Forms.Label();
            this.labelWarningSummary = new System.Windows.Forms.Label();
            this.butClose = new System.Windows.Forms.Button();
            this.butPrint = new System.Windows.Forms.Button();
            this.listBookingGrid = new System.Windows.Forms.DataGridView();
            this.TypeIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.ItemNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteItem = new System.Windows.Forms.DataGridViewImageColumn();
            this.listBookedItems = new System.Windows.Forms.ListView();
            this.ItemName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ItemType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ItemUnitCost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ItemQty = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ItemSub = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel_details = new System.Windows.Forms.Panel();
            this.group_services.SuspendLayout();
            this.panel_services.SuspendLayout();
            this.group_service_details.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.assetIcon)).BeginInit();
            this.group_meeting_summary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBookingGrid)).BeginInit();
            this.panel_details.SuspendLayout();
            this.SuspendLayout();
            // 
            // group_services
            // 
            this.group_services.Controls.Add(this.panel_services);
            resources.ApplyResources(this.group_services, "group_services");
            this.group_services.Name = "group_services";
            this.group_services.TabStop = false;
            // 
            // panel_services
            // 
            resources.ApplyResources(this.panel_services, "panel_services");
            this.panel_services.Controls.Add(this.butSelectARoom);
            this.panel_services.Controls.Add(this.labelWarningAssets);
            this.panel_services.Controls.Add(this.itemsCatering);
            this.panel_services.Controls.Add(this.itemsLayout);
            this.panel_services.Controls.Add(this.itemsEquipment);
            this.panel_services.Name = "panel_services";
            this.panel_services.Click += new System.EventHandler(this.panel1_Click);
            // 
            // butSelectARoom
            // 
            resources.ApplyResources(this.butSelectARoom, "butSelectARoom");
            this.butSelectARoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(41)))), ((int)(((byte)(51)))));
            this.butSelectARoom.ForeColor = System.Drawing.Color.White;
            this.butSelectARoom.Name = "butSelectARoom";
            this.butSelectARoom.Tag = "main";
            this.butSelectARoom.UseVisualStyleBackColor = false;
            this.butSelectARoom.Click += new System.EventHandler(this.butSelectARoom_Click);
            // 
            // labelWarningAssets
            // 
            resources.ApplyResources(this.labelWarningAssets, "labelWarningAssets");
            this.labelWarningAssets.Name = "labelWarningAssets";
            // 
            // itemsCatering
            // 
            this.itemsCatering.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.itemsCatering, "itemsCatering");
            this.itemsCatering.Name = "itemsCatering";
            this.itemsCatering.ToggleEvent += new System.EventHandler(this.selectionRegion_ToggleEvent);
            this.itemsCatering.ItemSelectedEvent += new System.EventHandler<OneSpace.OutlookPlugin.Controls.ItemSelectedEventArgs>(this.bookableItemRegion_ItemSelectedEvent);
            // 
            // itemsLayout
            // 
            this.itemsLayout.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.itemsLayout, "itemsLayout");
            this.itemsLayout.Name = "itemsLayout";
            this.itemsLayout.ToggleEvent += new System.EventHandler(this.selectionRegion_ToggleEvent);
            this.itemsLayout.ItemSelectedEvent += new System.EventHandler<OneSpace.OutlookPlugin.Controls.ItemSelectedEventArgs>(this.bookableItemRegion_ItemSelectedEvent);
            this.itemsLayout.Load += new System.EventHandler(this.itemsLayout_Load_1);
            // 
            // itemsEquipment
            // 
            this.itemsEquipment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.itemsEquipment, "itemsEquipment");
            this.itemsEquipment.Name = "itemsEquipment";
            this.itemsEquipment.ToggleEvent += new System.EventHandler(this.selectionRegion_ToggleEvent);
            this.itemsEquipment.ItemSelectedEvent += new System.EventHandler<OneSpace.OutlookPlugin.Controls.ItemSelectedEventArgs>(this.bookableItemRegion_ItemSelectedEvent);
            this.itemsEquipment.Load += new System.EventHandler(this.itemsEquipment_Load);
            // 
            // group_service_details
            // 
            this.group_service_details.Controls.Add(this.sizeBox);
            this.group_service_details.Controls.Add(this.labelBookingRestrictionWarning);
            this.group_service_details.Controls.Add(this.assetIcon);
            this.group_service_details.Controls.Add(this.labelItemCost);
            this.group_service_details.Controls.Add(this.labelCostUnit);
            this.group_service_details.Controls.Add(this.timeClearAway);
            this.group_service_details.Controls.Add(this.labelItemName);
            this.group_service_details.Controls.Add(this.timeDelivery);
            this.group_service_details.Controls.Add(this.butAddAsset);
            this.group_service_details.Controls.Add(this.labelQuantity);
            this.group_service_details.Controls.Add(this.labelWarningServiceDetails);
            this.group_service_details.Controls.Add(this.labelClearAwayTime);
            this.group_service_details.Controls.Add(this.labelItemNotes);
            this.group_service_details.Controls.Add(this.textItemNotes);
            this.group_service_details.Controls.Add(this.labelDeliveryTime);
            this.group_service_details.Controls.Add(this.labelItemDescription);
            resources.ApplyResources(this.group_service_details, "group_service_details");
            this.group_service_details.Name = "group_service_details";
            this.group_service_details.TabStop = false;
            // 
            // sizeBox
            // 
            resources.ApplyResources(this.sizeBox, "sizeBox");
            this.sizeBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sizeBox.Name = "sizeBox";
            this.sizeBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sizeBox.ValueChanged += new System.EventHandler(this.sizeBox_ValueChanged);
            // 
            // labelBookingRestrictionWarning
            // 
            resources.ApplyResources(this.labelBookingRestrictionWarning, "labelBookingRestrictionWarning");
            this.labelBookingRestrictionWarning.Name = "labelBookingRestrictionWarning";
            // 
            // assetIcon
            // 
            resources.ApplyResources(this.assetIcon, "assetIcon");
            this.assetIcon.Name = "assetIcon";
            this.assetIcon.TabStop = false;
            // 
            // labelItemCost
            // 
            resources.ApplyResources(this.labelItemCost, "labelItemCost");
            this.labelItemCost.Name = "labelItemCost";
            // 
            // labelCostUnit
            // 
            resources.ApplyResources(this.labelCostUnit, "labelCostUnit");
            this.labelCostUnit.Name = "labelCostUnit";
            // 
            // timeClearAway
            // 
            this.timeClearAway.FormattingEnabled = true;
            resources.ApplyResources(this.timeClearAway, "timeClearAway");
            this.timeClearAway.Name = "timeClearAway";
            this.timeClearAway.SelectedIndexChanged += new System.EventHandler(this.timeClearAway_SelectedIndexChanged);
            // 
            // labelItemName
            // 
            resources.ApplyResources(this.labelItemName, "labelItemName");
            this.labelItemName.Name = "labelItemName";
            // 
            // timeDelivery
            // 
            this.timeDelivery.FormattingEnabled = true;
            resources.ApplyResources(this.timeDelivery, "timeDelivery");
            this.timeDelivery.Name = "timeDelivery";
            this.timeDelivery.SelectedIndexChanged += new System.EventHandler(this.timeDelivery_SelectedIndexChanged);
            // 
            // butAddAsset
            // 
            this.butAddAsset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            resources.ApplyResources(this.butAddAsset, "butAddAsset");
            this.butAddAsset.ForeColor = System.Drawing.Color.White;
            this.butAddAsset.Name = "butAddAsset";
            this.butAddAsset.Tag = "main";
            this.butAddAsset.UseVisualStyleBackColor = false;
            this.butAddAsset.Click += new System.EventHandler(this.butAddAsset_Click);
            // 
            // labelQuantity
            // 
            resources.ApplyResources(this.labelQuantity, "labelQuantity");
            this.labelQuantity.Name = "labelQuantity";
            // 
            // labelWarningServiceDetails
            // 
            resources.ApplyResources(this.labelWarningServiceDetails, "labelWarningServiceDetails");
            this.labelWarningServiceDetails.Name = "labelWarningServiceDetails";
            this.labelWarningServiceDetails.Click += new System.EventHandler(this.labelWarningServiceDetails_Click);
            // 
            // labelClearAwayTime
            // 
            resources.ApplyResources(this.labelClearAwayTime, "labelClearAwayTime");
            this.labelClearAwayTime.Name = "labelClearAwayTime";
            // 
            // labelItemNotes
            // 
            resources.ApplyResources(this.labelItemNotes, "labelItemNotes");
            this.labelItemNotes.Name = "labelItemNotes";
            // 
            // textItemNotes
            // 
            resources.ApplyResources(this.textItemNotes, "textItemNotes");
            this.textItemNotes.Name = "textItemNotes";
            this.textItemNotes.TextChanged += new System.EventHandler(this.textItemNotes_TextChanged);
            // 
            // labelDeliveryTime
            // 
            resources.ApplyResources(this.labelDeliveryTime, "labelDeliveryTime");
            this.labelDeliveryTime.Name = "labelDeliveryTime";
            // 
            // labelItemDescription
            // 
            this.labelItemDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.labelItemDescription, "labelItemDescription");
            this.labelItemDescription.Name = "labelItemDescription";
            // 
            // group_meeting_summary
            // 
            this.group_meeting_summary.Controls.Add(this.boxCostCentre);
            this.group_meeting_summary.Controls.Add(this.labelMeetingSummary);
            this.group_meeting_summary.Controls.Add(this.labelTotalOrderCost);
            this.group_meeting_summary.Controls.Add(this.labelCostCentre);
            this.group_meeting_summary.Controls.Add(this.labelTotalCost);
            this.group_meeting_summary.Controls.Add(this.labelWarningSummary);
            this.group_meeting_summary.Controls.Add(this.butClose);
            this.group_meeting_summary.Controls.Add(this.butPrint);
            this.group_meeting_summary.Controls.Add(this.listBookingGrid);
            this.group_meeting_summary.Controls.Add(this.listBookedItems);
            resources.ApplyResources(this.group_meeting_summary, "group_meeting_summary");
            this.group_meeting_summary.Name = "group_meeting_summary";
            this.group_meeting_summary.TabStop = false;
            // 
            // boxCostCentre
            // 
            this.boxCostCentre.FormattingEnabled = true;
            resources.ApplyResources(this.boxCostCentre, "boxCostCentre");
            this.boxCostCentre.Name = "boxCostCentre";
            this.boxCostCentre.SelectedIndexChanged += new System.EventHandler(this.boxCostCentre_SelectedIndexChanged);
            this.boxCostCentre.TextChanged += new System.EventHandler(this.boxCostCentre_TextChanged);
            // 
            // labelMeetingSummary
            // 
            resources.ApplyResources(this.labelMeetingSummary, "labelMeetingSummary");
            this.labelMeetingSummary.Name = "labelMeetingSummary";
            // 
            // labelTotalOrderCost
            // 
            resources.ApplyResources(this.labelTotalOrderCost, "labelTotalOrderCost");
            this.labelTotalOrderCost.Name = "labelTotalOrderCost";
            // 
            // labelCostCentre
            // 
            resources.ApplyResources(this.labelCostCentre, "labelCostCentre");
            this.labelCostCentre.Name = "labelCostCentre";
            // 
            // labelTotalCost
            // 
            resources.ApplyResources(this.labelTotalCost, "labelTotalCost");
            this.labelTotalCost.Name = "labelTotalCost";
            // 
            // labelWarningSummary
            // 
            resources.ApplyResources(this.labelWarningSummary, "labelWarningSummary");
            this.labelWarningSummary.Name = "labelWarningSummary";
            // 
            // butClose
            // 
            this.butClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(179)))), ((int)(((byte)(175)))));
            resources.ApplyResources(this.butClose, "butClose");
            this.butClose.ForeColor = System.Drawing.Color.White;
            this.butClose.Name = "butClose";
            this.butClose.Tag = "main";
            this.butClose.UseVisualStyleBackColor = false;
            this.butClose.Click += new System.EventHandler(this.butClose_Click);
            // 
            // butPrint
            // 
            this.butPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(41)))), ((int)(((byte)(51)))));
            resources.ApplyResources(this.butPrint, "butPrint");
            this.butPrint.ForeColor = System.Drawing.Color.White;
            this.butPrint.Name = "butPrint";
            this.butPrint.Tag = "main";
            this.butPrint.UseVisualStyleBackColor = false;
            this.butPrint.Click += new System.EventHandler(this.butPrint_Click);
            // 
            // listBookingGrid
            // 
            this.listBookingGrid.AllowUserToAddRows = false;
            this.listBookingGrid.AllowUserToDeleteRows = false;
            this.listBookingGrid.AllowUserToResizeRows = false;
            this.listBookingGrid.BackgroundColor = System.Drawing.Color.White;
            this.listBookingGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listBookingGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TypeIcon,
            this.ItemNameCol,
            this.Type,
            this.UnitCost,
            this.Qty,
            this.SubTotal,
            this.DeleteItem});
            this.listBookingGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            resources.ApplyResources(this.listBookingGrid, "listBookingGrid");
            this.listBookingGrid.MultiSelect = false;
            this.listBookingGrid.Name = "listBookingGrid";
            this.listBookingGrid.ReadOnly = true;
            this.listBookingGrid.RowHeadersVisible = false;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(2);
            this.listBookingGrid.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.listBookingGrid.RowTemplate.Height = 48;
            this.listBookingGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.listBookingGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listBookingGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listBookingGrid_CellContentClick);
            this.listBookingGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.listBookingGrid_DataError);
            this.listBookingGrid.SelectionChanged += new System.EventHandler(this.listBookingGrid_SelectionChanged);
            // 
            // TypeIcon
            // 
            resources.ApplyResources(this.TypeIcon, "TypeIcon");
            this.TypeIcon.Name = "TypeIcon";
            this.TypeIcon.ReadOnly = true;
            this.TypeIcon.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TypeIcon.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ItemNameCol
            // 
            resources.ApplyResources(this.ItemNameCol, "ItemNameCol");
            this.ItemNameCol.Name = "ItemNameCol";
            this.ItemNameCol.ReadOnly = true;
            // 
            // Type
            // 
            resources.ApplyResources(this.Type, "Type");
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // UnitCost
            // 
            resources.ApplyResources(this.UnitCost, "UnitCost");
            this.UnitCost.Name = "UnitCost";
            this.UnitCost.ReadOnly = true;
            // 
            // Qty
            // 
            resources.ApplyResources(this.Qty, "Qty");
            this.Qty.Name = "Qty";
            this.Qty.ReadOnly = true;
            // 
            // SubTotal
            // 
            resources.ApplyResources(this.SubTotal, "SubTotal");
            this.SubTotal.Name = "SubTotal";
            this.SubTotal.ReadOnly = true;
            // 
            // DeleteItem
            // 
            resources.ApplyResources(this.DeleteItem, "DeleteItem");
            this.DeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("DeleteItem.Image")));
            this.DeleteItem.Name = "DeleteItem";
            this.DeleteItem.ReadOnly = true;
            this.DeleteItem.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // listBookedItems
            // 
            this.listBookedItems.AllowColumnReorder = true;
            this.listBookedItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ItemName,
            this.ItemType,
            this.ItemUnitCost,
            this.ItemQty,
            this.ItemSub});
            this.listBookedItems.FullRowSelect = true;
            this.listBookedItems.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listBookedItems.HideSelection = false;
            resources.ApplyResources(this.listBookedItems, "listBookedItems");
            this.listBookedItems.MultiSelect = false;
            this.listBookedItems.Name = "listBookedItems";
            this.listBookedItems.UseCompatibleStateImageBehavior = false;
            this.listBookedItems.View = System.Windows.Forms.View.Details;
            // 
            // ItemName
            // 
            resources.ApplyResources(this.ItemName, "ItemName");
            // 
            // ItemType
            // 
            resources.ApplyResources(this.ItemType, "ItemType");
            // 
            // ItemUnitCost
            // 
            resources.ApplyResources(this.ItemUnitCost, "ItemUnitCost");
            // 
            // ItemQty
            // 
            resources.ApplyResources(this.ItemQty, "ItemQty");
            // 
            // ItemSub
            // 
            resources.ApplyResources(this.ItemSub, "ItemSub");
            // 
            // panel_details
            // 
            resources.ApplyResources(this.panel_details, "panel_details");
            this.panel_details.Controls.Add(this.group_service_details);
            this.panel_details.Controls.Add(this.group_meeting_summary);
            this.panel_details.Name = "panel_details";
            // 
            // RoomAssets
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel_details);
            this.Controls.Add(this.group_services);
            this.Name = "RoomAssets";
            this.FormRegionShowing += new System.EventHandler(this.RoomAssets_FormRegionShowing);
            this.FormRegionClosed += new System.EventHandler(this.RoomAssets_FormRegionClosed);
            this.Enter += new System.EventHandler(this.RoomAssets_Enter);
            this.group_services.ResumeLayout(false);
            this.panel_services.ResumeLayout(false);
            this.group_service_details.ResumeLayout(false);
            this.group_service_details.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.assetIcon)).EndInit();
            this.group_meeting_summary.ResumeLayout(false);
            this.group_meeting_summary.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listBookingGrid)).EndInit();
            this.panel_details.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        #region Form Region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private static void InitializeManifest(Microsoft.Office.Tools.Outlook.FormRegionManifest manifest, Microsoft.Office.Tools.Outlook.Factory factory)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoomAssets));
            manifest.FormRegionName = Branding.Resource.ServicesRegionName;
            manifest.Icons.Page = Branding.Resource.ServicesImage;

        }

        #endregion

        private System.Windows.Forms.GroupBox group_services;
        private System.Windows.Forms.GroupBox group_service_details;
        private System.Windows.Forms.GroupBox group_meeting_summary;
        private System.Windows.Forms.Panel panel_services;
        private System.Windows.Forms.Button butSelectARoom;
        private System.Windows.Forms.Label labelWarningAssets;
        private Controls.BookableItemRegion2 itemsCatering;
        private Controls.BookableItemRegion2 itemsLayout;
        private Controls.BookableItemRegion2 itemsEquipment;
        private System.Windows.Forms.Label labelBookingRestrictionWarning;
        private System.Windows.Forms.PictureBox assetIcon;
        private System.Windows.Forms.ComboBox timeClearAway;
        private System.Windows.Forms.ComboBox timeDelivery;
        private System.Windows.Forms.Label labelClearAwayTime;
        private System.Windows.Forms.Label labelDeliveryTime;
        private System.Windows.Forms.Label labelWarningServiceDetails;
        private System.Windows.Forms.Label labelItemDescription;
        private System.Windows.Forms.Label labelCostUnit;
        private System.Windows.Forms.Label labelItemCost;
        private System.Windows.Forms.Label labelItemName;
        private System.Windows.Forms.Button butAddAsset;
        private System.Windows.Forms.Label labelItemNotes;
        private System.Windows.Forms.TextBox textItemNotes;
        private System.Windows.Forms.Label labelQuantity;
        private System.Windows.Forms.ComboBox boxCostCentre;
        private System.Windows.Forms.Label labelCostCentre;
        private System.Windows.Forms.Button butClose;
        private System.Windows.Forms.DataGridView listBookingGrid;
        private System.Windows.Forms.DataGridViewImageColumn TypeIcon;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTotal;
        private System.Windows.Forms.DataGridViewImageColumn DeleteItem;
        private System.Windows.Forms.Label labelWarningSummary;
        private System.Windows.Forms.Label labelTotalOrderCost;
        private System.Windows.Forms.Label labelTotalCost;
        private System.Windows.Forms.ListView listBookedItems;
        private System.Windows.Forms.ColumnHeader ItemName;
        private System.Windows.Forms.ColumnHeader ItemType;
        private System.Windows.Forms.ColumnHeader ItemUnitCost;
        private System.Windows.Forms.ColumnHeader ItemQty;
        private System.Windows.Forms.ColumnHeader ItemSub;
        private System.Windows.Forms.Button butPrint;
        private System.Windows.Forms.Label labelMeetingSummary;
        private System.Windows.Forms.Panel panel_details;
        private System.Windows.Forms.NumericUpDown sizeBox;

        public partial class RoomAssetsFactory : Microsoft.Office.Tools.Outlook.IFormRegionFactory
        {
            public event Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler FormRegionInitializing;

            private Microsoft.Office.Tools.Outlook.FormRegionManifest _Manifest;

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public RoomAssetsFactory()
            {
                this._Manifest = Globals.Factory.CreateFormRegionManifest();
                RoomAssets.InitializeManifest(this._Manifest, Globals.Factory);
                this.FormRegionInitializing += new Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler(this.RoomAssetsFactory_FormRegionInitializing);
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public Microsoft.Office.Tools.Outlook.FormRegionManifest Manifest
            {
                get
                {
                    return this._Manifest;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.IFormRegion Microsoft.Office.Tools.Outlook.IFormRegionFactory.CreateFormRegion(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            {
                RoomAssets form = new RoomAssets(formRegion);
                form.Factory = this;
                return form;
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            byte[] Microsoft.Office.Tools.Outlook.IFormRegionFactory.GetFormRegionStorage(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize)
            {
                throw new System.NotSupportedException();
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            bool Microsoft.Office.Tools.Outlook.IFormRegionFactory.IsDisplayedForItem(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize)
            {
                if (this.FormRegionInitializing != null)
                {
                    Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs cancelArgs = Globals.Factory.CreateFormRegionInitializingEventArgs(outlookItem, formRegionMode, formRegionSize, false);
                    this.FormRegionInitializing(this, cancelArgs);
                    return !cancelArgs.Cancel;
                }
                else
                {
                    return true;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.FormRegionKindConstants Microsoft.Office.Tools.Outlook.IFormRegionFactory.Kind
            {
                get
                {
                    return Microsoft.Office.Tools.Outlook.FormRegionKindConstants.WindowsForms;
                }
            }
        }
    }

    partial class WindowFormRegionCollection
    {
        internal RoomAssets RoomAssets
        {
            get
            {
                foreach (var item in this)
                {
                    if (item.GetType() == typeof(RoomAssets))
                        return (RoomAssets)item;
                }
                return null;
            }
        }
    }
}
