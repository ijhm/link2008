﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using OneSpace.OutlookPlugin;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using OneSpace.OutlookPlugin.UI.Forms;

namespace OneSpace.OutlookPlugin
{
    public partial class Ribbon1
    {

        public Timer BackgroundTimer;
        private bool LoadingMsg = false;

        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {

            groupReports.Label = Branding.Resource.LinkName;
            /* Todo: Set the Icons using the Branding.Resource class */

            Tabs[0].Label = Branding.Resource.RibbonName;
            butBookingReport.Label = Globals.ThisAddIn.resourceManager.GetString("ribbonButtonReports");
            groupReports.Label = Branding.Resource.RibbonName;
            butBookingSchedule.Label = Globals.ThisAddIn.resourceManager.GetString("ribbonButtonBookings");
            butAbout.Label = Globals.ThisAddIn.resourceManager.GetString("ribbonButtonAbout");
            butAdmin.Enabled = false;
            butBookingReport.Image = Branding.Resource.ReportsImage;
            butAbout.Image = Branding.Resource.AboutImage;
            butBookingSchedule.Image = Branding.Resource.BookingsImage;
            butAdmin.Image = Branding.Resource.AdminImage;

            LoadInBackground();
        }

        private void LoadInBackground()
        {
           BackgroundTimer = new Timer(new System.Threading.TimerCallback(ScanForConfig), null, 0, 100);
        }

        private void ScanForConfig(object state)
        {
            if (Globals.ThisAddIn.LoadState==ThisAddIn.enum_LoadingState.Loaded)
            {
                //       butAdmin.Enabled = true;

                //              
                //ToDo: Determine if the admin tool is installed
                //ToDo: Figure out if you have permissions to manage it
                bool canAccessAdmin = true;

                butAdmin.Enabled = canAccessAdmin;

                //        BackgroundTimer = null;
                butBookingSchedule.Enabled = Globals.ThisAddIn.IsReporting;
                butBookingReport.Enabled = Globals.ThisAddIn.IsReporting;
             //   BackgroundTimer = null;
                if (!LoadingMsg)
                {
                    groupReports.Label = "Download complete";
                    LoadingMsg = true;
                    BackgroundTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    groupReports.Label = Branding.Resource.LinkName;
                }
                else
                {
                    groupReports.Label = Branding.Resource.LinkName;
                }
            }
            else
            {
                butBookingSchedule.Enabled = false;
                butBookingReport.Enabled = false;
                groupReports.Label = string.Format("Downloading {0} of {1}", Globals.ThisAddIn.LoadingCount, Globals.ThisAddIn.LoadingTotal);
            }
        }

        private void butBookingReport_Click(object sender, RibbonControlEventArgs e)
        {
//            var dlg = new ReportingForm();
            var dlg = new ReportMakerForm();
            dlg.Show();

        }

        private void butBookingSchedule_Click(object sender, RibbonControlEventArgs e)
        {
            var dlg = new RoomBookingChart();
            dlg.Show();
        }

        private void butAbout_Click(object sender, RibbonControlEventArgs e)
        {
            var dlg = new FormAbout();
            dlg.Show();
        }

        private void butAdmin_Click(object sender, RibbonControlEventArgs e)
        {
            //Disabled session start for demo...
            Globals.ThisAddIn.AdminSession = new Data.Admin.Session(false);
            if (!Globals.ThisAddIn.AdminSession.IsRunning)
            {
                Globals.ThisAddIn.AdminSession.LaunchAdmin();
            }
            else
            {
                Globals.ThisAddIn.AdminSession.BringToFront();
            }
        }
    }
}
