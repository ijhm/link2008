﻿namespace OneSpace.OutlookPlugin
{
    partial class Ribbon1 : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon1()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ribbon1));
            this.tab_onespace_link = this.Factory.CreateRibbonTab();
            this.groupReports = this.Factory.CreateRibbonGroup();
            this.butBookingReport = this.Factory.CreateRibbonButton();
            this.butBookingSchedule = this.Factory.CreateRibbonButton();
            this.butAdmin = this.Factory.CreateRibbonButton();
            this.butAbout = this.Factory.CreateRibbonButton();
            this.ribbonImages = new System.Windows.Forms.ImageList(this.components);
            this.tab_onespace_link.SuspendLayout();
            this.groupReports.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab_onespace_link
            // 
            this.tab_onespace_link.Groups.Add(this.groupReports);
            resources.ApplyResources(this.tab_onespace_link, "tab_onespace_link");
            this.tab_onespace_link.Name = "tab_onespace_link";
            // 
            // groupReports
            // 
            this.groupReports.Items.Add(this.butBookingReport);
            this.groupReports.Items.Add(this.butBookingSchedule);
            this.groupReports.Items.Add(this.butAdmin);
            this.groupReports.Items.Add(this.butAbout);
            resources.ApplyResources(this.groupReports, "groupReports");
            this.groupReports.Name = "groupReports";
            // 
            // butBookingReport
            // 
            this.butBookingReport.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            resources.ApplyResources(this.butBookingReport, "butBookingReport");
            this.butBookingReport.Image = global::OneSpace.OutlookPlugin.Resource1.reports;
            this.butBookingReport.Name = "butBookingReport";
            this.butBookingReport.ShowImage = true;
            this.butBookingReport.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.butBookingReport_Click);
            // 
            // butBookingSchedule
            // 
            this.butBookingSchedule.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            resources.ApplyResources(this.butBookingSchedule, "butBookingSchedule");
            this.butBookingSchedule.Image = global::OneSpace.OutlookPlugin.Resource1.booking;
            this.butBookingSchedule.Name = "butBookingSchedule";
            this.butBookingSchedule.ShowImage = true;
            this.butBookingSchedule.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.butBookingSchedule_Click);
            // 
            // butAdmin
            // 
            this.butAdmin.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.butAdmin.Image = global::OneSpace.OutlookPlugin.Resource1.admin;
            resources.ApplyResources(this.butAdmin, "butAdmin");
            this.butAdmin.Name = "butAdmin";
            this.butAdmin.ShowImage = true;
            this.butAdmin.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.butAdmin_Click);
            // 
            // butAbout
            // 
            this.butAbout.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.butAbout.Image = global::OneSpace.OutlookPlugin.Resource1.info;
            resources.ApplyResources(this.butAbout, "butAbout");
            this.butAbout.Name = "butAbout";
            this.butAbout.ShowImage = true;
            this.butAbout.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.butAbout_Click);
            // 
            // ribbonImages
            // 
            this.ribbonImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ribbonImages.ImageStream")));
            this.ribbonImages.TransparentColor = System.Drawing.Color.Transparent;
            this.ribbonImages.Images.SetKeyName(0, "chart_bar.png");
            // 
            // Ribbon1
            // 
            this.Name = "Ribbon1";
            this.RibbonType = "Microsoft.Outlook.Explorer";
            this.Tabs.Add(this.tab_onespace_link);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.tab_onespace_link.ResumeLayout(false);
            this.tab_onespace_link.PerformLayout();
            this.groupReports.ResumeLayout(false);
            this.groupReports.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab_onespace_link;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupReports;
        private System.Windows.Forms.ImageList ribbonImages;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton butBookingReport;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton butBookingSchedule;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton butAbout;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton butAdmin;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon1 Ribbon1
        {
            get { return this.GetRibbon<Ribbon1>(); }
        }
    }
}
