﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Utilities {
    internal class ExcelData : IDisposable {
        private const string CLASS_NAME = "ExcelData";
        private const string PROVIDER_NAME = "System.Data.OleDb";
        private const string CONNECTION_STRING = "Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties='Excel 12.0 Xml;HDR=YES;ReadOnly=true';Data Source={0};";

        private object _dispose_locker = new object();

        private FileInfo _excel_file = null;
        private DbConnection _cn = null;

        /* Todo: Consider using the System.Data.OleDb.OleDbEnumerator to check for the existance of either and using the lastest avaliable:
         *         - Microsoft.ACE.OLEDB.12.0
         *         - Microsoft.ACE.OLEDB.15.0
         * */

        /* Todo: Consider makeing the header row optional */

        public ExcelData(FileInfo excel_file) {
            if (excel_file == null)
                throw new ArgumentNullException();

            excel_file.Refresh();
            if (!excel_file.Exists) {
                throw new FileNotFoundException();
            }

            IsDisposed = false;
            _excel_file = excel_file;
        }

        private DbProviderFactory GetFactory() {
            return DbProviderFactories.GetFactory(PROVIDER_NAME);
        }

        private DbConnection CreateConnection() {
            if (_cn == null) {
                DbProviderFactory factory = GetFactory();
                DbConnection cn = factory.CreateConnection();
                cn.ConnectionString = string.Format(CONNECTION_STRING, _excel_file.FullName);
                cn.Open();
                _cn = cn;
            }

            return _cn;
        }

        private DbCommand CreateCommand(DbConnection cn, string sql) {
            DbCommand cmd = cn.CreateCommand();
            cmd.CommandText = sql;

            return cmd;
        }

        public bool IsDisposed { get; private set; }

        public DbParameter CreateParameter() {
            if (IsDisposed) {
                throw new ObjectDisposedException(CLASS_NAME);
            }

            return GetFactory().CreateParameter();
        }

        public DbParameter CreateParameter(string name, object value) {
            if (IsDisposed) {
                throw new ObjectDisposedException(CLASS_NAME);
            }

            DbParameter param = CreateParameter();
            param.ParameterName = name;
            param.Value = value;

            return param;
        }

        public int ExecuteNonQuery(string sql, CommandType commandType) {
            if (IsDisposed) {
                throw new ObjectDisposedException(CLASS_NAME);
            }

            DbConnection cn = CreateConnection();
            using (DbCommand cmd = CreateCommand(cn, sql)) {
                cmd.CommandType = commandType;
                return cmd.ExecuteNonQuery();
            }
        }

        public int ExecuteNonQuery(string sql, CommandType commandType, params DbParameter[] parameters) {
            if (IsDisposed) {
                throw new ObjectDisposedException(CLASS_NAME);
            }

            DbConnection cn = CreateConnection();
            using (DbCommand cmd = CreateCommand(cn, sql)) {
                cmd.CommandType = commandType;

                foreach (DbParameter param in parameters) {
                    cmd.Parameters.Add(param);
                }

                return cmd.ExecuteNonQuery();
            }
        }

        public T ExecuteScala<T>(string sql, CommandType commandType) where T : struct {
            if (IsDisposed) {
                throw new ObjectDisposedException(CLASS_NAME);
            }

            DbConnection cn = CreateConnection();
            using (DbCommand cmd = CreateCommand(cn, sql)) {
                cmd.CommandType = commandType;
                return (T)cmd.ExecuteScalar();
            }
        }

        public T ExecuteScala<T>(string sql, CommandType commandType, params DbParameter[] parameters) where T : struct {
            if (IsDisposed) {
                throw new ObjectDisposedException(CLASS_NAME);
            }

            DbConnection cn = CreateConnection();
            using (DbCommand cmd = CreateCommand(cn, sql)) {
                cmd.CommandType = commandType;

                foreach (DbParameter param in parameters) {
                    cmd.Parameters.Add(param);
                }

                return (T)cmd.ExecuteScalar();
            }
        }

        public DbDataReader ExecuteQuery(string sql, CommandType commandType) {
            if (IsDisposed) {
                throw new ObjectDisposedException(CLASS_NAME);
            }

            using (DbCommand cmd = CreateCommand(CreateConnection(), sql)) {
                cmd.CommandType = commandType;
                return cmd.ExecuteReader(CommandBehavior.Default);
            }
        }

        public DbDataReader ExecuteQuery(string sql, CommandType commandType, params DbParameter[] parameters) {
            if (IsDisposed) {
                throw new ObjectDisposedException(CLASS_NAME);
            }

            using (DbCommand cmd = CreateCommand(CreateConnection(), sql)) {
                cmd.CommandType = commandType;

                foreach (IDbDataParameter param in parameters) {
                    cmd.Parameters.Add(param);
                }

                return cmd.ExecuteReader(CommandBehavior.Default);
            }
        }

        public void Dispose() {
            lock (_dispose_locker) {
                if (IsDisposed) {
                    return;
                }

                IsDisposed = true;

                if (_cn != null) {
                    try { _cn.Dispose(); }
                    catch { }
                    _cn = null;
                }
            }
        }
    }
}
