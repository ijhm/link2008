﻿using OneSpace.OutlookPlugin.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Utilities
{
    public class FileUtilities {
        public static void DeleteIfExists(string path) {
            if (File.Exists(path)) {
                File.Delete(path);
            }
        }

        public static DateTime GetLastUpdated(string fileName) {
            DateTime result = DateTime.MinValue;
            string filePath = Path.Combine(SystemConsts.TempPath, fileName);

            if (File.Exists(filePath)) {
                string fileText = File.ReadAllText(filePath);
                if (DateTime.TryParse(fileText, out result) == false) {
                    result = DateTime.MinValue;
                }
            }

            return result;
        }

        public static void SetLastUpdated(string fileName, DateTime updatedDate) {
            string filePath = Path.Combine(SystemConsts.TempPath, fileName);
            if (File.Exists(filePath)) {
                File.Delete(filePath);
            }
            /* Note: "O" is the standard round trip format pattern, this allows the value to be captured in a unambiguous mannor */
            File.AppendAllText(filePath, updatedDate.ToString("O"));
        }
    }
}
