﻿using OneSpace.OutlookPlugin;
using OneSpace.OutlookPlugin.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OneSpace.OutlookPlugin.Utilities
{
    public class OutlookUtilities
    {
        public static void SetPropertyNumber(Outlook.AppointmentItem appointment, string keyName, int value)
        {
            bool addNew = true;
            foreach (Outlook.UserProperty property in appointment.UserProperties)
            {
                if (property.Name == keyName)
                {
                    addNew = false;
                }
            }
            if (addNew)
            {
                appointment.UserProperties.Add(keyName, Outlook.OlUserPropertyType.olNumber);
            }
            appointment.UserProperties[keyName].Value = value;
        }

        public static void SetPropertyText(Outlook.AppointmentItem appointment, string keyName, string value)
        {
            bool addNew = true;
            foreach (Outlook.UserProperty property in appointment.UserProperties)
            {
                if (property.Name == keyName)
                {
                    addNew = false;
                }
            }
            if (addNew)
            {
                appointment.UserProperties.Add(keyName, Outlook.OlUserPropertyType.olText);
            }
            appointment.UserProperties[keyName].Value = value;
        }

        public static int GetPropertyNumber(Outlook.AppointmentItem appointment, string keyName)
        {
            int result = 0;
            try
            {
                var prop = appointment.UserProperties.Find(keyName);
                if (prop!=null)
                {
                    result = int.Parse(prop.Value.ToString());
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public static string GetPropertyText(Outlook.AppointmentItem appointment, string keyName) {
            var result = string.Empty;

            try {
                var prop = appointment.UserProperties.Find(keyName);
                if (prop != null) {
                    result = prop.Value.ToString();
                }
            }
            catch { }

            return result;
        }

        #region Base 64 encode - decode for image processing
        public static string Base64EncodeImage(Image img)
        {
            string result = "";
            using (MemoryStream m = new MemoryStream())
            {
                img.Save(m, img.RawFormat);
                byte[] imgBytes = m.ToArray();
                result = Convert.ToBase64String(imgBytes);
            }
            return result;
        }

        public static Image Base64DecodeImage(string imageText)
        {
            Image result;
            using (MemoryStream m = new MemoryStream(Convert.FromBase64String(imageText)))
            {
                result = Image.FromStream(m);
            }
            return result;
        }
        #endregion
        
        public static List<string> HourList(DateTime startTime, DateTime endTime, int minStep)
        {
            var _result = new List<string>();
            while (startTime <= endTime)
            {
                _result.Add(startTime.ToString("HH:mm"));
                startTime = startTime.AddMinutes(minStep);
            }
            return _result;
        }

        public static Outlook.Account GetAccountForEmailAddress(Outlook.Application application, string smtpAddress)
        {

            // Loop over the Accounts collection of the current Outlook session.
            Outlook.Accounts accounts = application.Session.Accounts;
            foreach (Outlook.Account account in accounts)
            {
                // When the e-mail address matches, return the account.
                if (account.SmtpAddress == smtpAddress)
                {
                    return account;
                }
            }
            throw new System.Exception(string.Format("No Account with SmtpAddress: {0} exists!", smtpAddress));
        }

        public static ServiceMailbox GetAddressForServiceAccount(string smtpAddress)
        {
            foreach (var item in Globals.ThisAddIn.OneSpaceData.ServiceMailboxes)
            {
                return item;
            }
            return null;
        }

        [Obsolete("This method uses the CibExchangeLib", false)]
        public static string CreateAppointment(string mailBox, string location, DateTime start, DateTime end, string subject, string body)
        {
            throw new NotImplementedException();
            //var account = OutlookUtilities.GetAddressForServiceAccount(mailBox);

            //var ews = new CibExchangeLib.CibExchangeAdaptor(
            //                Globals.ThisAddIn.OneSpaceData.EWSCredentials.ServerVersion,
            //                Globals.ThisAddIn.OneSpaceData.EWSCredentials.Username,
            //                Globals.ThisAddIn.OneSpaceData.EWSCredentials.Password,
            //                account.Username);

            //var appointment = ews.CreateAppointment(location,start,end,subject,body);
            //return appointment.UniqueId;
        }

        [Obsolete("This method uses the CibExchangeLib", false)]
        public static bool UpdateAppointment(string appointmentId, string mailBox, DateTime start, DateTime end, string subject,string body)
        {
            throw new NotImplementedException();
            //var account = OutlookUtilities.GetAddressForServiceAccount(mailBox);

            //var ews = new CibExchangeLib.CibExchangeAdaptor(
            //                Globals.ThisAddIn.OneSpaceData.EWSCredentials.ServerVersion,
            //                Globals.ThisAddIn.OneSpaceData.EWSCredentials.Username,
            //                Globals.ThisAddIn.OneSpaceData.EWSCredentials.Password,
            //                account.Username);

            //var appointment = ews.UpdateAppointment(appointmentId, start, end, subject,body);
            //return true;
        }

        [Obsolete("This method uses the CibExchangeLib", false)]
        public static bool DeleteAppointment(string appointmentId, string mailBox)
        {
            throw new NotImplementedException();
            //var account = OutlookUtilities.GetAddressForServiceAccount(mailBox);

            //var ews = new CibExchangeLib.CibExchangeAdaptor(
            //                Globals.ThisAddIn.OneSpaceData.EWSCredentials.ServerVersion,
            //                Globals.ThisAddIn.OneSpaceData.EWSCredentials.Username,
            //                Globals.ThisAddIn.OneSpaceData.EWSCredentials.Password,
            //                account.Username);

            //var appointment = ews.DeleteAppointment(appointmentId);
            //return true;
        }

        public static bool OverlappingDates(DateTime s1, DateTime s2, DateTime e1, DateTime e2)
        {
        //    Trace.WriteLine(string.Format("{0} - {1}            {2} - {3}", s1, e1, s2, e2));
            return ((s2 > s1 && s2 < e1) ||
                    (e2 > s1 && e2 < e1) ||
                    (s2 > s1 && e2 < e1));
        }
        
        public static void PopulateDeliveryDropDown(System.Windows.Forms.ComboBox box, Outlook.AppointmentItem appointment, BookingItem item)
        {
            var interval = TimeSpan.FromMinutes(15);
            var duration = TimeSpan.FromMinutes(appointment.Duration);
            var is_short_appointment = (duration < interval);
            var start = appointment.Start;
            var end = (is_short_appointment) ? start : appointment.End.Subtract(interval);
            var working_date = start;
            
            box.Items.Clear();

            while (working_date <= end){
                box.Items.Add(new ComboBoxItem() { Text = "{working_date:HH:mm}", Value = (int)(working_date - start).TotalMinutes });
                working_date += interval;
            }

            if(box.Items.Count > 0) {
                box.SelectedIndex = 0;
            }
        }
        
        public static void PopulateClearawayDropDown(System.Windows.Forms.ComboBox box, Outlook.AppointmentItem appointment, BookingItem item)
        {
            var interval = TimeSpan.FromMinutes(15);
            var duration = TimeSpan.FromMinutes(appointment.Duration);
            var is_short_appointment = (duration < interval);
            var start = appointment.End;
            var end = (is_short_appointment) ? start : appointment.Start.Add(interval);
            var working_date = start;

            box.Items.Clear();

            while (working_date >= end)
            {
                box.Items.Add(new ComboBoxItem() { Text = "{working_date:HH:mm}", Value = (int)(working_date - start).TotalMinutes });
                working_date -= interval;
            }

            if (box.Items.Count > 0)
            {
                box.SelectedIndex = 0;
            }
        }

        public static int GetComboBoxValue(System.Windows.Forms.ComboBox box)
        {
            ComboBoxItem boxItem = (ComboBoxItem)box.Items[box.SelectedIndex];
            return boxItem.Value;
        }

        public static string GetComboBoxText(System.Windows.Forms.ComboBox box)
        {
            ComboBoxItem boxItem = (ComboBoxItem)box.Items[box.SelectedIndex];
            return boxItem.Text;
        }

        public static void SetComboBox(System.Windows.Forms.ComboBox box, int selecedOffset)
        {
            for (int i = 0; i < box.Items.Count; i++)
            {
                ComboBoxItem boxItem = (ComboBoxItem)box.Items[i];
                if (boxItem.Value == selecedOffset)
                {
                    box.SelectedIndex = i;
                    break;
                }
            }
        }
    }

    public class ComboBoxItem
    {
        public string Text { get; set; }
        public int Value { get; set; }
        
        public override string ToString()
        {
            return Text;
        }

        public static bool NonUserUpdate(object sender)
        {
            System.Windows.Forms.ComboBox box = (System.Windows.Forms.ComboBox)sender;
            if (!box.Focused)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool TextboxNonUserUpdate(object sender)
        {
            System.Windows.Forms.TextBox box = (System.Windows.Forms.TextBox)sender;
            if (!box.Focused)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool UpdateDownNonUserUpdate(object sender)
        {
            System.Windows.Forms.NumericUpDown upDown = (System.Windows.Forms.NumericUpDown)sender;
            if (!upDown.Focused)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
