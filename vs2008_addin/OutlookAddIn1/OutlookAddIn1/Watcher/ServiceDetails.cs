﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Watcher
{
    public class ServiceDetails
    {
        public string Username { get; set; }
        public DateTime Updated { get; set; }

        public ServiceDetails()
        {
        }

        public ServiceDetails(string jsonData)
        {
            var data = JsonConvert.DeserializeObject<ServiceDetails>(jsonData);
            Username = data.Username;
            Updated = data.Updated;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
}
