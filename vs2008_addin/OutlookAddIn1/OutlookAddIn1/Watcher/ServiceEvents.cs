﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneSpace.OutlookPlugin.Watcher
{
    public class EventArgs<T> : EventArgs
    {
        private T _value;

        public EventArgs(T aValue)
        {
            _value = aValue;
        }

        public T Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    public class CancelEventArgs<T> : EventArgs<T>
    {
        private bool _cancel;

        public CancelEventArgs(T aValue)
            : base(aValue)
        {
        }

        public bool Cancel
        {
            get { return _cancel; }
            set { _cancel = value; }
        }
    }

}
